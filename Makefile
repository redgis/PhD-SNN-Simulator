all:
	mkdir --parents src/build && cd src/build && cmake ../ && make -s

clean:
	cd src/build && make -s clean && cd .. && rm -fr build

cleanobj:
	cd src/build && make -s clean
