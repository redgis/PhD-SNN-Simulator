    -- SimulationSettings

      -- General settings

RESHUFFLE_PATTERNS = true
TRAIN_PATTERN_FILE = "datasets/train.dat"
TEST_PATTERN_FILE = "datasets/test.dat"

TRAIN_CLASSES_TO_LOAD = {}
TEST_CLASSES_TO_LOAD = {}

-- for each class, number of patterns to load, -1 means all
--NB_TRAIN_PATTERNS_TO_LOAD = {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1}
--NB_TEST_PATTERNS_TO_LOAD = {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1}

NB_TRAIN_PATTERNS_TO_LOAD = {}
NB_TEST_PATTERNS_TO_LOAD = {}
