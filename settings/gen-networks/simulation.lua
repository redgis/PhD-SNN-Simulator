dofile ("settings/gen-networks/preset-meta.lua")

TIMESCALE = 10

EVT_RECURSIVE =            8  -- Treat a recursive event
EVT_NEXT_PHASE =          16  -- Means we should enqueue the events of the next phase known by the eventmanager

evtNextPhase =   { EventType     = EVT_NEXT_PHASE,  -- Event type
                   Assembly      = "Reservoir",     -- Assembly name on witch the event is applied
                   Time          = -1,              -- Time of start of the event. -1 means after previous event.
                   RecurssionStep = 0,              -- Recurssion step, in case of recursive event
                   Duration      = 0,               -- Duration
                   Function      = nil,             -- Event function
                   FuncParam1    = nil,             -- Parameters for the function
                   FuncParam2    = nil,
                   FuncParam3    = nil,
                   FuncParam4    = nil,
                   FuncDParam1    = nil,
                   FuncDParam2    = nil,
                   FuncStrParam1 = nil,
                   FuncStrParam2 = nil
                 }

evtSaveArchi   = { EventType     = EVT_RECURSIVE,
                   Assembly      = "Reservoir",
                   Time          = -1,
                   RecurssionStep = 1,
                   Duration      = 0,
                   Function      = "SaveArchitecture",
                   FuncParam1    = nil,
                   FuncParam2    = nil,
                   FuncParam3    = nil,
                   FuncParam4    = nil,
                   FuncDParam1   = nil,
                   FuncDParam2   = nil,
                   FuncStrParam1 = "architectures/smallworlds/sw-n-"..string.format("%04d",N).."-c-"..string.format("%.3f",C).."-k-"..string.format("%02d",K).."-p-"..string.format("%.2f",P).."-d-"..string.format("%03d",DELAI).."-dtd-"..string.format("%03d",DELTA_DELAI).."-"..string.format("%02d",NUM)..".xml", --generated-net-%010ld--seed-%d--size-%d--classes-%d--timescale-%d.xml"
                   FuncStrParam2 = nil
                 }


SaveArchitecture = {"save current state of architecture", "evtSaveArchi", "evtNextPhase" }
Simulation = { "SaveArchitecture" }







