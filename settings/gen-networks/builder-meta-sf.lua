dofile ("settings/gen-networks/preset-meta.lua")

FALSE = 0
TRUE = 1
UNSPECIFIED = 2

ASS_TO_ASS = 0
NEU_TO_ASS = 1
ASS_TO_NEU = 2
NEU_TO_NEU = 3
SUBASS_TO_SUBASS = 4
ONE_TO_ONE = 5
REGULAR = 6
REGULAR_AND_REWIRE = 7
SCALE_FREE = 8


RESTING_POTENTIAL = -65
THRESHOLD = -50
ABSOLUTE_REFRACTORY_POTENTIAL = 20000
ABSOLUTE_REFRACTORY_PERIODE = 5 * TIMESCALE
TAU_PSP = 3 * TIMESCALE -- 3 * TIMESCALE
TAU_REFRACT = 0.1 * TIMESCALE



--------------- NEURONS PARAMS

reservoir_nparam = { 
  MembranePotential = RESTING_POTENTIAL;
  Threshold = THRESHOLD;
  AbsoluteRefractoryPeriode = ABSOLUTE_REFRACTORY_PERIODE;
  AbsoluteRefractoryPotential = ABSOLUTE_REFRACTORY_POTENTIAL;
  Inhibitory = 0.0;
  TauPSP = TAU_PSP;
  TauRefract = TAU_REFRACT;
  }


----------- PROJECTIONS PARAMS

reservoir_pparam = {
  ProjectionRate = 1.0;
  DeltaDelay = DELTA_DELAI;
  DeltaWeight = 0.05;
  Weight = 0.5;
  Delay = DELAI;
  WeightAdaptation = false;
  DelayAdaptation = false;
  Inhibitory = UNSPECIFIED;
}

initial_pparam = {
  ProjectionRate = 1.0;
  DeltaDelay = DELTA_DELAI;
  DeltaWeight = 0.05;
  Weight = 0.5;
  Delay = DELAI;
  WeightAdaptation = false;
  DelayAdaptation = false;
  Inhibitory = UNSPECIFIED;
}

------------------------- DESCRIPTION DE CONSTRUCTION : ASSEMBLY PROJECTION


Assemblies = { "Reservoir" }

Reservoir = { "ReservoirSet1"}
ReservoirSet1 = { N, "reservoir_nparam" }


Projections = { "ReservoirReservoir" }

ReservoirReservoir = { SCALE_FREE, "Reservoir", PROJECTION_PARAMETERS /*InitialRandNetParam*/, ReservoirSet1[1]/10, "reservoir_pparam", K, K/4, K/4, 0.5, 0.5, 0.0}


-- print (ReservoirReservoir[1].." "..ReservoirReservoir[2].." "..ReservoirReservoir[3].." "..ReservoirReservoir[4].." "..ReservoirReservoir[5].." ")
