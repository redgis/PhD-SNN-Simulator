FALSE = 0
TRUE = 1
UNSPECIFIED = 2

TIMESCALE = 10

RESTING_POTENTIAL = -65
THRESHOLD = -50
ABSOLUTE_REFRACTORY_POTENTIAL = 20000
ABSOLUTE_REFRACTORY_PERIODE = 5 * TIMESCALE
TAU_PSP = 3 * TIMESCALE -- 3 * TIMESCALE
TAU_REFRACT = 0.1 * TIMESCALE



NbTestPatterns = 7291
NbTrainPatterns = 2007

input_nparam = { 
  MembranePotential = RESTING_POTENTIAL;
  Threshold = THRESHOLD;
  AbsoluteRefractoryPeriode = ABSOLUTE_REFRACTORY_PERIODE;
  AbsoluteRefractoryPotential = ABSOLUTE_REFRACTORY_POTENTIAL;
  Inhibitory = 0.0;
  TauPSP = TAU_PSP;
  TauRefract = TAU_REFRACT;
  }

reservoir_nparam = { 
  MembranePotential = RESTING_POTENTIAL;
  Threshold = THRESHOLD;
  AbsoluteRefractoryPeriode = ABSOLUTE_REFRACTORY_PERIODE;
  AbsoluteRefractoryPotential = ABSOLUTE_REFRACTORY_POTENTIAL;
  Inhibitory = 0.0;
  TauPSP = TAU_PSP;
  TauRefract = TAU_REFRACT;
  }

output_nparam = { 
  MembranePotential = RESTING_POTENTIAL;
  Threshold = THRESHOLD;
  AbsoluteRefractoryPeriode = ABSOLUTE_REFRACTORY_PERIODE;
  AbsoluteRefractoryPotential = ABSOLUTE_REFRACTORY_POTENTIAL;
  Inhibitory = 0.0;
  TauPSP = TAU_PSP;
  TauRefract = TAU_REFRACT;
  }


input_reservoir_pparam = {
  ProjectionRate = 0.01;
  DeltaDelay = 0 * TIMESCALE;
  DeltaWeight = 0;
  Weight = 1.0;
  Delay = -1;
  WeightAdaptation = false;
  DelayAdaptation = false;
  Inhibitory = UNSPECIFIED;
}


reservoir_pparam = {
  ProjectionRate = 1;
  DeltaDelay = 0 * TIMESCALE;
  DeltaWeight = 0;
  Weight = -1;
  Delay = -1;
  WeightAdaptation = false;
  DelayAdaptation = false;
  Inhibitory = UNSPECIFIED;
}

reservoir_output_pparam = {
  ProjectionRate = 1;
  DeltaDelay = 0 * TIMESCALE;
  DeltaWeight = 0;
  Weight = -1;
  Delay = 10;
  WeightAdaptation = false;
  DelayAdaptation = false;
  Inhibitory = UNSPECIFIED;
}


  --print (nparam.MembranePotential .." ".. nparam.Threshold .." ".. nparam.AbsoluteRefractoryPeriode .." ".. nparam.AbsoluteRefractoryPotential .." ".. nparam.Inhibitory .." ".. nparam.TauPSP .." ".. nparam.TauRefract)
  --print (pparam.ProjectionRate, pparam.DeltaDelay, pparam.DeltaWeight, pparam.Weight, pparam.Delay, pparam.WeightAdaptation , pparam.DelayAdaptation, pparam.Inhibitory)


nnlAddAssembly("Input")
nnlAddXNeurons(256, "Input", input_nparam)

nnlAddAssembly("Reservoir")
nnlAddXNeurons(800, "Reservoir", reservoir_nparam)
reservoir_nparam.Inhibitory = 1.0
nnlAddXNeurons(200, "Reservoir", reservoir_nparam)

nnlAddAssembly("Output")
nnlAddXNeurons(10, "Output", output_nparam)
 
nnlConnectRegularAndRewire("Reservoir", 10, 0.25, reservoir_pparam)
--reservoir_pparam.ProjectionRate = 0.01
--nnlConnectAssToAss("Reservoir", "Reservoir", reservoir_pparam)

nnlConnectAssToAss("Input", "Reservoir", input_reservoir_pparam)
nnlConnectAssToAss("Reservoir", "Output", reservoir_output_pparam)
