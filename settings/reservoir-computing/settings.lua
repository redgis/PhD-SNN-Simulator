TIMESCALE = 10

 -- Directories

ROOT = "./"

DATA_OUTPUT_DIR = "data/"
DATA_OUTPUT_EXT = "txt"

PATTERNS_DIR = "datasets/"
PATTERNS_EXT = "dat"

FIGURE_SCRIPTS_DIR = "plots/"
FIGURE_SCRIPTS_EXT = "plot"

FIGURES_DIR = "figures/"
FIGURES_EXT = "eps"

ARCHITECTURE_OUTPUT_DIR = "architectures/"
ARCHITECTURE_XML_FILE = "architecture-%010ld--RandSeed-%d--Size-%d--Classes-%d--timescale-%d.xml"

BUILDERS_DIR = "builders/"
SETTINGS_DIR = "settings/"
SIMULATIONS_DIR = "simulations/"

      -- STDP constants

local CLASSICAL_STDP = 0 
local MEUNIER_STDP = 1

STDP_AINH = 0.1
STDP_AEXC = 0.1
STDP_TYPE = MEUNIER_STDP

      -- Delay learning parameters

DELAY_EPSILON = 5 * TIMESCALE
DELAY_LEARNING_JITTER = 0.3 * TIMESCALE

-- * TIMESCALE

DELAY_AINH = 0.01 -- used for stdp-like delay learning. Currently unused.
DELAY_AEXC = 0.01 -- used for stdp-like delay learning. Currently unused.

      -- Synapse constants

MAX_DELAY = 20 * TIMESCALE
MIN_DELAY = 1
MAX_WEIGHT = 1
MIN_WEIGHT = 0
PSP_STRENGHT = 10
