PSP_STRENGHT = "0"
TIMESCALE = 10

 -- Synapse parameters

MAX_DELAY = 0
MIN_DELAY = 0

MAX_WEIGHT = 0
MIN_WEIGHT = 0

 -- Directories

ROOT = "./"

DATA_OUTPUT_DIR = "data/"
DATA_OUTPUT_EXT = "txt"

PATTERNS_DIR = "datasets/"
PATTERNS_EXT = "dat"

FIGURE_SCRIPTS_DIR = "plots/"
FIGURE_SCRIPTS_EXT = "plot"

FIGURES_DIR = "figures/"
FIGURES_EXT = "eps"

ARCHITECTURE_OUTPUT_DIR = "architectures/"
ARCHITECTURE_XML_FILE = "architecture-%010ld--RandSeed-%d--Size-%d--Classes-%d--timescale-%d.xml"

BUILDERS_DIR = "builders/"
SETTINGS_DIR = "settings/"
SIMULATIONS_DIR = "simulations/"
      -- STDP constants

local CLASSICAL_STDP = 0
local MEUNIER_STDP = 1

STDP_AINH = 0.1
STDP_AEXC = 0.1
STDP_TYPE = 1

      -- Delay learning parameters

DELAY_EPSILON = 50
DELAY_LEARNING_JITTER = 1

DELAY_AINH = 0.01 -- used for stdp-like delay learning. Currently unused.
DELAY_AEXC = 0.01 -- used for stdp-like delay learning. Currently unused.

