

TIMESCALE = 10

NbTestPatterns = 7291
NbTrainPatterns = 2007

EVT_RECURSIVE =            8  -- Treat a recursive event
EVT_NEXT_PHASE =          16  -- Means we should enqueue the events of the next phase known by the eventmanager

NB_TRAIN_EPOCS = 10

NB_RAND_STIM = 200
RAND_STIM_INTERVAL = 20 * TIMESCALE

SEPARATION_INTERVAL = 1000 * TIMESCALE
STIM_INTERVAL = 200 * TIMESCALE --200 * TIMESCALE

NB_STIM_PRESENTATIONS = 1
NB_STIM_REPETITION_IN_STIM_INTERVAL = 1
STIM_REPETITION_SEPERATION = 5 * TIMESCALE -- 5 ms

INITIALIZATION_START = 0
INITIALIZATION_LENGTH = NB_RAND_STIM * RAND_STIM_INTERVAL

TRAINING_START = INITIALIZATION_LENGTH + SEPARATION_INTERVAL
TRAINING_LENGTH = ( NbTrainPatterns * NB_TRAIN_EPOCS * STIM_INTERVAL * NB_STIM_PRESENTATIONS) 

TRAIN_PERF_START = TRAINING_START + TRAINING_LENGTH
TRAIN_PERF_LENGTH = ( NbTrainPatterns * 1 * STIM_INTERVAL * NB_STIM_PRESENTATIONS)

TEST_PERF_START = TRAIN_PERF_START + TRAIN_PERF_LENGTH
TEST_PERF_LENGTH = ( NbTestPatterns * 1 * STIM_INTERVAL * NB_STIM_PRESENTATIONS)

END_TIME = TEST_PERF_START + TEST_PERF_LENGTH
GENERALIZATION_PHASE = TEST_PERF_LENGTH

NOISE = 0


--  EmittingNeuron = NULL;
--  Time = 0;
--  EventType = EVT_NULL;
--  ImpactingSynapse = NULL;
--  Assembly = NULL;
--  End = 0;
--  RecurssionStep = 0;
--  func = NULL;
--  fparam.NbStimPresentations = 1;
--  fparam.NbStimRepetitionsInInterval = 1;
--  fparam.StimRepetitionSeparation = 0;
--  fparam.PatternNumber = 1;

evtNextPhase =   { EventType     = EVT_NEXT_PHASE,  -- Event type
                   Assembly      = "Input",     -- Assembly name on witch the event is applied
                   Time          = -1,              -- Time of start of the event. -1 means after previous event.
                   RecurssionStep = 0,               -- Recurssion step, in case of recursive event
                   Duration      = 0,               -- Duration
                   Function      = nil,             -- Event function
                   FuncParam1    = nil,             -- Parameters for the function
                   FuncParam2    = nil,
                   FuncParam3    = nil,
                   FuncParam4    = nil,
                    FuncDParam1    = nil,
                    FuncDParam2    = nil,
                   	FuncStrParam1 = nil,
                   	FuncStrParam2 = nil
                 }

evtRandomInput = { EventType     = EVT_RECURSIVE,
                   Assembly      = "Input",
                   Time          = -1,
                   RecurssionStep = 1 * TIMESCALE,
                   Duration      = 20 * TIMESCALE,
                   Function      = "RandomInputs",
                   FuncParam1    = nil,
                   FuncParam2    = nil,             
                   FuncParam3    = nil,
                   FuncParam4    = nil,
                    FuncDParam1    = nil,
                    FuncDParam2    = nil,
                   	FuncStrParam1 = nil,
                   	FuncStrParam2 = nil
                 }  

evtParticular6Pres = { EventType     = EVT_RECURSIVE,
                    Assembly      = "Input",
                    Time          = -1,
                    RecurssionStep = STIM_INTERVAL,
                    Duration      = STIM_INTERVAL * NB_STIM_PRESENTATIONS * 100,
                    Function      = "PostParticularTrainInput",
                    FuncParam1    = NB_STIM_PRESENTATIONS,
                    FuncParam2    = NB_STIM_REPETITION_IN_STIM_INTERVAL,
                    FuncParam3    = STIM_REPETITION_SEPERATION,
                    FuncParam4    = 1,
                    FuncDParam1    = nil,
                    FuncDParam2    = nil,
                   	FuncStrParam1 = nil,
                   	FuncStrParam2 = nil
                  }  
                  
--print (evtParticular6Pres.Assembly .. " " .. evtParticular6Pres.Duration .. " " .. evtParticular6Pres.RecurssionStep)

evtDoNothing = { EventType     = EVT_RECURSIVE,
                    Assembly      = "Input",
                    Time          = -1,
                    RecurssionStep = 100 * TIMESCALE,
                    Duration      = 4000,
                    Function      = "",
                    FuncParam1    = nil,
                    FuncParam2    = nil,
                    FuncParam3    = nil,
                    FuncParam4    = nil,
                    FuncDParam1    = nil,
                    FuncDParam2    = nil,
                   	FuncStrParam1 = nil,
                   	FuncStrParam2 = nil
                  }  

 
evtSaveArchi      = { EventType     = EVT_RECURSIVE,
                   Assembly      = "Input",
                   Time          = -1,
                   RecurssionStep = 1,
                   Duration      = 0,
                   Function      = "SaveArchitecture",
                   FuncParam1    = nil,
                   FuncParam2    = nil,
                   FuncParam3    = nil,
                   FuncParam4    = nil,
                    FuncDParam1    = nil,
                    FuncDParam2    = nil,
                   	FuncStrParam1 = "architectures/mynewarchi-%010ld--seed-%d--size-%d--classes-%d--timescale-%d.xml", -- architecture-%010ld--RandSeed-%d--Size-%d--Classes-%d--timescale-%d.xml
                   	FuncStrParam2 = nil
                 }  




Initialisation = { "random stimulation", "evtRandomInput" , "evtNextPhase" }

SaveArchitecture = {"save current state of architecture", "evtSaveArchi", "evtNextPhase" }

Particular6 = { "stimulate with one example of 6", "evtParticular6Pres", "evtNextPhase" }
  
Life = { "Let them live...", "evtDoNothing", "evtNextPhase" }

Simulation = { "Initialisation", "SaveArchitecture", "Particular6", "Life" }







