FALSE = 0
TRUE = 1
UNSPECIFIED = 2


ASS_TO_ASS = 0
NEU_TO_ASS = 1
ASS_TO_NEU = 2
NEU_TO_NEU = 3
SUBASS_TO_SUBASS = 4
ONE_TO_ONE = 5
REGULAR = 6
REGULAR_AND_REWIRE = 7
SCALE_FREE = 8

TIMESCALE = 10

RESTING_POTENTIAL = -65
THRESHOLD = -50
ABSOLUTE_REFRACTORY_POTENTIAL = 20000
ABSOLUTE_REFRACTORY_PERIODE = 5 * TIMESCALE
TAU_PSP = 3 * TIMESCALE -- 3 * TIMESCALE
TAU_REFRACT = 0.1 * TIMESCALE



NbTestPatterns = 7291
NbTrainPatterns = 2007

--------------- NEURONS PARAMS

input_nparam = { 
  MembranePotential = RESTING_POTENTIAL;
  Threshold = THRESHOLD;
  AbsoluteRefractoryPeriode = ABSOLUTE_REFRACTORY_PERIODE;
  AbsoluteRefractoryPotential = ABSOLUTE_REFRACTORY_POTENTIAL;
  Inhibitory = 0.0;
  TauPSP = TAU_PSP;
  TauRefract = TAU_REFRACT;
  }

reservoir_nparam_excit = { 
  MembranePotential = RESTING_POTENTIAL;
  Threshold = THRESHOLD;
  AbsoluteRefractoryPeriode = ABSOLUTE_REFRACTORY_PERIODE;
  AbsoluteRefractoryPotential = ABSOLUTE_REFRACTORY_POTENTIAL;
  Inhibitory = 0.0;
  TauPSP = TAU_PSP;
  TauRefract = TAU_REFRACT;
  }

reservoir_nparam_inhib = { 
  MembranePotential = RESTING_POTENTIAL;
  Threshold = THRESHOLD;
  AbsoluteRefractoryPeriode = ABSOLUTE_REFRACTORY_PERIODE;
  AbsoluteRefractoryPotential = ABSOLUTE_REFRACTORY_POTENTIAL;
  Inhibitory = 1.0;
  TauPSP = TAU_PSP;
  TauRefract = TAU_REFRACT;
  }
  
output_nparam = { 
  MembranePotential = RESTING_POTENTIAL;
  Threshold = THRESHOLD;
  AbsoluteRefractoryPeriode = ABSOLUTE_REFRACTORY_PERIODE;
  AbsoluteRefractoryPotential = ABSOLUTE_REFRACTORY_POTENTIAL;
  Inhibitory = 0.0;
  TauPSP = TAU_PSP;
  TauRefract = TAU_REFRACT;
  }




----------- PROJECTIONS PARAMS

input_reservoir_pparam = {
  ProjectionRate = 0.01;
  DeltaDelay = 0 * TIMESCALE;
  DeltaWeight = 0;
  Weight = 1.0;
  Delay = -1;
  WeightAdaptation = false;
  DelayAdaptation = false;
  Inhibitory = UNSPECIFIED;
}

reservoir_pparam = {
  ProjectionRate = 0.03;
  DeltaDelay = 0 * TIMESCALE;
  DeltaWeight = 0;
  Weight = -1;
  Delay = -1;
  WeightAdaptation = true;
  DelayAdaptation = false;
  Inhibitory = UNSPECIFIED;
}

reservoir_output_pparam = {
  ProjectionRate = 1;
  DeltaDelay = 0 * TIMESCALE;
  DeltaWeight = 0;
  Weight = -1;
  Delay = 10;
  WeightAdaptation = false;
  DelayAdaptation = false;
  Inhibitory = UNSPECIFIED;
}

  --print (nparam.MembranePotential .." ".. nparam.Threshold .." ".. nparam.AbsoluteRefractoryPeriode .." ".. nparam.AbsoluteRefractoryPotential .." ".. nparam.Inhibitory .." ".. nparam.TauPSP .." ".. nparam.TauRefract)
  --print (pparam.ProjectionRate, pparam.DeltaDelay, pparam.DeltaWeight, pparam.Weight, pparam.Delay, pparam.WeightAdaptation , pparam.DelayAdaptation, pparam.Inhibitory)


------------------------- DESCRIPTION DE CONSTRUCTION : ASSEMBLY PROJECTION


Assemblies = { "Input", "Reservoir", "Output" }

Input = { "InputSet" }
	InputSet = { 256, "input_nparam" } 

Reservoir = { "ReservoirSet1", "ReservoirSet2" }
	ReservoirSet1 = { 800, "reservoir_nparam_excit" }
	ReservoirSet2 = { 200, "reservoir_nparam_inhib" }

Output = { "OutputSet" }
	OutputSet = { 10, "output_nparam" }

Projections = { "InputReservoir", "ReservoirReservoir", "ReservoirOutput" }

InputReservoir = { ASS_TO_ASS, "Input", "Reservoir", "input_reservoir_pparam" }
ReservoirReservoir = { ASS_TO_ASS, "Reservoir", "Reservoir", "reservoir_pparam" }
ReservoirOutput = { ASS_TO_ASS, "Reservoir", "Output", "reservoir_output_pparam" }


--ExempleProj0 = { NEU_TO_ASS, "Input", id, "Reservoir", "pparam" }
--ExempleProj1 = { NEU_TO_NEU, "Input", id1, "Reservoir", id2, "pparam" }
--ExempleProj2 = { ASS_TO_NEU, "Input", "Reservoir", id, "pparam" }
--ExempleProj3 = { SUB_TO_SUB, "Input", from1, size1, "Reservoir", from2, size2, "pparam" }
--ExempleProj4 = { ONE_TO_ONE, "Input", "Reservoir", "pparam" }
--ExempleProj5 = { REGUAR, "Input", k, "pparam" }
--ExempleProj6 = { REGUAR_AND_REWIRE, "Input", k, p, "pparam" }
--ExempleProj7 = { SCALE_FREE, Ass, PROJECTION_PARAMETERS /*InitialRandNetParam*/, InitialNumberNeurons, PROJECTION_PARAMETERS, k, kin, kout, alpha, beta, gamma}



