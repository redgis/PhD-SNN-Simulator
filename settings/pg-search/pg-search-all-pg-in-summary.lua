
TIMESCALE = 10

MAX_MEMORY_USE = 1100*1024*1024 -- 1024*1024*1024 -- Computer memory allocated to PG search. I use 1Gbytes on my laptop (2Gb RAM)

PG_OUTPUT_DIR = "PGs/"
MAX_PG_TIME = 150 * TIMESCALE -- 80 * TIMESCALE
MIN_PG_TIME = 5 * TIMESCALE
MIN_PG_SIZE = 5 -- 5 -- 2 -- nb spike INCLUDING TRIGGERS (see CEventGrid::Save ())
MAX_PG_SIZE = 1000

PG_JITTER = 0.1 * TIMESCALE
-- Avec 3 triggers, PG_JITTER est lintersection de la droite y=(Seuil - Repos)-(PSP_STRENGHT*POIDS_MOYEN), et de la courbe (PSP_STRENGHT*POIDS_MOYEN) + f(x,PSP_STRENGHT)
-- f(x,y) = (y * POIDS_MOYEN * exp(-x/TAU_PSP))
-- exemple : plot 5.5 + f(x,11) + , 15-5.5 => intersection en 1.6, => c'est le temps maxi auquel le 2ième PSP 
-- peut arriver pour permettre un dernier qu'un 3ième PSP fasse spiker ! 

-- f(x,y,z) = (((y * POIDS_MOYEN) + z)  * exp(-x/TAU_PSP))  -- z est le PSP Sum qu'on a actuellement
-- plot f(x,PSP_STRENGTH,f(0.3,PSP_STRENGTH,0)) , 15-(PSP_STRENGTH/2)
-- 

NB_TRIGGER_NEURONS = 3 -- 4
AVOID_CYCLICS = false
SEARCH_BY_NB_PSP = false
NB_PSP_TO_SPIKE = 3 -- 3
WEIGHT_THRESHOLD = 0 -- 0.9953 --0.95

SIZE_DISTRIB_BEAN = 1 -- in nb of spikes
TIMESPAN_DISTRIB_BEAN = 0.1 * TIMESCALE -- in ms

SAVE_DETAILED_PGS = false
ONLY_TRIGGERS_IN_SUMMARY = false
