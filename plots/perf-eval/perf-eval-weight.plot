reset

set terminal postscript eps enhanced color defaultplex "Times-Roman" 18

set output "figures/perf-eval-weight.eps"

set xlabel 'Time'
set ylabel 'Total positive and negative Delta Weight'
#set logscale xy

#set key

set tics out

set xrange [20000:*]

#set style line 1 lt 1 lw 1
#set style line 2 lt 1 lw 1
#set style line 3 lt 1 lw 1


plot "data/perf-eval-weight-increase.txt" w lines, "data/perf-eval-weight-decrease.txt" w lines, "data/perf-eval-weight-delta.txt" w lines

#plot "data/perf-eval-weight-delta.txt" w lines
