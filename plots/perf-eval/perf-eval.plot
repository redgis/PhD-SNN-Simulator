reset

set terminal postscript eps enhanced color defaultplex "Times-Roman" 18

set output "figures/perf-eval.eps"

set xlabel 'Time'
set ylabel 'Success rate during time slice (%)'
#set logscale xy

#set key

set tics out

#set style line 1 lt 1 lw 1
#set style line 2 lt 1 lw 1
#set style line 3 lt 1 lw 1

set ytics 10
set grid ytics

set yrange [0:100]

plot 'data/perf-eval-wrong.txt' using 1:2 with lines title "error rate", \
     'data/perf-eval-good.txt' using 1:2 with lines title "success rate", \
     'data/perf-eval-unknown.txt' using 1:2 with lines title "undecision rate", \
     'data/perf-eval-nospike.txt' using 1:2 with lines title "no output rate" 
