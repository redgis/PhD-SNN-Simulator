
################################################
reset
set terminal postscript eps enhanced color defaultplex "Times-Roman" 12
set output 'figures/pgs-sizes.eps'

set pointsize 0.6

set yrange [0:*]
set xrange [0:*]

set ylabel 'Nb of PGs'
set xlabel 'Size of PG (in nb of spikes)'

#set logscale xy

plot 'PGs//pgs-sizes.txt' using 1:2 w l title 'PGs/', 'PGs//pgs-sizes.txt' using 1:2 w p title 'PGs/'