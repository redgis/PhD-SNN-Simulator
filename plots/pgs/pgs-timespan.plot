
################################################
reset
set terminal postscript eps enhanced color defaultplex "Times-Roman" 12
set output 'figures/pgs-timespan.eps'

set pointsize 0.6

set yrange [0:*]
set xrange [0:*]

set ylabel 'Nb of PGs'
set xlabel 'Time span of PG (in ms)'

#set logscale xy

plot 'PGs//pgs-timespan.txt' using (($2)/10):1 w l title 'PGs/'