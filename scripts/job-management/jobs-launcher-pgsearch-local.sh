#!/bin/bash

#rm ../jobs.txt

make

rm -fr localjobs 2>/dev/null
export TMPBATCH=localjobs

for reseau in $*
do
  mkdir $TMPBATCH
  jobname=`basename $reseau .xml`
#  qsub -N job-$jobname -l M=2200MB,T=3801600,scratch=16250MB,platform=LINUX,u_sps_liris -q T -o /sps/liris/rmartinez/joblogs/ -e /sps/liris/rmartinez/joblogs/ <<eof
#/sps/liris/rmartinez/src-simulateur/job-start-pgsearch.sh $reseau 
#eof
  echo "job $jobname submitted localy with submit time = `date +\"%D-%X\"`"
  /sps/liris/rmartinez/src-simulateur/scripts/job-management/job-start-pgsearch.sh $reseau 1>../joblogs/$jobname.o 2>../joblogs/$jobname.e
  
  rm -fr $TMPBATCH
  


#  nouveaunom=`echo "$reseau" | sed -re 's/[0-9][0-9][0-9]/0&/'`
#  mv $reseau $nouveaunom

done

