#!/bin/bash

# # PBS -l platform=LINUX     # Plateforme d'ex?cution
# # PBS -l M=2200MB             # M?moire en MB
# # PBS -l scratch=16GB       # Taille du scratch en MB
# # PBS -l T=3800000
# # PBS -q T
# # PBS -o /sps/liris/rmartinez/
# # PBS -e /sps/liris/rmartinez

# $1 = $reseau

# Copie des donnees d entree
cd /sps/liris/rmartinez/src-simulateur/
rm -fr $TMPBATCH/*
mkdir $TMPBATCH/src-simulateur
cp * $TMPBATCH/src-simulateur/
cp -R datasets lua settings scripts src $TMPBATCH/src-simulateur/
mkdir $TMPBATCH/src-simulateur/data
mkdir $TMPBATCH/src-simulateur/figures
mkdir $TMPBATCH/src-simulateur/plots
mkdir $TMPBATCH/src-simulateur/architectures/
cp $1 $TMPBATCH/src-simulateur/architectures/

archi="architectures/`basename $1`"

# Execution du run
cd $TMPBATCH/src-simulateur/
./pg-search2 $archi settings/pg-search/settings.lua settings/pg-search/pg-search.lua

rep=`basename $archi .xml`

# Copie des donnees de sortie
mv PGs PGs-$rep/
cp -R settings/pg-search/ $archi PGs-$rep/
#tar -cjf PGs-$rep.bz2 PGs-$rep/

#cp PGs-$rep.bz2 /sps/liris/rmartinez/runs/
rm -fr /sps/liris/rmartinez/src-simulateur/data/PGs-$rep/
mkdir /sps/liris/rmartinez/src-simulateur/data/PGs-$rep/
cp -R PGs-$rep/pg-scan.txt PGs-$rep/pg-size-distribution.txt PGs-$rep/pg-timespan-distribution.txt PGs-$rep/PGs.txt $archi settings/pg-search/ /sps/liris/rmartinez/src-simulateur/data/PGs-$rep/



