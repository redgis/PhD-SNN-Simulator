#!/bin/bash

# $* = liste de TOUS LES JOBS a executer

# Remove trailing simulation number (...-01 ...-02 ...-03 etc.)

lowerlimit=50
blocksize=350
sentjobs=0
totaljobs=$#

jobsinnode=20

sleeptime=120

archi=`echo $* | tr -t ' ' '\n' | sed -r -e 's/-[0-9][0-9].xml//' | sort | uniq`
nbjobs=`qjob -nh | wc -l`


while [ $# -ne 0 ]; do
  echo "`date +\"%d-%m-%Y %X\"` Submitted so far: $sentjobs   Remaining jobs:$#   Queued jobs: $nbjobs"
  echo "`date +\"%d-%m-%Y %X\"` Submitted so far: $sentjobs   Remaining jobs:$#   Queued jobs: $nbjobs" >> ../jobs-scheduler.txt
  nbjobs=`qjob -nh | wc -l`
  
  # if less than $lowerlimit jobs are queued
  if [ $nbjobs -le $lowerlimit ]; then
    newjobs=""
    echo "Nb jobs $nbjobs  < $lowerlimit"
    echo "Nb jobs $nbjobs  < $lowerlimit" >> ../jobs-scheduler.txt
    sentjobs=$((sentjobs+blocksize))    
    
    #Launch $blocksize jobs
    for index in `seq 1 $blocksize`; do
      newjobs="$newjobs $1"
      shift
    done
    
    # Jobs are ready do launch
    ./scripts/job-management/jobs-launcher-pgsearch-bqs.sh $jobsinnode $newjobs >> ../jobs.txt
    mail -s "[IN2P3] Job Scheduler Report" rmartine <<< "Jobs submitted so far: $sentjobs / $totaljobs.
Remaining $nbjobs in queue, just started `echo \"$newjobs\" | wc -w`  new jobs: 
`echo \"$newjobs\" | tr -t ' ' '\n'` "
  fi
  sleep $sleeptime
done



# echo $archi  
#  echo "Hello world" | mail -s "job finished, starting next" regis.martinez@liris.cnrs.fr


