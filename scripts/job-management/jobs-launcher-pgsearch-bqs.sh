#!/bin/bash

#rm ../jobs.txt

#################################################################################
function showhelp
{
  echo "use: $0 <# archi on each job> <archi list>"
  exit 1
}

#################################################################################

if [ "$1" = "--help" ]; then
  showhelp
fi



nodejobs=$1
shift

make 2>/dev/null

while [ $# -ne 0 ]
do
  reseau=$1
  jobname=`basename $reseau .xml` # `echo $reseau | cut -d '/' -f 2 | cut -d '.' -f 1`
  
  # build bqs command
  cmd=""
  for index in `seq 1 $nodejobs`; do
    if [ $# -ne 0 ]; then
      cmd="/sps/liris/rmartinez/src-simulateur/scripts/job-management/job-start-pgsearch.sh $1 ; $cmd"
      shift
    fi
  done
  
  # submit job
  qsub -N job-$jobname -l M=2200MB,T=3801600,scratch=16250MB,platform=LINUX,u_sps_liris -q T -o /sps/liris/rmartinez/joblogs/ -e /sps/liris/rmartinez/joblogs/ <<eof
$cmd
eof

Current=`cat /sps/liris/rmartinez/next.txt | tr -t '.' ' ' | tr -t '-' ' ' | cut -d ' ' -f 3`

echo "job-$jobname $cmd" >> /sps/liris/rmartinez/jobs-$Current.txt
#  nouveaunom=`echo "$reseau" | sed -re 's/[0-9][0-9][0-9]/0&/'`
#  mv $reseau $nouveaunom

done

