#!/bin/sh
########################## Make graph-properties

nbrep=$#
nbdone=0

for rep in $*
do
  nbdone=$((nbdone+1))
  echo -n "Doing $rep ... ($nbdone / $nbrep) ... "
  
  src/tools/graph-properties/graph-properties $rep/*-d.pajek -a > $rep/graph-basics.txt
  src/tools/graph-properties/graph-properties $rep/*-d.pajek -P > $rep/graph-distrib-path-lenght.txt
#  src/tools/graph-properties/graph-properties $rep/*-d.pajek -e > $rep/graph-nodes-degree.txt
#  src/tools/graph-properties/graph-properties $rep/*-d.pajek -i > $rep/graph-nodes-indegree.txt
#  src/tools/graph-properties/graph-properties $rep/*-d.pajek -o > $rep/graph-nodes-outdegree.txt
#  src/tools/graph-properties/graph-properties $rep/*-d.pajek -f > $rep/graph-nodes-betweeness.txt
  
  echo "done."
done
