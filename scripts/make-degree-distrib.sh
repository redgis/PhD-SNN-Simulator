#!/bin/sh

if [ "$1" = "--help" ]; then
  echo "use: $0 <architecture>
Generates the degree distribution of an architecture."
  exit 1
fi

basename=`basename $1 .xml`

echo "Generating data/degree-distribution-$basename.txt"

#echo `cat $1 | grep -E "^[0-9]*[ ][0-9]*$" | cut -d " " -f 1 && cat $1 | grep -E "^[0-9]+[ ][0-9]+$" | cut -d " " -f 2` | tr -t ' ' '\n' | sort -n | uniq -c | tr -s ' ' | cut -d ' ' -f 2 | sort -n | uniq -c > data/degree-distribution-$basename.txt

rm data/degree-distribution-$basename.txt
rm data/degree-out-distribution-$basename.txt
rm data/degree-in-distribution-$basename.txt
rm degree-distribution-$basename.txt.tmp

#count nb connexion for each neuron
for neuron in `grep -o -E "id=\"[0-9]*\"" $1 | cut -d '"' -f 2 | uniq`
do
  echo -n "#$neuron... "
  #degin=`grep -E "^[0-9]+ $neuron$" $1 | wc -l`
  degin=`grep -o -E "outputNeuron=\"$neuron\"" $1 | wc -l`
  
  #degout=`grep -E "^$neuron [0-9]+$" $1 | wc -l`
  degout=`grep -o -E "inputNeuron=\"$neuron\"" $1 | wc -l`
  deg=`echo "$degin+$degout" | bc`
  echo "$neuron $degin $degout $deg" >> degree-distribution-$basename.txt.tmp
done

cat degree-distribution-$basename.txt.tmp | cut -d ' ' -f 2 | sort -n | uniq -c > data/degree-in-distribution-$basename.txt
cat degree-distribution-$basename.txt.tmp | cut -d ' ' -f 3 | sort -n | uniq -c > data/degree-out-distribution-$basename.txt
cat degree-distribution-$basename.txt.tmp | cut -d ' ' -f 4 | sort -n | uniq -c > data/degree-distribution-$basename.txt

rm degree-distribution-$basename.txt.tmp


echo "
################################################
reset
set terminal postscript eps enhanced color defaultplex \"Times-Roman\" 12
set output 'figures/degree-distribution-$basename.eps'

set pointsize 0.6

bornemin=1
bornemax=4

#set logscale xy

f(x)=a*x + b
#fit f(x) \"data/degree-distribution-$basename.txt\" using 2:1 via a,b
fit [bornemin:bornemax] f(x) \"data/degree-distribution-$basename.txt\" using (log(\$2)):(log(\$1)) via a,b

#set logscale xy
#plot f(x) w l, 'data/degree-distribution-$basename.txt' using 2:1 w l

set yrange [0:*]
set xrange [0:*]

set ylabel 'Nb of neurons'
set xlabel 'Degree'

set format y \"log({%2.1f})\"
set format x \"log({%2.1f})\"

set arrow from bornemin,0 to bornemin,100 nohead
set arrow from bornemax,0 to bornemax,100 nohead



#plot 'data/degree-distribution-$basename.txt' using 2:1 w l title 'degrees', \
#     log(f(log(x))) w lines

plot 'data/degree-distribution-$basename.txt' using (log(\$2)):(log(\$1)) w p title 'degrees', \
     f(x) w lines
     
#plot 'data/degree-distribution-$basename.txt' using (log(\$2)):(log(\$1)) w p title 'degrees', \
#     'data/degree-in-distribution-$basename.txt' using (log(\$2)):(log(\$1)) w p title 'in degrees', \
#     'data/degree-out-distribution-$basename.txt' using (log(\$2)):(log(\$1)) w p title 'out degrees', \
#     f(x) w lines
     

" > plots/degree-distribution-$basename.plot

gnuplot plots/degree-distribution-$basename.plot
mv fit.log data/fit-$basename.log

epstopdf figures/degree-distribution-$basename.eps
evince figures/degree-distribution-$basename.eps &
#acroread figures/degree-distribution-$basename.pdf&

