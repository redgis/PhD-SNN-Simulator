#!/bin/bash
# $1 => name of the archi file      $2 => none : build by script to pass to distribs

if [ "$1" = "--help" ]; then
  echo "use: $0 <targetdir> <architecture(s)>
Generates the weights and delays distribution of an architecture."
  exit 1
fi

make #make sure everything is built

targetdir=$1
shift

finaldistribname=`basename $1 -01.xml` #`echo "$1" | cut -d "." -f 1 | cut -d "/" -f 2 | tr -t "/" "-"`
rm $targetdir/$finaldistribname*

# calculate distributions
for archi in $*; do
  distribname=`basename $archi .xml`
  ./distrib $archi $targetdir/$distribname

  ##### plot the distribs
    echo "

    reset

    set terminal postscript eps enhanced color defaultplex \"Times-Roman\" 18

    set output '$targetdir/$distribname-weight-distrib.eps'

    set xlabel 'Weight range'
    set ylabel '% of connexions in this range'

    set tics out

    unset key

    set yrange [0:100]

    set style line 1 lt 1 lw 8 pt 1
    set style line 2 lt 1 lw 1 pt 2
    set style line 3 lt 3 lw 1 pt 3
    set style line 4 lt 4 lw 1 pt 4
    set style line 5 lt 5 lw 1 pt 5
    set style line 6 lt 6 lw 1 pt 6
    set style line 7 lt 7 lw 1 pt 7
    set style line 8 lt 8 lw 1 pt 8
    set style line 9 lt 9 lw 1 pt 9

    plot '$targetdir/$distribname-weight-distrib.txt' using (\$1+0.025):2 w imp ls 1" > $targetdir/$distribname-weight-distrib.plot

    gnuplot $targetdir/$distribname-weight-distrib.plot
    epstopdf $targetdir/$distribname-weight-distrib.eps
    #evince $targetdir/$distribname-weight-distrib.eps&
    #acroread $targetdir/$distribname-weight-distrib.pdf&

    echo "

    reset

    set terminal postscript eps enhanced color defaultplex \"Times-Roman\" 18

    set output '$targetdir/$distribname-delay-distrib.eps'

    set xlabel 'Delay range'
    set ylabel '% of connexions in this range'

    set tics out

    #set yrange [0:100]

    set xtics 10

    set nokey

    set style line 1 lt 1 lw 4 pt 1
    set style line 2 lt 1 lw 1 pt 2
    set style line 3 lt 3 lw 1 pt 3
    set style line 4 lt 4 lw 1 pt 4
    set style line 5 lt 5 lw 1 pt 5
    set style line 6 lt 6 lw 1 pt 6
    set style line 7 lt 7 lw 1 pt 7
    set style line 8 lt 8 lw 1 pt 8
    set style line 9 lt 9 lw 1 pt 9

    plot '$targetdir/$distribname-delay-distrib.txt' using (\$1+2.5):2 w imp ls 1" > $targetdir/$distribname-delay-distrib.plot

    gnuplot $targetdir/$distribname-delay-distrib.plot
    epstopdf $targetdir/$distribname-delay-distrib.eps
    #evince $targetdir/$distribname-delay-distrib.eps&
    #acroread $targetdir/$distribname-delay-distrib.pdf&
done





# calculate mean distribution
finaldistribname=`basename $1 -01.xml` #`echo "$1" | cut -d "." -f 1 | cut -d "/" -f 2 | tr -t "/" "-"`

delays=`cat $targetdir/$finaldistribname-[0-9][0-9]-delay-distrib.txt | sort -n | cut -d ' ' -f 1 | uniq`
weights=`cat $targetdir/$finaldistribname-[0-9][0-9]-weight-distrib.txt | sort -n | cut -d ' ' -f 1 | uniq`

for delay in $delays; do
  addition=`grep -h -E "^$delay " $targetdir/$finaldistribname-[0-9][0-9]-delay-distrib.txt | cut -d " " -f 2 | tr -t '\n' '+';echo 0`
#  echo "$delay -> ($addition)/$#"
  echo $delay `bc <<< "scale=2;($addition)/$#"` >> $targetdir/$finaldistribname-delay-distrib.txt
done


for weight in $weights; do
  addition=`grep -h -E "^$weight " $targetdir/$finaldistribname-[0-9][0-9]-weight-distrib.txt | cut -d " " -f 2 | tr -t '\n' '+';echo 0`
#  echo "$weight -> ($addition)/$#"
  echo $weight `bc <<< "scale=2;($addition)/$#"` >> $targetdir/$finaldistribname-weight-distrib.txt
done


echo "

reset

set terminal postscript eps enhanced color defaultplex \"Times-Roman\" 18

set output '$targetdir/$finaldistribname-weight-distrib.eps'

set xlabel 'Weight range'
set ylabel '% of connexions in this range'

set tics out

unset key

#set yrange [0:100]

set style line 1 lt 1 lw 8 pt 1
set style line 2 lt 1 lw 1 pt 2
set style line 3 lt 3 lw 1 pt 3
set style line 4 lt 4 lw 1 pt 4
set style line 5 lt 5 lw 1 pt 5
set style line 6 lt 6 lw 1 pt 6
set style line 7 lt 7 lw 1 pt 7
set style line 8 lt 8 lw 1 pt 8
set style line 9 lt 9 lw 1 pt 9

plot '$targetdir/$finaldistribname-weight-distrib.txt' using (\$1+0.025):2 w imp ls 1" > $targetdir/$finaldistribname-weight-distrib.plot

gnuplot $targetdir/$finaldistribname-weight-distrib.plot
epstopdf $targetdir/$finaldistribname-weight-distrib.eps
#evince $targetdir/$finaldistribname-weight-distrib.eps&
#acroread $targetdir/$finaldistribname-weight-distrib.pdf&

echo "

reset

set terminal postscript eps enhanced color defaultplex \"Times-Roman\" 18

set output '$targetdir/$finaldistribname-delay-distrib.eps'

set xlabel 'Delay range'
set ylabel '% of connexions in this range'

set tics out

#set yrange [0:100]

set xtics 10

set nokey

set style line 1 lt 1 lw 4 pt 1
set style line 2 lt 1 lw 1 pt 2
set style line 3 lt 3 lw 1 pt 3
set style line 4 lt 4 lw 1 pt 4
set style line 5 lt 5 lw 1 pt 5
set style line 6 lt 6 lw 1 pt 6
set style line 7 lt 7 lw 1 pt 7
set style line 8 lt 8 lw 1 pt 8
set style line 9 lt 9 lw 1 pt 9

plot '$targetdir/$finaldistribname-delay-distrib.txt' using (\$1+2.5):2 w imp ls 1" > $targetdir/$finaldistribname-delay-distrib.plot

gnuplot $targetdir/$finaldistribname-delay-distrib.plot
epstopdf $targetdir/$finaldistribname-delay-distrib.eps
#evince $targetdir/$finaldistribname-delay-distrib.eps&
#acroread $targetdir/$finaldistribname-delay-distrib.pdf&

