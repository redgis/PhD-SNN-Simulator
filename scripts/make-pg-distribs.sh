#!/bin/sh

#################################################################################
function showhelp
{
  echo "use: $0 <PG directories>"
  echo "Generate the distribution plot for each PGs-...-XX/ => result in PGs-.../."
  exit 1
}

#################################################################################
# $1: basename of directory (without the -[0-9][0-9] at the end)  remaining $*: PGs dirs to process
function make_pg_distrib
{
	# get target dir, create/clean it
  local targetdir=$1
  if [ -d $targetdir ]; then
	  rm -fr $targetdir
	fi
	mkdir $targetdir; shift

  netsize=`echo $targetdir | grep -o -e "-n-[0-9][0-9][0-9][0-9]-" | cut -d '-' -f 3 | bc`
  
	rm -f tmpdb
	rm -f tmp.txt

  for rep in $*; do
  	grep -h -v "#"  $rep/PGs.txt | tr -s ' ' | cut -d ' ' -f 2,3,4,6 | sed -r -e 's/^ *//' | tr -t ' ' '|' >> tmp.txt
  done
  sqlite3 tmpdb "create table pg_distribs ( NbSpikes integer, TimeSpan integer, NbDistinctNeurons integer, Frequency real )"	
  sqlite3 tmpdb ".import tmp.txt pg_distribs"
    
  sqlite3 tmpdb "select NbSpikes,count(NbSpikes)/$#.0 from pg_distribs group by NbSpikes order by NbSpikes" | tr -t "|" " " > $targetdir/pg-size-distribution.txt
  sqlite3 tmpdb "select TimeSpan,count(TimeSpan)/$#.0 from pg_distribs group by TimeSpan order by TimeSpan" | tr -t "|" " " > $targetdir/pg-timespan-distribution.txt
  sqlite3 tmpdb "select NbDistinctNeurons,count(NbDistinctNeurons)/$#.0 from pg_distribs group by NbDistinctNeurons order by NbDistinctNeurons" | tr -t "|" " " > $targetdir/pg-population-distribution.txt
  sqlite3 tmpdb "select round(Frequency),count(round(Frequency))/$#.0 from pg_distribs group by round(Frequency) order by round(Frequency)" | tr -t "|" " " > $targetdir/pg-frequency-distribution.txt
  
  
  #AVGsize=`sqlite3 tmpdb "select avg(NbSpikes) from pg_distribs"`
  #AVGspan=`sqlite3 tmpdb "select avg(TimeSpan) from pg_distribs"`
  #AVGpopu=`sqlite3 tmpdb "select avg(NbDistinctNeurons) from pg_distribs"`
  #AVGfreq=`sqlite3 tmpdb "select avg(Frequency) from pg_distribs"`
  
  
  rm -f tmpdb
	rm -f tmp.txt
	
  echo "###########################################################
  reset

  set terminal postscript eps enhanced color defaultplex \"Times-Roman\" 18
  set output 'pg-size-distribution.eps'

  set title 'Distribution of size (in number of spikes) of polychronous groups'
  set xlabel 'Size in Nb of spikes'
  set ylabel 'Number of PGs'

  set tics out
  unset key
  set yrange [0:*]
  set xrange [0:1010]
  
  set style line 1 lt 1 lw 1 pt 1
  set style line 2 lt 1 lw 1 pt 2
  set style line 3 lt 3 lw 1 pt 3
  set style line 4 lt 4 lw 1 pt 4
  set style line 5 lt 5 lw 1 pt 5
  set style line 6 lt 6 lw 1 pt 6
  set style line 7 lt 7 lw 1 pt 7
  set style line 8 lt 8 lw 1 pt 8
  set style line 9 lt 9 lw 1 pt 9

  #set arrow 9 from $AVGsize,0 to $AVGsize,10000000

  plot 'pg-size-distribution.txt' using 1:2:(0.5) w boxes fs solid 1

  ###########################################################
  reset

  set terminal postscript eps enhanced color defaultplex \"Times-Roman\" 18
  set output 'pg-timespan-distribution.eps'

  set title 'Distribution of timespan (ms) of polychronous groups'
  set xlabel 'Timespan (ms)'
  set ylabel 'Number of PGs'

  set tics out
  unset key
  set yrange [0:*]
  set xrange [0:160]
  
  set style line 1 lt 1 lw 1 pt 1
  set style line 2 lt 1 lw 1 pt 2
  set style line 3 lt 3 lw 1 pt 3
  set style line 4 lt 4 lw 1 pt 4
  set style line 5 lt 5 lw 1 pt 5
  set style line 6 lt 6 lw 1 pt 6
  set style line 7 lt 7 lw 1 pt 7
  set style line 8 lt 8 lw 1 pt 8
  set style line 9 lt 9 lw 1 pt 9

  #set arrow 9 from $AVGspan,0 to $AVGspan,10000000
  
  plot 'pg-timespan-distribution.txt' using (\$1/10.0):2 w imp ls 1
  
  ###########################################################
  reset

  set terminal postscript eps enhanced color defaultplex \"Times-Roman\" 18
  set output 'pg-population-distribution.eps'

  set title 'Distribution of population recruited by polychronous groups'
  set xlabel 'Population (#Neurons)'
  set ylabel 'Number of PGs'

  set tics out
  unset key
  set yrange [0:*]
  set xrange [0:$netsize+10]
  
  set style line 1 lt 1 lw 1 pt 1
  set style line 2 lt 1 lw 1 pt 2
  set style line 3 lt 3 lw 1 pt 3
  set style line 4 lt 4 lw 1 pt 4
  set style line 5 lt 5 lw 1 pt 5
  set style line 6 lt 6 lw 1 pt 6
  set style line 7 lt 7 lw 1 pt 7
  set style line 8 lt 8 lw 1 pt 8
  set style line 9 lt 9 lw 1 pt 9

  #set arrow 9 from $AVGpopu,0 to $AVGpopu,10000000
  
  plot 'pg-population-distribution.txt' using 1:2:(0.5) w boxes fs solid 1
  
  ###########################################################
  reset

  set terminal postscript eps enhanced color defaultplex \"Times-Roman\" 18
  set output 'pg-frequency-distribution.eps'

  set title 'Distribution of frequencies of polychronous groups'
  set xlabel 'Frequency (Hz)'
  set ylabel 'Number of PGs'

  set tics out
  unset key
  set yrange [0:*]
  set xrange [0:*]
  
  set style line 1 lt 1 lw 1 pt 1
  set style line 2 lt 1 lw 1 pt 2
  set style line 3 lt 3 lw 1 pt 3
  set style line 4 lt 4 lw 1 pt 4
  set style line 5 lt 5 lw 1 pt 5
  set style line 6 lt 6 lw 1 pt 6
  set style line 7 lt 7 lw 1 pt 7
  set style line 8 lt 8 lw 1 pt 8
  set style line 9 lt 9 lw 1 pt 9

  #set arrow 9 from $AVGfreq,0 to $AVGfreq,10000000
  
  plot 'pg-frequency-distribution.txt' using 1:2:(0.5) w boxes fs solid 1" > $targetdir/pg-distributions.plot

  myPath=`pwd`
	cd $targetdir/
  gnuplot pg-distributions.plot

  epsname=`basename $targetdir`

  sed -e "s/^%%Title.*\$/%%Title: $epsname Size/" pg-size-distribution.eps > pg-size-distribution-1.eps
  mv pg-size-distribution-1.eps pg-size-distribution.eps
  
  sed -e "s/^%%Title.*\$/%%Title: $epsname TimeSpan/" pg-timespan-distribution.eps > pg-timespan-distribution-1.eps
  mv pg-timespan-distribution-1.eps pg-timespan-distribution.eps
  
  sed -e "s/^%%Title.*\$/%%Title: $epsname Population/" pg-population-distribution.eps > pg-population-distribution-1.eps
  mv pg-population-distribution-1.eps pg-population-distribution.eps

  sed -e "s/^%%Title.*\$/%%Title: $epsname Frequency/" pg-frequency-distribution.eps > pg-frequency-distribution-1.eps
  mv pg-frequency-distribution-1.eps pg-frequency-distribution.eps

  epstopdf pg-size-distribution.eps
  #evince pg-size-distribution.eps&
  #acroread pg-size-distribution.pdf&

  epstopdf pg-timespan-distribution.eps
  #evince pg-timespan-distribution.eps&
  #acroread pg-timespan-distribution.pdf&
  
  epstopdf pg-population-distribution.eps
  #evince pg-population-distribution.eps&
  #acroread pg-population-distribution.pdf&
  
  epstopdf pg-frequency-distribution.eps
  #evince pg-frequency-distribution.eps&
  #acroread pg-frequency-distribution.pdf&
  
  cd $myPath
}


#################################################################################

if [ "$1" = "--help" ]; then
  showhelp
fi


reps=`'ls' -d -1 $* | sed -r -e 's/-[0-9][0-9]$//' | sort | uniq`
nbreps=`echo $reps | wc -w`
numrep=0
nbdone=0

for repsetbasename in $reps; do
  if [ -d "$repsetbasename" ]; then
  	nbdone=$((nbdone+1))
  fi
done

echo "$nbdone configurations already processed. Overwrite or Resume ? (O/R)"
read ans

for repsetbasename in $reps; do
  numrep=$((numrep+1))
  echo -n "Calculating distribs for $repsetbasename ... ($numrep/$nbreps) ... "
  if [ -d "$repsetbasename" -a -f "$repsetbasename/pg-population-distribution.txt" -a -f "$repsetbasename/pg-timespan-distribution.txt" -a -f "$repsetbasename/pg-size-distribution.txt" -a -f "$repsetbasename/pg-frequency-distribution.txt" -a -f "$repsetbasename/pg-distributions.plot" -a "$ans" = "R" ]; then
	  echo -n "Already done. Skipping. "
	else
		make_pg_distrib $repsetbasename $repsetbasename-*
  fi
  echo "done."
done





