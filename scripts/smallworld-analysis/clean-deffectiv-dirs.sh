#!/bin/bash


if [ "$1" = "--help" ]; then
  echo "use: $0 [directories list]
Check if PGs.txt and pg-scan.txt are present in directories."
  exit 1
fi


for rep in $*; do
	if [ ! -f $rep/PGs.txt -o ! -f $rep/pg-scan.txt ]; then
		echo "Removing $rep"
		#rm -fr $rep
	fi
done
