#!/bin/bash

outputfile="data/smallworld-analysis/smallworld.txt"
avgcmd="src/tools/avg/avg"

#################################################################################
function showhelp
{
  echo "use: $0 <PG directories list>"
  echo "extract the data for each PGs-...-XX/ directory => write result in $outputfile"
	echo "($outputfile is ready for sqlite import)"
  exit 1
}

#################################################################################
function get_rep_population_and_freq_data
{ 
  
  local popmean
  local spanmean
  local freqmean
  local fulldata
  local fulldata_tmp
  fulldata_tmp=""
  fulldata=""

  for rep in $1-[0-9][0-9]; do
    # in PGs.txt: #NbPGsWritten NbSpikes TimeSpan NbDistinctNeurons Truncated AVGFrequencyByNeuron [Triggers...]
    #spanmean=`more +2 $rep/PGs.txt | cut -d ' ' -f 3 | $avgcmd`
    popmean=`grep -v -E "^#" $rep/PGs.txt | cut -d ' ' -f 4 | $avgcmd`
    freqmean=`grep -v -E "^#" $rep/PGs.txt | cut -d ' ' -f 6 | $avgcmd`
    #echo "$spanmean $popmean $freqmean"
    fulldata=`printf "$fulldata_tmp\n$popmean $freqmean"`
    fulldata_tmp=$fulldata
  done

  #spanmean=`echo "$fulldata" | cut -d ' ' -f 1 | $avgcmd -d` #timespan
  popmean=`echo "$fulldata" | cut -d ' ' -f 1 | $avgcmd -d` #population
  freqmean=`echo "$fulldata" | cut -d ' ' -f 2 | $avgcmd -d` #frequency
  
  #echo "$spanmean $popmean $freqmean"
  echo "$popmean $freqmean"
}


#################################################################################
function make_repset_data
{
  # Nb Neurons      Connectivity    k     p     delai     delta-delai     Nb PGs      Nb PGs Cycle      Calculation Time    Mean Timespan   Mean Size    Population used   
  output=`basename $1 | sed -r -e 's/[a-zA-Z]*//g' | tr -s '-' | tr '-' ' ' | sed -r -e 's/^ //'`
  
  local resultMatrix
  local result_bis
  local result
  
  resultMatrix=`cat $1-[0-9][0-9]/pg-scan.txt | tr -s ' ' | cut -d ' ' -f 2,5,10,15,19`
  result=$output
  
  for column in `echo "1 2 3 4 5"`; do # #PGs #Cyclics Time AvgSpan AvgSize
    result_bis=`echo -n "$result "; (printf "$resultMatrix" | cut -d ' ' -f $column | $avgcmd -d);`
    result=$result_bis
  done
  
  
  # if 0 PG, no data for all
  if [ "`echo $result | cut -d ' ' -f 7`" = "0" ]; then
    #printf "$result 0 0 0 0\n" >> $outputfile
    printf "$result null null null null" >> $outputfile
  else
    #get_rep_population_and_freq_data $1
    populationdata=`get_rep_population_and_freq_data $1 2>/dev/null`
    printf "$result $populationdata" >> $outputfile
  fi

  #entropy THIS PART OF CODE HAS NOT BEEN TESTED YES (06/06/2011)
  #rep="data/processed/PGs-sw-n-$size-*-k-$k-p-$p-d-$delay-dtd-$dtd"
  #echo "$rep ..."          
  echo "*** WARNING: calculating entropy. THIS PART OF CODE HAS NOT BEEN TESTED YES. VERIFY EVERYTHING OK in $outputfile (06/06/2011)"
  local rep=$1
  local mysum
  local entrpy_timespan
  local entrpy_nbspikes
  local entrpy_population
  local entrpy_frequency
  
  mysum=`cat $rep/pg-timespan-distribution.txt | cut -d " " -f 2 | src/tools/sum/sum`
  entrpy_timespan=`awk '{ print -($2/'$mysum')*log($2/'$mysum') }' $rep/pg-timespan-distribution.txt | src/tools/sum/sum`

  mysum=`cat $rep/pg-size-distribution.txt | cut -d " " -f 2 | src/tools/sum/sum`
  entrpy_nbspikes=`awk '{ print -($2/'$mysum')*log($2/'$mysum') }' $rep/pg-size-distribution.txt | src/tools/sum/sum`

  mysum=`cat $rep/pg-population-distribution.txt | cut -d " " -f 2 | src/tools/sum/sum`
  entrpy_population=`awk '{ print -($2/'$mysum')*log($2/'$mysum') }' $rep/pg-population-distribution.txt | src/tools/sum/sum`

  mysum=`cat $rep/pg-frequency-distribution.txt | cut -d " " -f 2 | src/tools/sum/sum`
  entrpy_frequency=`awk '{ print -($2/'$mysum')*log($2/'$mysum') }' $rep/pg-frequency-distribution.txt | src/tools/sum/sum`
  
  printf "$entrpy_timespan $entrpy_nbspikes $entrpy_population $entrpy_frequency\n" >> $outputfile
}


#################################################################################

if [ "$1" = "--help" ]; then
  showhelp
fi


pgdirs=`'ls' -d -1 $* | sed -r -e 's/-[0-9][0-9]$//' | sort | uniq`
#rmtmps=`'ls' -d -1 $* | sed -r -e 's/-[0-9][0-9]$/.tmp/' | sort | uniq`
#rmoctaves=`'ls' -d -1 $* | sed -r -e 's/-[0-9][0-9]$/.octave/' | sort | uniq`

#rm $rmtmps 2>/dev/null
#rm $rmoctaves 2>/dev/null
#rm $outputfile 2>/dev/null

NbDir=`echo "$pgdirs" | wc -w`
Current=1

echo "$# directories to process."

# If outputfile already exists, ask whether to append or overwrite
if [ -f $outputfile ]; then
	echo -n "Database $outputfile exists. Continue with same database ? (n to empty the DB, y to append to current DB): "
	read empty_db
	if [ $empty_db = "n" ]; then
	  echo "Overwriting $outputfile."
		echo "#Neu c k p delay deltad #PGs SD#PG #Cycle SD#Cyc Time SDTime AvgSpan SDSpan AvgSize SDSize Popul SDPopul Freq SDFreq EntropyTimeSpan EntropySize EntropyPopulation EntropyFrequency" | tr -t ' ' '\t' > $outputfile 
  else
    echo "Appending to $outputfile."
	fi
else
  echo "Creating $outputfile ..."
	echo "#Neu c k p delay deltad #PGs SD#PG #Cycle SD#Cyc Time SDTime AvgSpan SDSpan AvgSize SDSize Popul SDPopul Freq SDFreq EntropyTimeSpan EntropySize EntropyPopulation EntropyFrequency" | tr -t ' ' '\t' > $outputfile 
fi


echo "`'ls' -d -1 $* | sed -r -e 's/-[0-9][0-9]$//' | sort | uniq -c | tr -s ' ' | grep -v -E \"^ 10 \" | wc -l` cases do not have 10 runs :"
'ls' -d -1 $* | sed -r -e 's/-[0-9][0-9]$//' | sort | uniq -c | tr -s ' ' | grep -v -E "^ 10 "

for pgdir in $pgdirs; do
	nbrun=`ls -d -1 $pgdir-[0-9][0-9] | wc -l`
	if [ "$nbrun" -ge "5" ]; then	
		echo "Processing $Current / $NbDir : $pgdir has $nbrun runs..."
		Current=$(($Current+1))
		make_repset_data $pgdir
#		rm $pgdir.octave $pgdir.tmp 2>/dev/null
	else
		echo "Skipping $Current / $NbDir : $pgdir has $nbrun runs..."
		Current=$(($Current+1))
	fi
done


