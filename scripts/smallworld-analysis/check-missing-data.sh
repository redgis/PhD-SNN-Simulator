#!/bin/bash


if [ "$1" = "--help" ]; then
  echo "use: $0 
Check if some result are missing (due to problems in simulations). Edit the script to set parameters. Architectures list sent to 1, details sent to 2"
  exit 1
fi


ks="04 06 08" # 02 04 06 08 10 15 30 50 100"

#conectivities="0.01 0.04 0.08 0.1 0.15 0.2 0.25 0.3"
sizes="0100 0200 0400 0600 0800 1000" # 0100 0200 0400 0600 0800 1000
delais="010 050 100 150 200"
deltadelais="000 005 010 020 050 100 200"
Ps="0.00 0.10 0.15 0.20 0.25 0.30 0.50 0.80 1.00" #"0.00 0.10 0.15 0.20 0.25 0.30 0.50 0.80 1.00"

for k in $ks; do
#    for c in $connectivities; do
    for p in $Ps; do
        for size in $sizes; do
						c=`echo "C = string.format(\"%.3f\",(2*$k)/($size-1)) ; print(C)" | lua`
            for delai in $delais; do
							echo "# n-$size c-$c k-$k p-$p d-$delai" >&2
                for deltadelai in $deltadelais; do
                    for num in `seq --format=%02.0f 1 10`; do
											directory="PGs-sw-n-$size-c-$c-k-$k-p-$p-d-$delai-dtd-$deltadelai-$num"
											shortarchi="sw-n-$size-c-$c-k-$k-p-$p-d-$delai-dtd-$deltadelai-$num.xml"
											archi="architectures/smallworlds/sw-n-$size-c-$c-k-$k-p-$p-d-$delai-dtd-$deltadelai-$num.xml"
																						

											# Verify if PGs-... directory exists   AND   if files in directory exist (pg-scan.txt PGs.txt pg-size-distribution.txt pg-timespan-distribution.txt pg-search architecture .xml,)
											if [ ! -d "$directory" ] ; then
												echo "$archi" >&1
												echo "$directory: missing" >&2
											elif [ ! -f $directory/PGs.txt ];then
												echo "$archi" >&1
												echo "$directory: missing PGs.txt" >&2
											elif [ ! -f $directory/pg-scan.txt ]; then
												echo "$archi" >&1
												echo "$directory: missing pg-scan.txt" >&2
											elif [ ! -f $directory/pg-size-distribution.txt ]; then
												echo "$archi" >&1
												echo "$directory: missing pg-size-distribution.txt" >&2
											elif [ ! -f $directory/pg-timespan-distribution.txt ]; then
												echo "$archi" >&1
												echo "$directory: missing pg-timespan-distribution.txt" >&2
											elif [ ! -f $directory/$shortarchi ]; then
												echo "$archi" >&1
												echo "$directory: missing $shortarchi" >&2
											elif [ ! -d $directory/pg-search ]; then 
												echo "$archi" >&1
												echo "$directory: missing pg-search" >&2
											fi


                    done
                done
            done
        done
    done
#    done
done
