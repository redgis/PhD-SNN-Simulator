#!/bin/bash

############ SMALLWORLD analysis


prefix="smallworld"
outdir="data/smallworld-analysis"
tablename="smallworld"
dbfile="$outdir/topodb.db"


#ext="pdf"
#driver="pdfcairo font \"Luxi Sans,6\" enhanced"
#driver="pdfcairo font \"sans,8\" noenhanced"
#driver="pdfcairo font \"Times,8\" noenhanced"
#driver="pdfcairo font \"Times-New-Roman,6\" noenhanced"

#ext="svg"
#driver="svg font \"arial,8\" noenhanced"
#driver="svg font \"Helvetica,8\" noenhanced"

ext="eps"
driver="postscript eps enhanced color defaultplex font \"Helvetica,18\""

#ext="png"
#driver="png font \"arial,8\"" 

convertto="pdf"
#convertto=""

function echo_plot_header
{

echo "

################################################################################
################################################################################
reset

set terminal $driver

set pointsize 0.5

set style line 1 lw 1 pt 1 lc rgb \"black\"
set style line 2 lw 1 pt 2 lc rgb \"red\"
set style line 3 lw 1 pt 3 lc rgb \"green\"
set style line 4 lw 1 pt 4 lc rgb \"blue\"
set style line 5 lw 1 pt 5 lc rgb \"magenta\"
set style line 6 lw 1 pt 6 lc rgb \"orange\"
set style line 7 lw 1 pt 7 lc rgb \"dark-grey\"
set style line 8 lw 1 pt 8 lc rgb \"gold\"
set style line 9 lw 1 pt 9 lc rgb \"violet\"

set style line 10 lw 1 pt 1 lc rgb \"black\"
set style line 11 lw 1 pt 2 lc rgb \"red\"
set style line 12 lw 1 pt 3 lc rgb \"green\"
set style line 13 lw 1 pt 4 lc rgb \"blue\"
set style line 14 lw 1 pt 5 lc rgb \"magenta\"
set style line 15 lw 1 pt 6 lc rgb \"cyan\"
set style line 16 lw 1 pt 7 lc rgb \"dark-grey\"
set style line 17 lw 1 pt 8 lc rgb \"turquoise\"
set style line 18 lw 1 pt 9 lc rgb \"gold\"
set style line 19 lw 1 pt 10 lc rgb \"violet\"
set style line 20 lw 1 pt 11 lc rgb \"navy\"

#set size 1,1

"

}

#########################################################################################################################
#########################################################################################################################
#########################################################################################################################
# make_multiple_2d_plots <X> <Y> <error> <witheach> <foreeach> <opt plotmode> <enable legend 0/1> <optional additionnal where> <dashed> <X title> <Y title> <Graph title> <witheach rename> <foreach rename>
function make_multiple_2d_plots
{
  echo -n "Making make_multiple_2d_plots $* ... "

  local x=$1; shift;
  local y=$1; shift;
  local yerror=$1; shift;
  local witheach=$1; shift;
  local foreach=$1; shift;
  local plotmode=$1; shift;
  local haslegend=$1; shift;
 	local addwhere=$1; shift;
	local dashed=$1; shift;
  local xtitle=$1; shift;
  local ytitle=$1; shift;
  local graphtitle=$1; shift;
  local witheach_alias=$1; shift;
  local foreach_alias=$1; shift;
  
  if [ "$xtitle" = "" ]; then
    xtitle=$x
  fi

  if [ "$ytitle" = "" ]; then
    ytitle=$y
  fi

  if [ "$witheach_alias" = "" ]; then
    witheach_alias=$witheach
  fi

  if [ "$foreach_alias" = "" ]; then
    foreach_alias=$foreach
  fi

  for foreachvalue in `sqlite3 $dbfile "select distinct $foreach from $tablename order by $foreach"`
  do
    if [ "$graphtitle" = "" ]; then
      title="$ytitle / $xtitle for different $witheach_alias | $foreach_alias=$foreachvalue $addwhere"
    else
      title=$graphtitle
    fi
    
  	local suffix=`echo "$foreach-$foreachvalue$addwhere" | sed -r -e 's/and|or/-/g' | sed -r -e 's/[()]//g' | sed -r -e 's/ //g' | tr -t '=' '-' | tr -s '-'`
  	#echo "echo \"$foreach-$foreachvalue$addwhere\" | sed -r -e 's/and/-/g' | sed -r -e 's/ //g' | tr -t '=' '-'"
    make_2d_plot_witheach "$x" "$y" "$yerror" "$witheach" "$title" "$xtitle" "$ytitle" "$suffix" "$plotmode" "$haslegend" "and $foreach=$foreachvalue $addwhere" "$dashed" "$witheach_alias"
  done

  echo "done."
}

#########################################################################################################################
# make_2d_plot_witheach <X> <Y> <error> <witheach> <title> <X title> <Y title> <suffix> <opt. plotmode> <enable legend 0/1> <additionnal where> <dashed> <witheach_alias>
function make_2d_plot_witheach
{
  local x=$1; shift; 
  local y=$1; shift;
  local yerror=$1; shift;
  local witheach=$1; shift;
  local title=$1; shift;
  local xtitle=$1; shift;
  local ytitle=$1; shift;
  local suffix=$1; shift;
  local plotmode=$1; shift;
  local haslegend=$1; shift;
  local addwhere=$1; shift;
  local dashed=$1; shift;
  local witheach_alias=$1; shift;
  
  local lt="lt 1"
	if [ "$plotmode" = "" ]; then
		plotmode="lines"
	fi

  local key=""
  if [ "$haslegend" = "0" ]; then
    key=" set key off"
  fi

	data_filename=$outdir/`echo "$prefix-2d-$x-$y-$witheach-$suffix" | tr -t '/' '_'`	

  if [ -e $data_filename.$ext ]; then
    echo -n "already done ! "
    return 
  fi

  linestyle=1

	 >> $outdir/$prefix.plot
  echo -n "  
  #########################################
  set title '$title'
  set output '$data_filename.$ext'
  set xlabel '$xtitle'
  set ylabel '$ytitle'
  $key
  plot '$outdir/dummy.txt' w dots notitle" >> $outdir/$prefix.plot


  for witheachvalue in `sqlite3 $dbfile "select distinct $witheach from $tablename order by $witheach"`
  do
  	#echo "sqlite3 $dbfile \"select $x,avg($y),avg($yerror) from $tablename where $witheach=$witheachvalue $addwhere group by $x order by $x\""
  	
  	data_filename=$outdir/`echo "$prefix-2d-$x-$y-$witheach-$witheachvalue-$suffix" | tr -t '/' '_'`
  	
  	if [ "$yerror" = "noerrorbar" ]; then
  	  #echo "sqlite3 $dbfile \"select $x,avg($y) from $tablename where $witheach=$witheachvalue $addwhere group by $x order by $x\""
	    sqlite3 $dbfile "select $x,avg($y) from $tablename where $witheach=$witheachvalue $addwhere group by $x order by $x" | tr -t '|' ' ' > $data_filename.txt  		
	  else
	    sqlite3 $dbfile "select $x,avg($y),avg($yerror) from $tablename where $witheach=$witheachvalue $addwhere group by $x order by $x" | tr -t '|' ' ' > $data_filename.txt
	  fi
    
    
    if [ "$dashed" = "1" ]; then
    	lt="lt $linestyle"
    fi
    
    if [ "$yerror" = "noerrorbar" ]; then
		  echo -n ", \\
		    '$data_filename.txt' using 1:2 title '$witheach_alias=$witheachvalue' w $plotmode ls $linestyle $lt" >> $outdir/$prefix.plot
		else
		  echo -n ", \\
        '$data_filename.txt' using 1:2 title '$witheach_alias=$witheachvalue' w $plotmode ls $linestyle $lt, \\
		    '$data_filename.txt' using 1:2:3 notitle w yerrorbars ls 1" >> $outdir/$prefix.plot
		fi		 

    linestyle=$(($linestyle+1))
  done
  
}


#########################################################################################################################
#########################################################################################################################
#########################################################################################################################
# make_multiple_3d_plots <X> <Y> <Z> <foreeach> <opt. plotmode> <enable legend 0/1> <optional additionnal where> <opt thirddim> <opt z rotation> <X title> <Y title> <Z title> <Graph title> <Foreach alias> <Color scale 0/1>
function make_multiple_3d_plots
{
  echo -n "Making make_multiple_3d_plots $* ... "
  
  local x=$1; shift;
  local y=$1; shift;
  local z=$1; shift;
  local foreach=$1; shift;
  local plotmode=$1; shift;
  local haslegend=$1; shift;
 	local addwhere=$1; shift;
 	local thirddim=$1; shift;
 	local zrot=$1; shift;
  local xtitle=$1; shift;
  local ytitle=$1; shift;
  local ztitle=$1; shift;
  local graphtitle=$1; shift;
  local foreach_alias=$1; shift;
  local colorscale=$1; shift;
  
  
  if [ "$colorscale" = "" ]; then
    colorscale="0"
  fi
  
  if [ "$xtitle" = "" ]; then
    xtitle=$x
  fi

  if [ "$ytitle" = "" ]; then
    ytitle=$y
  fi

  if [ "$ztitle" = "" ]; then
    ztitle=$z
  fi
  
  if [ "$foreach_alias" = "" ]; then
    foreach_alias=$foreach
  fi

  for foreachvalue in `sqlite3 $dbfile "select distinct $foreach from $tablename order by $foreach"`
  do
  
    if [ "$graphtitle" = "" ]; then
      title="$ztitle / $xtitle for different $ytitle | $foreach_alias=$foreachvalue $addwhere"
    else
      title=$graphtitle
    fi
    
  	local suffix=`echo "$foreach-$foreachvalue$addwhere" | sed -r -e 's/and|or/-/g' | sed -r -e 's/[()]//g' | sed -r -e 's/ //g' | tr -t '=' '-' | tr -s '-'`
  	#echo "echo \"$foreach-$foreachvalue$addwhere\" | sed -r -e 's/and/-/g' | sed -r -e 's/ //g' | tr -t '=' '-'"
    #echo "make_3d_plot_witheach \"$x\" \"$y\" \"$z\" \"$x/$y for different $z | $foreach=$foreachvalue $addwhere\" \"$x\" \"$y\" \"$z\" \"$suffix\" \"and $foreach=$foreachvalue $addwhere\""
    make_3d_plot_witheach "$x" "$y" "$z" "$title" "$xtitle" "$ytitle" "$ztitle" "$suffix" "$plotmode" "$haslegend" "and $foreach=$foreachvalue $addwhere" "$thirddim" "$zrot" "$colorscale"
  done

  echo "done."
}

#########################################################################################################################
# make_3d_plot_witheach <X> <Y> <z> <title> <X title> <Y title> <suffix> <opt. plotmode> <enable legend 0/1> <additionnal where> <opt thirddim> <opt z rotation> <Colorscale 0/1>
function make_3d_plot_witheach
{
  local x=$1; shift; 
  local y=$1; shift;
  local z=$1; shift;
  local title=$1; shift;
  local xtitle=$1; shift;
  local ytitle=$1; shift;
  local ztitle=$1; shift;
  local suffix=$1; shift;
  local plotmode=$1; shift;
  local haslegend=$1; shift;
  local addwhere=$1; shift;
  local thirddim=$1; shift;
  local zrot=$1; shift;
  local colorscale=$1; shift;

  local lt="lt 1"
  local setview=""
  
  setcolorscalemode="  set hidden3d offset 0"
  if [ "$colorscale" = "1" ]; then
    setcolorscalemode=""
    plotmode="pm3d"
  fi
  
  if [ "$plotmode" = "" ]; then
		plotmode="lines"
	fi
  
  if [ "$thirddim" = "" ]; then
  	thirddim="3"
  fi
  
  if [ "$zrot" = "" ]; then
   	rotation=""
  else
    zrot=`expr $(($zrot+30)) % 360`
  	setview="set view 60,$zrot"
  fi  

	data_filename=$outdir/`echo "$prefix-3d-$x-$y-$z-$suffix" | tr -t '/' '_'`	
	
  if [ -e "$data_filename.$ext" ]; then
    echo -n "already done ! "
    return 
  fi
	
  local key=""
  if [ "$haslegend" = "0" ]; then
    key=" set key off"
  fi

	echo_plot_header >> $outdir/$prefix.plot
	if [ "$colorscale" = "1" ]; then
    echo -n "
    #########################################
    set title '$title'
    set output '$data_filename.$ext'
    set xlabel '$xtitle'
    set ylabel '$ytitle'

    $key
    unset grid    
    set pm3d interpolate 3,3 map
    
    splot '$outdir/dummy.txt' w dots notitle" >> $outdir/$prefix.plot
	else # no colorscale
    echo -n "
    #########################################
    set title '$title'
    set output '$data_filename.$ext'
    set xlabel '$xtitle'
    set ylabel '$ytitle'
    set zlabel '$ztitle' rotate by 90
  
    set hidden3d offset 0  
    set grid xtics ytics ztics  
    set ticslevel 0
    
    $key
    $setview
    
    splot '$outdir/dummy.txt' w dots notitle" >> $outdir/$prefix.plot
  fi
	

	rm -f $data_filename.txt

	#echo "sqlite3 $dbfile \"select distinct $z from $tablename order by $z\""

  for yvalue in `sqlite3 $dbfile "select distinct $y from $tablename order by $y"`
  do
  		#echo "sqlite3 $dbfile \"select $x,avg($y),avg($z) from $tablename where $y=$yvalue $addwhere group by $x order by $x\""
  		
    sqlite3 $dbfile "select $x,avg($y),avg($z) from $tablename where $y=$yvalue $addwhere group by $x order by $x" | tr -t '|' ' ' >> $data_filename.txt
    echo "" >> $data_filename.txt
    
  done
  
  echo -n ", \\
      '$data_filename.txt' using 1:2:$thirddim notitle w $plotmode ls 0 $lt" >> $outdir/$prefix.plot
	if [ "$colorscale" = "1" ]; then
    echo -n ", \\
        '$data_filename.txt' using 1:2:$thirddim notitle w lines ls 3 $lt" >> $outdir/$prefix.plot
  fi
	
}



#########################################################################################################################
#########################################################################################################################
#########################################################################################################################
# make_multiple_2d_fit_plots <function of x> <coefs> <X> <Y> <error> <witheach> <foreeach> <opt plotmode> <enable legend 0/1> <enable fit legend 0/1> <optional additionnal where> <dashed> <X title> <Y title> <Graph title> <witheach alias> <foreach alias>
# /!\ use single letters as coefs names
function make_multiple_2d_fit_plots
{
  echo -n "Making make_multiple_2d_fit_plots $* ... "

  local func=$1; shift;
  local coefs=$1; shift;
  local x=$1; shift;
  local y=$1; shift;
  local yerror=$1; shift;
  local witheach=$1; shift;
  local foreach=$1; shift;
  local plotmode=$1; shift;
  local haslegend=$1; shift;
  local hasfitlegend=$1; shift;
 	local addwhere=$1; shift;
	local dashed=$1; shift;
  local xtitle=$1; shift;
  local ytitle=$1; shift;
  local graphtitle=$1; shift;
  local witheach_alias=$1; shift;
  local foreach_alias=$1; shift;
  
  if [ "$xtitle" = "" ]; then
    xtitle=$x
  fi

  if [ "$ytitle" = "" ]; then
    ytitle=$y
  fi

  if [ "$witheach_alias" = "" ]; then
    witheach_alias=$witheach
  fi

  if [ "$foreach_alias" = "" ]; then
    foreach_alias=$foreach
  fi

  for foreachvalue in `sqlite3 $dbfile "select distinct $foreach from $tablename order by $foreach"`
  do
  
    if [ "$graphtitle" = "" ]; then
      title="$ytitle / $xtitle for different $witheach_alias | $foreach_alias=$foreachvalue $addwhere"
    else
      title=$graphtitle
    fi
    
  	local suffix=`echo "$foreach-$foreachvalue$addwhere" | sed -r -e 's/and|or/-/g' | sed -r -e 's/[()]//g' | sed -r -e 's/ //g' | tr -t '=' '-' | tr -s '-'`
  	#echo "echo \"$foreach-$foreachvalue$addwhere\" | sed -r -e 's/and/-/g' | sed -r -e 's/ //g' | tr -t '=' '-'"
    make_2d_fit_plot_witheach "$func" "$coefs" "$x" "$y" "$yerror" "$witheach" "$title" "$xtitle" "$ytitle" "$suffix" "$plotmode" "$haslegend" "$hasfitlegend" "and $foreach=$foreachvalue $addwhere" "$dashed" "$witheach_alias"
    #echo "make_2d_fit_plot_witheach \"$func\" \"$coefs\" \"$x\" \"$y\" \"$yerror\" \"$witheach\" \"$x/$y for different $witheach | $foreach=$foreachvalue $addwhere\" \"$x\" \"$y\" \"$suffix\" \"$plotmode\" \"and $foreach=$foreachvalue $addwhere\""
  done

  echo "done."
}

#########################################################################################################################
# make_2d_fit_plot_witheach <function of x> <coefs> <X> <Y> <error> <witheach> <title> <X title> <Y title> <suffix> <opt. plotmode> <enable legend 0/1> <enable fit legend 0/1> <additionnal where> <dashed> <witheach alias>
function make_2d_fit_plot_witheach
{
  local func=$1; shift;
  local coefs=$1; shift;
  local x=$1; shift; 
  local y=$1; shift;
  local yerror=$1; shift;
  local witheach=$1; shift;
  local title=$1; shift;
  local xtitle=$1; shift;
  local ytitle=$1; shift;
  local suffix=$1; shift;
  local plotmode=$1; shift;
  local haslegend=$1; shift;
  local hasfitlegend=$1; shift;
  local addwhere=$1; shift;
  local dashed=$1; shift;
  local witheach_alias=$1; shift;

  local lt="lt 1"
  local ncoefs
  local nfunc
  local tmpfunc
  
	if [ "$plotmode" = "" ]; then
		plotmode="lines"
	fi
	
  local key=""
  if [ "$haslegend" = "0" ]; then
    key=" set key off"
  fi

	data_filename=$outdir/`echo "$prefix-2d-fit-$x-$y-$witheach-$suffix" | tr -t '/' '_'`	

  if [ -e $data_filename.$ext ]; then
    echo -n "already done ! "
    return 
  fi

  linestyle=1

  local varchars=`echo "$coefs" | sed -e "s/ //g"`
  local funcforprintf=`echo "$func" | sed -r -e "s/[$varchars]/\%.2f/g" | sed -r -e "s/\*\*/\^/g"`
  local vars
  local sprintfcmd
  
	echo_plot_header >> $outdir/$prefix.plot
  echo "  
  #########################################
  set title '$title'
  set output '$data_filename.$ext'
  set xlabel '$xtitle'
  set ylabel '$ytitle'
  $key
  " >> $outdir/$prefix.plot


  for witheachvalue in `sqlite3 $dbfile "select distinct $witheach from $tablename order by $witheach"`
  do
    data_filename=$outdir/`echo "$prefix-2d-fit-$x-$y-$witheach-$witheachvalue-$suffix" | tr -t '/' '_'`
    
    ncoefs=`echo "$coefs" | sed -r -e "s/ /$linestyle, /g" | sed -r -e "s/$/$linestyle/g"`
    
    tmpfunc=$func
    for coef in $coefs; do
      nfunc=`echo "$tmpfunc" | sed -r -e "s/$coef/$coef$linestyle/g"`
      tmpfunc=$nfunc
    done
    
    #prepare the sprintf command for the legend with the functions with coefs 
    vars=`echo "$coefs" | sed -r -e "s/$/$linestyle/g" | sed -r -e "s/ /$linestyle, /g"`
    sprintfcmd="sprintf(\"$funcforprintf\",$vars)"
    
    echo "
    f$linestyle(x) = $nfunc
    undefine $ncoefs
    print \"\n============== Fitting f$linestyle(x) with $witheach=$witheachvalue $addwhere ==============\"
    fit f$linestyle(x) '$data_filename.txt' via  $ncoefs
    legend$linestyle=$sprintfcmd
    print \"f(x) = \",legend$linestyle
    print \"\n============== END Fitting f$linestyle(x) with $witheach=$witheachvalue $addwhere ==============\"
    " >> $outdir/$prefix.plot
    
    linestyle=$(($linestyle+1))
  done
  
  
  echo -n "
  
  plot '$outdir/dummy.txt' w dots notitle" >> $outdir/$prefix.plot
  
  linestyle=1
  
  for witheachvalue in `sqlite3 $dbfile "select distinct $witheach from $tablename order by $witheach"`
  do
  	#echo "sqlite3 $dbfile \"select $x,avg($y),avg($yerror) from $tablename where $witheach=$witheachvalue $addwhere group by $x order by $x\""
  	
  	data_filename=$outdir/`echo "$prefix-2d-fit-$x-$y-$witheach-$witheachvalue-$suffix" | tr -t '/' '_'`
  	
  	if [ "$yerror" = "noerrorbar" ]; then
  	  #echo "sqlite3 $dbfile \"select $x,avg($y) from $tablename where $witheach=$witheachvalue $addwhere group by $x order by $x\""
	    sqlite3 $dbfile "select $x,avg($y) from $tablename where $witheach=$witheachvalue $addwhere group by $x order by $x" | tr -t '|' ' ' > $data_filename.txt  		
	  else
	    sqlite3 $dbfile "select $x,avg($y),avg($yerror) from $tablename where $witheach=$witheachvalue $addwhere group by $x order by $x" | tr -t '|' ' ' > $data_filename.txt
	  fi
    
    tmpfunc=$func
    for coef in $coefs; do
      nfunc=`echo "$tmpfunc" | sed -r -e "s/$coef/$coef$linestyle/g"`
      tmpfunc=$nfunc
    done
    
    if [ "$dashed" = "1" ]; then
	    lt="lt $linestyle"
    fi
    
    # plot f(x) ...
    if [ "$showfitlegent" = "0" ]; then
      echo -n ", \\
        f$linestyle(x) title legend$linestyle w $plotmode ls $linestyle $lt" >> $outdir/$prefix.plot
    else
      echo -n ", \\
        f$linestyle(x) notitle w $plotmode ls $linestyle $lt" >> $outdir/$prefix.plot
    fi
      
    # plot XP data
    if [ "$yerror" = "noerrorbar" ]; then
		  echo -n ", \\
        '$data_filename.txt' using 1:2 title '$witheach_alias=$witheachvalue' w points ls $linestyle" >> $outdir/$prefix.plot
#		    f$linestyle(x) title '$nfunc' w $plotmode ls $linestyle, \\
		else
		  echo -n ", \\
        '$data_filename.txt' using 1:2 title 'experimental data for $witheach_alias=$witheachvalue' w points ls $linestyle, \\
        '$data_filename.txt' using 1:2:3 notitle w yerrorbars ls 1" >> $outdir/$prefix.plot
#		    f$linestyle(x) title 'function $nfunc' w $plotmode ls $linestyle, \\
		fi		 
    
    linestyle=$(($linestyle+1))
  done
  
}

#########################################################################################################################
#########################################################################################################################
#########################################################################################################################
# make_multiple_3d_fit_plots  <x/z function to fit> <coefficients> <y value to restrict the fit> 
#                             <y/z function to fit> <coefficients> <x value to restrict the fit> 
#                             <X> <Y> <Z> <foreeach> <opt. plotmode> <enable legend 0/1> <optional additionnal where> <opt thirddim> <opt z rotation> <X title> <Y title> <Z title> <Graph title> <foreach alias>
function make_multiple_3d_fit_plots
{
  echo -n "Making make_multiple_3d_fit_plots $* ... "
  
  local xzfunction=$1; shift;
  local xzcoefs=$1; shift;
  local yvalue=$1; shift;
  local yzfunction=$1; shift;
  local yzcoefs=$1; shift;
  local xvalue=$1; shift;
  local x=$1; shift;
  local y=$1; shift;
  local z=$1; shift;
  local foreach=$1; shift;
  local plotmode=$1; shift;
  local haslegend=$1; shift;
 	local addwhere=$1; shift;
 	local thirddim=$1; shift;
 	local zrot=$1; shift;
  local xtitle=$1; shift;
  local ytitle=$1; shift;
  local ztitle=$1; shift;
  local graphtitle=$1; shift;
  local foreach_alias=$1; shift;
  
  if [ "$xtitle" = "" ]; then
    xtitle=$x
  fi

  if [ "$ytitle" = "" ]; then
    ytitle=$y
  fi

  if [ "$ztitle" = "" ]; then
    ztitle=$z
  fi

  if [ "$foreach_alias" = "" ]; then
    foreach_alias=$foreach
  fi

  for foreachvalue in `sqlite3 $dbfile "select distinct $foreach from $tablename order by $foreach"`
  do
  
    if [ "$graphtitle" = "" ]; then
      title="$ztitle / $xtitle for different $ytitle | $foreach_alias=$foreachvalue $addwhere"
    else
      title=$graphtitle
    fi
  
  	local suffix=`echo "$foreach-$foreachvalue$addwhere" | sed -r -e 's/and|or/-/g' | sed -r -e 's/[()]//g' | sed -r -e 's/ //g' | tr -t '=' '-' | tr -s '-'`
  	#echo "echo \"$foreach-$foreachvalue$addwhere\" | sed -r -e 's/and/-/g' | sed -r -e 's/ //g' | tr -t '=' '-'"
    #echo "make_3d_plot_witheach \"$x\" \"$y\" \"$z\" \"$x/$y for different $z | $foreach=$foreachvalue $addwhere\" \"$x\" \"$y\" \"$z\" \"$suffix\" \"and $foreach=$foreachvalue $addwhere\""
    make_3d_fit_plot_witheach "$xzfunction" "$xzcoefs" "$yvalue" "$yzfunction" "$yzcoefs" "$xvalue" "$x" "$y" "$z" "$title" "$xtitle" "$ytitle" "$ztitle" "$suffix" "$plotmode" "$haslegend" "and $foreach=$foreachvalue $addwhere" "$thirddim" "$zrot" 
  done

  echo "done."
}

#########################################################################################################################
# make_3d_fit_plot_witheach <X> <Y> <z> <title> <X title> <Y title> <suffix> <opt. plotmode> <enable legend 0/1> <additionnal where> <opt thirddim> <opt z rotation> <dashed>
function make_3d_fit_plot_witheach
{
  local xzfunction=$1; shift;
  local xzcoefs=$1; shift;
  local yfitvalue=$1; shift;
  local yzfunction=$1; shift;
  local yzcoefs=$1; shift;
  local xfitvalue=$1; shift;
  local x=$1; shift; 
  local y=$1; shift;
  local z=$1; shift;
  local title=$1; shift;
  local xtitle=$1; shift;
  local ytitle=$1; shift;
  local ztitle=$1; shift;
  local suffix=$1; shift;
  local plotmode=$1; shift;
  local haslegend=$1; shift;
  local addwhere=$1; shift;
  local thirddim=$1; shift;
  local zrot=$1; shift;


  local lt="lt 1"
  local setview=""
  
  
  if [ "$plotmode" = "" ]; then
		plotmode="lines"
	fi
  
  if [ "$thirddim" = "" ]; then
  	thirddim="3"
  fi
  
  if [ "$zrot" = "" ]; then
   	rotation=""
  else
    zrot=`expr $(($zrot+30)) % 360`
  	setview="set view 60,$zrot"
  fi  
	
  local key=""
  if [ "$haslegend" = "0" ]; then
    key=" set key off"
  fi

	local data_filename=$outdir/`echo "$prefix-3d-fit-$x-$y-$z-$suffix" | tr -t '/' '_'`	
	
  if [ -e "$data_filename.$ext" ]; then
    echo -n "already done ! "
    return 
  fi
	
	echo_plot_header >> $outdir/$prefix.plot
	
  echo -n "  
  #########################################
  set title '$title'
  set output '$data_filename.$ext'
  set xlabel '$xtitle'
  set ylabel '$ytitle'
  set zlabel '$ztitle' rotate by 90
  $key
  
  set grid xtics ytics ztics
  set hidden3d offset 0
  set ticslevel 0
  set isosamples 100,100
  
  
  $setview" >> $outdir/$prefix.plot  
	
  # prepare aproximation function
  # prepare data files to fit in x/z and y/z
  local data_fit_xz_filename=$outdir/`echo "$prefix-2d-fitfor3d-$x-$z-$y-$yfitvalue-$suffix" | tr -t '/' '_'`
  local data_fit_yz_filename=$outdir/`echo "$prefix-2d-fitfor3d-$y-$z-$x-$xfitvalue-$suffix" | tr -t '/' '_'`
  sqlite3 $dbfile "select $x,avg($z) from $tablename where $y=$yfitvalue $addwhere group by $x order by $x" | tr -t '|' ' ' > $data_fit_xz_filename.txt
  sqlite3 $dbfile "select $y,avg($z) from $tablename where $x=$xfitvalue $addwhere group by $y order by $y" | tr -t '|' ' ' > $data_fit_yz_filename.txt	  
  
  # prepare x/z function to fit
  local nxzcoefs=`echo "$xzcoefs" | sed -r -e "s/ /0, /g" | sed -r -e "s/$/0/g"`  
  local tmpxzfunc=$xzfunction
  for coef in $xzcoefs; do
    nxzfunc=`echo "$tmpxzfunc" | sed -r -e "s/$coef/${coef}0/g"`
    local tmpxzfunc=$nxzfunc
  done
  echo "
  undefine $nxzcoefs
  fx(x) = $nxzfunc
  print \"\n============== 1ST Fitting $nxzfunc(x) with $y=$yfitvalue $addwhere ==============\"
  fit fx(x) '$data_fit_xz_filename.txt' via $nxzcoefs
  print \"\n============== END 1ST Fitting $nxzfunc(x) with $y=$yfitvalue $addwhere ==============\"
  " >> $outdir/$prefix.plot
  
  # prepare y/z function to fit
  local nyzcoefs=`echo "$yzcoefs" | sed -r -e "s/ /1, /g" | sed -r -e "s/$/1/g"`  
  local tmpyzfunc=$yzfunction
  for coef in $yzcoefs; do
    nyzfunc=`echo "$tmpyzfunc" | sed -r -e "s/$coef/${coef}1/g"`
    local tmpyzfunc=$nyzfunc
  done    
  echo "
  undefine $nyzcoefs
  fz(x) = $nyzfunc
  print \"\n============== 2ND Fitting $nyzfunc(x) with $x=$xfitvalue $addwhere ==============\"
  fit fz(x) '$data_fit_yz_filename.txt' via $nyzcoefs
  print \"\n============== END 2ND Fitting $nyzfunc(x) with $x=$xfitvalue $addwhere ==============\"
  " >> $outdir/$prefix.plot
  
  echo "  f(x,y)=fx(x)*(fz(y)/fz($yfitvalue))
" >> $outdir/$prefix.plot  
  
  linestyle=0
  # prepare experimental data
	rm -f $data_filename.txt
	#echo "sqlite3 $dbfile \"select distinct $z from $tablename order by $z\""
  for yvalue in `sqlite3 $dbfile "select distinct $y from $tablename order by $y"`
  do
  		#echo "sqlite3 $dbfile \"select $x,avg($y),avg($z) from $tablename where $y=$yvalue $addwhere group by $x order by $x\""
    sqlite3 $dbfile "select $x,avg($y),avg($z) from $tablename where $y=$yvalue $addwhere group by $x order by $x" | tr -t '|' ' ' >> $data_filename.txt
    echo "" >> $data_filename.txt
  done
  
  echo -n "  splot '$outdir/dummy.txt' w dots notitle" >> $outdir/$prefix.plot  
  echo ", \\
      '$data_filename.txt' using 1:2:$thirddim title 'Experimental data' w $plotmode ls 11 $lt, \\
      f(x,y) title 'Experimental model'  w $plotmode ls 13 $lt" >> $outdir/$prefix.plot
      
  
  echo "
  set title '$title - Difference Error'
  set zlabel 'Difference Error of $ztitle'
  set pm3d at b
  unset hidden3d
  set output '$data_filename-differror.$ext'
  " >> $outdir/$prefix.plot
  echo -n "splot '$outdir/dummy.txt' w dots notitle" >> $outdir/$prefix.plot
  echo ", \\
      '$data_filename.txt' using 1:2:(\$$thirddim - f(\$1,\$2)) notitle w $plotmode ls 11 $lt" >> $outdir/$prefix.plot


  echo "
  set title '$title - % Error'
  set zlabel '% Error of $ztitle'
  set pm3d at b
  unset hidden3d
  set output '$data_filename-percenterror.$ext'
  " >> $outdir/$prefix.plot
  echo -n "splot '$outdir/dummy.txt' w dots notitle" >> $outdir/$prefix.plot
  echo ", \\
      '$data_filename.txt' using 1:2:( ((\$$thirddim - f(\$1,\$2)) / \$$thirddim)*100.0) notitle w $plotmode ls 11 $lt" >> $outdir/$prefix.plot


}

#########################################################################################################################
#########################################################################################################################
#########################################################################################################################
function help
{
  echo '
# make_multiple_2d_plots 
  1  <X> 
  2  <Y> 
  3  <error> 
  4  <witheach>
  5  <foreeach>
  6  <opt plotmode>
     <enable legend 0/1>
  7  <optional additionnal where>
  8  <dashed>
  9  <X title>
  10 <Y title>
  11 <Graph title>
  12 <witeach rename>
  13 <foreach rename>

# make_multiple_3d_plots
  1  <X>
  2  <Y>
  3  <Z>
  4  <foreeach>
  5  <opt. plotmode>
     <enable legend 0/1>
  6  <optional additionnal where>
  7  <opt thirddim>
  8  <opt z rotation>
  9  <X title>
  10 <Y title>
  11 <Z title>
  12 <Graph title>
  13 <foreach rename>
  14 <Color scale 0/1>
  
# make_multiple_2d_fit_plots
  1  <function of x>
  2  <coefs>
  3  <X>
  4  <Y>
  5  <error>
  6  <witheach>
  7  <foreeach>
  8  <opt plotmode>
     <enable legend 0/1>
     <enable fit legend 0/1>
  9  <optional additionnal where>
  10 <dashed>
  11 <X title>
  12 <Y title>
  13 <Graph title>
  14 <witeach rename>
  15 <foreach rename>

# make_multiple_3d_fit_plots
  1  <x/z function to fit>
  2  <coefficients>
  3  <y value to restrict the fit> 
  4  <y/z function to fit>
  5  <coefficients>
  6  <x value to restrict the fit>
  7  <X>
  8  <Y>
  9  <Z>
  10 <foreeach>
  11 <opt. plotmode>
     <enable legend 0/1>
  12 <optional additionnal where>
  13 <opt thirddim>
  14 <opt z rotation>
  15 <X title>
  16 <Y title>
  17 <Z title>
  18 <Graph title>
  19 <foreach rename>

# fits : /!\ use single letters as coefs variables
# z rotation is between 0 and 360, and gives clockwise rotation to the default rotation
# additional where clauses start with a conjunction : "or ...", "and ..."
# dashed is 1 if you want different line types or nothing otherwise

'

  exit 1
}



#########################################################################################################################
#########################################################################################################################
#########################################################################################################################
rm $prefix-* 2>/dev/null

#rm $outdir/*.$ext $outdir/*.pdf $outdir/$prefix-*.txt 2>/dev/null
rm $outdir/$prefix-db.tmp $outdir/*.plot 2>/dev/null
rm $dbfile 2>/dev/null

cat $outdir/$prefix.txt | grep -v -E "^#" | tr -s ' ' | tr -t ' ' '|' > $outdir/$prefix-db.tmp

sqlite3 $dbfile "create table $tablename (NbNeurons REAL, Connectivity REAL, k REAL, p REAL, delay REAL, deltadelay REAL, NbPGs REAL, StdDev_NbPGs REAL, NbCyclics REAL, StdDev_Cyclics REAL, SearchTime REAL, StdDev_Time REAL, TimeSpan REAL, StdDev_TimeSpan REAL, Size REAL, StdDev_Size REAL, Population REAL, StdDev_Population REAL, Frequency REAL, StdDev_Frequency REAL, EntropyTimeSpan REAL, EntropySize REAL, EntropyPopulation REAL, EntropyFrequency REAL);"

sqlite3 $dbfile ".import $outdir/$prefix-db.tmp $tablename"
rm $outdir/$prefix-db.tmp


echo "# SMALLWORLD" > $outdir/$prefix.plot
echo_plot_header >> $outdir/$prefix.plot

linestyle=0

## -----------------------------------------------------------
# \subsubsection{Probabilité de recablage $p$}

#    \paragraph{Nombre de PGs}
#make_multiple_2d_plots "NbNeurons" "NbPGs" "StdDev_NbPGs" "p" "k" "lines"
make_multiple_2d_fit_plots "a*x**5 + b*x**4 + c*x**3 + d*x**2 + e*x + f" "a b c d e f" "p" "NbPGs" "StdDev_NbPGs" "NbNeurons" "k" "lines" "" "0" "" "1" "p" "Number of PG" "" "n" "k"
#make_multiple_2d_fit_plots "a*x**4 + b*x**3 + c*x**2 + d*x + e" "a b c d e" "p" "NbPGs" "noerrorbar" "NbNeurons" "k" "lines" ""
#make_multiple_2d_fit_plots "a*x**3 + b*x**2 + c*x + d" "a b c d" "p" "NbPGs" "noerrorbar" "NbNeurons" "k" "lines" ""
make_multiple_2d_fit_plots "a*x + b" "a b" "NbNeurons" "NbPGs" "StdDev_NbPGs" "p" "k" "lines" "" "0" "" "1" "Number of neurons" "Number of PG" "" "p" "k"
make_multiple_3d_fit_plots "a*x**5 + b*x**4 + c*x**3 + d*x**2 + e*x + f" "a b c d e f" "600" "a*x + b" "a b" "0.2" "p" "NbNeurons" "NbPGs" "k" "lines" "" "" "" "" "p" "Number of neurons" "Number of PG" "" "k"


#make_multiple_2d_plots "p" "NbPGs" "StdDev_NbPGs" "NbNeurons" "k" "lines"
make_multiple_3d_plots "p" "NbNeurons" "NbPGs" "k" "lines" "" "" "" "" "p" "Number of neurons" "Number of PG" "" "k"
make_multiple_3d_plots "p" "NbNeurons" "NbPGs" "k" "lines" "" "and delay=50" "" "" "p" "Number of neurons" "Number of PG" "" "k"

make_multiple_3d_plots "k" "NbNeurons" "NbPGs" "p" "lines" "" "" "" "290" "k" "Number of neurons" "Number of PG" "" "p" ##############
#make_multiple_3d_plots "NbNeurons" "k" "NbPGs" "p" "lines"
#make_multiple_2d_plots "NbNeurons" "NbPGs" "StdDev_NbPGs" "p" "k" "lines"

#    \paragraph{Proportion de PGs tronqués (cycliques)}
#make_multiple_2d_plots "NbNeurons" "NbCyclics/NbPGs" "noerrorbar" "p" "k" "lines"
#make_multiple_2d_plots "p" "NbCyclics/NbPGs" "noerrorbar" "NbNeurons" "k" "lines"
make_multiple_3d_plots "p" "NbNeurons" "NbCyclics/NbPGs" "k" "lines" "" "" "" "" "p" "Number of neurons" "Ratio of cyclic PG" "" "k"
make_multiple_3d_plots "k" "NbNeurons" "NbCyclics/NbPGs" "p" "lines" "" "" "" "" "k" "Number of neurons" "Ratio of cyclic PG" "" "p"

#    \paragraph{Taille des PGs en nombre de spikes}
#make_multiple_2d_plots "NbNeurons" "Size" "StdDev_Size" "p" "k" "lines"
#make_multiple_2d_plots "p" "Size" "StdDev_Size" "NbNeurons" "k" "lines"
make_multiple_3d_plots "p" "NbNeurons" "Size" "k" "lines" "" "" "" "20" "p" "Number of neurons" "Number of spikes" "" "k"   ##############
make_multiple_3d_plots "k" "NbNeurons" "Size" "p" "lines" "" "" "" "" "k" "Number of neurons" "Number of spikes" "" "p"

#    \paragraph{Longueur des PGs en ms}
#make_multiple_2d_plots "NbNeurons" "TimeSpan" "StdDev_TimeSpan" "p" "k" "lines"
#make_multiple_2d_plots "p" "TimeSpan" "StdDev_TimeSpan" "NbNeurons" "k" "lines"
make_multiple_3d_plots "p" "NbNeurons" "TimeSpan" "k" "lines" "" "" "" "20" "p" "Number of neurons" "Timespan" "" "k"   ##############
make_multiple_3d_plots "k" "NbNeurons" "TimeSpan" "p" "lines" "" "" "" "" "k" "Number of neurons" "Timespan" "" "p"

#    \paragraph{Population recrutée par des PGs}
#make_multiple_2d_plots "NbNeurons" "Population" "StdDev_Population" "p" "k" "lines"
#make_multiple_2d_plots "p" "Population" "StdDev_Population" "NbNeurons" "k" "lines"
make_multiple_3d_plots "p" "NbNeurons" "Population" "k" "lines" "" "" "" "" "p" "Number of neurons" "Population" "" "k"

#    \paragraph{Proportion de la population recrutée par des PGs}
#make_multiple_2d_plots "NbNeurons" "Population/NbNeurons" "StdDev_Population/NbNeurons" "p" "k" "lines"
#make_multiple_2d_plots "p" "Population/NbNeurons" "StdDev_Population/NbNeurons" "NbNeurons" "k" "lines"
make_multiple_3d_plots "p" "NbNeurons" "Population/NbNeurons" "k" "lines" "" "" "" "" "p" "Number of neurons" "Population as ratio of network" "" "k"

#    \paragraph{Fréquence moyenne en nombre de spikes par s}
make_multiple_3d_plots "p" "NbNeurons" "Frequency" "k" "lines" "" "" "" "0" "p" "Number of neurons" "Frequency" "" "k"
make_multiple_3d_plots "k" "NbNeurons" "Frequency" "p" "lines" "" "" "" "0" "k" "Number of neurons" "Frequency" "" "p"


## -----------------------------------------------------------
#	\subsubsection{Connectivité $c$}

#    \paragraph{Nombre de PGs}
#make_multiple_2d_plots "Connectivity" "NbPGs" "StdDev_NbPGs" "p" "NbNeurons" "lines"
#make_multiple_2d_plots "Connectivity" "NbPGs" "StdDev_NbPGs" "NbNeurons" "p" "lines"
make_multiple_3d_plots "Connectivity" "NbNeurons" "NbPGs" "p" "lines" "" "" "" "310" "Connectivity" "Number of neurons" "" "Number of PG" "p"
make_multiple_3d_plots "NbNeurons" "Connectivity" "NbPGs" "p" "lines" "" "" "" "" "Number of neurons" "Connectivity" "" "Number of PG" "p"
make_multiple_3d_plots "Connectivity" "p" "NbPGs" "NbNeurons" "lines" "" "" "" "290" "Connectivity" "p" "Number of PG" "" "Number of neurons"  ##############
#make_multiple_3d_plots "p" "Connectivity" "NbPGs" "NbNeurons" "lines"
#make_multiple_3d_plots "Connectivity" "p" "NbPGs" "NbNeurons" "lines" "and k<>8"

#    \paragraph{Proportion de PGs tronqués (cycliques)}
#make_multiple_2d_plots "Connectivity" "NbCyclics/NbPGs" "noerrorbar" "p" "NbNeurons" "lines"
#make_multiple_2d_plots "Connectivity" "NbCyclics/NbPGs" "noerrorbar" "NbNeurons" "p" "lines"
make_multiple_3d_plots "Connectivity" "NbNeurons" "NbCyclics/NbPGs" "p" "lines" "" "" "" "" "Connectivity" "Number of neurons" "Ratio of cyclic PG" "" "p"

#    \paragraph{Taille des PGs en nombre de spikes}
#make_multiple_2d_plots "Connectivity" "Size" "StdDev_Size" "p" "NbNeurons" "lines"
#make_multiple_2d_plots "Connectivity" "Size" "StdDev_Size" "NbNeurons" "p" "lines"
make_multiple_3d_plots "Connectivity" "NbNeurons" "Size" "p" "lines" "" "" "" "" "Connectivity" "Number of neurons" "Number of spikes" "" "p"

#    \paragraph{Longueur des PGs en ms}
#make_multiple_2d_plots "Connectivity" "TimeSpan" "StdDev_TimeSpan" "p" "NbNeurons" "lines"
#make_multiple_2d_plots "Connectivity" "TimeSpan" "StdDev_TimeSpan" "NbNeurons" "p" "lines"
make_multiple_3d_plots "Connectivity" "NbNeurons" "TimeSpan" "p" "lines" "" "" "" "" "Connectivity" "Number of neurons" "Timespan" "" "p"

#    \paragraph{Population recrutée par des PGs}
#make_multiple_2d_plots "Connectivity" "Population" "StdDev_Population" "p" "NbNeurons" "lines"
#make_multiple_2d_plots "Connectivity" "Population" "StdDev_Population" "NbNeurons" "p" "lines"
make_multiple_3d_plots "Connectivity" "NbNeurons" "Population" "p" "lines" "" "" "" "" "Connectivity" "Number of neurons" "Population" "" "p"

#    \paragraph{Proportion de la population recrutée par des PGs}
#make_multiple_2d_plots "Connectivity" "Population/NbNeurons" "StdDev_Population/NbNeurons" "p" "NbNeurons" "lines"
#make_multiple_2d_plots "Connectivity" "Population/NbNeurons" "StdDev_Population/NbNeurons" "NbNeurons" "p" "lines"
make_multiple_3d_plots "Connectivity" "NbNeurons" "Population/NbNeurons" "p" "lines" "" "" "" "" "Connectivity" "Number of neurons" "Population as ratio of network" "" "p"

#    \paragraph{Fréquence moyenne en nombre de spikes par s}
make_multiple_3d_plots "Connectivity" "NbNeurons" "Frequency" "p" "lines" "" "" "" "0" "Connectivity" "Number of neurons" "Frequency" "" "p"



## -----------------------------------------------------------
# \subsubsection{Longueur des délais $d$ et écart-type des délais}

#    \paragraph{Nombre de PGs}
make_multiple_3d_plots "delay" "NbNeurons" "NbPGs" "k" "lines" "" "and deltadelay=0" "" "" "d" "Number of neurons" "Number of PG" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "NbPGs" "k" "lines" "" "and deltadelay=5" "" "" "d" "Number of neurons" "Number of PG" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "NbPGs" "k" "lines" "" "and deltadelay=50" "" "" "d" "Number of neurons" "Number of PG" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "NbPGs" "k" "lines" "" "and deltadelay=100" "" "" "d" "Number of neurons" "Number of PG" "" "k"
make_multiple_3d_plots "deltadelay" "delay" "NbPGs" "k" "lines" "" "and NbNeurons=800" "" "" "dtd" "d" "Number of PG" "" "k"
make_multiple_3d_plots "deltadelay" "NbNeurons" "NbPGs" "k" "lines" "" "and delay=10" "" "" "dtd" "Number of neurons" "Number of PG" "" "k"
make_multiple_3d_plots "deltadelay" "NbNeurons" "NbPGs" "k" "lines" "" "and delay=50" "" "" "dtd" "Number of neurons" "Number of PG" "" "k"
make_multiple_3d_plots "deltadelay" "NbNeurons" "NbPGs" "k" "lines" "" "and delay=150" "" "" "dtd" "Number of neurons" "Number of PG" "" "k"

#    \paragraph{Proportion de PGs tronqués (cycliques)}
make_multiple_3d_plots "delay" "NbNeurons" "NbCyclics/NbPGs" "k" "lines" "" "and deltadelay=0" "" "" "d" "Number of neurons" "Ratio of cyclic PG" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "NbCyclics/NbPGs" "k" "lines" "" "and deltadelay=5" "" "" "d" "Number of neurons" "Ratio of cyclic PG" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "NbCyclics/NbPGs" "k" "lines" "" "and deltadelay=10" "" "" "d" "Number of neurons" "Ratio of cyclic PG" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "NbCyclics/NbPGs" "k" "lines" "" "and deltadelay=20" "" "" "d" "Number of neurons" "Ratio of cyclic PG" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "NbCyclics/NbPGs" "k" "lines" "" "and deltadelay=50" "" "" "d" "Number of neurons" "Ratio of cyclic PG" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "NbCyclics/NbPGs" "k" "lines" "" "and deltadelay=100" "" "" "d" "Number of neurons" "Ratio of cyclic PG" "" "k"
make_multiple_3d_plots "deltadelay" "delay" "NbCyclics/NbPGs" "k" "lines" "" "and NbNeurons=800" "" "" "dtd" "d" "Ratio of cyclic PG" "" "k"
make_multiple_3d_plots "deltadelay" "NbNeurons" "NbCyclics/NbPGs" "k" "lines" "" "and delay=10" "" "" "dtd" "Number of neurons" "Ratio of cyclic PG" "" "k"
make_multiple_3d_plots "deltadelay" "NbNeurons" "NbCyclics/NbPGs" "k" "lines" "" "and delay=50" "" "" "dtd" "Number of neurons" "Ratio of cyclic PG" "" "k"
make_multiple_3d_plots "deltadelay" "NbNeurons" "NbCyclics/NbPGs" "k" "lines" "" "and delay=150" "" "" "dtd" "Number of neurons" "Ratio of cyclic PG" "" "k"

#    \paragraph{Taille des PGs en nombre de spikes}
#make_multiple_3d_plots "delay" "NbNeurons" "Size" "k" "lines" "and deltadelay=0 and p=0.8"
make_multiple_3d_plots "delay" "NbNeurons" "Size" "k" "lines" "" "and deltadelay=5 and p=0" "" "" "d" "Number of neurons" "Number of spikes" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Size" "k" "lines" "" "and deltadelay=5 and p=0.2" "" "" "d" "Number of neurons" "Number of spikes" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Size" "k" "lines" "" "and deltadelay=5 and p=0.5" "" "" "d" "Number of neurons" "Number of spikes" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Size" "k" "lines" "" "and deltadelay=5 and p=0.8" "" "" "d" "Number of neurons" "Number of spikes" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Size" "k" "lines" "" "and deltadelay=5 and p=1" "" "" "d" "Number of neurons" "Number of spikes" "" "k"
#make_multiple_3d_plots "delay" "NbNeurons" "Size" "k" "lines" "" "and deltadelay=10 and p=0.8"
#make_multiple_3d_plots "delay" "NbNeurons" "Size" "k" "lines" "" "and deltadelay=50 and p=0.8"
#make_multiple_3d_plots "delay" "NbNeurons" "Size" "k" "lines" "" "and deltadelay=100 and p=0.8"

make_multiple_3d_plots "p" "delay" "Size" "k" "lines" "" "and deltadelay=0 and NbNeurons = 800" "" "" "p" "d" "Number of spikes" "" "k"
make_multiple_3d_plots "p" "delay" "Size" "k" "lines" "" "and deltadelay=5 and NbNeurons = 800" "" "" "p" "d" "Number of spikes" "" "k"
make_multiple_3d_plots "p" "delay" "Size" "k" "lines" "" "and deltadelay=50 and NbNeurons = 800" "" "" "p" "d" "Number of spikes" "" "k"
make_multiple_3d_plots "p" "delay" "Size" "k" "lines" "" "and deltadelay=100 and NbNeurons = 800" "" "" "p" "d" "Number of spikes" "" "k"

make_multiple_3d_plots "deltadelay" "delay" "Size" "k" "lines" "" "and NbNeurons=800" "" "" "dtd" "d" "Number of spikes" "" "k"
make_multiple_3d_plots "deltadelay" "NbNeurons" "Size" "k" "lines" "" "and delay=10" "" ""  "dtd" "Number of neurons" "Number of spikes" "" "k"
make_multiple_3d_plots "deltadelay" "NbNeurons" "Size" "k" "lines" "" "and delay=50" "" ""  "dtd" "Number of neurons" "Number of spikes" "" "k"
make_multiple_3d_plots "deltadelay" "NbNeurons" "Size" "k" "lines" "" "and delay=150" "" ""  "dtd" "Number of neurons" "Number of spikes" "" "k"

#    \paragraph{Longueur des PGs en ms}
#make_multiple_3d_plots "delay" "NbNeurons" "TimeSpan" "k" "lines" "and deltadelay=0 and p=0.8"
make_multiple_3d_plots "delay" "NbNeurons" "TimeSpan" "k" "lines" "" "and deltadelay=5 and p=0" "" "" "d" "Number of neurons" "Timespan" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "TimeSpan" "k" "lines" "" "and deltadelay=5 and p=0.2" "" "" "d" "Number of neurons" "Timespan" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "TimeSpan" "k" "lines" "" "and deltadelay=5 and p=0.5" "" "" "d" "Number of neurons" "Timespan" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "TimeSpan" "k" "lines" "" "and deltadelay=5 and p=0.8" "" "" "d" "Number of neurons" "Timespan" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "TimeSpan" "k" "lines" "" "and deltadelay=5 and p=1" "" "" "d" "Number of neurons" "Timespan" "" "k"
#make_multiple_3d_plots "delay" "NbNeurons" "TimeSpan" "k" "lines" "and deltadelay=10 and p=0.8"
#make_multiple_3d_plots "delay" "NbNeurons" "TimeSpan" "k" "lines" "and deltadelay=50 and p=0.8"
#make_multiple_3d_plots "delay" "NbNeurons" "TimeSpan" "k" "lines" "and deltadelay=100 and p=0.8"

make_multiple_3d_plots "p" "delay" "TimeSpan" "k" "lines" "" "and deltadelay=0 and NbNeurons = 800" "" "" "p" "d" "Timespan" "" "k"
make_multiple_3d_plots "p" "delay" "TimeSpan" "k" "lines" "" "and deltadelay=5 and NbNeurons = 800" "" "" "p" "d" "Timespan" "" "k"
make_multiple_3d_plots "p" "delay" "TimeSpan" "k" "lines" "" "and deltadelay=50 and NbNeurons = 800" "" "" "p" "d" "Timespan" "" "k"
make_multiple_3d_plots "p" "delay" "TimeSpan" "k" "lines" "" "and deltadelay=100 and NbNeurons = 800" "" "" "p" "d" "Timespan" "" "k"

make_multiple_3d_plots "deltadelay" "delay" "TimeSpan" "k" "lines" "" "and NbNeurons=800" "" "" "dtd" "d" "Timespan" "" "k"
make_multiple_3d_plots "deltadelay" "NbNeurons" "TimeSpan" "k" "lines" "" "and delay=10" "" "" "dtd" "Number of neurons" "Timespan" "" "k"
make_multiple_3d_plots "deltadelay" "NbNeurons" "TimeSpan" "k" "lines" "" "and delay=50" "" "" "dtd" "Number of neurons" "Timespan" "" "k"
make_multiple_3d_plots "deltadelay" "NbNeurons" "TimeSpan" "k" "lines" "" "and delay=150" "" "" "dtd" "Number of neurons" "Timespan" "" "k"

#    \paragraph{Population recrutée par des PGs}
make_multiple_3d_plots "delay" "NbNeurons" "Population" "k" "lines" "" "and deltadelay=0" "" "" "d" "Number of neurons" "Population" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population" "k" "lines" "" "and deltadelay=5" "" "" "d" "Number of neurons" "Population" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population" "k" "lines" "" "and deltadelay=20" "" "" "d" "Number of neurons" "Population" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population" "k" "lines" "" "and deltadelay=50" "" "" "d" "Number of neurons" "Population" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population" "k" "lines" "" "and deltadelay=100" "" "" "d" "Number of neurons" "Population" "" "k"

make_multiple_3d_plots "delay" "NbNeurons" "Population" "k" "lines" "" "and deltadelay=0 and p=0" "" "" "d" "Number of neurons" "Population" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population" "k" "lines" "" "and deltadelay=5 and p=0" "" "" "d" "Number of neurons" "Population" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population" "k" "lines" "" "and deltadelay=20 and p=0" "" "" "d" "Number of neurons" "Population" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population" "k" "lines" "" "and deltadelay=50 and p=0" "" "" "d" "Number of neurons" "Population" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population" "k" "lines" "" "and deltadelay=100 and p=0" "" "" "d" "Number of neurons" "Population" "" "k"

make_multiple_3d_plots "delay" "NbNeurons" "Population" "k" "lines" "" "and deltadelay=0 and p=0.2" "" "" "d" "Number of neurons" "Population" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population" "k" "lines" "" "and deltadelay=5 and p=0.2" "" "" "d" "Number of neurons" "Population" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population" "k" "lines" "" "and deltadelay=20 and p=0.2" "" "" "d" "Number of neurons" "Population" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population" "k" "lines" "" "and deltadelay=50 and p=0.2" "" "" "d" "Number of neurons" "Population" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population" "k" "lines" "" "and deltadelay=100 and p=0.2" "" "" "d" "Number of neurons" "Population" "" "k"

make_multiple_3d_plots "delay" "NbNeurons" "Population" "k" "lines" "" "and deltadelay=0 and p=0.5" "" "" "d" "Number of neurons" "Population" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population" "k" "lines" "" "and deltadelay=5 and p=0.5" "" "" "d" "Number of neurons" "Population" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population" "k" "lines" "" "and deltadelay=20 and p=0.5" "" "" "d" "Number of neurons" "Population" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population" "k" "lines" "" "and deltadelay=50 and p=0.5" "" "" "d" "Number of neurons" "Population" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population" "k" "lines" "" "and deltadelay=100 and p=0.5" "" "" "d" "Number of neurons" "Population" "" "k"

make_multiple_3d_plots "delay" "NbNeurons" "Population" "k" "lines" "" "and deltadelay=0 and p=1" "" "" "d" "Number of neurons" "Population" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population" "k" "lines" "" "and deltadelay=5 and p=1" "" "" "d" "Number of neurons" "Population" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population" "k" "lines" "" "and deltadelay=20 and p=1" "" "" "d" "Number of neurons" "Population" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population" "k" "lines" "" "and deltadelay=50 and p=1" "" "" "d" "Number of neurons" "Population" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population" "k" "lines" "" "and deltadelay=100 and p=1" "" "" "d" "Number of neurons" "Population" "" "k"

make_multiple_3d_plots "deltadelay" "delay" "Population" "k" "lines" "" "and NbNeurons=800" "" "" "dtd" "d" "Population" "" "k"
make_multiple_3d_plots "deltadelay" "NbNeurons" "Population" "k" "lines" "" "and delay=10" "" "" "dtd" "Number of neurons" "Population" "" "k"
make_multiple_3d_plots "deltadelay" "NbNeurons" "Population" "k" "lines" "" "and delay=50" "" "" "dtd" "Number of neurons" "Population" "" "k"
make_multiple_3d_plots "deltadelay" "NbNeurons" "Population" "k" "lines" "" "and delay=150" "" "" "dtd" "Number of neurons" "Population" "" "k"

#    \paragraph{Proportion de la population recrutée par des PGs}

make_multiple_3d_plots "delay" "NbNeurons" "Population/NbNeurons" "k" "lines" "" "and deltadelay=0 and p=0" "" "" "d" "Number of neurons" "Population as ratio of network" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population/NbNeurons" "k" "lines" "" "and deltadelay=5 and p=0" "" "" "d" "Number of neurons" "Population as ratio of network" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population/NbNeurons" "k" "lines" "" "and deltadelay=20 and p=0" "" "" "d" "Number of neurons" "Population as ratio of network" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population/NbNeurons" "k" "lines" "" "and deltadelay=50 and p=0" "" "" "d" "Number of neurons" "Population as ratio of network" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population/NbNeurons" "k" "lines" "" "and deltadelay=100 and p=0" "" "" "d" "Number of neurons" "Population as ratio of network" "" "k"

make_multiple_3d_plots "delay" "NbNeurons" "Population/NbNeurons" "k" "lines" "" "and deltadelay=0 and p=0.2" "" "" "d" "Number of neurons" "Population as ratio of network" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population/NbNeurons" "k" "lines" "" "and deltadelay=5 and p=0.2" "" "" "d" "Number of neurons" "Population as ratio of network" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population/NbNeurons" "k" "lines" "" "and deltadelay=20 and p=0.2" "" "" "d" "Number of neurons" "Population as ratio of network" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population/NbNeurons" "k" "lines" "" "and deltadelay=50 and p=0.2" "" "" "d" "Number of neurons" "Population as ratio of network" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population/NbNeurons" "k" "lines" "" "and deltadelay=100 and p=0.2" "" "" "d" "Number of neurons" "Population as ratio of network" "" "k"

make_multiple_3d_plots "delay" "NbNeurons" "Population/NbNeurons" "k" "lines" "" "and deltadelay=0 and p=0.5" "" "" "d" "Number of neurons" "Population as ratio of network" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population/NbNeurons" "k" "lines" "" "and deltadelay=5 and p=0.5" "" "" "d" "Number of neurons" "Population as ratio of network" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population/NbNeurons" "k" "lines" "" "and deltadelay=20 and p=0.5" "" "" "d" "Number of neurons" "Population as ratio of network" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population/NbNeurons" "k" "lines" "" "and deltadelay=50 and p=0.5" "" "" "d" "Number of neurons" "Population as ratio of network" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population/NbNeurons" "k" "lines" "" "and deltadelay=100 and p=0.5" "" "" "d" "Number of neurons" "Population as ratio of network" "" "k"

make_multiple_3d_plots "delay" "NbNeurons" "Population/NbNeurons" "k" "lines" "" "and deltadelay=0 and p=1" "" "" "d" "Number of neurons" "Population as ratio of network" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population/NbNeurons" "k" "lines" "" "and deltadelay=5 and p=1" "" "" "d" "Number of neurons" "Population as ratio of network" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population/NbNeurons" "k" "lines" "" "and deltadelay=20 and p=1" "" "" "d" "Number of neurons" "Population as ratio of network" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population/NbNeurons" "k" "lines" "" "and deltadelay=50 and p=1" "" "" "d" "Number of neurons" "Population as ratio of network" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population/NbNeurons" "k" "lines" "" "and deltadelay=100 and p=1" "" "" "d" "Number of neurons" "Population as ratio of network" "" "k"

make_multiple_3d_plots "delay" "NbNeurons" "Population/NbNeurons" "k" "lines" "" "and deltadelay=0" "" "" "d" "Number of neurons" "Population as ratio of network" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population/NbNeurons" "k" "lines" "" "and deltadelay=5" "" "" "d" "Number of neurons" "Population as ratio of network" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population/NbNeurons" "k" "lines" "" "and deltadelay=20" "" "" "d" "Number of neurons" "Population as ratio of network" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population/NbNeurons" "k" "lines" "" "and deltadelay=50" "" "" "d" "Number of neurons" "Population as ratio of network" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Population/NbNeurons" "k" "lines" "" "and deltadelay=100" "" "" "d" "Number of neurons" "Population as ratio of network" "" "k"

make_multiple_3d_plots "deltadelay" "delay" "Population/NbNeurons" "k" "lines" "" "and NbNeurons=800" "" "" "dtd" "d" "Population as ratio of network" "" "k"
make_multiple_3d_plots "deltadelay" "NbNeurons" "Population/NbNeurons" "k" "lines" "" "and delay=10" "" "" "dtd" "Number of neurons" "Population as ratio of network" "" "k"
make_multiple_3d_plots "deltadelay" "NbNeurons" "Population/NbNeurons" "k" "lines" "" "and delay=50" "" "" "dtd" "Number of neurons" "Population as ratio of network" "" "k"
make_multiple_3d_plots "deltadelay" "NbNeurons" "Population/NbNeurons" "k" "lines" "" "and delay=150" "" "" "dtd" "Number of neurons" "Population as ratio of network" "" "k"

#    \paragraph{Fréquence moyenne en nombre de spikes par s}
#make_multiple_3d_plots "delay" "NbNeurons" "Frequency" "k" "lines" "and deltadelay=0"
make_multiple_3d_plots "delay" "NbNeurons" "Frequency" "k" "lines" "" "and deltadelay=5 and p=0" "" "" "d" "Number of neurons" "Frequency" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Frequency" "k" "lines" "" "and deltadelay=5 and p=0.2" "" "" "d" "Number of neurons" "Frequency" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Frequency" "k" "lines" "" "and deltadelay=5 and p=0.5" "" "" "d" "Number of neurons" "Frequency" "" "k"
make_multiple_3d_plots "delay" "NbNeurons" "Frequency" "k" "lines" "" "and deltadelay=5 and p=0.8" "" "" "d" "Number of neurons" "Frequency" "" "k"
#make_multiple_3d_plots "delay" "NbNeurons" "Frequency" "k" "lines" "and deltadelay=50"
#make_multiple_3d_plots "delay" "NbNeurons" "Frequency" "k" "lines" "and deltadelay=100"

make_multiple_3d_plots "delay" "p" "Frequency" "k" "lines" "" "and deltadelay=5 and NbNeurons=800" "" "0" "d" "p" "Frequency" "" "k"
make_multiple_3d_plots "delay" "p" "Frequency" "k" "lines" "" "and deltadelay=5 and NbNeurons=800" "" "0" "d" "p" "Frequency" "" "k"
make_multiple_3d_plots "delay" "p" "Frequency" "k" "lines" "" "and deltadelay=5 and NbNeurons=800" "" "0" "d" "p" "Frequency" "" "k"
make_multiple_3d_plots "delay" "p" "Frequency" "k" "lines"  """and deltadelay=5 and NbNeurons=800" "" "0" "d" "p" "Frequency" "" "k"

#make_multiple_3d_plots "deltadelay" "delay" "Frequency" "k" "lines" "and NbNeurons=800"

make_multiple_3d_plots "deltadelay" "NbNeurons" "Frequency" "k" "lines" "" "and delay=10" "" "0" "dtd" "Number of neurons" "Frequency" "" "k"
make_multiple_3d_plots "deltadelay" "NbNeurons" "Frequency" "k" "lines" "" "and delay=50" "" "0" "dtd" "Number of neurons" "Frequency" "" "k"
make_multiple_3d_plots "deltadelay" "NbNeurons" "Frequency" "k" "lines" "" "and delay=150" "" "0" "dtd" "Number of neurons" "Frequency" "" "k"
#make_multiple_3d_plots "deltadelay" "NbNeurons" "Frequency" "delay" "lines" "and k=8"
  
#fi



# Calculated entropy for distribs
# make_multiple_2d_plots <X> <Y> <error> <witheach> <foreeach> <opt plotmode> <optional additionnal where>
make_multiple_2d_plots "p" "EntropyTimeSpan" "noerrorbar" "delay" "deltadelay" "lines" "" "and NbNeurons=600 and k=6 and (delay=10 or delay=50 or delay=150)" "" "p" "Entropy of Timespans" "" "dtd" "d" "1"
make_multiple_2d_plots "p" "EntropySize" "noerrorbar" "delay" "deltadelay" "lines" "" "and NbNeurons=600 and k=6 and (delay=10 or delay=50 or delay=150)" "" "p" "Entropy of Numbers of spikes" "" "dtd" "d" "1"
make_multiple_2d_plots "p" "EntropyPopulation" "noerrorbar" "delay" "deltadelay" "lines" "" "and NbNeurons=600 and k=6 and (delay=10 or delay=50 or delay=150)" "" "p" "Entropy of Populations" "" "dtd" "d" "1"
make_multiple_2d_plots "p" "EntropyFrequency" "noerrorbar" "delay" "deltadelay" "lines" "" "and NbNeurons=600 and k=6 and (delay=10 or delay=50 or delay=150)" "" "p" "Entropy of Frequencies" "" "dtd" "d" "1"

make_multiple_2d_plots "p" "EntropyTimeSpan" "noerrorbar" "deltadelay" "delay" "lines" "" "and NbNeurons=600 and k=6 and (deltadelay=0 or deltadelay=10 or deltadelay=20)" "" "p" "Entropy of Timespans" "" "dtd" "d" "1"
make_multiple_2d_plots "p" "EntropySize" "noerrorbar" "deltadelay" "delay" "lines" "" "and NbNeurons=600 and k=6 and (deltadelay=0 or deltadelay=10 or deltadelay=20)" "" "p" "Entropy of Numbers of spikes" "" "dtd" "d" "1"
make_multiple_2d_plots "p" "EntropyPopulation" "noerrorbar" "deltadelay" "delay" "lines" "" "and NbNeurons=600 and k=6 and (deltadelay=0 or deltadelay=10 or deltadelay=20)" "" "p" "Entropy of Populations" "" "dtd" "d" "1"
make_multiple_2d_plots "p" "EntropyFrequency" "noerrorbar" "deltadelay" "delay" "lines" "" "and NbNeurons=600 and k=6 and (deltadelay=0 or deltadelay=10 or deltadelay=20)" "" "p" "Entropy of Frequencies" "" "dtd" "d" "1"


# alterego 3D:
make_multiple_3d_plots "p" "delay" "EntropyTimeSpan" "deltadelay" "lines" "" "and NbNeurons=600 and k=6" "" "" "p" "d" "Entropy of Timespans" "" "dtd" "1"
make_multiple_3d_plots "p" "delay" "EntropySize" "deltadelay" "lines" "" "and NbNeurons=600 and k=6" "" "" "p" "d" "Entropy of Numbers of spikes" "" "dtd" "1"
make_multiple_3d_plots "p" "delay" "EntropyPopulation" "deltadelay" "lines" "" "and NbNeurons=600 and k=6" "" "" "p" "d" "Entropy of Populations" "" "dtd" "1"
make_multiple_3d_plots "p" "delay" "EntropyFrequency" "deltadelay" "lines" "" "and NbNeurons=600 and k=6" "" "" "p" "d" "Entropy of Frequencies" "" "dtd" "1"

make_multiple_3d_plots "p" "deltadelay" "EntropyTimeSpan" "delay" "lines" "" "and NbNeurons=600 and k=6" "" "" "p" "dtd" "Entropy of Timespans" "" "d" "1"
make_multiple_3d_plots "p" "deltadelay" "EntropySize" "delay" "lines" "" "and NbNeurons=600 and k=6" "" "" "p" "dtd" "Entropy of Numbers of spikes" "" "d" "1"
make_multiple_3d_plots "p" "deltadelay" "EntropyPopulation" "delay" "lines" "" "and NbNeurons=600 and k=6" "" "" "p" "dtd" "Entropy of Populations" "" "d" "1"
make_multiple_3d_plots "p" "deltadelay" "EntropyFrequency" "delay" "lines" "" "and NbNeurons=600 and k=6" "" "" "p" "dtd" "Entropy of Frequencies" "" "d" "1"


#make_multiple_2d_plots "Population" "TimeSpan" "noerrorbar" "NbNeurons" "p" "points" "and deltadelai=10"
#make_multiple_2d_plots "Population" "Connectivity" "noerrorbar" "NbNeurons" "p" "points" "and deltadelai=10"
#make_multiple_2d_plots "NbPGs" "delay" "noerrorbar" "NbNeurons" "p" "points" "and deltadelai=10"
#make_multiple_2d_plots "Connectivity" "NbPGs" "StdDev_NbPGs" "NbNeurons" "p"
##make_multiple_3d_plots "k" "NbNeurons" "NbPGs" "p"
#make_multiple_2d_plots "deltadelay" "NbPGs" "StdDev_NbPGs" "p" "delay" "and NbNeurons=600 and k=6"
#make_multiple_2d_plots "deltadelay" "NbPGs" "StdDev_NbPGs" "delay" "p" "and NbNeurons=600 and k=6"
#make_multiple_2d_plots "p" "NbPGs" "StdDev_NbPGs" "deltadelai" "delay" "and NbNeurons=600 and k=6"
#make_multiple_2d_plots "p" "NbPGs" "StdDev_NbPGs" "delay" "deltadelay" "and NbNeurons=600 and k=6"
#make_multiple_2d_plots "p" "NbPGs" "StdDev_NbPGs" "deltadelay" "NbNeurons" "and k=6 and delay=10"
#make_multiple_2d_plots "p" "NbPGs" "StdDev_NbPGs" "deltadelay" "NbNeurons" "and k=2 and delay=10"
#make_multiple_2d_plots "p" "Population" "StdDev_Population" "NbNeurons" "k"
##make_multiple_3d_plots "p" "NbNeurons" "Population" "k" "lines" "and delay=10" "(\$3/\$2)"
#make_multiple_3d_plots "p" "Timespan" "NbNeurons" "k" "and delay=10"


echo -e "
Ploting $ext figures ... "

gnuplot $outdir/$prefix.plot

echo "done."

if [ "$convertto" != "" ]; then
  if [ "$ext" = "eps" -a "$convertto" = "pdf" ]; then
    echo "Converting to $convertto ... "
    for figure in `ls -1 $outdir/*.$ext`
    do
      outfile=`echo "$figure" | sed -r -e "s/.$ext\$/.$convertto/"`
      if [ -f $outfile ]; then
        echo "  skipping $figure, already done."
      else
        echo "  $figure => $outfile"
	      epstopdf $figure
	    fi
    done  
  else
    echo "Converting to $convertto ... "
    for figure in `ls -1 $outdir/*.$ext`
    do
      outfile=`echo "$figure" | sed -r -e "s/.$ext\$/.$convertto/"`
      if [ -f $outfile ]; then
        echo "  skipping $figure, already done."
      else
        echo "  $figure => $outfile"
	      convert $figure -antialias $outfile
	    fi
    done
  fi
fi

echo "Done."
exit 1

