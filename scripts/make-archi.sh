#!/bin/bash


if [ "$1" = "--help" ]; then
  echo "use: $0
Generate smallworld networks with a large panel of parameters. Edit the script."
  exit 1
fi

make

ks="8" # 8 10 15 30 50 100"  fait : "2 4 6"

#conectivities="0.01 0.04 0.08 0.1 0.15 0.2 0.25 0.3"
sizes="100 200 400 600 800 1000"
delais="1 5 10 15 20"
deltadelais="0 0.5 1 2 5 10 20"
Ps="1.0" # "1.0"  faits : "0 0.1 0.15 0.2 0.25 0.3 0.5 0.8 "

for k in $ks; do
#    for c in $connectivities; do
    for p in $Ps; do
        for size in $sizes; do
            for delai in $delais; do
                for deltadelai in $deltadelais; do
                    for num in `seq --format=%02.0f 1 10`; do
                      echo "
                            TIMESCALE = 10
                            N = $size
                            K = $k
                            P = $p
                            DELAI = $delai * TIMESCALE
                            DELTA_DELAI = $deltadelai * TIMESCALE
                            NUM = $num
                            C = string.format(\"%.3f\",(2*K)/(N-1))
                            " > settings/gen-networks/preset-meta.lua
                            ./nnl --metaarchitecture settings/gen-networks/builder-meta.lua --settings settings/gen-networks/settings.lua --patterns settings/gen-networks/patterns.lua --simulation settings/gen-networks/simulation.lua 
                    done
                done
            done
        done
    done
#    done
done
