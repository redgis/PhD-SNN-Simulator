#!/bin/bash

avgcmd="src/tools/avg/avg"

#################################################################################
function showhelp
{
  echo "use: $0 <PG directories>"
  echo "Regenerates pg-scan.txt with a correct timespan, size (=nbspikes) and avg frequency, for each PGs-...-XX/."
  exit 1
}

#################################################################################
#re calculate AVG timespan and size and Freq
function fix_timespan_size_freq
{
  for rep in $* ; do
    nbdone=$((nbdone+1))
    echo -n "Doing $rep ... ($nbdone / $nbrep) ... "
    # in PGs.txt : #NbPGsWritten NbSpikes TimeSpan NbDistinctNeurons Truncated AVGFrequencyByNeuron [Triggers...]
    newTimeSpanAvg=`grep -v -E "^#" $rep/PGs.txt | cut -d ' ' -f 3 | $avgcmd`
    newSizeAvg=`grep -v -E "^#" $rep/PGs.txt | cut -d ' ' -f 2 | $avgcmd`
    newFrequency=`grep -v -E "^#" $rep/PGs.txt | cut -d ' ' -f 6 | $avgcmd`
    #newFrequency=`grep -v -E "^#" $rep/PGs.txt | tr -s ' ' | cut -d ' ' -f 2,3,4 | sed -e 's/^/scale\=5\;((/' | sed -e 's/ /\.0\*10\.0)\/(/' | sed -e 's/ /\.0\*/' | sed -e 's/$/\.0))\*1000\.0/' | bc | $avgcmd`
    sed -e "s/Average Timespan.*Average size.*$/Average Timespan : $newTimeSpanAvg  Average size : $newSizeAvg  Average Frequency : $newFrequency Hz/g" $rep/pg-scan.txt > $rep/pg-scan.new
    mv $rep/pg-scan.txt $rep/_old_pg-scan.txt
    mv $rep/pg-scan.new $rep/pg-scan.txt
    echo "Done."
  done
}

#################################################################################

if [ "$1" = "--help" ]; then
  showhelp
fi

nbrep=$#
nbdone=0


