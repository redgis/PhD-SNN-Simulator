#!/bin/sh

echo "Codename for this run source backup:"
read CODE_NAME

if [ ! -e $1 ]
then
  mkdir $1
fi

tar -czvf $1/`date +%Y-%m-%d-%Hh%M`-src-$CODE_NAME.tgz "../`basename \"\`pwd\`\"`"
