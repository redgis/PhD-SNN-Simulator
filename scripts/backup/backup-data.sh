#!/bin/sh

echo "Codename for this run data backup:"
read CODE_NAME
local_dir=`pwd`
sub_dir=`basename $local_dir`
BACKUP_DIR=$1/$sub_dir/`date +%Y-%m-%d-%Hh%M`-$CODE_NAME

if [ ! -e $1 ]
then
  mkdir $1
fi

if [ ! -e $1/$sub_dir ]
then
  mkdir $1/$sub_dir
fi

if [ ! -e $BACKUP_DIR ]
then
  mkdir $BACKUP_DIR
fi

cp -R data plots figures PGs settings architectures datasets manager.cpp parameters.* $BACKUP_DIR/
