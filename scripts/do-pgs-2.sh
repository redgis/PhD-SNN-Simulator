#!/bin/bash

if [ "$1" = "--help" ]; then
  echo "use: $0 <list of architecture(s)>
Search pgs for earch an architecture. and stores them in data/PGs-<architecture name>/"
  exit 1
fi

make
export LD_LIBRARY_PATH=.:$LD_LIBRARY_PATH

rm -fr PGs
pgreps=""

for archi in $*; do
#  ./pg-search2 $archi settings/pg-search/settings.lua settings/pg-search/pg-search.lua

  pgname=`basename $archi .xml`
#  cp -R settings/pg-search/ PGs/
#  cp $archi PGs/
#  mv PGs data/PGs-$pgname
  echo "PGs saved in data/PGs-$pgname/"
  pgreps="data/PGs-$pgname $pgreps"
done


outputreps=`echo "$pgreps " | sed -E 's/--[0-9][0-9][ $]/\n/g' | sort | uniq`

echo $outputreps
exit 1

for outputrep in $outputreps; do
  mkdir $outputrep
  echo $outputrep
  scripts/make-pg-distribs.sh $outputrep $outputrep--*
  scripts/make-distribs.sh $outputrep $outputrep--*/*.xml
done
