#!/bin/bash

#ecart_mini=10
#val_mini=10

#rm PGs/pg6.txt PGs/pg9.txt PGs/pg-none.txt PGs/PGs-color.txt

#for pgnum in `cat PGs/PGs.txt | cut -d ' ' -f 1`
#do
#  pg6=`grep -E " $pgnum\$" PGs/PGs-activation-histo-0039261400-0040589400.txt | cut -d ' ' -f "1"`
#  pg9=`grep -E " $pgnum\$" PGs/PGs-activation-histo-0040589400-0041877500.txt | cut -d ' ' -f "1"`
#  
#  pg6=`echo "0$pg6"|bc`
#  pg9=`echo "0$pg9"|bc`  
#    
#  if [ `echo "$pg6 > $pg9"|bc` == 1 ]; then
#    ecart=`echo "scale=4;(($pg6-$pg9)/($pg6+$pg9+1))*100"|bc`
#  else
#    ecart=`echo "scale=4;(($pg9-$pg6)/($pg9+$pg6+1))*100"|bc`
#  fi
  
#  if [[ ( `echo "$ecart > $ecart_mini" | bc` == 1 && `echo "$pg6 > $val_mini" | bc` == 1 && `echo "$pg9 > $val_mini" | bc` == 1 ) ]] ; then
#    if [ `echo "$pg6>$pg9"|bc` == 1 ]; then
#      echo $pgnum >> PGs/pg6.txt
#      echo "$pgnum 0" >> PGs/PGs-color.txt
#    else
#      echo $pgnum >> PGs/pg9.txt
#      echo "$pgnum 1" >> PGs/PGs-color.txt
#    fi
#  else
#    echo $pgnum >> PGs/pg-none.txt
#    echo "$pgnum 2" >> PGs/PGs-color.txt
#  fi
#  
#done


tools/make-spikePSPs.sh $1 $2

firststim=`head -n 1 data/PatternClasses-00000.txt | cut -d ' ' -f 1`
secondstim=`head -n 2 data/PatternClasses-00000.txt | cut -d ' ' -f 1 | tail -n 1`
stiminterval=`echo "$secondstim - $firststim" | bc`

echo "Stimulations every $stiminterval, starting from $firststim."

# get files numbers in which are the data we need. e.g. : 00001 to 00004
echo "Getting file numbers in which are the class number data we need ..."

firstfile=`ls -1 data/PatternClasses-*.txt | head -n 1 | cut -d "." -f 1 | cut -d "-" -f 2`
lastfile=`ls -1 data/PatternClasses-*.txt | tail -n 1 | cut -d "." -f 1 | cut -d "-" -f 2`

for fichier in `ls data/PatternClasses-*.txt`
do
  fichierMin=`head -n 1 $fichier | cut -d ' ' -f "1" | cut -d ',' -f "2"`
  fichierMax=`tail -n 1 $fichier | cut -d ' ' -f "1" | cut -d ',' -f "2"`
  
  if (( $1 >= $fichierMin && $1 <= $fichierMax )); then
    firstfile=`echo $fichier | cut -d "." -f 1 | cut -d "-" -f 2`
    echo "First : $fichier : [ $fichierMin , $fichierMax ]"
    break
  fi
done

for fichier in `ls data/PatternClasses-*.txt`
do
  fichierMin=`head -n 1 $fichier | cut -d ' ' -f "1" | cut -d ',' -f "2"`
  fichierMax=`tail -n 1 $fichier | cut -d ' ' -f "1" | cut -d ',' -f "2"`
  
  if (( $2 >= $fichierMin && $2 <= $fichierMax )); then
    lastfile=`echo $fichier | cut -d "." -f 1 | cut -d "-" -f 2`
    echo "Last : $fichier : [ $fichierMin , $fichierMax ]"
    break
  fi
done

#Copy data of files into PatternClasses.txt
echo "Copying data of files into PatternClasses.txt..."
for num_fichier in `seq $firstfile $lastfile`
do
  filename=`printf "data/PatternClasses-%05d.txt" $num_fichier`
  cat $filename >> data/PatternClasses.txt
  echo -n "$filename... "
done

echo "done."
echo "Ready to proceed."

make tools/pg-color
tools/pg-color $1 $2 $firststim $stiminterval



