#!/bin/sh

cd PGs/

rm PGs-$1.txt

for pgnum in `ls -1 *000[$1].txt | tr "_" "-" | cut -d "-" -f 1`
do
  grep -E "^$pgnum " PGs.txt >> PGs-$1.txt
done
