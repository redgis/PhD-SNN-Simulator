#!/bin/sh


min=`printf "%010d" \`echo $1 | bc -l\``
max=`printf "%010d" \`echo $2 | bc -l\``



echo -n "################ PGs activation ##############
reset

set terminal postscript eps enhanced color defaultplex \"Times-Roman\" 10

set output 'PGs/PGs-color-activation-$1-$2.eps'

unset key

set xlabel 'Time [s]'
set ylabel '# PG'

#set label '[s]' at $max,-10
set pointsize 0.2
set xrange [($min/10000.0):($max/10000.0)]
set yrange [0:*]
#set xtic 1000
#set ytic 10
#set grid xtics 0

set style line 1 lt 1 lw 1 pt 1
set style line 2 lt 2 lw 1 pt 2
set style line 3 lt 3 lw 3 pt 3
set style line 4 lt 6 lw 3 pt 4
set style line 5 lt 5 lw 1 pt 5
set style line 6 lt 6 lw 1 pt 6
set style line 7 lt 7 lw 1 pt 7
set style line 8 lt 8 lw 1 pt 8
set style line 9 lt 9 lw 1 pt 9


plot 'PGs/PGs-activation-6-$1-$2.txt' using ((\$1)/10000.0):2 with points, \\
     'PGs/PGs-activation-9-$1-$2.txt' using ((\$1)/10000.0):2 with points
#    'PGs/PGs-activation-none-$1-$2.txt' using ((\$1)/10000.0):2 with points" > plots/pg-color-activation-$1-$2.plot

echo "Gnuplot-ing the raster ..."
gnuplot plots/pg-color-activation-$1-$2.plot && epstopdf PGs/PGs-color-activation-$1-$2.eps
evince PGs/PGs-color-activation-$1-$2.eps &
#acroread PGs/PGs-color-activation-$1-$2.pdf &
echo "done..."

