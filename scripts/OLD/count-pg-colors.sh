#!/bin/sh

lowlimit=$1
uplimit=$2

rm data/PatternClasses-$1-$2.txt 

for reftimeClass in `cat data/PatternClasses*.txt | tr -t ' ' '#' `
do
  class=`echo $reftimeClass | cut -d '#' -f 2`
  reftime=`echo $reftimeClass | cut -d '#' -f 1`
  #echo $reftime
  
  if [[ ( `echo "$reftime >= $lowlimit" | bc` == 1  && `echo "$reftime <= $uplimit" | bc` == 1 ) ]]; then

    reftime2=`echo "$reftime+1999" | bc`
    
    nb6=0
    nb9=0
    nbNone=0
    
    #count pg for 6
    for mytime in `cat PGs/PGs-activation-6-$1-$2.txt | cut -d ' ' -f 1`
    do
      #echo $reftime $reftime2 $mytime
      
      if [[ ( `echo "$mytime >= $reftime" | bc` == 1  && `echo "$mytime <= $reftime2" | bc` == 1 ) ]]; then
        nb6=`echo "$nb6+1"|bc`
      fi
    done
    
    
    #count pg for 9
    for mytime in `cat PGs/PGs-activation-9-$1-$2.txt | cut -d ' ' -f 1`
    do
      #echo $reftime $reftime2 $mytime
      
      if [[ ( `echo "$mytime >= $reftime" | bc` == 1  && `echo "$mytime <= $reftime2" | bc` == 1 ) ]]; then
        nb9=`echo "$nb9+1"|bc`
      fi
    done
    
    #count pg for none
    for mytime in `cat PGs/PGs-activation-none-$1-$2.txt | cut -d ' ' -f 1`
    do
      #echo $reftime $reftime2 $mytime
      
      if [[ ( `echo "$mytime >= $reftime" | bc` == 1  && `echo "$mytime <= $reftime2" | bc` == 1 ) ]]; then
        nbNone=`echo "$nbNone+1"|bc`
      fi
    done
    
    echo "$reftime $class $nb6 $nb9 $nbNone" >> data/PatternClasses-$1-$2.txt  
  fi
  

done

echo -n "Ploting plots/count-pg-color-$1-$2.plot..."
echo "
################################################
reset
set terminal postscript eps enhanced color defaultplex \"Times-Roman\" 12
set output 'figures/count-pg-color-$1-$2.eps'

set xlabel 'Time [s]'
set ylabel 'Number of polychronous groups'

set pointsize 0.2

set yrange [0:*]

set style line 1 lt 1 lw 0.2 pt 1
set style line 2 lt 2 lw 0.2 pt 2
set style line 3 lt 0 lw 5 pt 2
set style line 4 lt 10 lw 5 pt 2
set style line 5 lt 5 lw 1 pt 5
set style line 6 lt 6 lw 1 pt 6
set style line 7 lt 7 lw 1 pt 7
set style line 8 lt 8 lw 1 pt 8
set style line 9 lt 9 lw 1 pt 9


set xrange [($1/10000.0):($2/10000.0)]
set title 'PG color count'

plot 'data/PatternClasses-$1-$2.txt' using ((\$1)/10000.0):3:(0.05) with boxes fs solid 0.1 ls 1 title 'Nb of Polychronous groups for 6', \\
     'data/PatternClasses-$1-$2.txt' using ((\$1+500)/10000.0):4:(0.05) with boxes fs solid 1 ls 1 title 'Nb of Polychronous groups for 9'" > plots/count-pg-color-$1-$2.plot
     

gnuplot plots/count-pg-color-$1-$2.plot
epstopdf figures/count-pg-color-$1-$2.eps
evince figures/count-pg-color-$1-$2.eps &
#acroread figures/count-pg-color-$1-$2.pdf &
echo "done."

