#!/bin/sh


min=`printf "%010d" \`echo $1 | bc -l\``
max=`printf "%010d" \`echo $2 | bc -l\``



echo -n "################ PGs activation ##############
reset

set terminal postscript eps enhanced color defaultplex \"Times-Roman\" 10

set output 'PGs/PGs-activation-$1-$2.eps'

unset key

set xlabel 'Time [s]'
set ylabel '# PG'

#set label '[s]' at $max,-10
set pointsize 0.2
set xrange [($min/10000.0):($max/10000.0)]
set yrange [0:*]
#set xtic 1000
#set ytic 10
#set grid xtics 0

plot 'PGs/PGs-activation-$1-$2.txt' using ((\$1)/10000.0):2 with points" > plots/pg-activation-$1-$2.plot

echo "Gnuplot-ing the raster ..."
gnuplot plots/pg-activation-$1-$2.plot && epstopdf PGs/PGs-activation-$1-$2.eps
#evince PGs/PGs-activation-$1-$2.eps &
acroread PGs/PGs-activation-$1-$2.pdf &
echo "done..."

