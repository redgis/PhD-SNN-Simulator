#!/bin/bash

rm data/Predictions-$1-$2.txt

echo "Generating result in data/Predictions-$1-$2.txt..."

for ClassTime in `cat data/PatternClasses-$1-$2.txt | cut -d ' ' -f 1`
do
  ColorClass=`grep $ClassTime data/PatternClasses-$1-$2.txt | cut -d ' ' -f 6`
  NeuronClass=`grep $ClassTime data/PredictedClasses-00000.txt | cut -d ' ' -f 2`
  RealClass=`grep $ClassTime data/PatternClasses-00000.txt | cut -d ' ' -f 2`
  
  printf "$RealClass $NeuronClass $ColorClass\n" >> data/Predictions-$1-$2.txt
done

echo "done."

echo -n "Total Nb of presentations : "
cat data/Predictions-$1-$2.txt | wc -l

echo -n "Nb of misclassif by color : "
cat data/Predictions-39261500-41877500.txt | grep -E "^0 [01\-]* -?1$|^1 [01\-]* 0$|^1 [01\-]* -1" | wc -l

echo -n "Nb of misclassif by neuron : "
cat data/Predictions-$1-$2.txt | grep -E "^0 1 |^1 0 |^[01] -1 " | wc -l

echo -n "NB of misclassif by color and neuron at the same time : "
cat data/Predictions-$1-$2.txt | grep -E "^0 [-]?1 [-]?1$|^1 0 0$|^1 -1 -1$|^1 -1 0$|^1 0 -1$" | wc -l


