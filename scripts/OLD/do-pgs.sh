#!/bin/bash

export LD_LIBRARY_PATH=.:$LD_LIBRARY_PATH

rm PGs
rm -fr PGs
mkdir PGs

cd tools/pg-search/
make
cd ../../

tools/pg-search/pg-search $*

cp -R settings/ PGs/
cp manager.cpp architecture.cpp PGs/
cp $1 PGs/

tools/PG-stats.sh PGs/

#printf "Collez le resultat la :\a\n"
#cat > PGs/pg-scan.txt


PGsTime=`basename $1 | cut -d "." -f 1`
mv PGsRegis-$PGsTime PGsRegis-$PGsTime.old
mv PGs PGsRegis-$PGsTime
echo "PGs saved in PGsRegis-$PGsTime/"

ln -s PGsRegis-$PGsTime PGs
