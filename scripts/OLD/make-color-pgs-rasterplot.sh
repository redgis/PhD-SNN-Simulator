#!/bin/bash

#set limits to write in eps output filename
min=`printf "%010d" $1`
max=`printf "%010d" $2`

#$3 should be the pg-raster file you need, e.g : PGs/PGs-raster-49491500-49531500.txt (e.g. containing [min:max]

#generate pattern classes
echo "Generating pattern classes ..."

if [ "$3" == "1" ]; then
  ./tools/make-patternsclasses.sh $1 $2
else
  echo "" > plots/PatternClasses-$min-$max.plot
fi



# get files numbers in which are the data we need. e.g. : 00001 to 00004 for excitatory spikes and 00000 and 00001 for inhib
echo "Getting file numbers in which are the data we need ..."

ExcitFirstfile=0
ExcitLastfile=0
InhibFirstfile=0
InhibLastfile=0

for fichier in `ls data/SpikeRasterExcit-*.txt`
do
  fichierMin=`head -n 1 $fichier | cut -d ' ' -f "1"`
  fichierMax=`tail -n 1 $fichier | cut -d ' ' -f "1"`
  
  if (( $1 >= $fichierMin && $1 <= $fichierMax )); then
    ExcitFirstfile=`echo $fichier | cut -d "." -f 1 | cut -d "-" -f 2`
    echo "First excit: $fichier : [ $fichierMin , $fichierMax ]"
    break
  fi
done

for fichier in `ls data/SpikeRasterExcit-*.txt`
do
  fichierMin=`head -n 1 $fichier | cut -d ' ' -f "1"`
  fichierMax=`tail -n 1 $fichier | cut -d ' ' -f "1"`
  
  if (( $2 >= $fichierMin && $2 <= $fichierMax )); then
    ExcitLastfile=`echo $fichier | cut -d "." -f 1 | cut -d "-" -f 2`
    echo "Last  excit: $fichier : [ $fichierMin , $fichierMax ]"
    break
  fi
done

for fichier in `ls data/SpikeRasterInhib-*.txt`
do
  fichierMin=`head -n 1 $fichier | cut -d ' ' -f "1"`
  fichierMax=`tail -n 1 $fichier | cut -d ' ' -f "1"`
  
  if (( $1 >= $fichierMin && $1 <= $fichierMax )); then
    InhibFirstfile=`echo $fichier | cut -d "." -f 1 | cut -d "-" -f 2`
    echo "First inhib: $fichier : [ $fichierMin , $fichierMax ]"
    break
  fi
done

for fichier in `ls data/SpikeRasterInhib-*.txt`
do
  fichierMin=`head -n 1 $fichier | cut -d ' ' -f "1"`
  fichierMax=`tail -n 1 $fichier | cut -d ' ' -f "1"`
  
  if (( $2 >= $fichierMin && $2 <= $fichierMax )); then
    InhibLastfile=`echo $fichier | cut -d "." -f 1 | cut -d "-" -f 2`
    echo "Last  inhib: $fichier : [ $fichierMin , $fichierMax ]"
    break
  fi
done


# for valeur in `seq $1 100 \`echo "$1+20000"|bc\``
# do
#   ExcitFirstfile=`grep -H --max-count=1 -E "^$valeur[ ]" data/SpikeRasterExcit-*.txt | cut -d "." -f 1 | cut -d "-" -f 2`
#   if [ "$ExcitFirstfile" != "" ]; then
#     echo "$valeur $ExcitFirstfile"
#     break
#   fi
# done
# 
# 
# for valeur in `seq $2 -100 \`echo "$2-20000"|bc\``
# do
#   ExcitLastfile=`grep -H --max-count=1 -E "^$valeur[ ]" data/SpikeRasterExcit-*.txt | cut -d "." -f 1 | cut -d "-" -f 2`
#   if [ "$ExcitLastfile" != "" ]; then
#     echo "$valeur $ExcitLastfile"
#     break
#   fi
# done
# 
# 
# for valeur in `seq $1 100 \`echo "$1+20000"|bc\``
# do
#   InhibFirstfile=`grep -H --max-count=1 -E "^$valeur[ ]" data/SpikeRasterInhib-*.txt | cut -d "." -f 1 | cut -d "-" -f 2`
#   if [ "$InhibFirstfile" != "" ]; then
#     echo "$valeur $InhibFirstfile"
#     break
#   fi
# done
# 
# 
# for valeur in `seq $2 -100 \`echo "$2-20000"|bc\``
# do
#   InhibLastfile=`grep -H --max-count=1 -E "^$valeur[ ]" data/SpikeRasterInhib-*.txt | cut -d "." -f 1 | cut -d "-" -f 2`
#   if [ "$InhibLastfile" != "" ]; then
#     echo -e "$valeur $InhibLastfile"
#     break
#   fi
# done


#generate data filenames to plot
tmpFilename=""
ExcitFilenames=""

for num_fichier in `seq $ExcitFirstfile $ExcitLastfile`
do
  ExcitFilenames=`printf "SpikeRasterExcit-%05d.txt $ExcitFilenames" $num_fichier`
done

InhibFilenames=""

for num_fichier in `seq $InhibFirstfile $InhibLastfile`
do
  InhibFilenames=`printf "SpikeRasterInhib-%05d.txt $InhibFilenames" $num_fichier`
done



#generate the gnuplot script
echo "Generating the gnuplot script ..."

echo "################################################
reset
set terminal postscript eps enhanced color defaultplex \"Times-Roman\" 12
set output 'figures/PGs-SpikeRasterPlot--$min-$max.eps'

set xlabel 'Time [s]'
set ylabel '# neurons'

set pointsize 0.2

set yrange [256:470]

set style line 1 lt 1 lw 1 pt 1
set style line 2 lt 2 lw 1 pt 2
set style line 3 lt 3 lw 0.2 pt 12 ps 1
set style line 4 lt 0 lw 0.2 pt 6 ps 1
set style line 5 lt 5 lw 1 pt 5
set style line 6 lt 6 lw 1 pt 6
set style line 7 lt 7 lw 1 pt 7
set style line 8 lt 8 lw 1 pt 8
set style line 9 lt 9 lw 1 pt 9


set xrange [($1/10000.0):($2/10000.0)]
set title 'Spike raster plot'

" > plots/PGs-SpikeRasterPlot--$min-$max.plot

if [ "$3" == "1" ]; then
  echo "load 'plots/PatternClasses-$min-$max.plot'" >> plots/PGs-SpikeRasterPlot--$min-$max.plot
fi

echo -n "plot " >> plots/PGs-SpikeRasterPlot--$min-$max.plot

#end of the gnuplot file, generate the "plot" command

premierFichier=1

for fichier in $ExcitFilenames
do
  if [ $premierFichier == 1 ]; then
    printf "'data/$fichier' using ((\$1)/10000.0):2 with points ls 1 title 'Excitatory spikes'" >> plots/PGs-SpikeRasterPlot--$min-$max.plot
    premierFichier=0
  else
    printf ", \\\\\n     'data/$fichier' using ((\$1)/10000.0):2 with points ls 1 notitle" >> plots/PGs-SpikeRasterPlot--$min-$max.plot
  fi
done

premierFichier=1

for fichier in $InhibFilenames
do
  if [ $premierFichier == 1 ]; then
    printf ", \\\\\n     'data/$fichier' using ((\$1)/10000.0):2 with points ls 2 title 'Inhibitory spikes'" >> plots/PGs-SpikeRasterPlot--$min-$max.plot
    premierFichier=0
  else
    printf ", \\\\\n     'data/$fichier' using ((\$1)/10000.0):2 with points ls 2 notitle" >> plots/PGs-SpikeRasterPlot--$min-$max.plot
  fi
done


echo ", \\
      'PGs/PGs-raster-6-$1-$2.txt' using ((\$2)/10000.0):1 with p ls 3 title 'Polychronous triggers for 6', \\
      'PGs/PGs-raster-9-$1-$2.txt' using ((\$2)/10000.0):1 with p ls 4 title 'Polychronous triggers for 9'
#     'PGs/PGs-raster-none-$1-$2.txt' using ((\$2)/10000.0):1 with p ls 5 title 'Polychronous triggers for none'" >> plots/PGs-SpikeRasterPlot--$min-$max.plot

echo "done."

echo "Gnuplot-ing the raster 'figures/PGs-SpikeRasterPlot--$min-$max.eps' ..."
gnuplot plots/PGs-SpikeRasterPlot--$min-$max.plot && epstopdf figures/PGs-SpikeRasterPlot--$min-$max.eps
evince figures/PGs-SpikeRasterPlot--$min-$max.eps &
#acroread figures/PGs-SpikeRasterPlot--$min-$max.pdf &
echo "done..."


