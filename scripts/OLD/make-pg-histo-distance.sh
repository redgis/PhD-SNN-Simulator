#!/bin/bash

nbpresentations=$1
shift

nbphases=$#

filename=`echo \`echo $1 | cut -d "." -f "1"\`"-avg.txt"`
basename=`echo \`basename $filename | cut -d "." -f 1\``

echo $filename
rm $filename


#calcul du barycentre des 4 fichiers
for pgnum in `cat $1 | cut -d " " -f "2"`
do
  echo "$pgnum" `echo "scale=3; (" \`grep -h -E " $pgnum\$" $* | cut -d " " -f 1 | tr '\n' '+' && echo 0\` ") / ($nbpresentations * $nbphases)" | bc` >> $filename
done

echo "Generating the gnuplot script ..."

echo -n "################################################
reset

set terminal postscript eps enhanced color defaultplex \"Times-Roman\" 12
set output 'figures/$basename.eps'

unset key

#set size ratio 0.25
#set size 6.0,1.0
#set size 16,12

set xlabel '# PG'
set ylabel 'Probability of activation in network response to a particular stimulus'

set pointsize 0.3

set style line 1 lt 1 lw 1 pt 1
set style line 2 lt 2 lw 1 pt 2
set style line 3 lt 3 lw 1 pt 3
set style line 4 lt 4 lw 1 pt 4
set style line 5 lt 5 lw 1 pt 5
set style line 6 lt 6 lw 1 pt 6
set style line 7 lt 7 lw 1 pt 7
set style line 8 lt 8 lw 1 pt 8
set style line 9 lt 9 lw 1 pt 9

#set title 'Spike raster plot'

plot '$filename' w p
" > plots/$basename.plot

gnuplot plots/$basename.plot
epstopdf figures/$basename.eps
#evince figures/$basename.eps&
acroread figures/$basename.pdf &

#calcul de la distance de chaque fichier au barycentre
for fichier in $*
do
  outfichier=`echo $fichier | cut -d "." -f 1`
  echo $outfichier-distance.txt
  rm $outfichier-distance.txt
  
  for ligne in `cat $filename | tr " " "_"`
  do
    pgnum=`echo $ligne | cut -d "_" -f "1"`
    pgavg=`echo $ligne | cut -d "_" -f "2"`

    #proba de trouver ce pg dans ce fichier
    tmp=`echo \`grep -E " $pgnum\$" $fichier | cut -d " " -f "1"\`".0 / ($nbpresentations)"`
    pgproba=`echo "scale=3; $tmp" | bc`
    
    #pgdistance=`echo "scale=3; $pgavg-$pgproba"`
    pgdistance=`echo "scale=3; $pgavg-$pgproba" | bc`
    echo "$pgnum $pgdistance" >> $outfichier-distance.txt
  done
  
done


echo "Generating the gnuplot script ..."

echo -n "################################################
reset

set terminal postscript eps enhanced color defaultplex \"Times-Roman\" 12
set output 'figures/$basename-distance.eps'

unset key

#set size ratio 0.25
set size 1,0.5
#set size 16,12

set xlabel '# PG'
#set ylabel 'Probability of activation in network response to a particular stimulus'

set grid ytics

#set yrange [0.2:-0.2]
#set xrange [0:6000]

set pointsize 0.3

set style line 1 lt 1 lw 0.5 pt 1
set style line 2 lt 2 lw 0.5 pt 2
set style line 3 lt 3 lw 0.5 pt 3
set style line 4 lt 4 lw 0.5 pt 4
set style line 5 lt 5 lw 0.5 pt 5
set style line 6 lt 6 lw 0.5 pt 6
set style line 7 lt 7 lw 1 pt 7
set style line 8 lt 8 lw 1 pt 8
set style line 9 lt 9 lw 1 pt 9

#set title 'Spike raster plot'

plot 0 w imp " > plots/$basename-distance.plot

for fichier in $*
do
  outfichier=`echo $fichier | cut -d "." -f 1`
  echo -n ", '$outfichier-distance.txt' using (\$1+0.5):2 w imp" >> plots/$basename-distance.plot
done

gnuplot plots/$basename-distance.plot
epstopdf figures/$basename-distance.eps
#evince figures/$basename-distance.eps&
acroread figures/$basename-distance.pdf &


