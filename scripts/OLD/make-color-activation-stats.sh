#!/bin/sh

tools/make-spikePSPs.sh $1 $2

firststim=`head -n 1 data/PatternClasses-00000.txt | cut -d ' ' -f 1`
secondstim=`head -n 2 data/PatternClasses-00000.txt | cut -d ' ' -f 1 | tail -n 1`
stiminterval=`echo "$secondstim - $firststim" | bc`

echo "Stimulations every $stiminterval, starting from $firststim."

# get files numbers in which are the data we need. e.g. : 00001 to 00004
echo "Getting file numbers in which are the class number data we need ..."

firstfile=`ls -1 data/PatternClasses-*.txt | head -n 1 | cut -d "." -f 1 | cut -d "-" -f 2`
lastfile=`ls -1 data/PatternClasses-*.txt | tail -n 1 | cut -d "." -f 1 | cut -d "-" -f 2`

for fichier in `ls data/PatternClasses-*.txt`
do
  fichierMin=`head -n 1 $fichier | cut -d ' ' -f "1" | cut -d ',' -f "2"`
  fichierMax=`tail -n 1 $fichier | cut -d ' ' -f "1" | cut -d ',' -f "2"`
  
  if (( $1 >= $fichierMin && $1 <= $fichierMax )); then
    firstfile=`echo $fichier | cut -d "." -f 1 | cut -d "-" -f 2`
    echo "First : $fichier : [ $fichierMin , $fichierMax ]"
    break
  fi
done

for fichier in `ls data/PatternClasses-*.txt`
do
  fichierMin=`head -n 1 $fichier | cut -d ' ' -f "1" | cut -d ',' -f "2"`
  fichierMax=`tail -n 1 $fichier | cut -d ' ' -f "1" | cut -d ',' -f "2"`
  
  if (( $2 >= $fichierMin && $2 <= $fichierMax )); then
    lastfile=`echo $fichier | cut -d "." -f 1 | cut -d "-" -f 2`
    echo "Last : $fichier : [ $fichierMin , $fichierMax ]"
    break
  fi
done

#Copy data of files into PatternClasses.txt
echo "Copying data of files into PatternClasses.txt..."
for num_fichier in `seq $firstfile $lastfile`
do
  filename=`printf "data/PatternClasses-%05d.txt" $num_fichier`
  cat $filename >> data/PatternClasses.txt
  echo -n "$filename... "
done

echo "done."
echo "Ready to proceed."

make tools/color-pg-active
tools/color-pg-active $1 $2 $firststim $stiminterval


good=`cut -d ' ' -f "2,6" data/PatternClasses-$1-$2.txt | grep -E "1 1|0 0" | wc -l`

wrong=`cut -d ' ' -f "2,6" data/PatternClasses-$1-$2.txt | grep -E "0 1|1 0" | wc -l`

unclassified=`cut -d ' ' -f "2,6" data/PatternClasses-$1-$2.txt | grep -E "[0-1] -1" | wc -l`

total=`cat data/PatternClasses-$1-$2.txt | wc -l`

echo "Statistics:
  Wrong: $wrong (`echo \"scale=2; ($wrong.0/$total.0)*100.0\" | bc`%)
   Good: $good (`echo \"scale=2; ($good.0/$total.0)*100.0\" | bc`%)
Unknown: $unclassified (`echo \"scale=2; ($unclassified.0/$total.0)*100.0\" | bc`%)
  Total: $total (`echo \"scale=2; ($total.0/$total.0)*100.0\" | bc`%)"

echo -n "Ploting plots/count-pg-color-$1-$2.plot..."
echo "
################################################
reset
set terminal postscript eps enhanced color defaultplex \"Times-Roman\" 12
set output 'figures/count-pg-color-$1-$2.eps'

set xlabel 'Time [s]'
set ylabel 'Number of polychronous groups'

set pointsize 0.2

set yrange [0:*]

set style line 1 lt 1 lw 0.2 pt 1
set style line 2 lt 2 lw 0.2 pt 2
set style line 3 lt 0 lw 5 pt 2
set style line 4 lt 10 lw 5 pt 2
set style line 5 lt 5 lw 1 pt 5
set style line 6 lt 6 lw 1 pt 6
set style line 7 lt 7 lw 1 pt 7
set style line 8 lt 8 lw 1 pt 8
set style line 9 lt 9 lw 1 pt 9


set xrange [($1/10000.0):($2/10000.0)]
set title 'PG color count'

plot 'data/PatternClasses-$1-$2.txt' using ((\$1)/10000.0):3:(0.05) with boxes fs solid 0.1 ls 1 title 'Nb of Polychronous groups for 6', \\
     'data/PatternClasses-$1-$2.txt' using ((\$1+500)/10000.0):4:(0.05) with boxes fs solid 1 ls 1 title 'Nb of Polychronous groups for 9'" > plots/count-pg-color-$1-$2.plot
     

gnuplot plots/count-pg-color-$1-$2.plot
epstopdf figures/count-pg-color-$1-$2.eps
evince figures/count-pg-color-$1-$2.eps &
#acroread figures/count-pg-color-$1-$2.pdf &
echo "done."

tools/make-color-pg-activation.sh $1 $2 

tools/make-color-pgs-rasterplot.sh $1 $2 1

echo "Collez le resultat ici : "
cat > PGs/count-pg-color-$1-$2.txt




