#!/bin/sh

min=`printf "%010d" \`echo $1 | bc -l\``
max=`printf "%010d" \`echo $2 | bc -l\``


cat PGs/PGs-activation-$1-$2.txt | cut -d " " -f 2 | sort -n | uniq -c | grep -o -E "[0-9]+ [0-9]+" > PGs/PGs-activation-histo-$min-$max.txt

echo "################################################
reset
set terminal postscript eps enhanced color defaultplex \"Times-Roman\" 12
set output 'PGs/PGs-activation-histo-$min-$max.eps'

set xlabel '# PGs'
set ylabel 'Activations count'

set pointsize 0.2

set style line 1 lt 1 lw 1 pt 1
set style line 2 lt 2 lw 1 pt 2
set style line 3 lt 3 lw 1 pt 3
set style line 4 lt 4 lw 1 pt 4
set style line 5 lt 5 lw 1 pt 5
set style line 6 lt 6 lw 1 pt 6
set style line 7 lt 7 lw 1 pt 7
set style line 8 lt 8 lw 1 pt 8
set style line 9 lt 9 lw 1 pt 9

set yrange [0:*]

set title 'PG activations histogram'


plot 'PGs/PGs-activation-histo-$min-$max.txt' using 2:1 with imp ls 1 title 'PGs activations counts'" > PGs/PGs-activation-histo-$min-$max.plot

echo "Gnuplot-ing the raster ..."
gnuplot PGs/PGs-activation-histo-$min-$max.plot && epstopdf PGs/PGs-activation-histo-$min-$max.eps
#evince PGs/PGs-activation-histo-$min-$max.eps &
acroread PGs/PGs-activation-histo-$min-$max.pdf &
echo "done..."

