#!/bin/bash

echo -n "
################################################
reset
set terminal postscript eps enhanced color defaultplex \"Times-Roman\" 12
set output 'figures/pgs-sizes.eps'

set pointsize 0.6

set yrange [0:*]
set xrange [0:*]

set ylabel 'Nb of PGs'
set xlabel 'Size of PG (in nb of spikes)'

#set logscale xy

plot " > plots/pgs-sizes.plot

firstline="1"

for repertoire in $*
do
  if [ $firstline == "0" ]; then
    echo ", \\" >> plots/pgs-sizes.plot
  fi

  echo -n "'$repertoire/pgs-sizes.txt' using 1:2 w l title '$repertoire'" >> plots/pgs-sizes.plot
  firstline="0"
done  

echo -n ", " >> plots/pgs-sizes.plot

firstline="1"

for repertoire in $*
do
  if [ $firstline == "0" ]; then
    echo ", \\" >> plots/pgs-sizes.plot
  fi

  echo -n "'$repertoire/pgs-sizes.txt' using 1:2 w p title '$repertoire'" >> plots/pgs-sizes.plot
  firstline="0"
done  

gnuplot plots/pgs-sizes.plot
epstopdf figures/pgs-sizes.eps
#evince figures/pgs-sizes.eps &
#acroread figures/pgs-sizes.pdf &
cp plots/pgs-sizes.plot figures/pgs-sizes.eps figures/pgs-sizes.pdf $repertoire/


