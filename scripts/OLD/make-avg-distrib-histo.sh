#!/bin/bash

basename=`basename $1 | cut -d "." -f 1`

rm PGs/$basename-distrib.txt

for range in `seq 0.1 0.1 1.1`
do
  count=0
  
  for value in `cat $1 | cut -d " " -f 2`
  do
    count=`echo "$count + (1 * (($value < $range) && ($value >= $range-0.1)))" | bc`
  done
  
  echo "$range $count" >> PGs/$basename-distrib.txt
  
done


echo "

reset

set terminal postscript eps enhanced color defaultplex \"Times-Roman\" 18

set output 'figures/$basename-distrib.eps'

#set xlabel 'Weight range'
#set ylabel '% of connexions in this range'

set tics out

unset key

set pointsize 0.5

#set yrange [0:100]

set style line 1 lt 1 lw 90 pt 1
set style line 2 lt 1 lw 1 pt 2
set style line 3 lt 3 lw 1 pt 3
set style line 4 lt 4 lw 1 pt 4
set style line 5 lt 5 lw 1 pt 5
set style line 6 lt 6 lw 1 pt 6
set style line 7 lt 7 lw 1 pt 7
set style line 8 lt 8 lw 1 pt 8
set style line 9 lt 9 lw 1 pt 9

plot 'PGs/$basename-distrib.txt' using (\$1-0.05):2 w imp ls 1" > plots/$basename-distrib.plot

echo "PGs/$basename-distrib.txt"
echo "plots/$basename-distrib.plot"
echo "figures/$basename-distrib.eps"
echo "figures/$basename-distrib.pdf"



gnuplot plots/$basename-distrib.plot
epstopdf figures/$basename-distrib.eps
#evince figures/$basename-distrib.eps
acroread figures/$basename-distrib.pdf &
