#!/bin/bash

rm plots/PatternClasses*.plot

min=`printf "%010d" $1`
max=`printf "%010d" $2`


for ligne in `cat data/PatternClasses-*.txt | tr -t " " "_"`
do
  temp=`echo $ligne | cut -d "_" -f 1`
  if [ $temp -ge $1 ]; then
    if  [ $temp -gt $2 ]; then
      break
    fi
    class=`echo $ligne | cut -d "_" -f 2`
    echo "set label '$class' at ($temp/10000.0),475" >> plots/PatternClasses-$min-$max.plot 
  fi
done


