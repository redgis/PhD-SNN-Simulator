#!/bin/bash

cat data/perf.txt | grep -E "Time|Good" | tr '\n' '#' | tr '\t\(\)\%' ' ' | tr -s ' ' | sed -r -e 's/# Good/Good/g' | tr '#' '\n' | grep Good | cut -d ' ' -f "3,15" > data/perf-eval-good.txt
cat data/perf.txt | grep -E "Time|Wrong" | tr '\n' '#' | tr '\t\(\)\%' ' ' | tr -s ' ' | sed -r -e 's/# Wrong/Wrong/g' | tr '#' '\n' | grep Wrong | cut -d ' ' -f "3,15" > data/perf-eval-wrong.txt
cat data/perf.txt | grep -E "Time|Unknown" | tr '\n' '#' | tr '\t\(\)\%' ' ' | tr -s ' ' | sed -r -e 's/#Unknown/Unknown/g' | tr '#' '\n' | grep Unknown | cut -d ' ' -f "3,15" > data/perf-eval-unknown.txt
cat data/perf.txt | grep -E "Time|NoSpike" | tr '\n' '#' | tr '\t\(\)\%' ' ' | tr -s ' ' | sed -r -e 's/#NoSpike/NoSpike/g' | tr '#' '\n' | grep NoSpike | cut -d ' ' -f "3,15" > data/perf-eval-nospike.txt
gnuplot plots/perf-eval.plot
epstopdf figures/perf-eval.eps
evince figures/perf-eval.eps&

cat data/perf.txt | grep -E "Time|increase" | tr '\n' '#' | tr '\t\(\)\%' ' ' | tr -s ' ' | sed -r -e 's/#Weigth/Weigth/g' | tr '#' '\n' | grep increase | cut -d ' ' -f "3,17" > data/perf-eval-weight-increase.txt
cat data/perf.txt | grep -E "Time|decrease" | tr '\n' '#' | tr '\t\(\)\%' ' ' | tr -s ' ' | sed -r -e 's/#Weigth/Weigth/g' | tr '#' '\n' | grep decrease | cut -d ' ' -f "3,17" > data/perf-eval-weight-decrease.txt
cat data/perf.txt | grep -E "Time|delta" | tr '\n' '#' | tr '\t\(\)\%' ' ' | tr -s ' ' | sed -r -e 's/#Weigth/Weigth/g' | tr '#' '\n' | grep delta | cut -d ' ' -f "3,17" > data/perf-eval-weight-delta.txt
gnuplot plots/perf-eval-weight.plot
epstopdf figures/perf-eval-weight.eps
evince figures/perf-eval-weight.eps&

cat data/perf.txt | grep -E "Time|increase" | tr '\n' '#' | tr '\t\(\)\%' ' ' | tr -s ' ' | sed -r -e 's/#Weigth/Weigth/g' | tr '#' '\n' | grep increase | cut -d ' ' -f "3,21" > data/perf-eval-average-weight-increase.txt
cat data/perf.txt | grep -E "Time|decrease" | tr '\n' '#' | tr '\t\(\)\%' ' ' | tr -s ' ' | sed -r -e 's/#Weigth/Weigth/g' | tr '#' '\n' | grep decrease | cut -d ' ' -f "3,21" > data/perf-eval-average-weight-decrease.txt
cat data/perf.txt | grep -E "Time|delta" | tr '\n' '#' | tr '\t\(\)\%' ' ' | tr -s ' ' | sed -r -e 's/#Weigth/Weigth/g' | tr '#' '\n' | grep delta | cut -d ' ' -f "3,21" > data/perf-eval-average-weight-delta.txt
gnuplot plots/perf-eval-average-weight.plot
epstopdf figures/perf-eval-average-weight.eps
evince figures/perf-eval-average-weight.eps&
