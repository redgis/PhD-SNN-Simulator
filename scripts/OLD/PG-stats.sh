#!/bin/bash

for pgrep in $*
do

  rm $pgrep/pgs-sizes.txt

  for Taille in `seq 1 1000`
  do
    Taille=`printf "%04d" $Taille`
    NbPGs=`ls $pgrep/*$Taille.txt 2>/dev/null | wc -l`
    if [ "$NbPGs" != "0" ]; then
      echo $Taille $NbPGs >> $pgrep/pgs-sizes.txt
    fi
  done

  nbTrunc=`ls $1/trunc*.txt 2>/dev/null | wc -l`
  echo "$pgrep : $nbTrunc PG truncated."

done

tools/do-pgs-sizes.sh $*
tools/do-pgs-timespan.sh $*
