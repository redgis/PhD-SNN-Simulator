#!/bin/bash

for pgrep in $*
do

  rm $pgrep/pgs-timespan.txt

  tail -q -n 2  $pgrep/*_[0-9][0-9][0-9][0-9]-spikes.txt $pgrep/*_[0-9][0-9][0-9][0-9].txt | grep -E "^[0-9]+" | cut -d ' ' -f 1 | sed -e 's/[0-9]$/0/g' | sort -n | uniq -c > $pgrep/pgs-timespan.txt

done


echo -n "
################################################
reset
set terminal postscript eps enhanced color defaultplex \"Times-Roman\" 12
set output 'figures/pgs-timespan.eps'

set pointsize 0.6

set yrange [0:*]
set xrange [0:*]

set ylabel 'Nb of PGs'
set xlabel 'Time span of PG (in ms)'

#set logscale xy

plot " > plots/pgs-timespan.plot

firstline="1"

for repertoire in $*
do
  if [ $firstline == "0" ]; then
    echo ", \\" >> plots/pgs-timespan.plot
  fi

  echo -n "'$repertoire/pgs-timespan.txt' using ((\$2)/10):1 w l title '$repertoire'" >> plots/pgs-timespan.plot
  firstline="0"
done  
     
gnuplot plots/pgs-timespan.plot
epstopdf figures/pgs-timespan.eps
#evince figures/pgs-timespan.eps &
#acroread figures/pgs-timespan.pdf &
cp plots/pgs-timespan.plot figures/pgs-timespan.eps figures/pgs-timespan.pdf $repertoire/

