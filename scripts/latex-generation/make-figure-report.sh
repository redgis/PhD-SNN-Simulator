#!/bin/bash


#################################################################################
function showhelp
{
  echo "use: $0 <output file name (without extension)> <landscape|a4paper> <target directory> <Nb fig. per page> <Nb fig. largeur> <Nb fig. hauteur> <figures ... >"
  echo "Generate a latex file presenting provided figures. Be careful to provide figures in order you want them to appear."
  exit 1
}



#################################################################################
# - title
# - $* restant: figures
function makepage 
{
  local title=$1; shift
  local widthscale=`bc <<< "scale=2;0.95/$NbFigWidth.0"`
  


  echo "    \begin{figure}[htb]"
  
  title=""
  
  for nothing in `seq 1 $NbFigHeight`; do  # for each line of figures in pages
    for nothing in `seq 1 $NbFigWidth`; do  # for each figure in the line
      title="$title | `dirname $1`"
      if [ "$1" != "" ]; then
        echo "      \includegraphics[width=0$widthscale\textwidth]{`readlink -f $1`}"
      fi
      shift
      if [ "$#" = "0" ]; then break; fi
    done
    if [ "$#" = "0" ]; then break; fi
    echo "\\\\"
    title="$title \\\\"
  done
  
  echo "      \caption{$title}
      \label{fig:}
    \end{figure}
    
    \newpage
    
    "  
}


#################################################################################
function doc_header
{ 
  echo "\documentclass[frenchb,9pt]{report}
\usepackage[T1]{fontenc}
%\usepackage[scaled]{uarial}
\usepackage{palatino}
%\usepackage{times}
%\renewcommand*\familydefault{\sfdefault} %% Only if the base font of the document is to be sans serif

\usepackage[latin1]{inputenc}
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{graphics}
\usepackage{array}
\usepackage[a4paper,$PageFormat,left=0.5cm,right=0.5cm,top=0.5cm,bottom=0.5cm]{geometry}
\usepackage{natbib}
\usepackage{babel}
\usepackage{pdfpages}
\usepackage[breaklinks=true,linktocpage=true,linkbordercolor={1 1 1},colorlinks=true,citecolor=blue,linkcolor=blue,urlcolor=blue,backref]{hyperref} % Package for links in PDF file

%\usepackage{stmaryrd}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{times}
\usepackage{color}
%\usepackage{psfig}

\usepackage{caption}
\captionsetup{justification=justified}

\usepackage{grffile} % to allow includegraphics with dots in the filename, ex. smallworld-k-8.0.eps

\linespread{1.1}

\begin{document}

"
}


#################################################################################
function doc_foot
{ 
  echo "
  
  
\end{document}"
}


#################################################################################
function get_absolute_path
{
  local curDir=`pwd`
  local fileDir=`basename $1`

  cd $fileDir
  local result=`pwd`
  cd $curDir
  return $result
}

#################################################################################

if [ "$1" = "--help" ]; then
  showhelp
fi

OutputBasename=$1; shift
PageFormat=$1; shift
TargetDir=$1; shift
NbFigPerPage=$1; shift
NbFigWidth=$1; shift
NbFigHeight=$1; shift
Figures=$*

NbPages=`bc <<< "scale=0;($#/$NbFigPerPage)+1"`



doc_header > $OutputBasename.tex

for nothing in `seq -s " " 1 $NbPages`; do
  title="`dirname $1`"
  figsInPage="$1 "; shift

  for nothing in `seq 2 $NbFigPerPage`; do
    title="$title | `dirname $1`"
    figsInPage="$figsInPage $1"; shift
  done
  
  makepage "$title" $figsInPage
  
  if [ "$#" = "0" ]; then
    break
  fi
done >> $OutputBasename.tex

doc_foot >> $OutputBasename.tex

mv $OutputBasename.tex $TargetDir/$OutputBasename.tex

