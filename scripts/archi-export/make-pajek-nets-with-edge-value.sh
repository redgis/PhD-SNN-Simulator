#!/bin/bash 

#generate pajek files
nbrep=$#
nbdone=0

if [ "$1" = "--help" -o "$#" = "0" ] ; then
  echo "use: $0 <PG directory list>
Converts architecture in PG dirs to pajek .pajek format : 2 files generated. One with weights as edge values, one with delays."
  exit 1
fi


for rep in $*
do
  nbdone=$((nbdone+1))
  archbasename=`echo $rep | grep -o -R "sw-.*-[0-9][0-9]"`
  archifile=`echo "$rep/$archbasename.xml"`
  echo -n "Doing $archifile ... ($nbdone / $nbrep)... "
  echo -n "with delays... "
  ./topajek $archifile $rep/$archbasename-d.pajek -d > /dev/null
  echo -n "with weights... "
  ./topajek $archifile $rep/$archbasename-w.pajek -w > /dev/null
  echo "done."
done
