#!/bin/sh

if [ "$1" = "--help" ]; then
  echo "use: $0 <architecture>
Convert architecture to pajek .net format."
  exit 1
fi

make

pajeknetname=`basename $1 .xml`

./topajek $1 architectures/pajek/$pajeknetname.net


