#!/bin/bash

if [ "$1" = "--help" ]; then
  echo "use: $0 <start> <end> <show class (0 or 1)>
Generates the spike rasterplot between to given times. Third argument si for displaying the input class presented"
  exit 1
fi

#set limits to write in eps output filename
min=`printf "%010d" $1`
max=`printf "%010d" $2`

#generate pattern classes
echo "Generating pattern classes ..."

if [ "$3" == "1" ]; then
  ./scripts/OLD/make-patternsclasses.sh $1 $2
else
  echo "" > plots/PatternClasses-$min-$max.plot
fi


ExcitFirstfile=`ls -1 data/SpikeRasterExcit-*.txt | head -n 1 | cut -d "." -f 1 | cut -d "-" -f 2`
ExcitLastfile=`ls -1 data/SpikeRasterExcit-*.txt | tail -n 1 | cut -d "." -f 1 | cut -d "-" -f 2`
InhibFirstfile=`ls -1 data/SpikeRasterInhib-*.txt | head -n 1 | cut -d "." -f 1 | cut -d "-" -f 2`
InhibLastfile=`ls -1 data/SpikeRasterInhib-*.txt | tail -n 1 | cut -d "." -f 1 | cut -d "-" -f 2`

# get files numbers in which are the data we need. e.g. : 00001 to 00004 for excitatory spikes and 00000 and 00001 for inhib
echo "Getting file numbers in which are the data we need ..."

for fichier in `ls data/SpikeRasterExcit-*.txt`
do
  fichierMin=`head -n 1 $fichier | cut -d ' ' -f "1"`
  fichierMax=`tail -n 1 $fichier | cut -d ' ' -f "1"`
  
  if (( $1 >= $fichierMin && $1 <= $fichierMax )); then
    ExcitFirstfile=`echo $fichier | cut -d "." -f 1 | cut -d "-" -f 2`
    echo "First excit: $fichier : [ $fichierMin , $fichierMax ]"
    break
  fi
done

for fichier in `ls data/SpikeRasterExcit-*.txt`
do
  fichierMin=`head -n 1 $fichier | cut -d ' ' -f "1"`
  fichierMax=`tail -n 1 $fichier | cut -d ' ' -f "1"`
  
  if (( $2 >= $fichierMin && $2 <= $fichierMax )); then
    ExcitLastfile=`echo $fichier | cut -d "." -f 1 | cut -d "-" -f 2`
    echo "Last  excit: $fichier : [ $fichierMin , $fichierMax ]"
    break
  fi
done

for fichier in `ls data/SpikeRasterInhib-*.txt`
do
  fichierMin=`head -n 1 $fichier | cut -d ' ' -f "1"`
  fichierMax=`tail -n 1 $fichier | cut -d ' ' -f "1"`
  
  if (( $1 >= $fichierMin && $1 <= $fichierMax )); then
    InhibFirstfile=`echo $fichier | cut -d "." -f 1 | cut -d "-" -f 2`
    echo "First inhib: $fichier : [ $fichierMin , $fichierMax ]"
    break
  fi
done

for fichier in `ls data/SpikeRasterInhib-*.txt`
do
  fichierMin=`head -n 1 $fichier | cut -d ' ' -f "1"`
  fichierMax=`tail -n 1 $fichier | cut -d ' ' -f "1"`
  
  if (( $2 >= $fichierMin && $2 <= $fichierMax )); then
    InhibLastfile=`echo $fichier | cut -d "." -f 1 | cut -d "-" -f 2`
    echo "Last  inhib: $fichier : [ $fichierMin , $fichierMax ]"
    break
  fi
done


# for valeur in `seq $1 10 \`echo "$1+20000"|bc\``
# do
#   ExcitFirstfile=`grep -H --max-count=1 -E "^$valeur[ ]" data/SpikeRasterExcit-*.txt | cut -d "." -f 1 | cut -d "-" -f 2`
#   if [ "$ExcitFirstfile" != "" ]; then
#     echo "excit: $valeur $ExcitFirstfile"
#     break
#   fi
# done
# 
# 
# for valeur in `seq $2 -10 \`echo "$2-20000"|bc\``
# do
#   ExcitLastfile=`grep -H --max-count=1 -E "^$valeur[ ]" data/SpikeRasterExcit-*.txt | cut -d "." -f 1 | cut -d "-" -f 2`
#   if [ "$ExcitLastfile" != "" ]; then
#     echo "excit: $valeur $ExcitLastfile"
#     break
#   fi
# done
# 
# 
# for valeur in `seq $1 10 \`echo "$1+20000"|bc\``
# do
#   InhibFirstfile=`grep -H --max-count=1 -E "^$valeur[ ]" data/SpikeRasterInhib-*.txt | cut -d "." -f 1 | cut -d "-" -f 2`
#   if [ "$InhibFirstfile" != "" ]; then
#     echo "inhib: $valeur $InhibFirstfile"
#     break
#   fi
# done
# 
# 
# for valeur in `seq $2 -10 \`echo "$2-20000"|bc\``
# do
#   InhibLastfile=`grep -H --max-count=1 -E "^$valeur[ ]" data/SpikeRasterInhib-*.txt | cut -d "." -f 1 | cut -d "-" -f 2`
#   if [ "$InhibLastfile" != "" ]; then
#     echo "inhib: $valeur $InhibLastfile"
#     break
#   fi
# done



#generate data filenames to plot
tmpFilename=""
ExcitFilenames=""

for num_fichier in `seq $ExcitFirstfile $ExcitLastfile`
do
  ExcitFilenames=`printf "SpikeRasterExcit-%05d.txt $ExcitFilenames" $num_fichier`
done

InhibFilenames=""

for num_fichier in `seq $InhibFirstfile $InhibLastfile`
do
  InhibFilenames=`printf "SpikeRasterInhib-%05d.txt $InhibFilenames" $num_fichier`
done



#generate the gnuplot script
echo "Generating the gnuplot script ..."

echo -n "################################################
reset

set terminal postscript eps enhanced color defaultplex \"Times-Roman\" 12
set output 'figures/SpikeRasterPlot--$min-$max.eps'

#set terminal png giant enhanced notransparent
#set output 'figures/SpikeRasterPlot--$min-$max.png'


#set size ratio 0.25
#set size 6.0,1.0
#set size 16,12

set xlabel 'Time [s]'
set ylabel '# neurons'

set pointsize 0.1

set style line 1 lt 1 lw 1 pt 1
set style line 2 lt 2 lw 1 pt 2
set style line 3 lt 3 lw 1 pt 3
set style line 4 lt 4 lw 1 pt 4
set style line 5 lt 5 lw 1 pt 5
set style line 6 lt 6 lw 1 pt 6
set style line 7 lt 7 lw 1 pt 7
set style line 8 lt 8 lw 1 pt 8
set style line 9 lt 9 lw 1 pt 9

set xrange [($1/10000.0):($2/10000.0)]
set title 'Spike raster plot'

" > plots/SpikeRasterPlot--$min-$max.plot

if [ "$3" == "1" ]; then
  echo "load 'plots/PatternClasses-$min-$max.plot'" >> plots/SpikeRasterPlot--$min-$max.plot
fi

echo -n "
plot " >> plots/SpikeRasterPlot--$min-$max.plot

#end of the gnuplot file, generate the "plot" command

echo "fichiers : $ExcitFilenames $InhibFilenames"

premierFichier=1

for fichier in $ExcitFilenames
do
  if [ $premierFichier == 1 ]; then
    printf "'data/$fichier' using ((\$1)/10000.0):2 with dots ls 1 title 'Excitatory spikes'" >> plots/SpikeRasterPlot--$min-$max.plot
    premierFichier=0
  else
    printf ", \\\\\n     'data/$fichier' using ((\$1)/10000.0):2 with dots ls 1 notitle" >> plots/SpikeRasterPlot--$min-$max.plot
  fi
done

premierFichier=1

for fichier in $InhibFilenames
do
  if [ $premierFichier == 1 ]; then
    printf ", \\\\\n     'data/$fichier' using ((\$1)/10000.0):2 with dots ls 2 title 'Inhibitory spikes'" >> plots/SpikeRasterPlot--$min-$max.plot
    premierFichier=0
  else
    printf ", \\\\\n     'data/$fichier' using ((\$1)/10000.0):2 with dots ls 2 notitle" >> plots/SpikeRasterPlot--$min-$max.plot
  fi
done

echo ", 256 ls 3" >> plots/SpikeRasterPlot--$min-$max.plot

echo "done."

echo "Gnuplot-ing the raster 'figures/SpikeRasterPlot--$min-$max.eps' ..."
gnuplot plots/SpikeRasterPlot--$min-$max.plot && epstopdf figures/SpikeRasterPlot--$min-$max.eps
evince figures/SpikeRasterPlot--$min-$max.eps &
#acroread figures/SpikeRasterPlot--$min-$max.pdf & 
echo "done..."


