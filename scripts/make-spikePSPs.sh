#!/bin/bash

#regexp1="^[0-9]*[,]$1[ ]"

#for time in `seq $1 $2`
#do
#  regexp2="^[0-9]*[,]$time[ ]|$regexp1"
#  regexp1=$regexp2
  #echo $regexp1
#done

#echo $regexp1
#grep -E "$regexp1" SpikePSPs-*.txt

if [ "$1" = "--help" ]
then
  echo "usage : $0 <lower_value> <upper_value>"
  echo " WARNING : do not use directly. This script is used by make-rasterplot.sh"
  exit 1
fi

#cd data/
#cd ../data/

rm data/SpikePSPs.txt


# get files numbers in which are the data we need. e.g. : 00001 to 00004
echo "Getting file numbers in which are the data we need ..."

firstfile=`ls -1 data/SpikePSPs-*.txt | head -n 1 | cut -d "." -f 1 | cut -d "-" -f 2`
lastfile=`ls -1 data/SpikePSPs-*.txt | tail -n 1 | cut -d "." -f 1 | cut -d "-" -f 2`

for fichier in `ls data/SpikePSPs-*.txt`
do
  fichierMin=`head -n 1 $fichier | cut -d ' ' -f "1" | cut -d ',' -f "2"`
  fichierMax=`tail -n 1 $fichier | cut -d ' ' -f "1" | cut -d ',' -f "2"`
  
  if (( $1 >= $fichierMin && $1 <= $fichierMax )); then
    firstfile=`echo $fichier | cut -d "." -f 1 | cut -d "-" -f 2`
    echo "First : $fichier : [ $fichierMin , $fichierMax ]"
    break
  fi
done

for fichier in `ls data/SpikePSPs-*.txt`
do
  fichierMin=`head -n 1 $fichier | cut -d ' ' -f "1" | cut -d ',' -f "2"`
  fichierMax=`tail -n 1 $fichier | cut -d ' ' -f "1" | cut -d ',' -f "2"`
  
  if (( $2 >= $fichierMin && $2 <= $fichierMax )); then
    lastfile=`echo $fichier | cut -d "." -f 1 | cut -d "-" -f 2`
    echo "Last : $fichier : [ $fichierMin , $fichierMax ]"
    break
  fi
done


# for valeur in `seq $1 100 \`echo "$1+20000"|bc\``
# do
#   firstfile=`grep -H --max-count=1 -E "^[0-9]*[,]$valeur[ ]" SpikePSPs-*.txt | cut -d "." -f 1 | cut -d "-" -f 2`
#   if [ "$firstfile" != "" ]; then
#     echo "$valeur $firstfile"
#     break
#   fi
# done
# 
# for valeur in `seq $2 -100 \`echo "$2-20000"|bc\``
# do
#   lastfile=`grep -H --max-count=1 -E "^[0-9]*[,]$valeur[ ]" SpikePSPs-*.txt | cut -d "." -f 1 | cut -d "-" -f 2`
#   if [ "$lastfile" != "" ]; then
#     echo "$valeur $lastfile"
#     break
#   fi
# done


#Copy data of files into SpikePSPs.txt
echo "Copying data of files into SpikePSPs.txt..."
for num_fichier in `seq $firstfile $lastfile`
do
  filename=`printf "data/SpikePSPs-%05d.txt" $num_fichier`
  cat $filename >> data/SpikePSPs.txt
  echo -n "$filename... "
done

echo "done."
echo "and this is it."

