/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#define MAX_DELAY 200

// ANSI C
#include <stdlib.h>
#include <string.h>

// C++
#include <iostream>
#include <fstream>

using namespace std;

int main (int argc, char * argv[])
{
  if (argc != 5 + 1)
  {
    cerr << "*** ERROR : expecting argument." << endl;
    cerr << "    Usage : " << argv[0] << " <start> <end> <from neuron> <to neuron> <spike data file>" << endl;
    exit (1);
  }
  


  long int Start = atoi (argv[1]);
  long int End = atoi (argv[2]);
  long int firstNeuronID = atoi(argv[3]);
  long int lastNeuronID = atoi(argv[4]);
  
  long int NbNeurons = lastNeuronID - firstNeuronID +1;

  long int TimeSpan = End - Start + 2;
    
  ifstream * SpikeFile = new ifstream (argv[5], ios::in);
  
  long int SpikeTime = 0;
  long int NeuronId = 0;
  
  /* Memory space for rasterplot withing given time interval */
  bool SpikeRaster [NbNeurons][TimeSpan];
  //memset (&SpikeRaster, false, NbNeurons * TimeSpan * sizeof (bool));
  for (int N1 = 0; N1 < NbNeurons; N1++)
    for (SpikeTime = 0; SpikeTime < TimeSpan; SpikeTime++)
        SpikeRaster [N1][SpikeTime] = false;

  while ( !(SpikeFile->eof ()) && (SpikeTime <= End) )
  {
    //cout << SpikeTime << "\t\r";
    (*SpikeFile) >> SpikeTime;
    (*SpikeFile) >> NeuronId;
    if ( (NeuronId >= firstNeuronID) && (NeuronId < lastNeuronID) && (SpikeTime >= Start) && (SpikeTime < End) )
    { SpikeRaster[NeuronId - firstNeuronID][SpikeTime - Start] = true; }
  }
  //cout << endl;

  /* Data structure for statistics */
  //int SpikeDistrib [NbNeurons][NbNeurons][MAX_DELAY];
  int * SpikeDistrib;
  //cout << (NbNeurons * NbNeurons * MAX_DELAY * sizeof (int))/(1024*1024) << "Mo" << endl;
  SpikeDistrib = (int *) malloc ((NbNeurons+1) * (NbNeurons+1) * (MAX_DELAY+1) * sizeof (int));

  for (int N1 = 0; N1 < NbNeurons; N1++)
  {
    for (int N2 = 0; N2 < NbNeurons; N2++)
    {
      for (int Delay = 0; Delay < MAX_DELAY; Delay++)
      {
        //cout << N1 << "\t/" << NbNeurons << "\t\r" << flush;
        *(SpikeDistrib+(NbNeurons*NbNeurons*Delay + NbNeurons*N1 + N2)) = 0;
      }
    }
  }
  //cout << endl;

  /* Statistical result statistics */
  for (int N1 = 0; N1 < NbNeurons; N1++) // Foreach neuron N1
  {
    //cout << N1 << "\r";
    for (SpikeTime = 0; SpikeTime <= TimeSpan; SpikeTime ++)
    {
      if (SpikeRaster[N1][SpikeTime] == true) // Foreach spike of N1
      {
        for (int N2 = 0; N2 < NbNeurons; N2++)
        {
          if (N2 != N1)
          {
            for (int Delay = 0; Delay < MAX_DELAY; Delay++) // Foreach spike of N2, after N1, within MAX_DELAY
            {
              if (SpikeRaster[N2][SpikeTime+Delay] == true)
              {
                (*(SpikeDistrib+(NbNeurons*NbNeurons*Delay + NbNeurons*N1 + N2))) = (*(SpikeDistrib+(NbNeurons*NbNeurons*Delay + NbNeurons*N1 + N2))) + 1;
              }
            }
          }
        }
      }
    }
  }

  for (int N1 = 0; N1 < NbNeurons; N1++)
  {
    for (int N2 = N1; N2 < NbNeurons; N2++)
    {
      if (N1 != N2)
      {
        cout << firstNeuronID + N1 << " " << firstNeuronID + N2 << " ";
        for (int Delay = 0 ; Delay < MAX_DELAY; Delay++)
        {
          cout << *(SpikeDistrib+(NbNeurons*NbNeurons*Delay + NbNeurons*N1 + N2)) << " ";
        }
        cout << endl;
      }
    }
    cout << endl;
  }
  return 0;
}
