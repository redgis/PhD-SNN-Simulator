/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "graph-properties.hpp"

bool do_all = false; // A
bool do_all_simple = false; // a
bool do_all_vector = false; // v

bool basic_properties = false; // b
bool path_length_properties = false; // s
bool path_length_histogram_properties = false; // S
bool degree_properties = false; // d
bool alldegrees_properties = false; // e
bool indegrees_properties = false; // i
bool outdegrees_properties = false; // o
bool vertices_betweeness_properties = false; // f
bool clustering_properties = false; // c

igraph_t graph;


/*************************************************************************************/
void help ()
{
  cout << "Print properties of a graph." << endl;
  cout << "  Usage: graph-properties <pajek formated file> [-a] [-b] [-p] [-P] [-d]" << endl;
  cout << "         -A all possible properties" << endl;
  cout << "         -a all possible simple properties" << endl;
  cout << "         -v all possible vector properties" << endl;
  cout << "         -b basic properties (nodes, edges, directed or not...)" << endl;
  cout << "         -p weighted path length properties" << endl;
  cout << "         -P path length histogram properties" << endl;
  cout << "         -d basic degree properties" << endl;
  cout << "         -e degrees of all nodes" << endl;
  cout << "         -i indegrees of all nodes" << endl;
  cout << "         -o outdegrees of all nodes" << endl;
  cout << "         -f betweeness of all vertices" << endl;
  cout << "         -c clustering properties" << endl;

  exit (1);
}


/*************************************************************************************/
void parseArg (char * argument)
{
  if (argument[0] == '-')
    switch (argument[1])
    {
      case 'A':
        do_all = true;
        break;

      case 'a':
        do_all_simple = true;
        break;

      case 'v':
        do_all_vector = true;
        break;

      case 'b':
        basic_properties = true;
        break;

      case 'p':
        path_length_properties = true;
        break;

      case 'P':
        path_length_histogram_properties = true;
        break;

      case 'd':
        degree_properties = true;
        break;

      case 'e':
        alldegrees_properties = true;
        break;

      case 'i':
        indegrees_properties = true;
        break;

      case 'o':
        outdegrees_properties = true;
        break;

      case 'f':
        vertices_betweeness_properties = true;
        break;

      case 'c':
        clustering_properties = true;
        break;

      default:
        help ();
        break;
    }
  else
    help ();
}


/*************************************************************************************/
void get_basic_properties ()
{
  cout << "vertex count: " << igraph_vcount(&graph) << endl;
  cout << "edge count: " << igraph_ecount(&graph) << endl;
  cout << "is directed: " << igraph_is_directed(&graph) << endl;
}


/*************************************************************************************/
void get_path_length_histogram_properties (bool show = true)
{
  /*
   * graph: The input graph.
   * res: Pointer to an initialized vector, the result is stored here. The first (i.e. zeroth) element contains the number of shortest paths of length 1, etc. The supplied vector is resized as needed.
   * unconnected: Pointer to a real number, the number of pairs for which the second vertex is not reachable from the first is stored here.
   * directed: Whether to consider directed paths in a directed graph (if not zero). This argument is ignored for undirected graphs.
   */
  igraph_vector_t histogram_vector;
  igraph_real_t unreachables;
  int graph_size;
  igraph_vector_init (&histogram_vector, 0);
  igraph_path_length_hist(&graph, &histogram_vector, &unreachables, true);
  graph_size = igraph_vector_size (&histogram_vector);

  for (int index = 0; index < graph_size; index++)
  {
    if (show)
      cout << index+1 << "\t" << VECTOR(histogram_vector)[index] << endl;
  }
  cout << "unconnected pairs of vertices: " << unreachables << endl;
}


/*************************************************************************************/
void get_path_length_properties ()
{
  /* graph: The graph object.
   * res: Pointer to a real number, this will contain the result.
   * directed: Boolean, whether to consider directed paths. Ignored for undirected graphs.
   * unconn: What to do if the graph is not connected. If TRUE the average of thr geodesics within the components will be returned, otherwise the number of vertices is used for the length of non-existing geodesics. (The rationale behind this is that this is always longer than the longest possible geodesic in a graph.)
   */
  igraph_real_t res;
  igraph_average_path_length(&graph, &res, true, true);
  cout << "average path length: " << res << endl;

  /*
   * graph: The input graph.
   * name: The name of the attribute.
   * eids: The edges to query.
   * result: Pointer to an initialized vector, the result is stored here. It will be resized, if needed.
   */
  igraph_vector_t weights;
  igraph_vector_init (&weights, 0);
  igraph_cattribute_EANV(&graph, "weight", igraph_ess_all(IGRAPH_EDGEORDER_FROM), &weights);
  //cout << VECTOR(weights)[0] << " " << VECTOR(weights)[1] << " " << VECTOR(weights)[2] << " " << endl;
  /*
   * graph: The input graph, can be directed.
   * res: The result, a matrix. Each row contains the distances from a single source, in the order of vertex ids. Unreachable vertices has distance IGRAPH_INFINITY.
   * from: The source vertices.
   * weights: The edge weights. They must be all non-negative for Dijkstra's algorithm to work. An error code is returned if there is a negative edge weight in the weight vector. If this is a null pointer, then the unweighted version, igraph_shortest_paths() is called.
   * mode: For directed graphs; whether to follow paths along edge directions (IGRAPH_OUT), or the opposite (IGRAPH_IN), or ignore edge directions completely (IGRAPH_ALL). It is ignored for undirected graphs.
   */
  long int avgres = 0;
  int nbpaths = 0;
  int nbcol, nbrow;
  igraph_matrix_t matres;
  igraph_matrix_init(&matres, 0, 0);
  //igraph_matrix_fill (&matres, 0);
  igraph_shortest_paths_dijkstra(&graph, &matres, igraph_vss_all(), igraph_vss_all(), &weights, IGRAPH_OUT);
  //igraph_shortest_paths_bellman_ford(&graph, &matres, igraph_vss_all(), &weights, IGRAPH_OUT);
  nbrow = igraph_matrix_nrow(&matres);
  nbcol = igraph_matrix_ncol(&matres);
  for (long int i = 0; i < nbrow; i++)
  {
    for (long int j = 0; j < nbcol; j++)
    {
      if (MATRIX(matres, i, j) != IGRAPH_INFINITY)
      {
        avgres += MATRIX(matres, i, j);
        nbpaths++;
      }
    }
  }

  cout << "average weighted path length: " << (double)avgres/(double)nbpaths/*igraph_matrix_size(&matres)*/ << endl;

  get_path_length_histogram_properties(false);
}


/*************************************************************************************/
void get_outdegrees_properties (bool show = true)
{
  /*
   * graph: The graph.
   * res: Vector, this will contain the result. It should be initialized and will be resized to be the appropriate size.
   * vids: Vector, giving the vertex ids of which the degree will be calculated.
   * mode: Defines the type of the degree. IGRAPH_OUT, out-degree, IGRAPH_IN, in-degree, IGRAPH_ALL, total degree (sum of the in- and out-degree). This parameter is ignored for undirected graphs.
   * loops: Boolean, gives whether the self-loops should be counted.
   */
  igraph_vector_t degree;
  igraph_vector_init(&degree,0);
  igraph_degree(&graph, &degree, igraph_vss_all(), IGRAPH_OUT, true);
  int res_size = igraph_vector_size (&degree);
  long int sum = 0;
  for (int index = 0; index < res_size; index ++)
  {
    if (show)
      cout << index << "\t" << VECTOR(degree)[index] << endl;
    sum += VECTOR(degree)[index];
  }
  cout << "average outdegree: " << (double)sum / (double)res_size << endl;
}


/*************************************************************************************/
void get_indegrees_properties (bool show = true)
{
  igraph_vector_t degree;
  igraph_vector_init(&degree,0);
  igraph_degree(&graph, &degree, igraph_vss_all(), IGRAPH_IN, true);
  int res_size = igraph_vector_size (&degree);
  long int sum = 0;
  for (int index = 0; index < res_size; index ++)
  {
    if (show)
      cout << index << "\t" << VECTOR(degree)[index] << endl;
    sum += VECTOR(degree)[index];
  }
  cout << "average indegree: " << (double)sum / (double)res_size << endl;
}


/*************************************************************************************/
void get_alldegrees_properties (bool show = true)
{
  /*
   * graph: The graph.
   * res: Vector, this will contain the result. It should be initialized and will be resized to be the appropriate size.
   * vids: Vector, giving the vertex ids of which the degree will be calculated.
   * mode: Defines the type of the degree. IGRAPH_OUT, out-degree, IGRAPH_IN, in-degree, IGRAPH_ALL, total degree (sum of the in- and out-degree). This parameter is ignored for undirected graphs.
   * loops: Boolean, gives whether the self-loops should be counted.
   */
  igraph_vector_t degree;
  igraph_vector_init(&degree,0);
  igraph_degree(&graph, &degree, igraph_vss_all(), IGRAPH_ALL, true);
  int res_size = igraph_vector_size (&degree);
  long int sum = 0;
  for (int index = 0; index < res_size; index ++)
  {
    if (show)
      cout << index << "\t" << VECTOR(degree)[index] << endl;
    sum += VECTOR(degree)[index];
  }
  cout << "average degree: " << (double)sum / (double)res_size << endl;
}


/*************************************************************************************/
void get_vertices_betweeness_properties (bool show = true)
{
  igraph_vector_t weights;
  igraph_vector_init (&weights, 0);
  igraph_cattribute_EANV(&graph, "weight", igraph_ess_all(IGRAPH_EDGEORDER_FROM), &weights);

  igraph_vector_t res;
  igraph_vector_init (&res, 0);
  igraph_betweenness(&graph, &res, igraph_vss_all (), true, &weights, 0);

  int res_size = igraph_vector_size (&res);
  long int sum = 0;
  for (int index = 0; index < res_size; index ++)
  {
    if (show)
      cout << index << "\t" << VECTOR(res)[index] << endl;
    sum += VECTOR(res)[index];
  }
  cout << "average betweeness: " << (double)sum / (double)res_size << endl;
}


/*************************************************************************************/
void get_degree_properties ()
{
  /*
   * graph: The input graph object.
   * res: Pointer to a real number, the result will be stored here.
   * loops: Logical constant, whether to include loops in the calculation. If this constant is TRUE then loop edges are thought to be possible in the graph (this does not neccessary means that the graph really contains any loops). If this FALSE then the result is only correct if the graph does not contain loops.
   */
  igraph_real_t res;
  igraph_density(&graph, &res, true);
  cout << "density: " << res << endl;

  /*
   * graph: The input graph.
   * res: Pointer to an integer (igraph_integer_t), the result will be stored here.
   * mode: Defines the type of the degree. IGRAPH_OUT, out-degree, IGRAPH_IN, in-degree, IGRAPH_ALL, total degree (sum of the in- and out-degree). This parameter is ignored for undirected graphs.
   * loops: Boolean, gives whether the self-loops should be counted.
   */
  igraph_integer_t maxdegree;
  igraph_maxdegree(&graph, &maxdegree,
           igraph_vss_all(), IGRAPH_IN, true);
  cout << "max indegree: " << maxdegree << endl;
  igraph_maxdegree(&graph, &maxdegree,
             igraph_vss_all(), IGRAPH_OUT, true);
  cout << "max outdegree: " << maxdegree << endl;
  igraph_maxdegree(&graph, &maxdegree,
             igraph_vss_all(), IGRAPH_ALL, true);
  cout << "max degree: " << maxdegree << endl;

  //average degrees
  get_outdegrees_properties(false);
  get_indegrees_properties(false);
  get_alldegrees_properties(false);

  //betweeness
  get_vertices_betweeness_properties (false);
}


/*************************************************************************************/
void get_clustering_properties ()
{
  igraph_real_t res;
  igraph_transitivity_undirected(&graph, &res, IGRAPH_TRANSITIVITY_ZERO);
  cout << "transitivity: " << res << endl;
  igraph_transitivity_avglocal_undirected(&graph, &res, IGRAPH_TRANSITIVITY_ZERO);
  cout << "average local transitivity: " << res << endl;
  /*igraph_modularity(&graph, membership,
            igraph_real_t *modularity,
          const igraph_vector_t *weights);
          */
}


/*************************************************************************************/
bool loadFromPajekFile (char * pajekfilename, igraph_t *graph)
{
  bool result = true;
  string errorMessage;
  FILE * fileobj;
  int errorCode;

  fileobj = fopen (pajekfilename, "r");

  if (fileobj == NULL)
  {
    errorMessage = string("ERROR: couldn't open \"") + pajekfilename + "\"";
    perror (errorMessage.c_str());
    result = false;
  }

  if (result)
  {
    errorCode = igraph_read_graph_pajek(graph, fileobj);
    if (errorCode)
    {
      // normaly the default error handler of igraph is printing a message and aborting the program (exiting)
      cout << "ERROR: couldn't load \"" << pajekfilename << "\"" << igraph_strerror(errorCode) << endl;
      result = false;
    }

    fclose (fileobj);
  }

  return result;
}


/*************************************************************************************/
int main (int argc, char * argv[])
{
  igraph_i_set_attribute_table(&igraph_cattribute_table);


  /* Manage arguments */
  if (argc >= 3)
  {
    if (loadFromPajekFile (argv[1], &graph))
    {
      for (int Index = 2; Index < argc; Index++)
      {
        parseArg(argv[Index]);
      }
    }
    else
      help();
  }
  else
  {
    help ();
  }


  /* File loaded successfully, begin analysis */

  if (basic_properties or do_all or do_all_simple)
    get_basic_properties ();

  if (path_length_properties or do_all or do_all_simple)
    get_path_length_properties ();

  if (path_length_histogram_properties or do_all or do_all_vector)
    get_path_length_histogram_properties ();

  if (degree_properties or do_all or do_all_simple)
    get_degree_properties ();

  if (alldegrees_properties or do_all or do_all_vector)
    get_alldegrees_properties ();

  if (indegrees_properties or do_all or do_all_vector)
    get_indegrees_properties ();

  if (outdegrees_properties or do_all or do_all_vector)
    get_outdegrees_properties ();

  if (vertices_betweeness_properties or do_all or do_all_vector)
    get_vertices_betweeness_properties ();

  if (clustering_properties or do_all or do_all_simple)
    get_clustering_properties ();

  return 0;
}
