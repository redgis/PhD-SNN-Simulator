/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef _PG_SEARCH_H 
#define _PG_SEARCH_H 

//#include "../../globals.h"

/********************** Headers includes ******************/

using namespace std;

#define _GLOBALS_H

// ANSI C++
#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
#include <sstream>
#include <exception>
#include <list>
#include <set>

// STL C++
#include <vector>
#include <map>
#include <algorithm>
#include <queue>

extern "C" {

// ANSI C
#include <stdlib.h>
#include <math.h>

// UNIX/Linux/POSIX C includes
#include <unistd.h>
#include <sys/time.h>
#include <semaphore.h>
  
}

// Other headers
#include "tinyxml/tinyxml.h"
#include "common/tools.h"

class CNeuron;
class CSynapse;
class CAssembly;
class CArchitecture;
//class CEventManager;
//class CPatternManager;
//class CPatternSet;
//class CPattern;
//class CDataFile;
//class CManager;
class CParameters;
//class CGraphManager;
//class CGnuplotGraph;


/************** type definitions **************/

/* usual types */  
typedef long int NNLtime;
typedef double NNLweight;
typedef double NNLpotential;
  
typedef enum { FALSE, TRUE, UNSPECIFIED } NNLbool;


typedef enum eEvtType {EVT_SPIKE, EVT_PSP} TEventType;

/*****************************************************************************/
typedef struct sEvent
{
  TEventType evtType;
  CNeuron * EmittingNeuron;
  CNeuron * ImpactedNeuron;
  NNLtime Time;
  NNLpotential PSPWeight;
} TEvent;

/*****************************************************************************/
typedef struct sSpike
{
  CNeuron * EmittingNeuron;
  NNLtime Time;
  list <TEvent *> * PSPList;
} TSpike;

//~ typedef enum {
  //~ EVT_NULL =                 0,  /* empty event */
  //~ EVT_SPIKE =                1,  /* spike event */
  //~ EVT_SPIKE_IMPACT =         2,  /* spike impact (after axonal transmission) */
  //~ EVT_SAVE_SPIKE_PSP =       4,  /* log a spike and its PSPs (used for PGs monitoring) */
  //~ EVT_RECURSIVE =            8,  /* Treat a recursive event */
  //~ } NNLeventType;
  
//~ /* Event type */
//~ class NNLevent
//~ {
  //~ public :
  //~ NNLevent ();
  //~ virtual ~NNLevent () {};
  
  //~ /* ALL events */  
  //~ NNLeventType EventType;       /* what sort of event is it ? */
  //~ NNLtime Time;                 /* occuring time */
  
  //~ /* EVT_SPIKE and EVT_SPIKE_IMPACT and EVT_SAVE_SPIKE_PSP */
  //~ CNeuron * EmittingNeuron;     /* neuron emiting the spike */
  
  //~ /* EVT_SPIKE_IMPACT */
  //~ CSynapse * ImpactingSynapse;  /* synapse impacting post-syn neuron */
  
  //~ /* EVT_SPIKING_RATIO and EVT_RECURSIVE */
  //~ CAssembly * Assembly;
  
  //~ /* EVT_RECURSIVE */
  //~ NNLtime End;            /* Last occurence (the start time is the first post of the occurence */
  //~ NNLtime RecurssionStep; /* Event will occur every RecurssionStep time units */
  //~ void (*func) (CAssembly*);        /* Function containing the desired recursive treatment */
  
  
  //~ bool operator < (NNLevent const &);
  //~ bool operator == (NNLevent const &);
  
  //~ friend bool operator < (NNLevent const &, NNLevent const &);
  //~ friend bool operator == (NNLevent const &, NNLevent const &);
//~ };


/* Parameters of a neuron */
typedef struct sNeuronParameters
{
  NNLpotential Threshold;           /* Neuron threshold */
  NNLpotential MembranePotential;   /* Neuron potential */
  NNLpotential AbsoluteRefractoryPotential; /* Neuron absolute refractory potential */
  NNLtime AbsoluteRefractoryPeriode;        /* Neuron absolute refractory periode   */
  float Inhibitory;                 /* Probability for the neuron to be inhibitory  */
  NNLtime TauPSP;                   /* Time constante for a PSP */
  NNLtime TauRefract;
} NeuronParameters;


/* Parameters of a synapse */
typedef struct sSynapseParameters
{
  NNLweight Weight;       /* Synaptic weight */
  NNLtime Delay;          /* Axonal delay before the synapse. -1 = random */
  NNLtime DeltaDelay;     /* Variations around Delay if not -1  */
  NNLweight DeltaWeight;  /* Variations around Weight if not -1 */
  bool WeightAdaptation;  /* Do we make weight adaptation ?   */
  bool DelayAdaptation;   /* Do we make delay adaptation ?    */
  NNLbool Inhibitory;     /* The synapse will be inhibitory ? */
} SynapseParameters;


/* Parameters of a projection */
typedef struct sProjectionParameters
{
  float ProjectionRate;       /* probability of connexion - between 0 and 1, -1 = randomized */
  SynapseParameters SynParam; /* parameters of synapses of the projection */
} ProjectionParameters;


/* Pattern presentation method, used by the pattern manager */
typedef enum { NNL_RAND, NNL_FILE_ORDER, NNL_ALTERNATE_CLASS } NNLPresentationMethod;

typedef enum { CLASSICAL_STDP, MEUNIER_STDP } NNLStdpType;


/* Init various data */
int init ();


/****************** Data definition ********************/

using namespace std;

extern map<int, CNeuron *> NeuronsByID;
extern vector <vector <NNLtime> > DelaysByNeuronID;
extern vector <vector <NNLweight> > WeightsByNeuronID;
extern NNLpotential * ThresholdsByNeuronID;
extern NNLtime DelaySlice;
extern NNLweight WeightSlice;

#define StayInAssembly true
//#define __DEBUG


#define NB_MAX_EVENTS 10000

#endif /* _PG_SEARCH_H */
