/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

typedef struct {
  unsigned short int magic;
  unsigned int size;
  unsigned short int reserved1;
  unsigned short int reserved2;
  unsigned int offset;
  } FILEHEADER;

typedef struct {
   unsigned int size;               /* Header size in bytes      */
   int width,height;                /* Width and height of image */
   unsigned short int planes;       /* Number of colour planes   */
   unsigned short int bits;         /* Bits per pixel            */
   unsigned int compression;        /* Compression type          */
   unsigned int imagesize;          /* Image size in bytes       */
   int xresolution,yresolution;     /* Pixels per meter          */
   unsigned int ncolours;           /* Number of colours         */
   unsigned int importantcolours;   /* Important colours         */
} INFOHEADER;


int main (int argc, char * argv [])
{
  string fromFile = argv[1];
  string toFile = fromFile+"-out";
  int NbFeatures;
  int NbClasses;
  int PatternClass;
  float Feature;
  int numFile = 0;
  char numFileTmp [1024];

  float Image[16][16];

  unsigned char Pixel[4];


  FILEHEADER FileHeader;
  INFOHEADER ImageHeader;

  cout << argv[1] << endl;

  //cout << sizeof (FileHeader.magic) << " " << sizeof (FileHeader.size) << " " << sizeof (FileHeader.reserved1) << " " << sizeof(FileHeader.reserved2) << " " << sizeof (FileHeader.offset) << endl;

  FileHeader.magic = (unsigned short int) 19778; /* BM */
  FileHeader.size = (unsigned int) (sizeof(FILEHEADER)-2 + sizeof(INFOHEADER) + 16*(16*3+2));
  FileHeader.reserved1 = (unsigned short int) 0;
  FileHeader.reserved2 = (unsigned short int) 0;
  FileHeader.offset = (unsigned int) ((unsigned int)sizeof(FILEHEADER)-2 + (unsigned int)sizeof(INFOHEADER));

  //cout << FileHeader.size << " " << sizeof (FileHeader) - 2 << " " << sizeof (ImageHeader) << endl;

  ImageHeader.size = (unsigned int) (sizeof(INFOHEADER));
  ImageHeader.width = (int) 16;
  ImageHeader.height = (int) 16;
  ImageHeader.planes = (unsigned short int) 1;
  ImageHeader.bits = (unsigned short int) 24;
  ImageHeader.compression = (unsigned int) 0;
  ImageHeader.imagesize = (unsigned int) 0;
  ImageHeader.xresolution = (int) 0;
  ImageHeader.yresolution = (int) 0;
  ImageHeader.ncolours = (unsigned int) 0;
  ImageHeader.importantcolours = (unsigned int) 0;

  /* Open stream with this filename */
  ifstream * DataFile = new ifstream (fromFile.c_str(), ios::in);
  ofstream * ImageFile;

  /* Check if the opening was successful, otherwise, means the file doesn't exist */
  if (!(*DataFile))
  {
    cerr << "File \"" << fromFile << "\" could not be open, does it exist ? Is it the right filename ?" << endl;
    exit (1);
  }

  (*DataFile) >> NbClasses >> NbFeatures;

  /* For each pattern */
  while (!(*DataFile).eof ())
  {
    /* Read the pattern class */
    (*DataFile) >> PatternClass;

    /* Create an output file */
    sprintf (numFileTmp, "%02d-%05d", PatternClass, numFile);
    ImageFile = new ofstream ((toFile+"-"+numFileTmp+".bmp").c_str(), ios::out);

    /* Write headers */
    (*ImageFile).write((char*) &FileHeader.magic, sizeof (FileHeader.magic));
    (*ImageFile).write((char*) &FileHeader.size, sizeof (FileHeader.size));
    (*ImageFile).write((char*) &FileHeader.reserved1, sizeof (FileHeader.reserved1));
    (*ImageFile).write((char*) &FileHeader.reserved2, sizeof (FileHeader.reserved2));
    (*ImageFile).write((char*) &FileHeader.offset, sizeof (FileHeader.offset));

    (*ImageFile).write((char*) &ImageHeader.size, sizeof (ImageHeader.size));
    (*ImageFile).write((char*) &ImageHeader.width, sizeof (ImageHeader.width));
    (*ImageFile).write((char*) &ImageHeader.height, sizeof (ImageHeader.height));
    (*ImageFile).write((char*) &ImageHeader.planes, sizeof (ImageHeader.planes));
    (*ImageFile).write((char*) &ImageHeader.bits, sizeof (ImageHeader.bits));
    (*ImageFile).write((char*) &ImageHeader.compression, sizeof (ImageHeader.compression));
    (*ImageFile).write((char*) &ImageHeader.imagesize, sizeof (ImageHeader.imagesize));
    (*ImageFile).write((char*) &ImageHeader.xresolution, sizeof (ImageHeader.xresolution));
    (*ImageFile).write((char*) &ImageHeader.yresolution, sizeof (ImageHeader.yresolution));
    (*ImageFile).write((char*) &ImageHeader.ncolours, sizeof (ImageHeader.ncolours));
    (*ImageFile).write((char*) &ImageHeader.importantcolours, sizeof (ImageHeader.importantcolours));

    /* Read each pattern feature into the Image array */
    for (int Height = 15; Height >= 0; Height--)
    {
      for (int Width = 0; Width < 16; Width++)
      {
        (*DataFile) >> Feature;
        Image[Height][Width] = Feature;
      }
    }

    for (int Height = 0; Height < 16; Height++)
    {
      for (int Width = 0; Width < 16; Width++)
      {
        Pixel[0] = Pixel[1] = Pixel[2] = (char) ((unsigned int) (255.0 - (Image[Height][Width] * 127.5)));
        Pixel[3] = 0;

        //cout << " " << (unsigned int) (255.0 - (Image[Height][Width] * 127.5));

        //cout << (unsigned int)(Feature * 128.0) << " ";

        (*ImageFile).write ((char*) &Pixel, 3);
      }
    }

    /* Close the output file */
    (*ImageFile).close ();
    numFile++;
  }
}
