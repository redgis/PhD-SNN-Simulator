/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "d2pdf.hpp"

gsl_histogram * Histo;
gsl_histogram_pdf * probaHisto;
int NbBin;
int xMin;
int xMax;

void help ()
{
  cout << "Print the probability distribution of input histogram." << endl;
  cout << "  Usage: d2dp <Nb bin> <xMin> <xMax>" << endl;
  cout << "  Data should be send to standard input of process: [bin] [amount] [bin] [amount] ..." << endl;

  exit (1);
}

void parseArg (char * argument)
{
  if (argument[0] == '-')
    switch (argument[0])
    {

      default:
        help ();
        break;
    }
  else
    help ();
}


int main (int argc, char * argv[])
{
  size_t Size = 0;

  /* Manage arguments */
  if (argc == 4)
  {
    NbBin = atoi (argv[1]);
    xMin = atoi (argv[2]);
    xMax = atoi (argv[3]);
  }
  else
  {
    help ();
  }

  /* Allocate input buffer */
  Histo = gsl_histogram_alloc (NbBin);
  gsl_histogram_set_ranges_uniform (Histo, xMin, xMax);

  double curBin;
  double curAmount;

  /* Fill in the buffer with input data */
  do
  {
    cin >> curBin;
    cin >> curAmount;

    //cout << "bin:" << curBin << " amount:" << curAmount << endl;

    if ( !(cin.fail() || cin.eof()) )
    {
      Size++;
      gsl_histogram_accumulate (Histo, curBin, curAmount);
    }

  } while (!(cin.fail()) && !(cin.eof()));


  probaHisto = gsl_histogram_pdf_alloc (Histo->n);
  gsl_histogram_pdf_init (probaHisto, Histo);

  double previousProba = 0;
  for (int index = 0; index < probaHisto->n; index ++)
  {
    cout << probaHisto->range[index] << " " << probaHisto->sum[index]-previousProba << endl;
    previousProba = probaHisto->sum[index-1];
  }

  gsl_histogram_free (Histo);
  gsl_histogram_pdf_free (probaHisto);


  return 0;
}
