/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/



#include "totulip.h" // generic includes for everybody


map<int, CNeuron *> NeuronsByID;
vector <vector <NNLtime> > DelaysByNeuronID;
vector <vector <NNLweight> > WeightsByNeuronID;
NNLpotential * ThresholdsByNeuronID;
NNLtime DelaySlice;
NNLweight WeightSlice;

int SynapseIDcounter = 1;


/****************************************************************************/
int init ()
{
  return 0;
}

