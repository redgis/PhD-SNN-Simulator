/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
 
#include "totulip.h"

#include "../../parameters.h"
#include "architecture.h"
#include "assembly.h"
#include "neuron.h"
#include "synapse.h"


/****************************************************************************/
// Because I am clever, and want to have easy access to various informations
void CreateUsefulStructures ();


/****************************************************************************/
int main (int argc, char * argv[])
{
  struct timeval start;
  struct timeval end;
  struct timeval elapsed;

  CArchitecture Architecture;
  
  //Loading default settings
  cout << "Loading parameters from LUA script " << "\""+CParameters::SETTINGS_DIR+"settings-USPS.lua\"...";
  CParameters::loadFromLuaFile ("../../"+CParameters::SETTINGS_DIR+"settings-USPS.lua");
  cout << " done." << endl;
  
  if ((argc != 3) && (argc != 4))
  {
    cerr << "*** ERROR : expecting argument." << endl;
    cerr << "    Usage : " << argv[0] << " <XML architecture file> <tulip output filename> [<assembly name>]" << endl;
    exit (1);
  }
  else
  {
    cout << "Loading " << "\"" << argv[1] << "\"" << "... ";
    Architecture.XMLload (argv[1]);
    cout << " done." << endl;
  }
  
  //Loading the previously saved network
  //cout << "Loading " << "\"../../"+CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0000000000--RandSeed-1153245669--Size-200--Classes-10--timescale-10.xml\"" << "... ";
  //Architecture.XMLload ("../../"+CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0000000000--RandSeed-1153245669--Size-200--Classes-10--timescale-10.xml");
  //cout << " done." << endl;
  
  //cout << "Loading " << "\"../../"+CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0310258999--RandSeed-1153245669--Size-200--Classes-10--timescale-10.xml\"" << "... ";
  //Architecture.XMLload ("../../"+CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0310258999--RandSeed-1153245669--Size-200--Classes-10--timescale-10.xml");
  //cout << " done." << endl;
  
  //cout << "Loading " << "\"../../"+CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0000000.xml\"" << "... ";
  //Architecture.XMLload ("../../"+CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0000000.xml");
  //cout << " done." << endl;
  
  //cout << "Loading " << "\"../../"+CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0200009.xml\"" << "... ";
  //Architecture.XMLload ("../../"+CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0200009.xml");
  //cout << " done." << endl;
  
  //Init empty neurons in NeuronByID, to avoid segfault

  map<int, CNeuron *>::iterator itNeuronID;
  map<int, CNeuron *>::iterator itLastNeuronID;

  int MaxNeuronID = (*(NeuronsByID.begin ())).first;
  
  // Find max neuron ID
  for (itNeuronID = NeuronsByID.begin (); itNeuronID != NeuronsByID.end (); itNeuronID++)
  {
    if ( (*itNeuronID).first > MaxNeuronID )
      MaxNeuronID = (*itNeuronID).first;
  }
  
  // Set every empty id to a dummy neuron
  for (int Index = 0; Index <= MaxNeuronID; Index ++)
  {
    if (NeuronsByID.find (Index) == NeuronsByID.end())
    {
      NeuronsByID[Index] = new CNeuron ();
      NeuronsByID[Index]->ID = Index;
    }
  }
  
  
  CreateUsefulStructures ();
  
  
  string AssemblyName = "";

  vector<CAssembly *>::iterator itAss;
  vector<CAssembly *>::iterator itLastAss = Architecture.Assemblies.end ();
  vector<CAssembly *>::iterator itFirstAss = Architecture.Assemblies.begin ();;

  if (argc == 4)
  {
    AssemblyName = argv[3];

  
    if (!Architecture.getAssemblyByName (AssemblyName))
    {
      cout << "*** ERROR : Assembly \"" << AssemblyName << "\" not found ! Exiting." << endl;
      return 1;
    }

    itFirstAss = find (itFirstAss, itLastAss, Architecture.getAssemblyByName(argv[3]));
    itLastAss = itFirstAss + 1;
  }

  gettimeofday (&start, NULL);

  //Open Weight and delay distribution files
  ofstream * TulipOutput = new ofstream (string(argv[2]).c_str (), ios::out|ios::trunc);

  cout << "Writing to " << string(argv[2])+" ..." << endl;
  
  vector<CSynapse *>::iterator itSynapse;
  vector<CSynapse *>::iterator itLastSynapse;
  
  vector<CNeuron *>::iterator itNeuron;
  vector<CNeuron *>::iterator itLastNeuron;

  // Writing file header
  (*TulipOutput) << "(tlp \"2.0\"" << endl;

  // Writing node section
  (*TulipOutput) << "(nodes ";
  itAss = itFirstAss;
  while (itAss != itLastAss)
  {
    itNeuron = (*itAss)->Neurons.begin ();
    itLastNeuron = (*itAss)->Neurons.end ();

    while (itNeuron != itLastNeuron)
    {
      (*TulipOutput) << (*itNeuron)->ID+1 << " ";
      itNeuron++;
    }

    itAss++;
  }
  //closing node section
  (*TulipOutput) << ")" << endl;

  // Writing edges
  itAss = itFirstAss;
  while (itAss != itLastAss)
  {
    itNeuron = (*itAss)->Neurons.begin ();
    while (itNeuron != itLastNeuron)
    {
      itSynapse = (*itNeuron)->outputSynapses.begin ();
      itLastSynapse = (*itNeuron)->outputSynapses.end ();

      while (itSynapse != itLastSynapse)
      {
        (*TulipOutput) << "(edge " << (*itSynapse)->Id << " " << (*itSynapse)->InputNeuron->ID+1 << " " << (*itSynapse)->OutputNeuron->ID+1 << ")"<< endl;
        itSynapse++;
      }
      itNeuron++;
    }

    itAss++;
  }
  
  //Writing assemblies as clusters
  int NbAss = 1;
  itAss = itFirstAss;
  while (itAss != itLastAss)
  {
    //Open cluster
    (*TulipOutput) << "(cluster " << NbAss << " \"" << (*itAss)->Name << "\"" << endl;

    // Write nodes
    itNeuron = (*itAss)->Neurons.begin ();
    (*TulipOutput) << "  (nodes" <<  endl;
    while (itNeuron != itLastNeuron)
    {
      (*TulipOutput) << " " << (*itNeuron)->ID+1;
      itNeuron++;
    }
    // Close nodes section
    (*TulipOutput) << ")" <<  endl;

    // Write edges
    itNeuron = (*itAss)->Neurons.begin ();
    (*TulipOutput) << "  (edges";
    while (itNeuron != itLastNeuron)
    {
      itSynapse = (*itNeuron)->outputSynapses.begin ();
      itLastSynapse = (*itNeuron)->outputSynapses.end ();

      while (itSynapse != itLastSynapse)
      {
        if ((*itSynapse)->OutputNeuron->ParentAssembly == (*itAss))
          (*TulipOutput) << " " << (*itSynapse)->Id;
        itSynapse++;
      }
      itNeuron++;
    }
    //close edges list
    (*TulipOutput) << ")" << endl;

    // Close cluster
    (*TulipOutput) << ")" << endl;

    NbAss++;
    itAss++;
  }

/*
 *   (property  0 color "viewColor"
    (default "(235,0,23,255)" "(0,0,0,0)" )
    (node 1 "(200,0,200,255)")
    (node 2 "(100,100,0,255)")
    (node 3 "(100,100,0,255)")
    (edge 2 "(200,100,100)")
  )
 */

  // Writing properties ...
  (*TulipOutput) << "(property 0 color \"viewColor\"" << endl;
  (*TulipOutput) << "  (default \"(0,0,0,255)\" \"(0,0,0,255)\" )" << endl;

  // Writing node section
  itAss = itFirstAss;
  while (itAss != itLastAss)
  {
    itNeuron = (*itAss)->Neurons.begin ();
    itLastNeuron = (*itAss)->Neurons.end ();

    while (itNeuron != itLastNeuron)
    {
      itSynapse = (*itNeuron)->outputSynapses.begin ();
      itLastSynapse = (*itNeuron)->outputSynapses.end ();

      if ((*itNeuron)->Inhibitory)
      {
        (*TulipOutput) << "  (node " << (*itNeuron)->ID+1 << "\"(135,206,250,255)\")" << endl;

        while (itSynapse != itLastSynapse)
        {
          (*TulipOutput) << "  (edge " << (*itSynapse)->Id << "\"(150,150,150,255)\")" << endl;
          itSynapse++;
        }

      }
      else
      {
        (*TulipOutput) << "  (node " << (*itNeuron)->ID+1 << "\"(178,34,34,255)\")" << endl;

        while (itSynapse != itLastSynapse)
        {
          (*TulipOutput) << "  (edge " << (*itSynapse)->Id << "\"(150,150,150,255)\")" << endl;
          itSynapse++;
        }
      }

      itNeuron++;
    }

    //closing node section
    (*TulipOutput) << ")" << endl;


    itAss++;
  }

  // Closing file
  (*TulipOutput) << ")" << endl;
  (*TulipOutput).close();
  
  gettimeofday (&end, NULL);
  
  elapsed.tv_sec = end.tv_sec - start.tv_sec;
  elapsed.tv_usec = end.tv_usec - start.tv_usec;
  
  cout << endl << elapsed.tv_sec << "." << setw(6) << setfill ('0') << (unsigned long) elapsed.tv_usec << " s" << ".          " << endl;
  
  return 0;
}

/****************************************************************************/
// Because I am clever, and want to have easy access to various informations
void CreateUsefulStructures ()
{
  int NbNeurons = NeuronsByID.size ();
  
  int InputNeuron;
  int OutputNeuron;
  
  DelaysByNeuronID.resize (NbNeurons);
  WeightsByNeuronID.resize(NbNeurons);
  
  ThresholdsByNeuronID = new NNLpotential[NbNeurons];
  
  // Initialise neuron tables
  for (int Index = 0; Index < NbNeurons; Index++)
  {
    ThresholdsByNeuronID[Index] = NeuronsByID[Index]->Threshold;
  }
  
  // Initialise synapse matrices
  for ( int InputIndex = 0; InputIndex != NbNeurons; InputIndex++ )
  {
    DelaysByNeuronID [InputIndex].resize (NbNeurons);
    WeightsByNeuronID [InputIndex].resize (NbNeurons);
    
    for ( int OutputIndex = 0; OutputIndex != NbNeurons; OutputIndex++ )
    {
      DelaysByNeuronID [InputIndex][OutputIndex] = -1; // -1 = no connexion
      WeightsByNeuronID [InputIndex][OutputIndex] = 0; // 0 = no connexion or no weight ... the same anyway
    }
  }
  
  // Matrix of weightes , delays
  map<int, CNeuron *>::iterator itNeuron = NeuronsByID.begin ();
  map<int, CNeuron *>::iterator itLastNeuron = NeuronsByID.end ();
  vector<CSynapse *>::iterator itSynapse;
  vector<CSynapse *>::iterator itLastSynapse;
  
  for ( ; itNeuron != itLastNeuron; itNeuron++ )
  {
    itLastSynapse = (*itNeuron).second->outputSynapses.end ();
    
    for (itSynapse = (*itNeuron).second->outputSynapses.begin (); itSynapse != itLastSynapse; itSynapse++ )
    {
      InputNeuron = (*itSynapse)->InputNeuron->ID;
      OutputNeuron = (*itSynapse)->OutputNeuron->ID;
      
      DelaysByNeuronID [InputNeuron][OutputNeuron] = (*itSynapse)->Delay;
      WeightsByNeuronID [InputNeuron][OutputNeuron] = (*itSynapse)->Weight;
      
      //cout << (*itSynapse)->Delay << " " << (*itSynapse)->Weight << endl;
      //cout << DelaysByNeuronID [InputNeuron][OutputNeuron] << " " << WeightsByNeuronID [InputNeuron][OutputNeuron] << endl;
    }
  }
  
  
}


