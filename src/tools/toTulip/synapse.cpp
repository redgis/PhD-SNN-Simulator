/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
 

#include "synapse.h"

#include "../../parameters.h"

#include "neuron.h"

/****************************************************************************/
TiXmlElement * CSynapse::XMLload (TiXmlElement * pXmlSynapse, map<int, CNeuron*> * NeuronByID)
{

  int tmpInputNeuron, tmpOutputNeuron;
  CNeuron * tmpNeuron;
  double tmpWeight;//, tmpWeightAdaptation, tmpDelayAdaptation;
  double tmpDelay;
  int tmpInhibitory;


  pXmlSynapse->Attribute ("weight", &tmpWeight);
  Weight = (NNLweight) tmpWeight;
  //cout << Weight << " ";

  pXmlSynapse->Attribute ("delay", &tmpDelay);
  tmpDelay *= (double) CParameters::TIMESCALE;
  Delay = (NNLtime) tmpDelay;
  

  pXmlSynapse->Attribute ("inhibitory", &tmpInhibitory);
  Inhibitory = (bool) tmpInhibitory;

  /*
  pXmlSynapse->Attribute ("weightSTDP", &tmpWeightAdaptation);
  WeightAdaptation = (bool) tmpWeightAdaptation;

  pXmlSynapse->Attribute ("delaySTDP", &tmpDelayAdaptation);
  DelayAdaptation = (bool) tmpDelayAdaptation;
  */

  pXmlSynapse->Attribute ("inputNeuron", &tmpInputNeuron);
  pXmlSynapse->Attribute ("outputNeuron", &tmpOutputNeuron);

  if ((*NeuronByID)[tmpOutputNeuron] == NULL)
  {
    tmpNeuron = new CNeuron ((CAssembly*) NULL);
    (*NeuronByID)[tmpOutputNeuron] = tmpNeuron;
  }

  (*NeuronByID)[tmpOutputNeuron]->ID = tmpOutputNeuron;
  (*NeuronByID)[tmpOutputNeuron]->inputSynapses.push_back (this);

  InputNeuron = (*NeuronByID)[tmpInputNeuron];
  OutputNeuron = (*NeuronByID)[tmpOutputNeuron];

  //cout << tmpInputNeuron << ";" << tmpOutputNeuron << " ";
  //cout << InputNeuron << ";" << OutputNeuron << endl;

  return pXmlSynapse;
}
