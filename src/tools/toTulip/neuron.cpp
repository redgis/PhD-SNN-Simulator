/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
 
#include "neuron.h"

#include "../../parameters.h"
#include "synapse.h"
#include "assembly.h"



/****************************************************************************/
bool CNeuron::XMLload (TiXmlElement * pXmlNeuron, map<int, CNeuron*> * NeuronByID)
{
  CSynapse * NewSynapse;
  double tmpAbsoluteRefractoryPeriode, tmpInhibitory, tmpTauPSP, tmpTauRefract;
  double tmpAbsoluteRefractPotential, tmpThreshold;
  const char * res;
  
  
  pXmlNeuron->Attribute ("id", &ID);
  pXmlNeuron->Attribute ("refractoryPeriod", &tmpAbsoluteRefractoryPeriode);
  AbsoluteRefractoryPeriode = (NNLtime) (tmpAbsoluteRefractoryPeriode * CParameters::TIMESCALE);
  //AbsoluteRefractoryPeriode *= CParameters::TIMESCALE;

  pXmlNeuron->Attribute ("refractoryPotential", &tmpAbsoluteRefractPotential);
  AbsoluteRefractPotential = (NNLpotential) tmpAbsoluteRefractPotential;

  pXmlNeuron->Attribute ("inhibitory", &tmpInhibitory);
  Inhibitory = (bool) tmpInhibitory;

  pXmlNeuron->Attribute ("threshold", &tmpThreshold);
  Threshold = (NNLpotential) tmpThreshold;

  MembranePotential = (NNLpotential) CParameters::RESTING_POTENTIAL;
  PSPSum = (NNLpotential) 0;
  
  //TauPSP = (NNLtime) CParameters::TAU_PSP;
  //TauRefract = (NNLtime) CParameters::TAU_REFRACT;
  
  res = pXmlNeuron->Attribute ("tauPSP", &tmpTauPSP);
  if (!res)
  {
    TauPSP = CParameters::TAU_PSP;
  }
  else
  {
    TauPSP = (NNLtime) tmpTauPSP;
    TauPSP *= CParameters::TIMESCALE;
  }

  res = pXmlNeuron->Attribute ("tauRefract", &tmpTauRefract);
  if (!res)
  {
    TauRefract = CParameters::TAU_REFRACT;
  }
  else
  {
    TauRefract = (NNLtime) tmpTauRefract;
    TauRefract *= CParameters::TIMESCALE;
  }


  //inputSynapses.clear ();
  //outputSynapses.clear ();
  //~ LastPSP.Time = -1;
  //~ LastSpike = -1;
  //~ OldSpike = -1;
  //~ PSPSum = 0;

  /* Generates the tabbed exponential values */
  InitPSPShape ();
  InitRefractShape ();

	TiXmlElement* pXmlSynapse = pXmlNeuron->FirstChildElement( "SynapseOut" );

  while (pXmlSynapse)
  {
    //cout << root->Value() << endl;
    NewSynapse = new CSynapse ();
    NewSynapse->XMLload (pXmlSynapse, NeuronByID);
    NewSynapse->Id = SynapseIDcounter++;
    outputSynapses.push_back (NewSynapse);
    pXmlSynapse = pXmlSynapse->NextSiblingElement( "SynapseOut" );
  }

  return true;
}


/****************************************************************************/
/* This function is used to generate an array containing the PSP values from 0 to 5*TauPSP */
NNLpotential CNeuron::PSPValue (int t)
{
  return (NNLpotential) exp((double)(-((float)t)/((float)TauPSP)));
}

/****************************************************************************/
/* This function is used to generate an array containing the refract values from 0 to 5*TauRefract */
NNLpotential CNeuron::RefractValue (int t)
{
  return (NNLpotential) - exp((double)(-((float)t)/((float)TauRefract)));
}


/****************************************************************************/
void CNeuron::InitPSPShape ()
{
  int lowLimit = 0;
  int upLimit = 5*TauPSP;

  int NbVal = upLimit - lowLimit + 1;

  PSPShape = new NNLpotential[NbVal];

  for (int t = 0; t < NbVal; t++)
  {
    PSPShape[t] = PSPValue(t);
  }
}

/****************************************************************************/
void CNeuron::InitRefractShape ()
{
  int lowLimit = 0;
  int upLimit = 5*TauRefract;

  int NbVal = upLimit - lowLimit + 1;

  RefractShape = new NNLpotential[NbVal];

  for (int t = 0; t < NbVal; t++)
  {
    RefractShape[t] = RefractValue(t);
  }
}



/****************************************************************************/
NNLpotential CNeuron::PSPFunction (NNLtime t)
{
  //cout << t << " - " << ExpPSP(t) << ":" << (float) (*(PSPShapes[TauPSP]))[t] << ":" << exp ((double)(-((float)t)/((float)TauPSP))) << endl;
  /* Decreasing exponential : from 1 to 0 (exp (-5) considered to be 0)*/
  if (t > 5*TauPSP)
    return 0;
  else /* NB : we only pass t as parameter, but the function calculates exp(-t/Tau) where Tau is the neuron time constant */
    return PSPShape[t];
}

/****************************************************************************/
NNLpotential CNeuron::RefractoryPotential (NNLtime t)
{
  /* Increasing exponential : from -1*RefractoryPotential to 0 (exp (5*Tau) considered to be 0) */
  //if (t < AbsoluteRefractoryPeriode)
  //  return AbsoluteRefractPotential;
  if ((t >= AbsoluteRefractoryPeriode) && (t <= (AbsoluteRefractoryPeriode + 5*TauRefract))) /* NB : we only pass t as parameter, but the function calculates exp(-t/Tau) where Tau is the neuron time constant */
    return AbsoluteRefractPotential * RefractShape[t-AbsoluteRefractoryPeriode];
  
  return 0;
}

//~ /****************************************************************************/
//~ NNLpotential CNeuron::InputFunction (NNLevent & evt)
//~ {
  //~ NNLpotential RefractPotential = 0;
  //~ NNLpotential newMembranePotential;
  
  //~ NNLtime LastPSPage;
  
  //~ /* Only if the last PSP was not at this time */
  //~ if (LastPSP.Time != CurrentTime)
  //~ {
    //~ /* Recalculate refractory potential if needed */
    //~ if (LastSpike != -1)
      //~ RefractPotential = RefractoryPotential (CurrentTime - LastSpike);
    //~ else
      //~ RefractPotential = 0;

  
    //~ /* If there was a PSP in the past, calculate the potential remaining from PSPs, since the last PSP */
    //~ if (LastPSP.Time != -1)
    //~ {
      //~ LastPSPage = CurrentTime - LastPSP.Time;
      //~ PSPSum = PSPSum * PSPFunction(LastPSPage);
    //~ }
  //~ }

   //~ if (evt.ImpactingSynapse->Inhibitory)
    //~ PSPSum -= (NNLpotential) (evt.ImpactingSynapse->Weight * (NNLweight) CParameters::PSP_STRENGHT);
  //~ else
    //~ PSPSum += (NNLpotential) (evt.ImpactingSynapse->Weight * (NNLweight) CParameters::PSP_STRENGHT);

  //~ newMembranePotential = (NNLpotential) CParameters::RESTING_POTENTIAL + RefractPotential + PSPSum;
  
  //~ /*if (PSPSum > -ABSOLUTE_REFRACTORY_POTENTIAL)
    //~ cout << "************************************* WARNING OVERDRIVE DETECTED FOR NEURON " << ID << "\a" << endl;
  //~ */
  //~ /*if (ID == 132)
  //~ {
    //~ cout << ID << "- Membrane: " << newMembranePotential << "   PSPSum: " << PSPSum <<  "   Refraction: " << RefractPotential << "   delta-Membrane: " << PSPSum+RefractPotential << "  Poids: " << evt.ImpactingSynapse->getWeight() << endl;
  //~ }*/

  //~ LastPSP = evt;
  
  //~ return newMembranePotential;
//~ }

//~ /****************************************************************************/
//~ void CNeuron::emitSpike (NNLtime emissionTime)
//~ {
  //~ NNLevent * eventTemp;
  
  //~ if (emissionTime < CurrentTime) 
  //~ {
    //~ cout << endl << " *** WARNING *** emissionTime < CurrentTime: " << emissionTime << " Current time: " << CurrentTime << endl;
    //~ cout << " *** WARNING *** EmittingNeuron: " << ID << "(" << localID << ")" << endl;
    //~ if (ParentAssembly != NULL)
      //~ cout << "from " << ParentAssembly->Name << endl;
    
    //~ //test ();
  //~ }

  
  //~ /* Enqueue this spike as an event */
  //~ eventTemp = new NNLevent ();
  //~ eventTemp->EventType = EVT_SPIKE;
  //~ eventTemp->EmittingNeuron = this;
  //~ eventTemp->Time = emissionTime;
  //~ //EventQueue.push (*eventTemp);
  //~ delete eventTemp;
  
  //~ /* Enqueue post synaptic impacts for this spike, for each synapse */
  //~ vector<CSynapse*>::iterator it = outputSynapses.begin();
  
  //~ while (it != outputSynapses.end())
  //~ {
    //~ eventTemp = new NNLevent ();
    //~ eventTemp->EventType = EVT_SPIKE_IMPACT;
    //~ eventTemp->EmittingNeuron = this;
    //~ eventTemp->ImpactingSynapse = (CSynapse*)(*it);
    //~ eventTemp->Time = emissionTime + (((CSynapse*)(*it))->Delay);
    //~ //EventQueue.push (*eventTemp);
    //~ delete eventTemp;
    //~ it++;
  //~ }
  
  //~ /* Reset the PSP sum */
  //~ PSPSum = 0;
  
  //~ /* Say the last spike of this neuron is this spike */
  //~ LastSpike = CurrentTime;
  
  //~ /* Make the learning for all input synapses */
  //~ it = inputSynapses.begin();
  
  //while (it != inputSynapses.end())
  //{
    //((CSynapse *)(*it))->Learn (LastSpike);
    //it++;
  //}
//~ }

//~ /****************************************************************************/
//~ void CNeuron::ImpactedBy (NNLevent & evt)
//~ {
  
  //~ if (evt.EventType == EVT_SPIKE_IMPACT)
  //~ {    
    //~ /* Only if the last spike is older than 80 ms */
    //~ if (CurrentTime - LastSpike > AbsoluteRefractoryPeriode)
    //~ {
      //~ //if (/*!evt.ImpactingSynapse->isInhibitory() &&*/ (evt.ImpactingSynapse->getWeight () >= 0.3) /*&& /*(evt.EmittingNeuron->getAssembly () == Container)*/)
      
      //~ NNLevent * newEvt = new NNLevent (evt);
      //~ SpikingPSPs.push_back (newEvt);
      
      
      //~ /* Delete PSP that occured longer than 2 ms ago */
      //~ while ( (SpikingPSPs.size() != 0) && (SpikingPSPs.front()->Time < CurrentTime - (TauPSP*5)))
      //~ {
        //~ delete SpikingPSPs.front();
        //~ SpikingPSPs.pop_front ();
      //~ }
    //~ }
  
    
    //~ /* Let us remember the last spike time for the learning (if the neuron spiked at 
       //~ this time, the PSP-learning on the synapses must be done with the previous spike time) */
    //~ if ((LastSpike != CurrentTime) && (LastSpike != -1))
      //~ OldSpike = LastSpike;
    
    /* Make the learning for this impacting synapse */
    //evt.ImpactingSynapse->Learn (OldSpike);
    
    
    //~ /* Recalculate membrane potential with this PSP, only if the neuron is not in refractory period */
    //~ if ((LastSpike == -1) || ((CurrentTime - LastSpike) >= AbsoluteRefractoryPeriode))
    //~ {
      //~ MembranePotential = InputFunction (evt);
            
      //~ if (MembranePotential >= Threshold)
      //~ {
        //~ emitSpike (CurrentTime);
        //~ SpikingPSP = evt;
        
        //~ if ( ParentAssembly->Name == "Reservoir" )
        //~ {
          //~ NNLevent * evtTmp;
          //~ evtTmp = new NNLevent ();
          //~ evtTmp->EventType = EVT_SAVE_SPIKE_PSP;
          //~ evtTmp->Time = CurrentTime + 1;
          //~ evtTmp->EmittingNeuron = this;
          
          //~ //EventQueue.push (*evtTmp);
          //~ delete evtTmp;
        //~ }
      //~ }
    //~ }
    
  //~ }
//~ }


/****************************************************************************/
bool CNeuron::DoYouSpike (TEvent * myPSP, NNLpotential * LastKnownPotential, NNLtime * LastPSP, NNLtime * LastSpike)
{
  bool result = false;
  
  NNLpotential newPotential = *LastKnownPotential;
  NNLpotential refractPotential = 0;
  
  // If the event is not a PSP, exit
  if ( myPSP->evtType != EVT_PSP )
    return false;
  
  // If we are in absolute refractory periode, exit
  if ( ((*LastSpike) != -1 ) && (myPSP->Time - *LastSpike) < AbsoluteRefractoryPeriode )
  {
    (*LastKnownPotential) = -AbsoluteRefractPotential;
    (*LastPSP) = myPSP->Time;
    return false;
  }
  
  // If there was a spike in the past, calculate refractory influence
  if ( *LastSpike != -1 )
    refractPotential = RefractoryPotential (myPSP->Time - (*LastSpike)+AbsoluteRefractoryPeriode);
  
  // If there was a PSP in the past
  if ( *LastPSP != -1 )
    newPotential = CParameters::RESTING_POTENTIAL + ( (*LastKnownPotential) - CParameters::RESTING_POTENTIAL) * PSPFunction (myPSP->Time - *LastPSP) + myPSP->PSPWeight;
  else
    newPotential = CParameters::RESTING_POTENTIAL + myPSP->PSPWeight;
  
  // Finally, do we spike ? Set last spike time
  if ( newPotential >= Threshold )
  {
    result = true;
#ifdef __DEBUG  
    cout << myPSP->Time << ":" << ID << ": Spike **** Before : " << (*LastKnownPotential) << " Now : " << -AbsoluteRefractPotential << endl;
#endif
    (*LastSpike) = myPSP->Time;
    (*LastKnownPotential) = -AbsoluteRefractPotential;
  }
  
  // Set new last psp time, and new potential
  (*LastPSP) = myPSP->Time;
  (*LastKnownPotential) = newPotential;
  

  
  return result;
}


/****************************************************************************/
bool CNeuron::DoYouSpike (TEvent * myPSP, list <TEvent *> * PSPList, NNLtime * LastSpike, int NbSpikesRequired)
{
  bool result = false;
  
  // If the event is not a PSP, exit
  if ( myPSP->evtType != EVT_PSP )
    return false;
  
  //~ // If we are in absolute refractory periode, exit
  //~ if ( ((*LastSpike) != -1 ) && (myPSP->Time - *LastSpike) < AbsoluteRefractoryPeriode )
  //~ {
    //~ (*LastKnownPotential) = -AbsoluteRefractPotential;
    //~ (*LastPSP) = myPSP->Time;
    //~ return false;
  //~ }

  
  //cout << ID << ":" << (*LastSpike) << " : " << myPSP->Time << endl << flush;
    
  if ( ((*LastSpike) != -1) && (myPSP->Time - (*LastSpike) < AbsoluteRefractoryPeriode) )
    return false;
    
  if ( PSPList->size () >= (unsigned int) NbSpikesRequired )
  {
    result = true;
    (*LastSpike) = myPSP->Time;
#ifdef __DEBUG  
    cout << myPSP->Time << ":" << ID << ": Spike **** Before : " << PSPList->size () - 1 << " Now : " << PSPList->size () << endl;
#endif
  }
  
  return result;

}
