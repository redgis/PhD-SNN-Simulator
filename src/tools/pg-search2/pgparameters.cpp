/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

extern "C" {
#include "lua.hpp"
}

#include <iostream>
#include <fstream>
#include "pgparameters.hpp"

using namespace std;

/*****************************************************************************/
CPGParameters::CPGParameters ()
{

}

/*****************************************************************************/
CPGParameters::~CPGParameters ()
{
}

/*****************************************************************************/
bool CPGParameters::loadFromLuaFile (string InputSettingFile)
{
  cout << "Loading PGs settings \"" << InputSettingFile << "\" ... " << flush;

  // Create a lua state
  //lua_State * pLua = lua_open ();
  lua_State * pLua = luaL_newstate ();

  // Reset lua stack
  lua_settop (pLua, 0);

  int hasError;
  //load and run the script
  hasError = luaL_dofile(pLua, InputSettingFile.c_str ());
  if (hasError)
  {
    cerr << "-- " << lua_tostring(pLua, -1) << endl;
    luaL_error (pLua, "An error occured while loading %s", InputSettingFile.c_str());
    exit (hasError);
  }

  lua_getglobal (pLua, "MAX_MEMORY_USE");
  MAX_MEMORY_USE = (int) lua_tonumber (pLua, 1);
  lua_pop (pLua,1);

  lua_getglobal (pLua, "PG_OUTPUT_DIR");
  PG_OUTPUT_DIR = lua_tostring (pLua, 1);
  lua_pop (pLua,1);

  lua_getglobal (pLua, "MIN_PG_TIME");
  MIN_PG_TIME = (NNLtime) lua_tonumber (pLua, 1);
  lua_pop (pLua,1);

  lua_getglobal (pLua, "MAX_PG_TIME");
  MAX_PG_TIME = (NNLtime) lua_tonumber (pLua, 1);
  lua_pop (pLua,1);

  lua_getglobal (pLua, "MIN_PG_SIZE");
  MIN_PG_SIZE = (int) lua_tonumber (pLua, 1);
  lua_pop (pLua,1);

  lua_getglobal (pLua, "MAX_PG_SIZE");
  MAX_PG_SIZE = (int) lua_tonumber (pLua, 1);
  lua_pop (pLua,1);

  lua_getglobal (pLua, "PG_JITTER");
  PG_JITTER = (NNLtime) lua_tonumber (pLua, 1);
  lua_pop (pLua,1);

  lua_getglobal (pLua, "NB_TRIGGER_NEURONS");
  NB_TRIGGER_NEURONS = (int) lua_tonumber (pLua, 1);
  lua_pop (pLua,1);

  lua_getglobal (pLua, "AVOID_CYCLICS");
  AVOID_CYCLICS = (bool) lua_toboolean(pLua, 1);
  lua_pop (pLua,1);

  lua_getglobal (pLua, "SEARCH_BY_NB_PSP");
  SEARCH_BY_NB_PSP = (bool) lua_toboolean (pLua, 1);
  lua_pop (pLua,1);

  lua_getglobal (pLua, "NB_PSP_TO_SPIKE");
  NB_PSP_TO_SPIKE = (int) lua_tonumber (pLua, 1);
  lua_pop (pLua,1);

  lua_getglobal (pLua, "WEIGHT_THRESHOLD");
  WEIGHT_THRESHOLD = (NNLweight) lua_tonumber (pLua, 1);
  lua_pop (pLua,1);

  lua_getglobal (pLua, "SIZE_DISTRIB_BEAN");
  SIZE_DISTRIB_BEAN = (int) lua_tonumber (pLua, 1);
  lua_pop (pLua,1);

  lua_getglobal (pLua, "TIMESPAN_DISTRIB_BEAN");
  TIMESPAN_DISTRIB_BEAN = (NNLtime) lua_tonumber (pLua, 1);
  lua_pop (pLua,1);

  lua_getglobal (pLua, "SAVE_DETAILED_PGS");
  SAVE_DETAILED_PGS = (bool) lua_toboolean (pLua, 1);
  lua_pop (pLua,1);

  lua_getglobal (pLua, "ONLY_TRIGGERS_IN_SUMMARY");
  ONLY_TRIGGERS_IN_SUMMARY = (bool) lua_toboolean (pLua, 1);
  lua_pop (pLua,1);

  // close lua
  lua_close (pLua); //Theoretically, no need to close : automatically done by destructor of lua_State (i.e. pLua) class. But we do it anyway.

  cout << "done." << endl << flush;
  return true;
}

/*****************************************************************************/
bool CPGParameters::saveToLuaFile (string OutputSettingFile)
{
  ofstream * OutputSettingsStream = new ofstream (OutputSettingFile.c_str(), ios::out|ios::trunc);

  if (!OutputSettingsStream)
  {
    cerr << "*** Error opening \"" << OutputSettingFile << "\"."<< endl;
    return false;
  }

  (*OutputSettingsStream) << "MAX_MEMORY_USE = \"" << MAX_MEMORY_USE << "\"" << endl;
  (*OutputSettingsStream) << "PG_OUTPUT_DIR = " << PG_OUTPUT_DIR  << endl;
  (*OutputSettingsStream) << "MIN_PG_TIME = " << MIN_PG_TIME << endl;
  (*OutputSettingsStream) << "MAX_PG_TIME = " << MAX_PG_TIME << endl;
  (*OutputSettingsStream) << "MIN_PG_SIZE = " << MIN_PG_SIZE << endl;
  (*OutputSettingsStream) << "MAX_PG_SIZE = " << MAX_PG_SIZE << endl;
  (*OutputSettingsStream) << "PG_JITTER = " << PG_JITTER << endl;
  (*OutputSettingsStream) << "NB_TRIGGER_NEURONS = " << NB_TRIGGER_NEURONS << endl;
  (*OutputSettingsStream) << "AVOID_CYCLICS = " << AVOID_CYCLICS << endl;
  (*OutputSettingsStream) << "SEARCH_BY_NB_PSP = " << SEARCH_BY_NB_PSP << endl;
  (*OutputSettingsStream) << "NB_PSP_TO_SPIKE = " << NB_PSP_TO_SPIKE << endl;
  (*OutputSettingsStream) << "WEIGHT_THRESHOLD = " << WEIGHT_THRESHOLD << endl;
  (*OutputSettingsStream) << "SIZE_DISTRIB_BEAN = " << SIZE_DISTRIB_BEAN << endl;
  (*OutputSettingsStream) << "TIMESPAN_DISTRIB_BEAN = " << TIMESPAN_DISTRIB_BEAN << endl;
  (*OutputSettingsStream) << "SAVE_DETAILED_PGS = " << SAVE_DETAILED_PGS << endl;
  (*OutputSettingsStream) << "ONLY_TRIGGERS_IN_SUMMARY = " << ONLY_TRIGGERS_IN_SUMMARY << endl;
  OutputSettingsStream->close ();

  return true;
}


int CPGParameters::MAX_MEMORY_USE = 1024*1024*1024; //1Go
string CPGParameters::PG_OUTPUT_DIR = "PGs/";
NNLtime CPGParameters::MAX_PG_TIME = 1500; //150*TIMECALE
NNLtime CPGParameters::MIN_PG_TIME = 100; //10*TIMESCALE
int CPGParameters::MIN_PG_SIZE = 5;
int CPGParameters::MAX_PG_SIZE = 1000;
NNLtime CPGParameters::PG_JITTER = 1; // (0.1*TIMESCALE)
int CPGParameters::NB_TRIGGER_NEURONS = 3;
bool CPGParameters::AVOID_CYCLICS = false;
bool CPGParameters::SEARCH_BY_NB_PSP = false;
int CPGParameters::NB_PSP_TO_SPIKE = 3;
NNLweight CPGParameters::WEIGHT_THRESHOLD = 0;

int CPGParameters::SIZE_DISTRIB_BEAN = 10;
NNLtime CPGParameters::TIMESPAN_DISTRIB_BEAN = 10; //1 * TIMESCALE

bool CPGParameters::SAVE_DETAILED_PGS = true;
bool CPGParameters::ONLY_TRIGGERS_IN_SUMMARY = true;

