/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


#include "eventgrid.h" 

#include "parameters/parameters.h"
#include "pgparameters.hpp"

#include "neuron.h"
#include "synapse.h"
#include "eventcell.h"

/****************************************************************************/
CEventGrid::CEventGrid ()
{
  // Initialize the grid
  Cells = new CEventCell [NbNeurons * CPGParameters::MAX_PG_TIME];
  
  // Initialize the last spike time of each neuron at -1
  LastSpikes = new NNLtime [NbNeurons];
  LastPSPSums = new NNLpotential [NbNeurons];
  LastPSPs = new NNLtime [NbNeurons];
  
  
  for (int Index = 0; Index < NbNeurons; Index++)
  {

    LastPSPs[Index] = LastSpikes[Index] = - (NeuronsByID[Index]->AbsoluteRefractoryPeriode + CPGParameters::PG_JITTER + (10 * NeuronsByID[Index]->TauRefract));
    LastPSPSums[Index] = 0;
  }
  
  Truncated = false;
  
  Filename = NULL;
}

/****************************************************************************/
void CEventGrid::Check ()
{
  for (int neu = 0; neu < NbNeurons; neu++)
  {
    for (int ti = 0; ti < CPGParameters::MAX_PG_TIME; ti++)
    {
      if (Cells[neu * CPGParameters::MAX_PG_TIME + ti].ExcitatoryPSPs.size () != 0)
        cout << neu << ":" << ti << " " << Cells[neu * CPGParameters::MAX_PG_TIME + ti].ExcitatoryPSPs.size () << endl;
    }
  }
}

/****************************************************************************/
CEventGrid::~CEventGrid ()
{
  delete [] Cells;
  delete [] LastSpikes;
}

/****************************************************************************/
void CEventGrid::Clear ()
{
  CEventCell * currentCell;
    
  // Free memory for all events
  list <CPSPEvent *>::iterator itPSP = AllEvents.begin ();
  list <CPSPEvent *>::iterator itLastPSP = AllEvents.end ();
  
  // Delete all affected cell contents
  while (itPSP != itLastPSP)
  {
    currentCell = &(Cells[((*itPSP)->ImpactedNeuron) * CPGParameters::MAX_PG_TIME + (*itPSP)->ImpactingTime]);
    currentCell->Treated = FALSE;
    currentCell->SpikeOut = FALSE;
    currentCell->Empty = TRUE;
    
    LastSpikes[(*itPSP)->ImpactedNeuron] = LastPSPs[((*itPSP)->ImpactedNeuron)] = - (NeuronsByID[(*itPSP)->EmittingNeuron]->AbsoluteRefractoryPeriode + CPGParameters::PG_JITTER + (10 * NeuronsByID[(*itPSP)->EmittingNeuron]->TauRefract));
    LastPSPSums[((*itPSP)->ImpactedNeuron)] = 0;
    
    LastSpikes[(*itPSP)->EmittingNeuron] = LastPSPs[((*itPSP)->EmittingNeuron)] = - (NeuronsByID[(*itPSP)->EmittingNeuron]->AbsoluteRefractoryPeriode + CPGParameters::PG_JITTER + (10 * NeuronsByID[(*itPSP)->EmittingNeuron]->TauRefract));
    LastPSPSums[((*itPSP)->EmittingNeuron)] = 0;
    
    // Delete PSPs of this cell
    currentCell->InhibitoryPSPs.clear ();
    currentCell->ExcitatoryPSPs.clear ();
    
    delete (*itPSP);
    
    itPSP++;
  }
    
  AllEvents.clear ();

  // Free triggering spikes
  list <TSpike *>::iterator itSpike = TriggeringSpikes.begin ();
  list <TSpike *>::iterator itLastSpike = TriggeringSpikes.end ();
  
  while (itSpike != itLastSpike)
  {
    LastPSPs[(*itSpike)->EmittingNeuron] = LastSpikes[(*itSpike)->EmittingNeuron] = - (NeuronsByID[(*itSpike)->EmittingNeuron]->AbsoluteRefractoryPeriode + CPGParameters::PG_JITTER + (10 * NeuronsByID[(*itSpike)->EmittingNeuron]->TauRefract));
    LastPSPSums[(*itSpike)->EmittingNeuron] = 0;
    delete (*itSpike);
    itSpike++;
  }
  
  TriggeringSpikes.clear ();
  
  // Free all spikes
  itSpike = Spikes.begin ();
  itLastSpike = Spikes.end ();
  
  while (itSpike != itLastSpike)
  {
    LastPSPs[(*itSpike)->EmittingNeuron] = LastSpikes[(*itSpike)->EmittingNeuron] = - (NeuronsByID[(*itSpike)->EmittingNeuron]->AbsoluteRefractoryPeriode + CPGParameters::PG_JITTER + (10 * NeuronsByID[(*itSpike)->EmittingNeuron]->TauRefract));
    LastPSPSums[(*itSpike)->EmittingNeuron] = 0;
    delete *(itSpike);
    itSpike++;
  }
  
  Spikes.clear ();

  if (Filename != NULL)
    delete Filename;
  
  Filename = NULL;
  
  Truncated = false;
  
}

/****************************************************************************/
void CEventGrid::PostEventFromSpike (int EmittingNeuron, int EmittingTime)
{
  // Let say the spike is at current neuron / current time.
  CPSPEvent * newEvent;
  CEventCell * currentCell;
  
  //bool Inhibitory = false;
  
  vector<CSynapse * >::iterator itSynapse = NeuronsByID[EmittingNeuron]->outputSynapses.begin ();
  vector<CSynapse * >::iterator itLastSynapse = NeuronsByID[EmittingNeuron]->outputSynapses.end ();
  
  while (itSynapse != itLastSynapse)
  {
    if ( (*itSynapse)->OutputNeuron->ParentAssembly == NeuronsByID[EmittingNeuron]->ParentAssembly )
    {
      if (EmittingTime + (*itSynapse)->Delay >= CPGParameters::MAX_PG_TIME)
      {
        Truncated = TRUE;
      }
      else
      {
        newEvent = new CPSPEvent ();
        
        newEvent->EmittingNeuron = EmittingNeuron;
        newEvent->EmittingTime = EmittingTime;
        newEvent->ImpactedNeuron = (*itSynapse)->OutputNeuron->ID;
        newEvent->ImpactingTime = EmittingTime + (*itSynapse)->Delay;
        
        AllEvents.push_back (newEvent);
        PSPQueue.push (newEvent);
        
        currentCell = &(Cells[(newEvent->ImpactedNeuron) * CPGParameters::MAX_PG_TIME + newEvent->ImpactingTime]);
        
        // If the cell is not created yet, initialize it.
        if ( currentCell->Empty )
        {
          //(*currentCell) = CEventCell ();
          currentCell->Empty = FALSE;
          currentCell->Treated = FALSE;
          currentCell->SpikeOut = FALSE;
        }
        
        // Push the event to this cell PSP list
        if ((*itSynapse)->Inhibitory)
          currentCell->InhibitoryPSPs.push_back (newEvent);
        else
          currentCell->ExcitatoryPSPs.push_back (newEvent);
        
      }
    }
    
    itSynapse++;
    
  }
  
}

/****************************************************************************/
void CEventGrid::Simulate ()
{
  CPSPEvent * currentPSP;
  TSpike * Spike;
  
  currentTime = 0;
  
  //cout << "Simulating ..." << endl;
  
  // Propagate triggering spikes
  list<TSpike *>::iterator itSpike = TriggeringSpikes.begin ();
  list<TSpike *>::iterator itLastSpike = TriggeringSpikes.end ();
  
  while (itSpike != itLastSpike)
  {
    PostEventFromSpike ((*itSpike)->EmittingNeuron, (*itSpike)->EmittingTime);
    itSpike++;
  }
  
  // Treat events
  while (!PSPQueue.empty ())
  {
    currentPSP = PSPQueue.top ();
    currentNeuron = currentPSP->ImpactedNeuron;
    currentTime = currentPSP->ImpactingTime;
    
    // If limit time span exceeded  OR  If limit spike number exceeded
    if ( (currentTime >= CPGParameters::MAX_PG_TIME) || ( (Spikes.size () + TriggeringSpikes.size ()) >= CPGParameters::MAX_PG_SIZE) )
    {
      Truncated = true;
      while (!PSPQueue.empty())
      {
        PSPQueue.pop ();
      }
      
      break;
    }
    
    CEventCell * currentCell =  &(Cells[currentNeuron * CPGParameters::MAX_PG_TIME + currentTime]);
    
    // If spike or treated already on "true" => ignore de PSP, this cell has already been treated.
    if ((currentCell->SpikeOut == FALSE) && (currentCell->Treated == FALSE))
    {
      currentCell->Treated = TRUE;
      
      // Does this cell emit a spike ?
      if (DoYouSpike (currentNeuron, currentTime))
      {
        // Set current cell as spiking.
        currentCell->SpikeOut = TRUE;
        
        // Memorize the spike
        /*
        Spike = new TSpike;
        Spike->EmittingNeuron = currentNeuron;
        Spike->EmittingTime = currentTime;
        Spikes.push_back (Spike);
        */
          
        AddSpike (currentNeuron, currentTime);
        
        // Set last spike time of this neuron to currenttime : it's to calculate the absolute refractory periode
        //LastSpikes[currentNeuron] = currentTime;
        
        // Propagate the spike
        PostEventFromSpike (currentNeuron, currentTime);
      }
    }
    
    PSPQueue.pop ();
    
  }
  
}

/****************************************************************************/
void CEventGrid::Simulate2 ()
{
  CPSPEvent * currentPSP;
  TSpike * Spike;
  
  currentTime = 0;
  
  //cout << "Simulating ..." << endl;
  
  // Propagate triggering spikes
  list<TSpike *>::iterator itSpike = TriggeringSpikes.begin ();
  list<TSpike *>::iterator itLastSpike = TriggeringSpikes.end ();
  
  while (itSpike != itLastSpike)
  {
    PostEventFromSpike ((*itSpike)->EmittingNeuron, (*itSpike)->EmittingTime);
    itSpike++;
  }
  
  // Treat events
  while (!PSPQueue.empty ())
  {
    currentPSP = PSPQueue.top ();
    currentNeuron = currentPSP->ImpactedNeuron;
    currentTime = currentPSP->ImpactingTime;
    
    // If limit time span exceeded  OR  If limit spike number exceeded
    if (( currentTime >= CPGParameters::MAX_PG_TIME) || ((Spikes.size () + TriggeringSpikes.size ()) >= CPGParameters::MAX_PG_SIZE) )
    {
      Truncated = true;
      while (!PSPQueue.empty())
      {
        PSPQueue.pop ();
      }
      
      break;
    }
    
    CEventCell * currentCell =  &(Cells[currentNeuron * CPGParameters::MAX_PG_TIME + currentTime]);
    
    
    // If spike or treated already on "true" => ignore de PSP, this cell has already been treated.
    if ((currentCell->SpikeOut == FALSE) && (currentCell->Treated == FALSE))
    {
      currentCell->Treated = TRUE;
      
      // Does this cell emit a spike ?
      if ( DoYouSpike2 (currentNeuron, currentTime) )
      {
        // Set current cell as spiking.
        currentCell->SpikeOut = TRUE;
        
        // Memorize the spike
        /*
        Spike = new TSpike;
        Spike->EmittingNeuron = currentNeuron;
        Spike->EmittingTime = currentTime;
        Spikes.push_back (Spike);
        */
        
        AddSpike (currentNeuron, currentTime);
        
        // Set last spike time of this neuron to currenttime : it's to calculate the absolute refractory periode
        LastSpikes[currentNeuron] = currentTime;
        LastPSPs[currentNeuron] = currentTime;
        LastPSPSums[currentNeuron] = 0;
        
        // Propagate the spike
        PostEventFromSpike (currentNeuron, currentTime);
      }
    }
    
    LastPSPs[currentNeuron] = currentTime;
    PSPQueue.pop ();
  }
  
}

/****************************************************************************/
// Nb psp needed version
bool CEventGrid::DoYouSpike (int ImpactedNeuron, int ImpactingTime)
{
  bool result = false;
  
  int NbIPSP = 0;
  int NbEPSP = 0;
  
  
  
  if ((ImpactingTime - LastSpikes[ImpactedNeuron]) >= NeuronsByID[ImpactedNeuron]->AbsoluteRefractoryPeriode)
  {
    for (int Jitter = 0 /*- CParameters::PG_JITTER */; Jitter <= CPGParameters::PG_JITTER; Jitter ++)
    {
      if ( (ImpactingTime-Jitter >= 0) && \
           (ImpactingTime-Jitter < CPGParameters::MAX_PG_TIME) && \
           (Cells[ImpactedNeuron * CPGParameters::MAX_PG_TIME + ImpactingTime-Jitter].Empty == FALSE) && \
           ( (ImpactingTime-Jitter - LastSpikes[ImpactedNeuron]) >= NeuronsByID[ImpactedNeuron]->AbsoluteRefractoryPeriode ) )
      {
        //cout << "A " << ImpactingTime-Jitter << flush;
        NbIPSP += Cells[ImpactedNeuron * CPGParameters::MAX_PG_TIME + ImpactingTime-Jitter].InhibitoryPSPs.size ();
        //cout << " B" << flush;
        NbEPSP += Cells[ImpactedNeuron * CPGParameters::MAX_PG_TIME + ImpactingTime-Jitter].ExcitatoryPSPs.size ();
        //cout << " C" << endl << flush;
      }
    }
  }
  
  if ((NbEPSP - NbIPSP) >= CPGParameters::NB_PSP_TO_SPIKE)
    result = true;
  
  return result;
}

/****************************************************************************/
// Version pour la recherche en potentiel de membrane
bool CEventGrid::DoYouSpike2 (int ImpactedNeuron, int ImpactingTime)
{
  bool result = false;
  
  NNLpotential dPreviousPSPSum = LastPSPSums[ImpactedNeuron];
  NNLtime tPreviousPSP = LastPSPs[ImpactedNeuron];
  NNLtime tPreviousSpike = LastSpikes[ImpactedNeuron];
  NNLtime tElapsedPSPTime = ImpactingTime - tPreviousPSP; //elapsed time since last PSP
  NNLtime tElapsedSpikeTime = ImpactingTime - tPreviousSpike; //elapsed time since last Spike
  NNLtime tEndOfAbsoluteRefractoryPeriode = tPreviousSpike + NeuronsByID[ImpactedNeuron]->AbsoluteRefractoryPeriode; //Ending time of abs. refract. period
  NNLtime tEndOfRelativeRefractoryPeriode = tEndOfAbsoluteRefractoryPeriode + 5*NeuronsByID[ImpactedNeuron]->TauRefract; //Ending time of rel. refract. period
  CNeuron * pCurrentNeuron = NeuronsByID[ImpactedNeuron];
  
  NNLpotential dRefractoryPotential = 0;
  NNLpotential dNewMembranePotential = 0;
  NNLpotential dNewPSPSum = 0;
  
  // If still influenced by last spike emission (younger than AbsoluteRefractoryPeriode + 5*NeuronsByID[ImpactedNeuron]->TauRefract)
  if (ImpactingTime < tEndOfRelativeRefractoryPeriode)
  {
    /* Absolute refractory periode */
    if (ImpactingTime < tEndOfAbsoluteRefractoryPeriode)
    {
      dRefractoryPotential = - (pCurrentNeuron->AbsoluteRefractPotential);
    }
    /* Relative refractory periode */
    else
    {
      dRefractoryPotential = (pCurrentNeuron->AbsoluteRefractPotential) * pCurrentNeuron->RefractShape[ImpactingTime - tEndOfAbsoluteRefractoryPeriode];
    }
  }
  
  /* Recalculate the PSP sum considering membrane leak */
  if (tElapsedPSPTime > 5 * pCurrentNeuron->TauPSP)
    dNewPSPSum = 0;
  else
  {
    dNewPSPSum = dPreviousPSPSum * pCurrentNeuron->PSPShape[tElapsedPSPTime];
    //cout << dPreviousPSPSum << " " << tElapsedPSPTime/10.0 << "ms ago -> " << dNewPSPSum << endl;
  }
  
  /* Add current time weighted Excitatory PSPs */  
  list<CPSPEvent *>::iterator itPSP = Cells[ImpactedNeuron * CPGParameters::MAX_PG_TIME + ImpactingTime].ExcitatoryPSPs.begin ();
  list<CPSPEvent *>::iterator itLastPSP = Cells[ImpactedNeuron * CPGParameters::MAX_PG_TIME + ImpactingTime].ExcitatoryPSPs.end ();
  while (itPSP != itLastPSP)
  {
    dNewPSPSum += (NNLpotential) (WeightsByNeuronID [(*itPSP)->EmittingNeuron][ImpactedNeuron] * (NNLweight) CParameters::PSP_STRENGHT);
    itPSP++;
  }
  
  /* Add current time weighted Inhibitory PSPs */  
  itPSP = Cells[ImpactedNeuron * CPGParameters::MAX_PG_TIME + ImpactingTime].InhibitoryPSPs.begin ();
  itLastPSP = Cells[ImpactedNeuron * CPGParameters::MAX_PG_TIME + ImpactingTime].InhibitoryPSPs.end ();
  while (itPSP != itLastPSP)
  {
    dNewPSPSum -= (NNLpotential) (WeightsByNeuronID [(*itPSP)->EmittingNeuron][ImpactedNeuron] * (NNLweight) CParameters::PSP_STRENGHT);
    itPSP++;
  }
  
  // Calculate the membrane potential.
  dNewMembranePotential = (NNLpotential) NeuronsByID[ImpactedNeuron]->RestingPotential + dNewPSPSum + dRefractoryPotential;
  //cout << dPreviousPSPSum << " ==> " << dRefractoryPotential << " + " << dNewPSPSum << " + " << CParameters::RESTING_POTENTIAL << " = " << dNewMembranePotential;
  
  // See if we should spike
  result = (dNewMembranePotential >= pCurrentNeuron->Threshold);  
  
  // See if we should NOT spike (absolute refractory periode)
  if (ImpactingTime < tEndOfAbsoluteRefractoryPeriode)
  {
    result = false;
    LastPSPSums[ImpactedNeuron] = 0;
  }
  else  // Remember this historical moment (store pspsum & time)
    LastPSPSums[ImpactedNeuron] = dNewPSPSum;
  
  //cout << " ==> " <<  result << endl;
  //if (dPreviousPSPSum != 0)
  
  /*
  if (tElapsedPSPTime/10.0 < 0)
  {
    cout << __FILE__ << ":" << __LINE__ << " " << ImpactingTime/10.0 << " " << tPreviousPSP/10.0 << " " << - (CParameters::ABSOLUTE_REFRACTORY_PERIODE + CParameters::PG_JITTER + (10.0 * CParameters::TAU_REFRACT)) << endl;
  }
  */
  return result;
}

/****************************************************************************/
void CEventGrid::AddSpike (int NeuronID, int EmittingTime)
{
  TSpike * newSpike = new TSpike;
  newSpike->EmittingNeuron = NeuronID;
  newSpike->EmittingTime = EmittingTime;
  Spikes.push_back (newSpike);
}

/****************************************************************************/
void CEventGrid::AddTriggeringSpike (int NeuronID, int EmittingTime)
{
  TSpike * newSpike = new TSpike;
  newSpike->EmittingNeuron = NeuronID;
  newSpike->EmittingTime = EmittingTime;
  TriggeringSpikes.push_back (newSpike);
  
  LastPSPs[NeuronID] = LastSpikes[NeuronID] = EmittingTime;
  LastPSPSums[NeuronID] = 0;
}

/****************************************************************************/
void CEventGrid::MakeFilename ()
{
  char IDs[256] = "\0";
  char Times[256] = "\0";
  
  list<TSpike *>::iterator itSpike;
  list<TSpike *>::iterator itLastSpike = TriggeringSpikes.end ();
  
  if (Truncated)
    sprintf (IDs, (const char *) "trunc-\0");
  
  // write triggering spikes
  for (itSpike = TriggeringSpikes.begin (); itSpike != itLastSpike; itSpike++)
  {
    sprintf (IDs + strlen(IDs), "%04d-\0", (int) ((*itSpike)->EmittingNeuron));
    sprintf (Times + strlen(Times), "%04d-\0", (int) ((*itSpike)->EmittingTime));
  }
  
  Times[strlen(Times)-1]='\0';
  
  sprintf (Times + strlen(Times), "__%04d\0", (int) (Spikes.size () + TriggeringSpikes.size ())); 
  
  IDs[strlen(IDs)-1]='\0';

  
  Filename = new string (IDs);
  (*Filename) += string ("__");
  (*Filename) += string (Times);
}

/****************************************************************************/
bool CEventGrid::Save ()
{

  if ( ((Spikes.size () + TriggeringSpikes.size ()) < (unsigned int) CPGParameters::MIN_PG_SIZE) || (TimeSpan() < CPGParameters::MIN_PG_TIME) )
  {
    return false;
  }
  
  if (Truncated)
  {
    if (CPGParameters::AVOID_CYCLICS)
    {
      return false;
    }
    else
    {
      NbTruncated++;
    }
  }
  
  TimeSpanAccumulator += (double) TimeSpan ();
  SizeAccumulator += (double) (Spikes.size () + TriggeringSpikes.size ());

  NbPGs++;
  
  MakeFilename ();
    
  // Check if file already exists
  ofstream * myPGSpikes = new ofstream ((CPGParameters::PG_OUTPUT_DIR + (*Filename) + "-spikes." + CParameters::DATA_OUTPUT_EXT).c_str(), ios::in);
  // We dont need to check all three files ...
  ofstream * myPGExcitPSPs;// = new ofstream ((CPGParameters::PG_OUTPUT_DIR + (*Filename) + "-EPSPs." + CParameters::DATA_OUTPUT_EXT).c_str(), ios::in);
  ofstream * myPGInhibPSPs;// = new ofstream ((CPGParameters::PG_OUTPUT_DIR + (*Filename) + "-IPSPs." + CParameters::DATA_OUTPUT_EXT).c_str(), ios::in);

  if (!(myPGSpikes->fail()))
  {
    myPGSpikes->close ();
    delete myPGSpikes;
    
    return false; //don't count this PG as written
  }
  else
  {
    if (CPGParameters::SAVE_DETAILED_PGS)
      SavePlot (PGPlot);

    SaveTriggers (PGs);
    NbPGsWritten++;

    myPGSpikes->close ();
    delete myPGSpikes;
  }
  

  if (Truncated)
    NbTruncatedWritten++;

  SizeDistribs[(Spikes.size () + TriggeringSpikes.size ())/CPGParameters::SIZE_DISTRIB_BEAN]++;
  TimeSpanDistribs[(TimeSpan())/CPGParameters::TIMESPAN_DISTRIB_BEAN]++;
  
  
  if (!CPGParameters::SAVE_DETAILED_PGS)
  {
    // unfortunately, we still need to create this file to be able to know if this PG has already been found
    myPGSpikes = new ofstream ((CPGParameters::PG_OUTPUT_DIR + (*Filename) + "-spikes." + CParameters::DATA_OUTPUT_EXT).c_str(), ios::out|ios::binary|ios::trunc);
    myPGSpikes->close ();
    delete myPGSpikes;
  }
  else
  {
    // create a PG file ...
    myPGSpikes = new ofstream ((CPGParameters::PG_OUTPUT_DIR + (*Filename) + "-spikes." + CParameters::DATA_OUTPUT_EXT).c_str(), ios::out|ios::binary|ios::trunc);
    myPGExcitPSPs = new ofstream ((CPGParameters::PG_OUTPUT_DIR + (*Filename) + "-EPSPs." + CParameters::DATA_OUTPUT_EXT).c_str(), ios::out|ios::binary|ios::trunc);
    myPGInhibPSPs = new ofstream ((CPGParameters::PG_OUTPUT_DIR + (*Filename) + "-IPSPs." + CParameters::DATA_OUTPUT_EXT).c_str(), ios::out|ios::binary|ios::trunc);

    //(*myPGSpikes) << "$0 0" << endl << endl;
    (*myPGExcitPSPs) << "0 0" << endl << endl;
    (*myPGInhibPSPs) << "0 0" << endl << endl;

    CEventCell * currentCell;

    list<TSpike *>::iterator itSpike;
    list<TSpike *>::iterator itLastSpike = TriggeringSpikes.end ();

    list<CPSPEvent *>::iterator itPSP;
    list<CPSPEvent *>::iterator itLastPSP;

    // write triggering spikes
    for (itSpike = TriggeringSpikes.begin (); itSpike != itLastSpike; itSpike++)
    {
      (*myPGSpikes) << (*itSpike)->EmittingTime << " " << (*itSpike)->EmittingNeuron << endl << endl;
    }
    
    // write spikes and PSPs
    itLastSpike = Spikes.end ();
    for (itSpike = Spikes.begin (); itSpike != itLastSpike; itSpike++)
    {
      // Save spikes
      (*myPGSpikes) << (*itSpike)->EmittingTime << " " << (*itSpike)->EmittingNeuron << endl << endl;
      
      // Save Inhinibitory PSPs
      for (NNLtime Jitter = 0/*- CParameters::PG_JITTER*/; Jitter <= CPGParameters::PG_JITTER; Jitter++)
      {
        currentCell = &(Cells[((*itSpike)->EmittingNeuron) * CPGParameters::MAX_PG_TIME + ((*itSpike)->EmittingTime)-Jitter]);

        if ( !(currentCell->Empty) \
           && ((((*itSpike)->EmittingTime)-Jitter) < CPGParameters::MAX_PG_TIME) \
           && ((((*itSpike)->EmittingTime)-Jitter) >= 0) )
        {
          itLastPSP = currentCell->InhibitoryPSPs.end ();
          for (itPSP = currentCell->InhibitoryPSPs.begin (); itPSP != itLastPSP; itPSP++)
          {
            (*myPGInhibPSPs) << (*itPSP)->EmittingTime << " " << (*itPSP)->EmittingNeuron << endl;
            (*myPGInhibPSPs) << (*itPSP)->ImpactingTime << " " << (*itPSP)->ImpactedNeuron << endl << endl;
          }
        }
      }

      // Save Excitatory PSPs
      for (NNLtime Jitter = 0/*- CParameters::PG_JITTER*/; Jitter <= CPGParameters::PG_JITTER; Jitter++)
      {
        currentCell = &(Cells[((*itSpike)->EmittingNeuron) * CPGParameters::MAX_PG_TIME + ((*itSpike)->EmittingTime)-Jitter]);

        if ( !(currentCell->Empty) \
           && ((((*itSpike)->EmittingTime)-Jitter) < CPGParameters::MAX_PG_TIME) \
           && ((((*itSpike)->EmittingTime)-Jitter) >= 0) )
        {
          itLastPSP = currentCell->ExcitatoryPSPs.end ();
          for (itPSP = currentCell->ExcitatoryPSPs.begin (); itPSP != itLastPSP; itPSP++)
          {
            (*myPGExcitPSPs) << (*itPSP)->EmittingTime << " " << (*itPSP)->EmittingNeuron << endl;
            (*myPGExcitPSPs) << (*itPSP)->ImpactingTime << " " << (*itPSP)->ImpactedNeuron << endl << endl;
          }
        }
      }
    }

    myPGSpikes->close ();
    myPGExcitPSPs->close ();
    myPGInhibPSPs->close ();

    delete myPGSpikes;
    delete myPGExcitPSPs;
    delete myPGInhibPSPs;
  }
  
  return true;
}

/****************************************************************************/
void CEventGrid::SavePlot (ofstream * myPGPlot)
{
  (*myPGPlot) << "reset" << endl;

  (*myPGPlot) << "set terminal postscript eps enhanced color defaultplex \"Times-Roman\" 10" << endl;
  //(*myPGPlot) << "set size ratio 0.3" << endl;
  (*myPGPlot) << "set size 0.8,0.5" << endl;
  //(*myPGPlot) << "set size 0.4,0.25" << endl;

  (*myPGPlot) << "unset key" << endl;

  (*myPGPlot) << "set output \""<<  CPGParameters::PG_OUTPUT_DIR + (*Filename) + "." + CParameters::FIGURES_EXT << "\"" << endl;
  (*myPGPlot) << "set xlabel 'Time [ms]'" << endl;
  (*myPGPlot) << "set ylabel '# Neurons'" << endl;

  (*myPGPlot) << "set pointsize 0.5" << endl;
  
  //(*myPGPlot) << "set xtics 10" << endl;
  (*myPGPlot) << "set ytics 1" << endl;
  (*myPGPlot) << "set grid" << endl;
  
  //(*myPGPlot) << "set yrange [1:" << NeuronsByID.size () << "]" << endl;
  (*myPGPlot) << "set xrange [0:" << ((float)(CPGParameters::MAX_PG_TIME)) / (float)CParameters::TIMESCALE << "]" << endl;
  //(*myPGPlot) << "set xrange [0:150]" << endl;
  (*myPGPlot) << "plot '" << CPGParameters::PG_OUTPUT_DIR + (*Filename) + "-spikes." + CParameters::DATA_OUTPUT_EXT << "' using (($1)/" << CParameters::TIMESCALE << "):2 w p, \\" << endl;
  (*myPGPlot) << "     '" << CPGParameters::PG_OUTPUT_DIR + (*Filename) + "-EPSPs." + CParameters::DATA_OUTPUT_EXT << "' using (($1)/" << CParameters::TIMESCALE << "):2 w lines, \\" << endl;
  (*myPGPlot) << "     '" << CPGParameters::PG_OUTPUT_DIR + (*Filename) + "-IPSPs." + CParameters::DATA_OUTPUT_EXT << "' using (($1)/" << CParameters::TIMESCALE << "):2 w lines" << endl << endl << endl;
  
  return;
}

/*****************************************************************************/
void CEventGrid::SaveTriggers (ofstream * PGs)
{
  list <TSpike *>::iterator itSpike;
  list <TSpike *>::iterator itLastSpike = TriggeringSpikes.end ();
  
  (*PGs) << NbPGsWritten << " " << Spikes.size () + TriggeringSpikes.size () << " " << TimeSpan() << " " << NbDistinctNeurons();

  if (isTruncated ())
    (*PGs) << " 1";
  else
    (*PGs) << " 0";

  (*PGs) << " " << ( ( (double)(Spikes.size() + TriggeringSpikes.size()) / (double)NbDistinctNeurons() ) / ((double)TimeSpan()/(double)CParameters::TIMESCALE) )* (double)1000.0;

  // write triggering spikes
  for (itSpike = TriggeringSpikes.begin (); itSpike != itLastSpike; itSpike++)
  {
    (*PGs) << " " << (*itSpike)->EmittingNeuron << "," << (*itSpike)->EmittingTime;
  }

  if (!CPGParameters::ONLY_TRIGGERS_IN_SUMMARY)
  {
    itLastSpike = Spikes.end ();

    (*PGs) << " : ";
    // write the rest of PG
    for (itSpike = Spikes.begin (); itSpike != itLastSpike; itSpike++)
    {
      (*PGs) << " " << (*itSpike)->EmittingNeuron << "," << (*itSpike)->EmittingTime;
    }
  }

  (*PGs) << endl;
}

/****************************************************************************/
bool CEventGrid::isTruncated ()
{
  return Truncated;
}

/****************************************************************************/
NNLtime CEventGrid::TimeSpan ()
{
  if (Spikes.size () == 0)
    return 0;
  
  return Spikes.back ()->EmittingTime;
}

/****************************************************************************/
int CEventGrid::NbSpikes ()
{
  return Spikes.size ();
}

/****************************************************************************/
int CEventGrid::NbTriggers ()
{
  return TriggeringSpikes.size ();
}

/****************************************************************************/
int CEventGrid::NbDistinctNeurons ()
{
  map<int, int> UsedNeurons;

  list<TSpike *>::iterator currentSpike;
  list<TSpike *>::iterator lastSpike;

  currentSpike = TriggeringSpikes.begin ();
  lastSpike = TriggeringSpikes.end ();

  while (currentSpike != lastSpike)
  {
    UsedNeurons[(*currentSpike)->EmittingNeuron] = 1;
    currentSpike++;
  }


  currentSpike = Spikes.begin ();
  lastSpike = Spikes.end ();

  while (currentSpike != lastSpike)
  {
    UsedNeurons[(*currentSpike)->EmittingNeuron] = 1;
    currentSpike++;
  }


  return UsedNeurons.size ();
}


