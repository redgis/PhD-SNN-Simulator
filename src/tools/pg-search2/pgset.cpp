/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "pgset.h"
#include "eventgrid.h"
#include "pgparameters.hpp"

/****************************************************************************/
void CPGSet::SaveAll ()
{
  //cout << endl << "Saving data ..." << flush;
  
  for (int Index = 0; Index < CurrentNbGrids; Index++)
  {
    AllEventGrids[Index].Save ();
    AllEventGrids[Index].Clear ();
  }
  
  //cout << "done." << endl << flush;

/* Mettre les comptages ici ou dans CEventGrid 

      if ( (myPG->TimeSpan () >= CParameters::MIN_PG_TIME) && (myPG->NbSpikes () >= CParameters::MIN_PG_SIZE) && ( !(myPG->isTruncated ()) || !(CParameters::AVOID_CYCLICS) ) ) 
      {
#ifdef __DEBUG
        myPG->DisplayPG ();
#endif
        NbPGs++;
        if (myPG->isTruncated ())
          NbTruncated++;
*/
  
}

/****************************************************************************/
CPGSet::CPGSet (int NbGrids)
{
  NbMaxGrids = NbGrids;
  CurrentNbGrids = 0;
  AllEventGrids = new CEventGrid [NbMaxGrids];
  
  if (CPGParameters::SAVE_DETAILED_PGS)
    PGPlot = new ofstream ((CPGParameters::PG_OUTPUT_DIR + string ("PGs") + "." + CParameters::FIGURE_SCRIPTS_EXT).c_str(), ios::out|ios::binary|ios::trunc);

  PGs = new ofstream ((CPGParameters::PG_OUTPUT_DIR + string ("PGs") + "." + CParameters::DATA_OUTPUT_EXT).c_str(), ios::out|ios::binary|ios::trunc);

  (*PGs) << "#NbPGsWritten NbSpikes TimeSpan NbDistinctNeurons Truncated AVGFrequencyByNeuron [Triggers...] " << endl;

  NbDistribSizes = CPGParameters::MAX_PG_SIZE/CPGParameters::SIZE_DISTRIB_BEAN;
  NbDistribTimeSpans = CPGParameters::MAX_PG_TIME/CPGParameters::TIMESPAN_DISTRIB_BEAN;
  SizeDistribs = new int [NbDistribSizes];
  TimeSpanDistribs = new int [NbDistribTimeSpans];

  //Fill distribution tabs with zeros
  for (int Index = 0; Index < NbDistribSizes; Index++)
  {
    SizeDistribs[Index] = 0;
  }

  for (int Index = 0; Index < NbDistribTimeSpans; Index++)
  {
    TimeSpanDistribs[Index] = 0;
  }

}

/****************************************************************************/
CEventGrid * CPGSet::NextGrid ()
{
  if (CurrentNbGrids >= NbMaxGrids)
  {
    SaveAll ();
    CurrentNbGrids = 0;
  }
  
  CEventGrid * result = &(AllEventGrids[CurrentNbGrids]);
  
  CurrentNbGrids++;
  
  return result;
}

/****************************************************************************/
CPGSet::~CPGSet ()
{
  SaveAll (); 
  PGs->close ();
  PGPlot->close ();

  delete PGPlot;
  delete PGs;
};
