/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/



#include "pg-search.h" // generic includes for everybody

#include "pgset.h"
#include "parameters/parameters.h"
#include "eventcell.h"

map<int, CNeuron *> NeuronsByID;
vector <vector <NNLtime> > DelaysByNeuronID;
vector <vector <NNLweight> > WeightsByNeuronID;
NNLpotential * ThresholdsByNeuronID;
int NbNeurons;

CPGSet * PGSet;

int NbPGs = 0;
int NbPGsWritten = 0;
int NbTruncated = 0;
int NbTruncatedWritten = 0;

int * SizeDistribs = NULL;
int * TimeSpanDistribs = NULL;
int NbDistribSizes = 0;
int NbDistribTimeSpans = 0;

double TimeSpanAccumulator = 0;
double SizeAccumulator = 0;

//ofstream * PGPlot = new ofstream ("PGs.txt", ios::out|ios::binary|ios::trunc);

ofstream * PGPlot;
ofstream * PGs;

/*****************************************************************************/
bool Prioritize::operator() ( const CPSPEvent * p1, const CPSPEvent * p2 )
{
     return p1->ImpactingTime > p2->ImpactingTime;
}


/*****************************************************************************/
bool TEvent::operator < (TEvent const & b)
{
  return this->pEvent->ImpactingTime > b.pEvent->ImpactingTime;
}

/*****************************************************************************/
bool TEvent::operator == (TEvent const & b)
{
  return this->pEvent->ImpactingTime == b.pEvent->ImpactingTime;
}

/*****************************************************************************/
bool operator < (TEvent const &  a, TEvent const & b)
{
  return a.pEvent->ImpactingTime > b.pEvent->ImpactingTime;
}

/*****************************************************************************/
bool operator == (TEvent const  & a, TEvent const & b)
{
  return a.pEvent->ImpactingTime == b.pEvent->ImpactingTime;
}



/*****************************************************************************/
bool CPSPEvent::operator < (CPSPEvent const & b)
{
  return this->ImpactingTime > b.ImpactingTime;
}

/*****************************************************************************/
bool CPSPEvent::operator == (CPSPEvent const & b)
{
  return this->ImpactingTime == b.ImpactingTime;
}

/*****************************************************************************/
bool operator < (CPSPEvent const &  a, CPSPEvent const & b)
{
  return a.ImpactingTime > b.ImpactingTime;
}

/*****************************************************************************/
bool operator == (CPSPEvent const  & a, CPSPEvent const & b)
{
  return a.ImpactingTime == b.ImpactingTime;
}
