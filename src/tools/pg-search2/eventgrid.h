/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
 
#ifndef _EVENTGRID_H
#define _EVENTGRID_H

#include "pg-search.h" // generic includes for everybody

#include "parameters/parameters.h"

/******************************************************************************/
class CEventGrid
{
  private:
    CEventCell * Cells;                      // Grid of polychronous group events. Accessed by Cells [NeuronID][Time]
    priority_queue <CPSPEvent *, vector<CPSPEvent *>, Prioritize> PSPQueue;    // Chronological queue of upcoming PSPs
    
    list <TSpike *> TriggeringSpikes;       // Triggering spikes
    list <TSpike *> Spikes;                 // Spikes emitted - used to know which cell to look at to save PGs
    list <CPSPEvent *> AllEvents;           // All PSP events emitted - used to free memory
    
    NNLtime * LastSpikes;               // Last spike time of each neuron.
    NNLtime * LastPSPs;                 // Last PSP recieved time of each neuron
    NNLpotential * LastPSPSums; // Last membrane potential of each neuron
    
    string * Filename;
  
    NNLtime currentTime;
    int currentNeuron;
    
    bool Truncated; // See "isTruncated ()"
  
  public:
    CEventGrid ();
    virtual ~CEventGrid ();
  
    void AddSpike (int NeuronID, int EmittingTime);
    void AddTriggeringSpike (int NeuronID, int EmittingTime);
    
    
    void JumpToNextEvent ();
    void Clear ();                             // Clear the grid, and associated events
    void PostEventFromSpike (int EmittingNeuron, int EmittingTime);  // Posting PSPs triggered from this spike.
    bool DoYouSpike (int EmittingNeuron, int EmittingTime);          // Verify if the cell spikes at this time.
    void Simulate ();                          // Simulate with given starting neurons
  
    bool DoYouSpike2 (int EmittingNeuron, int EmittingTime);          // Verify if the cell spikes at this time.
    void Simulate2 ();                         // Simulate with given starting neurons

  bool Save ();                              // Save events as a PG.
    void MakeFilename ();
    void SavePlot (ofstream * myPGPlot);
    void SaveTriggers (ofstream * PGs);
  
    bool isTruncated ();  // Returns true if PG has been stopped before it dies
    NNLtime TimeSpan ();  // Returns this PG time span
    int NbSpikes ();      // returns the number of spikes in the PG
    int NbTriggers ();    // returns the number of triggering spikes in the PG
    int NbDistinctNeurons ();
    void Check ();
    
};


#endif /* _EVENTGRID_H */
