/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef PGPARAMETERS_HPP_
#define PGPARAMETERS_HPP_

#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
#include <string>

#include "pg-search.h"

using namespace std;

class CPGParameters
{
  private:
    CPGParameters ();
    virtual ~CPGParameters();


  public:

    static bool loadFromLuaFile (string InputSettingFile);
    static bool saveToLuaFile (string OutputSettingFile);

    static int MAX_MEMORY_USE;
    static string PG_OUTPUT_DIR;

    static NNLtime MAX_PG_TIME;
    static NNLtime MIN_PG_TIME;

    static int MIN_PG_SIZE;
    static int MAX_PG_SIZE;

    static NNLtime PG_JITTER;
    static int NB_TRIGGER_NEURONS;
    static bool AVOID_CYCLICS;
    static bool SEARCH_BY_NB_PSP;
    static int NB_PSP_TO_SPIKE;
    static NNLweight WEIGHT_THRESHOLD;

    static int SIZE_DISTRIB_BEAN;
    static NNLtime TIMESPAN_DISTRIB_BEAN;

    static bool SAVE_DETAILED_PGS;
    static bool ONLY_TRIGGERS_IN_SUMMARY;
};

#endif /* PGPARAMETERS_HPP_ */
