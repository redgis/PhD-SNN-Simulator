/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef _PG_SEARCH_H 
#define _PG_SEARCH_H 

//#include "../../globals.h"

/********************** Headers includes ******************/

using namespace std;

#define _GLOBALS_H

// ANSI C++
#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
#include <sstream>
#include <exception>

// STL C++
#include <vector>
#include <list>
#include <map>
#include <algorithm>
#include <queue>
#include <set>

extern "C" {

// ANSI C
#include <stdlib.h>
#include <math.h>
#include <string.h>

// UNIX/Linux/POSIX C includes
#include <unistd.h>
#include <sys/time.h>
#include <semaphore.h>
  
}

// Other headers
#include "tinyxml/tinyxml.h"
#include "common/tools.h"

class CNeuron;
class CSynapse;
class CAssembly;
class CArchitecture;
//class CEventManager;
//class CPatternManager;
//class CPatternSet;
//class CPattern;
//class CDataFile;
//class CManager;
class CParameters;
//class CGraphManager;
//class CGnuplotGraph;
class CNeuronsCombination;
class CThread;
class CPGSet;
class CEventGrid;
class CEventCell;
class CPSPEvent;

/************** type definitions **************/

/* usual types */  
typedef long int NNLtime;
typedef double NNLweight;
typedef double NNLpotential;

class Prioritize 
{
  public:
    bool operator() (const CPSPEvent * p1, const CPSPEvent * p2);
};

class TEvent
{
  public:
    CPSPEvent * pEvent;
  
    TEvent (CPSPEvent * newEvent)
    {
      pEvent = newEvent;
    }
  
  bool operator < (TEvent const &);
  bool operator == (TEvent const &);
  
  friend bool operator < (TEvent const &, TEvent const &);
  friend bool operator == (TEvent const &, TEvent const &);
};
  
typedef enum { FALSE, TRUE, UNSPECIFIED } NNLbool;

/* Spike type */
typedef struct sSpike {
NNLtime EmittingTime;
int EmittingNeuron;
} TSpike;

/* Event type */
class CPSPEvent
{
  public :
    CPSPEvent () { };
    virtual ~CPSPEvent () { };
  
  int EmittingNeuron;
  int ImpactedNeuron;
  NNLtime EmittingTime;
  NNLtime ImpactingTime;
  
  bool operator < (CPSPEvent const &);
  bool operator == (CPSPEvent const &);
  
  friend bool operator < (CPSPEvent const &, CPSPEvent const &);
  friend bool operator == (CPSPEvent const &, CPSPEvent const &);
};


/* Parameters of a neuron */
typedef struct sNeuronParameters
{
  NNLpotential Threshold;           /* Neuron threshold */
  NNLpotential MembranePotential;   /* Neuron potential */
  NNLpotential AbsoluteRefractoryPotential; /* Neuron absolute refractory potential */
  NNLtime AbsoluteRefractoryPeriode;        /* Neuron absolute refractory periode   */
  float Inhibitory;                 /* Probability for the neuron to be inhibitory  */
  NNLtime TauPSP;                   /* Time constante for a PSP */
  NNLtime TauRefract;
} NeuronParameters;


/* Parameters of a synapse */
typedef struct sSynapseParameters
{
  NNLweight Weight;       /* Synaptic weight */
  NNLtime Delay;          /* Axonal delay before the synapse. -1 = random */
  NNLtime DeltaDelay;     /* Variations around Delay if not -1  */
  NNLweight DeltaWeight;  /* Variations around Weight if not -1 */
  bool WeightAdaptation;  /* Do we make weight adaptation ?   */
  bool DelayAdaptation;   /* Do we make delay adaptation ?    */
  NNLbool Inhibitory;     /* The synapse will be inhibitory ? */
} SynapseParameters;


/* Parameters of a projection */
typedef struct sProjectionParameters
{
  float ProjectionRate;       /* probability of connexion - between 0 and 1, -1 = randomized */
  SynapseParameters SynParam; /* parameters of synapses of the projection */
} ProjectionParameters;


/* Pattern presentation method, used by the pattern manager */
typedef enum { NNL_RAND, NNL_FILE_ORDER, NNL_ALTERNATE_CLASS } NNLPresentationMethod;

typedef enum { CLASSICAL_STDP, MEUNIER_STDP } NNLStdpType;



/****************** Data definition ********************/

using namespace std;

extern map<int, CNeuron *> NeuronsByID;
extern vector <vector <NNLtime> > DelaysByNeuronID;
extern vector <vector <NNLweight> > WeightsByNeuronID;
extern NNLpotential * ThresholdsByNeuronID;
extern int NbNeurons;

extern int NbPGs;               // Total number of PGs found
extern int NbPGsWritten;        // Total number of PGs actually written
extern int NbTruncated;         // Total number of truncated PGs found
extern int NbTruncatedWritten;  // Total number of truncated PGs actually written

extern int * SizeDistribs;
extern int * TimeSpanDistribs;
extern int NbDistribSizes;
extern int NbDistribTimeSpans;

extern double TimeSpanAccumulator;
extern double SizeAccumulator;

extern CPGSet * PGSet;
extern ofstream * PGPlot;
extern ofstream * PGs;

#define StayInAssembly true
//#define __DEBUG

#endif /* _PG_SEARCH_H */

