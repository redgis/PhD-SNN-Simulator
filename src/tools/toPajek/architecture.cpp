/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


#include "architecture.h"

#include "assembly.h"

/*****************************************************************************/
bool CArchitecture::XMLload (string InputXMLFile)
{
  CAssembly * NewAssembly;

  TiXmlDocument doc (InputXMLFile.c_str());
  if (!doc.LoadFile()) return false;


	TiXmlElement* root = doc.FirstChildElement( "Assembly" );

  while (root)
  {
    //cout << root->Value() << endl;
    NewAssembly = new CAssembly ();
    NewAssembly->XMLload (root, &NeuronsByID);
    Assemblies.push_back(NewAssembly);
    root = root->NextSiblingElement( "Assembly" );
  }

  return true;

}


/*****************************************************************************/
CAssembly * CArchitecture::getAssemblyByName ( string SearchName )
{
  vector<CAssembly *>::iterator Current = Assemblies.begin ();
  vector<CAssembly *>::iterator End = Assemblies.end ();
  
  while (Current != End)
  {    
    if ( (*Current)->Name == SearchName )
      break;
    
    Current ++;   
  }
  
  if (Current == End)
    return NULL;
  
  return (*Current);
}
