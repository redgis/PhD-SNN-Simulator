/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
 
#include "topajek.h"

#include "parameters/parameters.h"
#include "architecture.h"
#include "assembly.h"
#include "neuron.h"
#include "synapse.h"


bool exportWeights = false;
bool exportDelays = false;

/****************************************************************************/
// Because I am clever, and want to have easy access to various informations
void CreateUsefulStructures ();


void help ()
{
  cerr << "*** ERROR : expecting argument." << endl;
  cerr << "    Usage : topajek <XML architecture file> <pajek output filename> -[w|d]" << endl;
  exit (1);
}

/****************************************************************************/
int main (int argc, char * argv[])
{
  struct timeval start;
  struct timeval end;
  struct timeval elapsed;

  CArchitecture Architecture;

  //Loading default settings
  //  CParameters::loadFromLuaFile ("../../"+CParameters::SETTINGS_DIR+"settings-USPS.lua");
  
  if (argc >= 3)
  {
    if (argc == 4)
    {
      if (argv[3][0] == '-')
      {
        if (argv[3][1] == 'w')
        { exportWeights = true; }
        if (argv[3][1] == 'd')
        { exportDelays = true; }
      }
      else
      {
        help();
      }
    }
  }
  else
  {
    help ();
  }
  
  Architecture.XMLload (argv[1]);

  //Loading the previously saved network
  //cout << "Loading " << "\"../../"+CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0000000000--RandSeed-1153245669--Size-200--Classes-10--timescale-10.xml\"" << "... ";
  //Architecture.XMLload ("../../"+CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0000000000--RandSeed-1153245669--Size-200--Classes-10--timescale-10.xml");
  //cout << " done." << endl;
  
  //cout << "Loading " << "\"../../"+CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0310258999--RandSeed-1153245669--Size-200--Classes-10--timescale-10.xml\"" << "... ";
  //Architecture.XMLload ("../../"+CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0310258999--RandSeed-1153245669--Size-200--Classes-10--timescale-10.xml");
  //cout << " done." << endl;
  
  //cout << "Loading " << "\"../../"+CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0000000.xml\"" << "... ";
  //Architecture.XMLload ("../../"+CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0000000.xml");
  //cout << " done." << endl;
  
  //cout << "Loading " << "\"../../"+CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0200009.xml\"" << "... ";
  //Architecture.XMLload ("../../"+CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0200009.xml");
  //cout << " done." << endl;
  
  //Init empty neurons in NeuronByID, to avoid segfault

  map<int, CNeuron *>::iterator itNeuronID;
  map<int, CNeuron *>::iterator itLastNeuronID;

  int MaxNeuronID = (*(NeuronsByID.begin ())).first;
  
  // Find max neuron ID
  for (itNeuronID = NeuronsByID.begin (); itNeuronID != NeuronsByID.end (); itNeuronID++)
  {
    if ( (*itNeuronID).first > MaxNeuronID )
      MaxNeuronID = (*itNeuronID).first;
  }
  
  // Set every empty id to a dummy neuron
  for (int Index = 0; Index <= MaxNeuronID; Index ++)
  {
    if (NeuronsByID.find (Index) == NeuronsByID.end())
    {
      NeuronsByID[Index] = new CNeuron ();
      NeuronsByID[Index]->ID = Index;
    }
  }
  
  
  CreateUsefulStructures ();
  
  
  string AssemblyName = "Reservoir";
  
  if (!Architecture.getAssemblyByName (AssemblyName))
  {
    cout << "*** ERROR : Assembly \"" << AssemblyName << "\" not found ! Exiting." << endl;
    return 1;
  }
  
  CAssembly * Ass = Architecture.getAssemblyByName (AssemblyName);
  
  gettimeofday (&start, NULL);

  //Open Weight and delay distribution files
  ofstream * PajekOutput = new ofstream (string(argv[2]).c_str (), ios::out|ios::trunc);

  cout << "Writing to " << argv[2] << "... " << endl;
  
  vector<CSynapse *>::iterator itSynapse;
  vector<CSynapse *>::iterator itLastSynapse;
  
  vector<CNeuron *>::iterator itNeuron = Ass->Neurons.begin ();
  vector<CNeuron *>::iterator itLastNeuron = Ass->Neurons.end ();
  
  (*PajekOutput) << "*Vertices " << Ass->Neurons.size () << endl;
  
  while (itNeuron != itLastNeuron)
  {
    (*PajekOutput) << (*itNeuron)->ID+1 << endl;
    itNeuron++;
  }
  
  (*PajekOutput) << "*Arcs" << endl;
  
  itNeuron = Ass->Neurons.begin ();
  
  while (itNeuron != itLastNeuron)
  {
    itSynapse = (*itNeuron)->outputSynapses.begin ();
    itLastSynapse = (*itNeuron)->outputSynapses.end ();
    
    while (itSynapse != itLastSynapse)
    {
      (*PajekOutput) << (*itSynapse)->InputNeuron->ID+1 << " " << (*itSynapse)->OutputNeuron->ID+1;

      if (exportWeights)
      {
        (*PajekOutput) << " " << (*itSynapse)->Weight;
      }
      else if (exportDelays)
      {
        (*PajekOutput) << " " << (*itSynapse)->Delay;
      }

      (*PajekOutput) << endl;
      itSynapse++;
    }
    itNeuron++;
  }
  
  
  (*PajekOutput).close();
  
  gettimeofday (&end, NULL);
  
  elapsed.tv_sec = end.tv_sec - start.tv_sec;
  elapsed.tv_usec = end.tv_usec - start.tv_usec;
  
  cout << endl << elapsed.tv_sec << "." << setw(6) << setfill ('0') << (unsigned long) elapsed.tv_usec << " s" << ".          " << endl;
  
  return 0;
}

/****************************************************************************/
// Because I am clever, and want to have easy access to various informations
void CreateUsefulStructures ()
{
  int NbNeurons = NeuronsByID.size ();
  
  int InputNeuron;
  int OutputNeuron;
  
  DelaysByNeuronID.resize (NbNeurons);
  WeightsByNeuronID.resize(NbNeurons);
  
  ThresholdsByNeuronID = new NNLpotential[NbNeurons];
  
  // Initialise neuron tables
  for (int Index = 0; Index < NbNeurons; Index++)
  {
    ThresholdsByNeuronID[Index] = NeuronsByID[Index]->Threshold;
  }
  
  // Initialise synapse matrices
  for ( int InputIndex = 0; InputIndex != NbNeurons; InputIndex++ )
  {
    DelaysByNeuronID [InputIndex].resize (NbNeurons);
    WeightsByNeuronID [InputIndex].resize (NbNeurons);
    
    for ( int OutputIndex = 0; OutputIndex != NbNeurons; OutputIndex++ )
    {
      DelaysByNeuronID [InputIndex][OutputIndex] = -1; // -1 = no connexion
      WeightsByNeuronID [InputIndex][OutputIndex] = 0; // 0 = no connexion or no weight ... the same anyway
    }
  }
  
  // Matrix of weightes , delays
  map<int, CNeuron *>::iterator itNeuron = NeuronsByID.begin ();
  map<int, CNeuron *>::iterator itLastNeuron = NeuronsByID.end ();
  vector<CSynapse *>::iterator itSynapse;
  vector<CSynapse *>::iterator itLastSynapse;
  
  for ( ; itNeuron != itLastNeuron; itNeuron++ )
  {
    itLastSynapse = (*itNeuron).second->outputSynapses.end ();
    
    for (itSynapse = (*itNeuron).second->outputSynapses.begin (); itSynapse != itLastSynapse; itSynapse++ )
    {
      InputNeuron = (*itSynapse)->InputNeuron->ID;
      OutputNeuron = (*itSynapse)->OutputNeuron->ID;
      
      DelaysByNeuronID [InputNeuron][OutputNeuron] = (*itSynapse)->Delay;
      WeightsByNeuronID [InputNeuron][OutputNeuron] = (*itSynapse)->Weight;
      
      //cout << (*itSynapse)->Delay << " " << (*itSynapse)->Weight << endl;
      //cout << DelaysByNeuronID [InputNeuron][OutputNeuron] << " " << WeightsByNeuronID [InputNeuron][OutputNeuron] << endl;
    }
  }
  
  
}


