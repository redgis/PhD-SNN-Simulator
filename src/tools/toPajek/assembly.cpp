/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
 

#include "assembly.h"

#include "neuron.h"

/*****************************************************************************/
bool CAssembly::XMLload (TiXmlElement* pXmlAssembly, map<int, CNeuron*> * NeuronByID)
{
  CNeuron * NewNeuron = NULL;

  Name = pXmlAssembly->Attribute ("name");

  TiXmlElement* pXmlNeuron = pXmlAssembly->FirstChildElement ("Neuron");
  int NeuronID;

  while (pXmlNeuron)
  {
    pXmlNeuron->Attribute ("id", &NeuronID);

    //cout << NeuronID << "...";

    if ((*NeuronByID)[NeuronID] == NULL)
    {
      NewNeuron = new CNeuron (this);
      (*NeuronByID)[NeuronID] = NewNeuron;
      NewNeuron->XMLload (pXmlNeuron, NeuronByID);
    }
    else
    {
      //cout << "Existe déja !" << endl;
      (*NeuronByID)[NeuronID]->ParentAssembly = this;
      (*NeuronByID)[NeuronID]->XMLload (pXmlNeuron, NeuronByID);
    }

    Neurons.push_back ((*NeuronByID)[NeuronID]);
    (*NeuronByID)[NeuronID]->localID = Neurons.size ()-1;
    pXmlNeuron = pXmlNeuron->NextSiblingElement( "Neuron" );

    //cout << (*NeuronByID)[NeuronID] << " ";
  }

  cout << "\"" << Name << "\"(" << Neurons.size() << ")...";

  return true;
}
