/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef _NEURON_H
#define _NEURON_H

#include "topajek.h" // generic includes for everybody


class CNeuron
{
  public:
    CNeuron (CAssembly * ParentAss = NULL) : ParentAssembly (ParentAss) {}

    vector <CSynapse *> inputSynapses;
    vector <CSynapse *> outputSynapses;

    CAssembly * ParentAssembly;

    NNLpotential MembranePotential;
    NNLpotential PSPSum;
    NNLpotential RestingPotential;

    int ID;
    int localID;

    bool Inhibitory;
    NNLtime AbsoluteRefractoryPeriode;
    NNLpotential AbsoluteRefractPotential;
    NNLpotential Threshold;
    NNLtime TauPSP;
    NNLtime TauRefract;

    //list<NNLevent *> SpikingPSPs; /* PSP which made the spike */
    //NNLevent SpikingPSP;    /* PSP which made the spike */
    //NNLevent LastPSP;
    //NNLtime LastSpike;
    //NNLtime OldSpike;

    NNLpotential * PSPShape;      /* Shape of a PSP */
    NNLpotential * RefractShape;  /* Shape of the relative refractory period */

    bool XMLload (TiXmlElement * pXmlNeuron, map<int, CNeuron*> * NeuronByID);
    void InitPSPShape ();
    void InitRefractShape ();

    NNLpotential PSPValue (int t);
    NNLpotential RefractValue (int t);


    NNLpotential PSPFunction (NNLtime t);
    NNLpotential RefractoryPotential (NNLtime t);
    //NNLpotential InputFunction (NNLevent &); /* Input calculation function */

    //void emitSpike (NNLtime);  /* Post a spike event */
    //void ImpactedBy (NNLevent &); /* Adds this SPIKE_IMPACT_EVENT to NewPSPs list */

    //bool DoYouSpike (TEvent * myPSP, NNLpotential * LastKnownPotential, NNLtime * LastPSP, NNLtime * LastSpike);
    //bool DoYouSpike (TEvent * myPSP, list <TEvent * > *, NNLtime * LastSpike, int);

};


#endif /* _NEURON_H */
