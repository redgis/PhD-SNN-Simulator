/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


#include "avg.hpp"


double * DataBuffer;
double * WeightBuffer;
bool WeightedMean = false;
bool StdDev = false;

void help ()
{
  cout << "Print the mean of input data." << endl;
  cout << "  Usage: avg [-w] [-d]" << endl;
  cout << "         -w Weighed mean: input data is alternatively [weight] [data] [weight] [data] ..." << endl;
  cout << "            and weighted mean should be calculated" << endl;
  cout << "         -d Standard deviation: also print standard deviation" << endl;
  cout << "  Data should be send to standard input of process." << endl;

  exit (1);
}

void parseArg (char * argument)
{
  if (argument[0] == '-')
    switch (argument[1])
    {
      case 'w':
        WeightedMean = true;
        break;

      case 'd':
        StdDev = true;
        break;

      default:
        help ();
        break;
    }
  else
    help ();
}


int main (int argc, char * argv[])
{
  size_t Size = 0;
  size_t BlockSize = 1*100*1000;
  size_t MaxSize = BlockSize;

  double mean;
  double stddev;



  /* Manage arguments */
  if (argc <= 3)
  {
    for (int Index = 1; Index < argc; Index++)
    {
      parseArg(argv[Index]);
    }
  }
  else
  {
    help ();
  }

  /* Allocate input buffer */
  DataBuffer = (double*) malloc (sizeof(double) * MaxSize);
  if (WeightedMean)
    WeightBuffer = (double*) malloc (sizeof(double) * MaxSize);

  /* Fill in the buffer with input data */
  do
  {
    if (WeightedMean)
      cin >> WeightBuffer[Size];
    cin >> DataBuffer[Size];

    if ( !(cin.fail() || cin.eof()) )
      Size++;

    if (Size >= MaxSize)
    {
      MaxSize += BlockSize;
      DataBuffer = (double*) realloc (DataBuffer, MaxSize * sizeof(double));
      if (WeightedMean)
        WeightBuffer = (double*) realloc (WeightBuffer, MaxSize * sizeof(double));

      if ((DataBuffer == NULL) || (WeightedMean && (WeightBuffer == NULL)))
      {
        cerr << "**** ERROR Reallocating data buffer." << endl;
        exit (1);
      }
    }
    //cout << (double) Size / (double) MaxSize << '\r';
  } while (!(cin.fail()) && !(cin.eof()) && (Size < MaxSize));


  if (WeightedMean)
    mean = gsl_stats_wmean (WeightBuffer, 1, DataBuffer, 1, Size);
  else
    mean = gsl_stats_mean (DataBuffer, 1, Size);


  if (StdDev)
  {
    if (WeightedMean)
      stddev = gsl_stats_wsd_m (WeightBuffer, 1, DataBuffer, 1, Size, mean);
    else
      stddev = gsl_stats_sd_m (DataBuffer, 1, Size, mean);
  }


  if (StdDev)
    cout << mean << " " << stddev << endl;
  else
    cout << mean << endl;


  free (DataBuffer);
  return 0;
}
