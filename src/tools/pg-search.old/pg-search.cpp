/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
 

#include <iostream>
#include <iomanip>
#include <sys/time.h>

using namespace std;



//NNLweight Weights[N][N];  // Weights matrix
//NNLtime Delays[N][N];     // Delays Matrix


/*****************************************************************************/
/* This function should be custumised to fit to your personnal network 
 structure file format.*/
void LoadNetwork (string Filname="")
{

}

int main (int argc, char * argv[])
{
  /* Check arguments */
  if (argc != 2)
    cout << "Use : " << argv[0] << " <Network_structure_file>" << endl;
  
  struct timeval start;
  struct timeval end;
  struct timeval elapsed;
  
  gettimeofday (&start, NULL);
  
  int Taille = 1000*10*150;
  
  void * memoire = malloc (Taille*sizeof(int));
  
  for (int i = 0; i < 10000; i++)
  {
    //for (int j = 0; j < 100; j++)
    //{
      memset (memoire, 0, Taille);
    //}
  }
       
       
  gettimeofday (&end, NULL);
  
  elapsed.tv_sec = end.tv_sec - start.tv_sec;
  elapsed.tv_usec = end.tv_usec - start.tv_usec;
  
  cout << "Done in " << elapsed.tv_sec << "." << setw(6) << setfill ('0') << (unsigned long) elapsed.tv_usec << " s" << ".          " << endl;
}
