/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "circledeventqueue.h"


/****************************************************************************/
CCircledEventQueue::CCircledEventQueue (int QSize)
{
  QueueSize = QSize;
  EventQueue.resize (QSize);
  
  //~ for (int Index = 0; Index < QSize; Index ++)
  //~ {
    //~ EventQueue[Index].resize (NbEventByTime);
  //~ }
  
  NbEvents = 0;
  
  itTime = EventQueue.begin ();
  CurrentTime = 0;
}

/****************************************************************************/
CCircledEventQueue::~CCircledEventQueue ()
{
	//vector < list <TEvent * > > EventQueue;

  vector <list <TEvent * > >::iterator itTime;
  vector <list <TEvent * > >::iterator itLastTime;
  
  list <TEvent *>::iterator itEvent;
  list <TEvent *>::iterator itLastEvent;
  
  
  itLastTime = EventQueue.end ();
  
  for (itTime = EventQueue.begin (); itTime != itLastTime; itTime++)
  {
    itLastEvent = (*itTime).end ();
    
    for (itEvent = (*itTime).begin (); itEvent != itLastEvent; itEvent++)
    {
      delete (*itEvent);
    }
  }
}


/****************************************************************************/
void CCircledEventQueue::PostEvent (TEvent * evt)
{
  if (evt->Time - CurrentTime > QueueSize)
    cout << "*** WARNING : " << __FILE__ << " " << __LINE__ << ": QueueSize looks too small for your the scope of your events !" << endl;
  
  NbEvents++;
  
  EventQueue [evt->Time % QueueSize].push_back (evt);
}

/****************************************************************************/
int CCircledEventQueue::Next ()
{
  int result = 0;
  TEvent * evtTmp;
  
  if (itTime != EventQueue.end ())
  {
    // Clear current event stack, and go to next time step
    NbEvents -= (*itTime).size ();
    
    
    while (! ((*itTime).empty ()))
    {
      evtTmp = (*itTime).front ();
      (*itTime).pop_front ();
      
      delete evtTmp;
    }
    
    (*itTime).clear ();
    itTime++;
    CurrentTime ++;
  }
  
  if (itTime == EventQueue.end ())
  {
    itTime = EventQueue.begin ();
    result = 1;
  }
  
  return result;
}

/****************************************************************************/
bool CCircledEventQueue::isChangingTime ()
{
  return (*itTime).empty ();
}

/****************************************************************************/
TEvent * CCircledEventQueue::GetNextEvent ()
{
  TEvent * evt = NULL;
  
  // counts nb time we looped the queue. helps to detect when queue is empty
  int NbCycle = 0;
  
  // Go ahead in time 
  while ( (evt == NULL) && (NbCycle < 2) )
  {
    if ( !((*itTime).empty ()) )
    {
      evt = (*itTime).front ();
      
      if (evt->Time < CurrentTime)
        cout << "*** WARNING : " << __FILE__ << " " << __LINE__ << ": Be careful Marty ! We are back to the futur ! " \
        "CurrentTime : " << CurrentTime << ", event time : " << evt->Time << endl;
      
      CurrentTime = evt->Time;
      //cout << evt->Time << "-";
      
      (*itTime).pop_front ();
      
      NbEvents--;
      //cout << evt->Time << endl;
    }
    else
    {
      NbCycle += Next ();
    }
    
  }

  
  return evt;
}


/****************************************************************************/
int CCircledEventQueue::size ()
{
  return NbEvents;
}
