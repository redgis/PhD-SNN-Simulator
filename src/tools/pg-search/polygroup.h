/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
 
#ifndef _POLYGROUP_H
#define _POLYGROUP_H

#include "pg-search.h" // generic includes for everybody


/*****************************************************************************/
class CPolyGroup
{
  private:
    vector <TSpike *> TriggeringSpikes;
    vector <TSpike *> Spikes;
    
    CCircledEventQueue * PSPEventQueue;
  
    bool Truncated;
    
  public:
    CPolyGroup (CNeuronsCombination *, CNeuron * TriggeredNeuron);
    virtual ~CPolyGroup ();
    
    //~ void AddSpike (CNeuron *, NNLtime);
    void AddSpike (TSpike * spike);
    void AddTriggeringSpike (CNeuron *, NNLtime); 
    
    void PostPSPEventsFromSpike (TSpike * spikes);
    
    //Calculates the polychronous group based on existing spikes and the number of spikes that is enough to trigger a new spike. Returns the number of spikes.
    bool GeneratePG (int NbSpikeRequired);
    
    //Calculates the polychronous group based on existing spikes and the threshold/resting potential/weighte. Returns the number of spikes.
    bool GeneratePG (); 
    
    //Saves the pg to file.
    bool SavePG ();
    
    //Save gnuplot commands
    void SavePlot (ofstream * myPGPlot);
    void SaveTriggers (ofstream * myPGPlot);
  
    string * GenerateFilename();
    
    void DisplayPG ();
    
    // Returns the number of spikes
    int NbSpikes ();
    
    // Returns the number of triggers
    int NbTriggers ();
  
    // Says if the PG seems cyclic
    bool isTruncated ();
  
    // Returns the last spike time
    NNLtime TimeSpan ();
};


#endif /* _POLYGROUP_H */
