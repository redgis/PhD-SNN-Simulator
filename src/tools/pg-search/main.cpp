/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
 
#include "pg-search.h"

#include "../../parameters.h"
#include "architecture.h"
#include "assembly.h"
#include "neuron.h"
#include "neuroncombination.h"
#include "synapse.h"
#include "polygroupsaver.h"

/****************************************************************************/
void ParseCombinations (vector <CNeuron *>::iterator itPreviousNeuron, vector <CNeuron *>::iterator itLastNeuron, CNeuronsCombination * Combination, int NeuronRank, int NbTriggeringNeurons);

/****************************************************************************/
// Because I am clever, and want to have easy access to various informations
void CreateUsefulStructures ();


/****************************************************************************/
int main (int argc, char * argv[])
{
  struct timeval start;
  struct timeval end;
  struct timeval elapsed;

  CArchitecture Architecture;
  
  PolyGroupSaver = new CPolyGroupSaver ();
  
  if (argc < 2)
  {
    cerr << "*** ERROR : expecting argument." << endl;
    cerr << "    Usage : " << argv[0] << " <XML architecture file> <settings file>" << endl;
    exit (1);
  }
  else
  {
    if (argc == 3)
    {
      //Loading specified settings
      cout << "Loading parameters from LUA script " << "\""+string(argv[2])+"\"...";
      CParameters::loadFromLuaFile (argv[2]);
      cout << " done." << endl;
    }
    else
    {
      //Loading default settings
      cout << "Loading parameters from LUA script " << "\""+CParameters::SETTINGS_DIR+"settings-USPS.lua\"...";
      CParameters::loadFromLuaFile (CParameters::SETTINGS_DIR+"settings-USPS.lua");
      cout << " done." << endl;
    }
      
      
    cout << "Loading " << "\"" << argv[1] << "\"" << "... ";
    Architecture.XMLload (argv[1]);
    //Architecture.XMLload ("architectures/architecture-0000310000--RandSeed-1153245669--Size-1000--Classes-10--timescale-10.xml");
    cout << " done." << endl;
  }
  
  //Loading the previously saved network
  //cout << "Loading " << "\"../../"+CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0000000000--RandSeed-1153245669--Size-200--Classes-10--timescale-10.xml\"" << "... ";
  //Architecture.XMLload ("../../"+CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0000000000--RandSeed-1153245669--Size-200--Classes-10--timescale-10.xml");
  //cout << " done." << endl;
  
  //cout << "Loading " << "\"../../"+CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0310258999--RandSeed-1153245669--Size-200--Classes-10--timescale-10.xml\"" << "... ";
  //Architecture.XMLload ("../../"+CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0310258999--RandSeed-1153245669--Size-200--Classes-10--timescale-10.xml");
  //cout << " done." << endl;
  
  //cout << "Loading " << "\"../../"+CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0000000.xml\"" << "... ";
  //Architecture.XMLload ("../../"+CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0000000.xml");
  //cout << " done." << endl;
  
  //cout << "Loading " << "\"../../"+CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0200009.xml\"" << "... ";
  //Architecture.XMLload ("../../"+CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0200009.xml");
  //cout << " done." << endl;
  
  //Init empty neurons in NeuronByID, to avoid segfault

  map<int, CNeuron *>::iterator itNeuronID;
  map<int, CNeuron *>::iterator itLastNeuronID;

  int MaxNeuronID = (*(NeuronsByID.begin ())).first;
  
  // Find max neuron ID
  for (itNeuronID = NeuronsByID.begin (); itNeuronID != NeuronsByID.end (); itNeuronID++)
  {
    if ( (*itNeuronID).first > MaxNeuronID )
      MaxNeuronID = (*itNeuronID).first;
  }
  
  // Set every empty id to a dummy neuron
  for (int Index = 0; Index <= MaxNeuronID; Index ++)
  {
    if (NeuronsByID.find (Index) == NeuronsByID.end())
    {
      NeuronsByID[Index] = new CNeuron ();
      NeuronsByID[Index]->ID = Index;
    }
  }
  
  
  CreateUsefulStructures ();
  
  //NeuronCombination
  CNeuronsCombination Combination;
  
  
  //Creating Work chunks 
  vector <CNeuron *>::iterator itNeuron;
  vector <CNeuron *>::iterator itLastNeuron;
    
  int NbTriggeringNeurons = CParameters::NB_TRIGGER_NEURONS;
  
  string AssemblyName = "Reservoir";
  
  if (!Architecture.getAssemblyByName (AssemblyName))
  {
    cout << "*** ERROR : Assembly \"" << AssemblyName << "\" not found ! Exiting." << endl;
    return 1;
  }
  
  //itNeuron     = find (Architecture.getAssemblyByName (AssemblyName)->Neurons.begin(), Architecture.getAssemblyByName (AssemblyName)->Neurons.end (), NeuronsByID[434]);
  itNeuron     = Architecture.getAssemblyByName (AssemblyName)->Neurons.begin ();
  itLastNeuron = Architecture.getAssemblyByName (AssemblyName)->Neurons.end ();
  
  
  cout << "------------------------------------" << endl;
  
  gettimeofday (&start, NULL);
  
  //Starting PG search 
  ParseCombinations (itNeuron, itLastNeuron, &Combination, 0, NbTriggeringNeurons);

  gettimeofday (&end, NULL);
  
  elapsed.tv_sec = end.tv_sec - start.tv_sec;
  elapsed.tv_usec = end.tv_usec - start.tv_usec;
  
  PolyGroupSaver->Exit = true;
  sem_post (&semThread);
  PolyGroupSaver->Join ();
  
  cout << endl << "Found " << NbPGsWritten << " distinct PGs, " << NbTruncatedWritten << " truncated (probably cyclics) in " << elapsed.tv_sec << "." << setw(6) << setfill ('0') << (unsigned) elapsed.tv_usec << " s" << ".          " << endl;
  
  ofstream * PGscan = new ofstream ((CParameters::PG_OUTPUT_DIR + "pg-scan" + CParameters::DATA_OUTPUT_EXT).c_str(), ios::out);
  
  (*PGscan) << "Found " << NbPGsWritten << " distinct PGs, " << NbTruncatedWritten << " truncated (probably cyclics) in " << (time_t) elapsed.tv_sec << "." << setw(6) << setfill ('0') << (unsigned long) elapsed.tv_usec << " s" << ".          " << endl;
  
  PGscan->close ();
  
  return 0;
}

/****************************************************************************/
// Because I want to have easy access to various informations
void CreateUsefulStructures ()
{
  int NbNeurons = NeuronsByID.size ();
  
  int InputNeuron;
  int OutputNeuron;
  
  DelaysByNeuronID.resize (NbNeurons);
  WeightsByNeuronID.resize(NbNeurons);
  
  ThresholdsByNeuronID = new NNLpotential[NbNeurons];
  
  // Initialise neuron tables
  for (int Index = 0; Index < NbNeurons; Index++)
  {
    ThresholdsByNeuronID[Index] = NeuronsByID[Index]->Threshold;
  }
  
  // Initialise synapse matrices
  for ( int InputIndex = 0; InputIndex != NbNeurons; InputIndex++ )
  {
    DelaysByNeuronID [InputIndex].resize (NbNeurons);
    WeightsByNeuronID [InputIndex].resize (NbNeurons);
    
    for ( int OutputIndex = 0; OutputIndex != NbNeurons; OutputIndex++ )
    {
      DelaysByNeuronID [InputIndex][OutputIndex] = -1; // -1 = no connexion
      WeightsByNeuronID [InputIndex][OutputIndex] = 0; // 0 = no connexion or no weight ... the same anyway
    }
  }
  
  // Matrix of weightes , delays
  map<int, CNeuron *>::iterator itNeuron = NeuronsByID.begin ();
  map<int, CNeuron *>::iterator itLastNeuron = NeuronsByID.end ();
  vector<CSynapse *>::iterator itSynapse;
  vector<CSynapse *>::iterator itLastSynapse;
  
  for ( ; itNeuron != itLastNeuron; itNeuron++ )
  {
    itLastSynapse = (*itNeuron).second->outputSynapses.end ();
    
    for (itSynapse = (*itNeuron).second->outputSynapses.begin (); itSynapse != itLastSynapse; itSynapse++ )
    {
      InputNeuron = (*itSynapse)->InputNeuron->ID;
      OutputNeuron = (*itSynapse)->OutputNeuron->ID;
      
      if ((*itSynapse)->Weight >= CParameters::WEIGHT_THRESHOLD)
        DelaysByNeuronID [InputNeuron][OutputNeuron] = (*itSynapse)->Delay;
      WeightsByNeuronID [InputNeuron][OutputNeuron] = (*itSynapse)->Weight;
      
      //cout << InputNeuron << "->" << OutputNeuron << " : " << (*itSynapse)->Delay << " " << DelaysByNeuronID [InputNeuron][OutputNeuron] << endl;
    }
  }
  
  
}


/****************************************************************************/
// recursive function that scans all neuron combinations
void ParseCombinations (vector <CNeuron *>::iterator itPreviousNeuron, vector <CNeuron *>::iterator itLastNeuron, CNeuronsCombination * Combination, int NeuronRank, int NbTriggeringNeurons)
{
  //~ struct timeval start;
  //~ struct timeval end;
  //~ struct timeval elapsed;
  
  int tmpNbPGs;
  
  if ( itPreviousNeuron == itLastNeuron ) 
    return;
  
  vector <CNeuron *>::iterator itNeuron;
  
  if (NeuronRank == 0)
    itNeuron = itPreviousNeuron;
  else
    itNeuron = itPreviousNeuron + 1;
  
  CNeuronsCombination * newCombination;
  
  while (itNeuron != itLastNeuron)
  {
    if (! (*itNeuron)->Inhibitory)
    {
      if (NeuronRank == NbTriggeringNeurons - 1)
      {
        //Set the new trigger neurons combination
        Combination->AddNeuron ((*itNeuron));
        
        if ((*itNeuron)->ID%50 == 0)
        {
          Combination->DisplayPermutation ();
          cout << " " << NbPGs << " PGs found so far (" << NbPGsWritten << " actually written), " << NbTruncated << " truncated ( " << NbTruncatedWritten << " actually written)     \r\b" << flush;
        }
        
        newCombination = new CNeuronsCombination ((*Combination));
        
        //gettimeofday (&start, NULL);
        
        if (CParameters::SEARCH_BY_NB_PSP)
          tmpNbPGs = newCombination->FindPGs (CParameters::NB_PSP_TO_SPIKE);
        else
          tmpNbPGs = newCombination->FindPGs ();
        //gettimeofday (&end, NULL);
        
        //elapsed.tv_sec = end.tv_sec - start.tv_sec;
        //elapsed.tv_usec = end.tv_usec - start.tv_usec;
        
        //if (tmpNbPGs > 0)
        //  cout << elapsed.tv_sec << "." << setw(6) << setfill ('0') << (unsigned long) elapsed.tv_usec << " s" << endl;
        
        //if (tmpNbPGs > 0)
        //  cout << "----" << endl;
        
        /********************************************************/
        /*
        gettimeofday (&start, NULL);
        tmpNbPGs = newCombination->FindPGs (3);
        gettimeofday (&end, NULL);
        
        elapsed.tv_sec = end.tv_sec - start.tv_sec;
        elapsed.tv_usec = end.tv_usec - start.tv_usec;
        
        if (tmpNbPGs > 0)
          cout << elapsed.tv_sec << "." << setw(6) << setfill ('0') << (unsigned long) elapsed.tv_usec << " s" << endl;
        
        if (tmpNbPGs > 0)
          cout << "------------------------------------" << endl;
        */
        
        NbPGs += tmpNbPGs;
        
        delete newCombination;
        Combination->RemoveLastNeuron ();
      }
      else
      {
        Combination->AddNeuron ((*itNeuron));
        ParseCombinations (itNeuron, itLastNeuron, Combination, NeuronRank+1, NbTriggeringNeurons);
        Combination->RemoveLastNeuron ();
      }
    }
    
    itNeuron++;
  }
}
