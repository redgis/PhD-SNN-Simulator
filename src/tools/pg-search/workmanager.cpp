/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

CWorkManager::CWorkManager (int NbW = 2) : NbWorkers(NbW);

/****************************************************************************/
// recursive function that scans all neuron combinations
void CWorkManager::ParseCombinations (vector <CNeuron *>::iterator itPreviousNeuron, vector <CNeuron *>::iterator itLastNeuron, CNeuronsCombination * Combination, int NeuronRank, int NbTriggeringNeurons)
{
  //~ struct timeval start;
  //~ struct timeval end;
  //~ struct timeval elapsed;
  
  int tmpNbPGs;
  
  if ( itPreviousNeuron == itLastNeuron ) 
    return;
  
  vector <CNeuron *>::iterator itNeuron = itPreviousNeuron + 1;
  
  CNeuronsCombination * newCombination;
  
  while (itNeuron != itLastNeuron)
  {
    
    if (NeuronRank == NbTriggeringNeurons - 1)
    {
      //Set the new trigger neurons combination
      Combination->AddNeuron ((*itNeuron));
      
      if ((*itNeuron)->ID%50 == 0)
      {
        Combination->DisplayPermutation ();
        cout << " " << NbPGs << " PGs found so far, " << NbTruncated << " truncated    \r\b";
      }
      
      newCombination = new CNeuronsCombination ((*Combination));
      
      //gettimeofday (&start, NULL);
      tmpNbPGs = newCombination->FindPGs ();
      //gettimeofday (&end, NULL);
      
      //elapsed.tv_sec = end.tv_sec - start.tv_sec;
      //elapsed.tv_usec = end.tv_usec - start.tv_usec;
      
      //if (tmpNbPGs > 0)
      //  cout << elapsed.tv_sec << "." << setw(6) << setfill ('0') << (unsigned long) elapsed.tv_usec << " s" << endl;
      
      //if (tmpNbPGs > 0)
      //  cout << "----" << endl;
      
      /********************************************************/
      /*
      gettimeofday (&start, NULL);
      tmpNbPGs = newCombination->FindPGs (3);
      gettimeofday (&end, NULL);
      
      elapsed.tv_sec = end.tv_sec - start.tv_sec;
      elapsed.tv_usec = end.tv_usec - start.tv_usec;
      
      if (tmpNbPGs > 0)
        cout << elapsed.tv_sec << "." << setw(6) << setfill ('0') << (unsigned long) elapsed.tv_usec << " s" << endl;
      
      if (tmpNbPGs > 0)
        cout << "------------------------------------" << endl;
      */
      
      NbPGs += tmpNbPGs;
      
      delete newCombination;
      Combination->RemoveLastNeuron ();
    }
    else
    {
      Combination->AddNeuron ((*itNeuron));
      ParseCombinations (itNeuron, itLastNeuron, Combination, NeuronRank+1, NbTriggeringNeurons);
      Combination->RemoveLastNeuron ();
    }
    
    itNeuron++;
  }
}
