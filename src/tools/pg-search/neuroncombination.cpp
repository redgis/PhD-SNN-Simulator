/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
 
 
#include "neuroncombination.h"

#include "neuron.h"
#include "synapse.h"
#include "assembly.h"
#include "../../parameters.h"
#include "polygroup.h"
#include "polygroupsaver.h"

/****************************************************************************/
long int CNeuronsCombination::factorielle (long int n)
{
  long int res = 1;
  
  for (int cpt = 2; cpt <= n; cpt ++)
  {
    res = res * cpt;
  }
  
  return res;
}


/****************************************************************************/
//~ CNeuronsCombination::CNeuronsCombination (int NbTrigNeurons)
//~ {
//~ }

/****************************************************************************/
void CNeuronsCombination::DisplayPermutation ()
{
  vector<CNeuron *>::iterator CurrentElement = TriggerNeurons.begin ();
  vector<CNeuron *>::iterator LastElement = TriggerNeurons.end ();
  
  cout << "[ " << (*(CurrentElement))->ID;
  CurrentElement ++;
  while (CurrentElement != LastElement)
  {
    cout << ", " << (*(CurrentElement))->ID;
    CurrentElement ++;
  }
  cout << " ]";
}

/****************************************************************************/
int CNeuronsCombination::FindPGs (int NbSpikesRequired)
{  
  //Number of PGs
  int NbPGs = 0;
  
  // Assembly we are analysing
  CAssembly * TriggersAssembly = TriggerNeurons[0]->ParentAssembly;
  
  // Number of triggers
  int NbTriggerNeurons = TriggerNeurons.size ();
  
  // Spike times of triggers to trigger a polych. gr.
  NNLtime * TriggerTimes = new NNLtime[NbTriggerNeurons];
  
  // Number of input connexions of targeted neurons : to see if they are triggered or not
  int * TargetedNeuronsNbSpikes = NULL;
  
  // List of possibly triggered neurons
  set<CNeuron *> TargetedNeurons;
  
  vector<CNeuron *>::iterator itTriggerNeuron = TriggerNeurons.begin ();
  vector<CNeuron *>::iterator itLastTriggerNeuron = TriggerNeurons.end ();
  
  // fill TargetedNeurons with output neurons of the first trigger neuron
  vector <CSynapse *>::iterator itSynapse = (*itTriggerNeuron)->outputSynapses.begin ();
  vector <CSynapse *>::iterator itLastSynapse = (*itTriggerNeuron)->outputSynapses.end ();
  
  set<CNeuron *>::iterator itTargetNeuron;
  set<CNeuron *>::iterator itTmpTargetNeuron;
  set<CNeuron *>::iterator itLastTargetNeuron;
    
  TargetedNeurons.clear ();
  
  int NbNeurons = NeuronsByID.size ();
  
  
  // Initialise table of trigger count to 0
  if (TargetedNeuronsNbSpikes  != NULL)
    delete TargetedNeuronsNbSpikes ;
  
  TargetedNeuronsNbSpikes = new int[NbNeurons];
  
  
  for (int Index = 0; Index < NbNeurons; Index++)
  {
    TargetedNeuronsNbSpikes [Index] = 0;
  }
  
  // fill TargetedNeurons with output neurons of each trigger neuron
  while (itTriggerNeuron != itLastTriggerNeuron)
  {
    itSynapse = (*itTriggerNeuron)->outputSynapses.begin ();
    itLastSynapse = (*itTriggerNeuron)->outputSynapses.end ();
    
    while (itSynapse != itLastSynapse)
    {
      if (StayInAssembly)
      {
        if ((*itSynapse)->OutputNeuron->ParentAssembly == TriggersAssembly)
        {
          TargetedNeurons.insert ((*itSynapse)->OutputNeuron);
          if ( NeuronsByID[(*itSynapse)->OutputNeuron->ID]->Inhibitory )
            TargetedNeuronsNbSpikes [(*itSynapse)->OutputNeuron->ID] --;
          else
            TargetedNeuronsNbSpikes [(*itSynapse)->OutputNeuron->ID] ++;
        }
      }
      else
      {
        TargetedNeurons.insert ((*itSynapse)->OutputNeuron);
        if ( NeuronsByID[(*itSynapse)->OutputNeuron->ID]->Inhibitory )
          TargetedNeuronsNbSpikes [(*itSynapse)->OutputNeuron->ID] --;
        else
          TargetedNeuronsNbSpikes [(*itSynapse)->OutputNeuron->ID] ++;
      }
      
      itSynapse++;
    }
    
    itTriggerNeuron++;
  }
  
  itTargetNeuron = TargetedNeurons.begin ();
  itLastTargetNeuron = TargetedNeurons.end ();  
  
  // for each target neuron remove non triggered neurons
  while (itTargetNeuron != itLastTargetNeuron)
  {
    if (TargetedNeuronsNbSpikes[(*itTargetNeuron)->ID] < NbSpikesRequired)
    {
      itTmpTargetNeuron = itTargetNeuron;
      itTargetNeuron++;
      TargetedNeurons.erase (itTmpTargetNeuron);
    }
    else
      itTargetNeuron++;
  }
  
  CPolyGroup * myPG;
  
  if (TargetedNeurons.size() > 0)
  {
    //~ DisplayPermutation ();
    //~ cout <<  " may trigger these neurons : ";
    
    //~ cout << "   [";
    //~ for (itTargetNeuron = TargetedNeurons.begin (); itTargetNeuron != itLastTargetNeuron; itTargetNeuron ++)
    //~ {
      //~ cout << (*itTargetNeuron)->ID << ",";
    //~ }
    //~ cout << "] " << endl;
    
    for (itTargetNeuron = TargetedNeurons.begin (); itTargetNeuron != itLastTargetNeuron; itTargetNeuron ++)
    {
      myPG = new CPolyGroup (this, *itTargetNeuron);
      
      if (myPG->NbTriggers () == CParameters::NB_TRIGGER_NEURONS)
      {
        myPG->GeneratePG (NbSpikesRequired);
        
        if ( (myPG->TimeSpan () >= CParameters::MIN_PG_TIME) && ((myPG->NbSpikes () + myPG->NbTriggers ()) >= CParameters::MIN_PG_SIZE) && ( !(myPG->isTruncated ()) || !(CParameters::AVOID_CYCLICS) ) ) 
        {
#ifdef __DEBUG
          myPG->DisplayPG ();
#endif
          NbPGs++;
          if (myPG->isTruncated ())
            NbTruncated++;
          
          PolyGroupSaver->AddPolyGroup (myPG); 
          //delete myPG;
        }
        else
          delete myPG;
      }
      else
        delete myPG;
    }
  }    
    
  delete [] TargetedNeuronsNbSpikes;
  delete [] TriggerTimes;
 
  return NbPGs;
}

/****************************************************************************/
int CNeuronsCombination::FindPGs ()
{
  //bool StayInAssembly = true;

  //Number of PGs
  int NbPGs = 0;

  
  // Assembly we are analysing
  CAssembly * TriggersAssembly = TriggerNeurons[0]->ParentAssembly;
  
  // Number of triggers
  int NbTriggerNeurons = TriggerNeurons.size ();
  
  // Spike times of triggers to trigger a polych. gr.
  NNLtime * TriggerTimes = new NNLtime[NbTriggerNeurons];
  
  // NNLpotential of targeted neurons : to see if they are triggered or not
  NNLpotential * TargetedNeuronsPotential = NULL;
  
  // List of possibly triggered neurons
  set<CNeuron *> TargetedNeurons;
  
  vector<CNeuron *>::iterator itTriggerNeuron = TriggerNeurons.begin ();
  vector<CNeuron *>::iterator itLastTriggerNeuron = TriggerNeurons.end ();
  
  // Fill TargetedNeurons with output neurons of the first trigger neuron
  vector <CSynapse *>::iterator itSynapse = (*itTriggerNeuron)->outputSynapses.begin ();
  vector <CSynapse *>::iterator itLastSynapse = (*itTriggerNeuron)->outputSynapses.end ();
  
  set<CNeuron *>::iterator itTargetNeuron;
  set<CNeuron *>::iterator itTmpTargetNeuron;
  set<CNeuron *>::iterator itLastTargetNeuron;
  
  if (TargetedNeuronsPotential != NULL)
    delete TargetedNeuronsPotential;
  
  TargetedNeuronsPotential = new NNLpotential[NeuronsByID.size ()];
  
  int NbNeurons = NeuronsByID.size ();
  
  for(int Index = 0; Index < NbNeurons; Index++)
  {
    //if (NeuronsByID.find (Index) != NeuronsByID.end())
      TargetedNeuronsPotential[Index] = CParameters::RESTING_POTENTIAL - ThresholdsByNeuronID[Index];
  }
  
  itTriggerNeuron = TriggerNeurons.begin ();
  itLastTriggerNeuron = TriggerNeurons.end ();
  
  TargetedNeurons.clear ();
  
  // Fill TargetedNeurons with output neurons with output neurons of each trigger neuron
  while (itTriggerNeuron != itLastTriggerNeuron)
  {
    itSynapse = (*itTriggerNeuron)->outputSynapses.begin ();
    itLastSynapse = (*itTriggerNeuron)->outputSynapses.end ();
    
    while (itSynapse != itLastSynapse)
    {
      if (StayInAssembly)
      {
        if ((*itSynapse)->OutputNeuron->ParentAssembly == TriggersAssembly)
        {
          TargetedNeurons.insert ((*itSynapse)->OutputNeuron);
          TargetedNeuronsPotential[(*itSynapse)->OutputNeuron->ID] += (*itSynapse)->Weight * CParameters::PSP_STRENGHT;
        }
      }
      else
      {
        TargetedNeurons.insert ((*itSynapse)->OutputNeuron);
        TargetedNeuronsPotential[(*itSynapse)->OutputNeuron->ID] += (*itSynapse)->Weight * CParameters::PSP_STRENGHT;
      }
      
      itSynapse++;
    }
    
    itTriggerNeuron++;
  }
  
  itTargetNeuron = TargetedNeurons.begin ();
  itLastTargetNeuron = TargetedNeurons.end ();  
  
  // for each target neuron
  while (itTargetNeuron != itLastTargetNeuron)
  {
    if (TargetedNeuronsPotential[(*itTargetNeuron)->ID] < 0)
    {
      itTmpTargetNeuron = itTargetNeuron;
      itTargetNeuron++;
      TargetedNeurons.erase (itTmpTargetNeuron);
    }
    else
      itTargetNeuron++;
  }
  
  CPolyGroup * myPG;
  
  if (TargetedNeurons.size() > 0)
  {
    //~ DisplayPermutation ();
    //~ cout << " may trigger these neurons : ";
    
    //~ cout << "   [";
    //~ for (itTargetNeuron = TargetedNeurons.begin (); itTargetNeuron != itLastTargetNeuron; itTargetNeuron ++)
    //~ {
      //~ cout << (*itTargetNeuron)->ID << ",";
    //~ }
    //~ cout << "] " << endl;
    
    
    for (itTargetNeuron = TargetedNeurons.begin (); itTargetNeuron != itLastTargetNeuron; itTargetNeuron ++)
    {
      myPG = new CPolyGroup (this, *itTargetNeuron);
      
      if (myPG->NbTriggers () == CParameters::NB_TRIGGER_NEURONS)
      {
        myPG->GeneratePG ();
      
        if ( (myPG->TimeSpan () >= CParameters::MIN_PG_TIME) && ((myPG->NbSpikes () + myPG->NbTriggers ()) >= CParameters::MIN_PG_SIZE) && ( !(myPG->isTruncated ()) || !(CParameters::AVOID_CYCLICS) ) ) 
        {
#ifdef __DEBUG  
          myPG->DisplayPG ();
#endif
          //cout << myPG->TimeSpan () << " " << CParameters::MIN_PG_TIME << endl;
          NbPGs++;
          if (myPG->isTruncated ())
            NbTruncated++;
          
          PolyGroupSaver->AddPolyGroup (myPG);
        }
        else
          delete myPG;
      }
      else
        delete myPG;
    }
    
    //~ if (NbPGs > 0)
    //~ {
      //~ DisplayPermutation ();
      //~ cout << " may trigger these neurons : ";
      
      //~ cout << "   [";
      //~ for (itTargetNeuron = TargetedNeurons.begin (); itTargetNeuron != itLastTargetNeuron; itTargetNeuron ++)
      //~ {
        //~ cout << (*itTargetNeuron)->ID << ",";
      //~ }
      //~ cout << "] " << endl;
    //~ }
  }
  
  delete TargetedNeuronsPotential;
  delete TriggerTimes;
  
  return NbPGs;
}

/****************************************************************************/
void CNeuronsCombination::AddNeuron (CNeuron * newNeu)
{
  TriggerNeurons.push_back (newNeu);
}

/****************************************************************************/
void CNeuronsCombination::RemoveLastNeuron ()
{
  TriggerNeurons.pop_back ();
}

/****************************************************************************/
void CNeuronsCombination::setNthNeuron (int Nth, CNeuron * newNeu)
{
  TriggerNeurons[Nth] = newNeu;
}

/****************************************************************************/
void CNeuronsCombination::Clear ()
{
  TriggerNeurons.clear ();
}

