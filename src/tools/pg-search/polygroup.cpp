/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
 
#include "polygroup.h"

#include "neuroncombination.h"
#include "neuron.h"
#include "synapse.h"
#include "../../parameters.h"
#include "circledeventqueue.h"

/*****************************************************************************/
CPolyGroup::CPolyGroup (CNeuronsCombination * Combination, CNeuron * TriggeredNeuron)
{
  Truncated = false;
  
  PSPEventQueue = new CCircledEventQueue (CParameters::MAX_DELAY*3);//CParameters::MAX_DELAY * 3);
  
  //Initialise PG with fisrt spikes to trigger the TriggeredNeuron !
  vector<CNeuron *>::iterator itTriggerNeuron = Combination->TriggerNeurons.begin ();
  vector<CNeuron *>::iterator itLastTriggerNeuron = Combination->TriggerNeurons.end ();
  NNLtime MaxDelay = 0;
  
  NNLtime tmpDelay;

  /* Store max delay and set a spike for each trigger neuron impacting the target neuron */
  for (itTriggerNeuron = Combination->TriggerNeurons.begin (); itTriggerNeuron != itLastTriggerNeuron; itTriggerNeuron++)
  {
    tmpDelay = DelaysByNeuronID [(*itTriggerNeuron)->ID][TriggeredNeuron->ID];
    
    if (tmpDelay != -1 )
    {
      AddTriggeringSpike ((*itTriggerNeuron), tmpDelay );
      
      if (tmpDelay > MaxDelay)
      {
        MaxDelay = tmpDelay;
      }
    }
  }
  
  // For each spike, we stored the delay of the synapse, not de spike time. Correct this using MaxDelay
  vector<TSpike *>::iterator itSpike = TriggeringSpikes.begin ();
  vector<TSpike *>::iterator itLastSpike = TriggeringSpikes.end ();
  
  for ( ; itSpike != itLastSpike; itSpike++ )
  {
    (*itSpike)->Time = MaxDelay - (*itSpike)->Time;
  }
  
}

/*****************************************************************************/
CPolyGroup::~CPolyGroup ()
{
  delete PSPEventQueue;
  
  vector <TSpike *>::iterator itSpike;
  vector <TSpike *>::iterator itLastSpike;
  
  list <TEvent *>::iterator itPSP;
  list <TEvent *>::iterator itLastPSP;
  
  
  itLastSpike = TriggeringSpikes.end ();
  
  for (itSpike = TriggeringSpikes.begin (); itSpike != itLastSpike; itSpike ++)
  {
    delete (*itSpike);
  }
    
  itLastSpike = Spikes.end ();
  
  for (itSpike = Spikes.begin (); itSpike != itLastSpike; itSpike ++)
  {
    if ((*itSpike)->PSPList != NULL)
    {  
      itLastPSP = (*itSpike)->PSPList->end ();
      
      for (itPSP = (*itSpike)->PSPList->begin (); itPSP != itLastPSP; itPSP++)
      {
        delete (*itPSP);
      }
      delete (*itSpike)->PSPList;
      
    }
    
    delete (*itSpike);
  }
  
}

/*****************************************************************************/
void CPolyGroup::AddSpike (TSpike * newSpike)
{  
  Spikes.push_back (newSpike);
}


/*****************************************************************************/
void CPolyGroup::AddTriggeringSpike (CNeuron * neuron, NNLtime spikeTime)
{
  TSpike * Spike = new TSpike;
  
  Spike->EmittingNeuron = neuron;
  Spike->Time = spikeTime;
  Spike->PSPList = NULL;
  
  TriggeringSpikes.push_back (Spike);
}


/*****************************************************************************/
// Calculates the polychronous group based on existing spikes and the number of spikes that is enough to trigger a new spike. Returns the number of spikes.
bool CPolyGroup::GeneratePG (int NbSpikeRequired)
{
  vector <CNeuron *>::iterator itNeuron;
  vector <CNeuron *>::iterator itLastNeuron;
  
  vector <CSynapse *>::iterator itSynapse;
  vector <CSynapse *>::iterator itLastSynapse;
  
  vector <TSpike *>::iterator itSpike;
  vector <TSpike *>::iterator itLastSpike;
  
  int NbNeurons = NeuronsByID.size ();
    
  // Last known membrane potential of the neuron
  //NNLpotential * NeuronsPotential = new NNLpotential [NbNeurons];
  
  // PSPs recieved by each Neuron in the last 5 * TauPSP time steps.
  list <TEvent *> ** NeuronsPSPs = new list <TEvent *> * [NbNeurons];
  
  // Last PSP recieved by the neuron
  //NNLtime * NeuronsLastPSP = new NNLtime [NbNeurons];
 
  // Last spike emitted by neuron
  NNLtime * NeuronsLastSpike = new NNLtime [NbNeurons];
  
  // Init neurons potential to resting value
  for (int Index = 0; Index < NbNeurons; Index ++)
  {
    //NeuronsPotential[Index] = CParameters::RESTING_POTENTIAL;
    //NeuronsLastPSP[Index] = -1;
    NeuronsLastSpike[Index] = - NeuronsByID[Index]->AbsoluteRefractoryPeriode - CParameters::PG_JITTER;
    NeuronsPSPs[Index] = NULL;
  }
  
  // Initialise PSP event queue with triggering spikes
  itLastSpike = TriggeringSpikes.end ();
  
  for (itSpike = TriggeringSpikes.begin (); itSpike != itLastSpike; itSpike++)
  {
    NeuronsLastSpike[(*itSpike)->EmittingNeuron->ID] = (*itSpike)->Time;
    PostPSPEventsFromSpike ((*itSpike));
  }
  
  TEvent * myEvt;
    
  list <TEvent*> TreatedEvents;
  list <TEvent*>::iterator itEvent;
  list <TEvent*>::iterator itLastEvent;
  
  
  // Calculate the PG, parsing the event queue
  for ( myEvt = PSPEventQueue->GetNextEvent (); \
          (myEvt != NULL) && \
          (myEvt->Time < CParameters::MAX_PG_TIME) && \
          ((Spikes.size () + TriggeringSpikes.size ()) < CParameters::MAX_PG_SIZE ); \
       myEvt = PSPEventQueue->GetNextEvent () ) 
  {
    if (NeuronsPSPs[myEvt->ImpactedNeuron->ID] == NULL)
      NeuronsPSPs[myEvt->ImpactedNeuron->ID] = new list <TEvent *>;
    
    //if ( (myEvt->Time - NeuronsLastSpike[myEvt->ImpactedNeuron->ID]) >= (myEvt->ImpactedNeuron->AbsoluteRefractoryPeriode) )
      NeuronsPSPs[myEvt->ImpactedNeuron->ID]->push_back (myEvt);
    
    TreatedEvents.push_back (myEvt);
    
    // Remove old PSPs for this neuron, only if no spike. If we just spiked, no need to empty old psp : there are none !
    if (NeuronsPSPs[myEvt->ImpactedNeuron->ID]->size () > 0)
    {
      while ( (NeuronsPSPs[myEvt->ImpactedNeuron->ID]->size () > 0) && \
              ( ((myEvt->Time - NeuronsPSPs[myEvt->ImpactedNeuron->ID]->front()->Time) > CParameters::PG_JITTER ) /* || \
                ((myEvt->Time - NeuronsLastSpike[myEvt->ImpactedNeuron->ID]) < (myEvt->ImpactedNeuron->AbsoluteRefractoryPeriode)) */) )
      {
        //cout << "myEvt->Time - NeuronsPSPs[myEvt->ImpactedNeuron->ID]->front()->Time : " << myEvt->Time - NeuronsPSPs[myEvt->ImpactedNeuron->ID]->front()->Time << endl;
        
        delete NeuronsPSPs[myEvt->ImpactedNeuron->ID]->front ();
        NeuronsPSPs[myEvt->ImpactedNeuron->ID]->pop_front ();
      }
    }    
#ifdef __DEBUG  
    cout << myEvt->Time << ":" << myEvt->ImpactedNeuron->ID << " from " << myEvt->EmittingNeuron->ID << ": PSP " << NeuronsPotential[myEvt->ImpactedNeuron->ID] << endl;
#endif
    
    
    if (PSPEventQueue->isChangingTime ()) /* est-ce que le isChangingTime nous fait perdre des spikes ? */
    {      
      while (TreatedEvents.size () > 0)
      {
        myEvt = TreatedEvents.front ();
        
        // Will this PSP make us spike ?
        if ( myEvt->ImpactedNeuron->DoYouSpike (myEvt, NeuronsPSPs[myEvt->ImpactedNeuron->ID], &(NeuronsLastSpike [myEvt->ImpactedNeuron->ID]), NbSpikeRequired) )
        {
          TSpike * mySpike = new TSpike;
          mySpike->EmittingNeuron = myEvt->ImpactedNeuron;
          mySpike->Time = myEvt->Time;
          
          // save the list of PSPs that triggered this spike, with the spike
          mySpike->PSPList = NeuronsPSPs[myEvt->ImpactedNeuron->ID];
          AddSpike (mySpike);
          
          // create a new PSP list for this neuron
          NeuronsPSPs[myEvt->ImpactedNeuron->ID] = new list <TEvent *>;
          
          PostPSPEventsFromSpike (mySpike);
        }
        
        TreatedEvents.pop_front ();
      }
    }
  }
  
  
  
  if (myEvt != NULL)
    Truncated = (myEvt->Time >= CParameters::MAX_PG_TIME) || ((Spikes.size () + TriggeringSpikes.size ()) >= CParameters::MAX_PG_SIZE );
  
  //delete NeuronsLastPSP;
  delete [] NeuronsLastSpike;
  //delete NeuronsPotential;
  
  list <TEvent *>::iterator itPSP;
  list <TEvent *>::iterator itLastPSP;
  
  for (int Index = 0; Index < NbNeurons; Index ++)
  {
    if (NeuronsPSPs[Index] != NULL)
    {
      itLastPSP = NeuronsPSPs[Index]->end ();
      
      for (itPSP = NeuronsPSPs[Index]->begin (); itPSP != itLastPSP; itPSP++)
      {
        delete (*itPSP);
      }
      
      delete NeuronsPSPs[Index];
    }
  }
  
  delete [] NeuronsPSPs;
  
  return Truncated;
}


/*****************************************************************************/
// Calculates the polychronous group based on existing spikes and the threshold/resting potential/weight. Returns the number of spikes.
bool CPolyGroup::GeneratePG ()
{
    
  //cout << endl << "=============================" << endl;
  vector <CNeuron *>::iterator itNeuron;
  vector <CNeuron *>::iterator itLastNeuron;
  
  vector <CSynapse *>::iterator itSynapse;
  vector <CSynapse *>::iterator itLastSynapse;
  
  vector <TSpike *>::iterator itSpike;
  vector <TSpike *>::iterator itLastSpike;
  
  int NbNeurons = NeuronsByID.size ();
    
  // Last known membrane potential of the neuron
  NNLpotential * NeuronsPotential = new NNLpotential [NbNeurons];
  
  // PSPs recieved by each Neuron in the last 5 * TauPSP time steps.
  list <TEvent *> ** NeuronsPSPs = new list <TEvent *> * [NbNeurons];
  
  // Last PSP recieved by the neuron
  NNLtime * NeuronsLastPSP = new NNLtime [NbNeurons];
 
  // Last spike emitted by neuron
  NNLtime * NeuronsLastSpike = new NNLtime [NbNeurons];
  
  // Init neurons potential to resting value
  for (int Index = 0; Index < NbNeurons; Index ++)
  {
    NeuronsPotential[Index] = CParameters::RESTING_POTENTIAL;
    NeuronsLastPSP[Index] = - 2*CParameters::TAU_PSP * 5;
    NeuronsLastSpike[Index] = - 2*CParameters::ABSOLUTE_REFRACTORY_PERIODE ;
    NeuronsPSPs[Index] = NULL;
  }
  
  // Initialise PSP event queue with triggering spikes
  itLastSpike = TriggeringSpikes.end ();
  
  for (itSpike = TriggeringSpikes.begin (); itSpike != itLastSpike; itSpike++)
  {
    PostPSPEventsFromSpike ((*itSpike));
  }
  
  TEvent * myEvt;
  
  // Calculate the PG, parsing the event queue
  for ( myEvt = PSPEventQueue->GetNextEvent (); \
          (myEvt != NULL) && \
          (myEvt->Time < CParameters::MAX_PG_TIME) && \
          ((Spikes.size () + TriggeringSpikes.size ()) < CParameters::MAX_PG_SIZE ); \
       myEvt = PSPEventQueue->GetNextEvent () )
  {
    if (NeuronsPSPs[myEvt->ImpactedNeuron->ID] == NULL)
      NeuronsPSPs[myEvt->ImpactedNeuron->ID] = new list <TEvent *>;
    
    NeuronsPSPs[myEvt->ImpactedNeuron->ID]->push_back (myEvt);
    
    // Remove old PSPs for this neuron, only if no spike. If we just spiked, no need to empty old psp : there are none !
    if (NeuronsPSPs[myEvt->ImpactedNeuron->ID]->size () > 0)
    {
      while ((myEvt->Time - NeuronsPSPs[myEvt->ImpactedNeuron->ID]->front()->Time) > (myEvt->ImpactedNeuron->TauPSP * 5))
      {
        delete NeuronsPSPs[myEvt->ImpactedNeuron->ID]->front ();
        NeuronsPSPs[myEvt->ImpactedNeuron->ID]->pop_front ();
      }
    }
    
#ifdef __DEBUG  
    cout << myEvt->Time << ":" << myEvt->ImpactedNeuron->ID << " from " << myEvt->EmittingNeuron->ID << ": PSP " << NeuronsPotential[myEvt->ImpactedNeuron->ID] << endl;
#endif
    
    // Will this PSP make us spike ?
    if (myEvt->ImpactedNeuron->DoYouSpike (myEvt, &(NeuronsPotential [myEvt->ImpactedNeuron->ID]), &(NeuronsLastPSP [myEvt->ImpactedNeuron->ID]), &(NeuronsLastSpike [myEvt->ImpactedNeuron->ID])))
    {
      TSpike * mySpike = new TSpike;
      mySpike->EmittingNeuron = myEvt->ImpactedNeuron;
      mySpike->Time = myEvt->Time;
      
      // save the list of PSPs that triggered this spike, with the spike
      mySpike->PSPList = NeuronsPSPs[myEvt->ImpactedNeuron->ID];
      AddSpike (mySpike);
      
      // create a new PSP list for this neuron
      NeuronsPSPs[myEvt->ImpactedNeuron->ID] = new list <TEvent *>;
        
      PostPSPEventsFromSpike (mySpike);
    }
    //~ if (Spikes.size() > 1)
    //~ cout << endl << "***************************  Size : " << Spikes.size ();
  }
  
  if (myEvt != NULL)
    Truncated = (myEvt->Time >= CParameters::MAX_PG_TIME) || ((Spikes.size () + TriggeringSpikes.size ()) >= CParameters::MAX_PG_SIZE );
  
  delete NeuronsLastPSP;
  delete NeuronsLastSpike;
  delete NeuronsPotential;
  
  list <TEvent *>::iterator itPSP;
  list <TEvent *>::iterator itLastPSP;
  
  for (int Index = 0; Index < NbNeurons; Index ++)
  {
    if (NeuronsPSPs[Index] != NULL)
    {
      itLastPSP = NeuronsPSPs[Index]->end ();
      
      for (itPSP = NeuronsPSPs[Index]->begin (); itPSP != itLastPSP; itPSP++)
      {
        delete (*itPSP);
      }
      
      delete NeuronsPSPs[Index];
    }
  }
  
  delete NeuronsPSPs;
  
  /*  *
  if (Spikes.size () > 0)
    {  
    cout << "Taille : " << Spikes.size () << endl;

    DisplayPG ();
        
         //       exit (1);
    }
  /**/    
  
  return Truncated;
}


/*****************************************************************************/
void CPolyGroup::PostPSPEventsFromSpike (TSpike * spike)
{
  vector <CSynapse *>::iterator itSynapse = spike->EmittingNeuron->outputSynapses.begin ();
  vector <CSynapse *>::iterator itLastSynapse  = spike->EmittingNeuron->outputSynapses.end ();
  
  TEvent * evt;
  
#ifdef __DEBUG  
  cout << spike->Time << ":" << spike->EmittingNeuron->ID << ": Spike ****" << endl ;
#endif  
  
  for ( ; itSynapse != itLastSynapse; itSynapse++)
  {
    if ((!StayInAssembly) || ((*itSynapse)->OutputNeuron->ParentAssembly == spike->EmittingNeuron->ParentAssembly))
    {
      evt = new TEvent;
      
      evt->evtType = EVT_PSP;
      evt->Time = spike->Time + (*itSynapse)->Delay;
      evt->EmittingNeuron = spike->EmittingNeuron;
      evt->ImpactedNeuron = (*itSynapse)->OutputNeuron;
      //evt->PSPWeight = (*itSynapse)->Weight * (NNLpotential) CParameters::PSP_STRENGHT;
      
      if (! (CParameters::SEARCH_BY_NB_PSP))
      {
        if ((*itSynapse)->Inhibitory)
        {
          evt->PSPWeight = - (*itSynapse)->Weight * (NNLweight) CParameters::PSP_STRENGHT;
        }
        else
          evt->PSPWeight = (*itSynapse)->Weight * (NNLweight) CParameters::PSP_STRENGHT;
        
      }
      
      
      PSPEventQueue->PostEvent (evt);
    } 
  }
}


/*****************************************************************************/
void CPolyGroup::DisplayPG ()
{
  vector<TSpike *>::iterator itSpike;
  vector<TSpike *>::iterator itLastSpike = TriggeringSpikes.end ();
  
  for ( itSpike = TriggeringSpikes.begin (); itSpike != itLastSpike; itSpike++ )
  {
    cout << (*itSpike)->EmittingNeuron->ID << "(" << (*itSpike)->Time << "), ";
  }

  cout << " => ";

  itLastSpike = Spikes.end ();
  
  for ( itSpike = Spikes.begin (); itSpike != itLastSpike; itSpike++ )
  {
    cout << (*itSpike)->EmittingNeuron->ID << "(" << (*itSpike)->Time << "), ";
  }
  
  
  cout << endl;
  
}

/*****************************************************************************/
int CPolyGroup::NbSpikes ()
{
  return Spikes.size ();
}

/*****************************************************************************/
NNLtime CPolyGroup::TimeSpan ()
{
  if (Spikes.size () == 0)
    return 0;
  else
    return Spikes.back ()->Time;
}

/*****************************************************************************/
int CPolyGroup::NbTriggers ()
{
  return TriggeringSpikes.size ();
}


/*****************************************************************************/
string * CPolyGroup::GenerateFilename ()
{
  string * Filename = new string ("");
  char * Triggers;
  char * Timings;
  
  unsigned int NbCharacters = TriggeringSpikes.size () * 5 + 10; // 4 char for neuron ID or timing, one for "-"
  
  Triggers = new char[NbCharacters];
  Timings = new char[NbCharacters];

  Triggers [0] = '\0';
  Timings [0] = '\0';

  //sprintf (Triggers, "%05d_", NbPGsWritten);
  
  if (isTruncated ())
    (*Filename) += "trunc-";
  
  vector <TSpike *>::iterator itSpike;
  vector <TSpike *>::iterator itLastSpike = TriggeringSpikes.end ();
  
  // construct file name
  for (itSpike = TriggeringSpikes.begin (); itSpike != itLastSpike; itSpike++)
  {
    sprintf (Triggers, "%s%04d-\0", Triggers, (int)(*itSpike)->EmittingNeuron->ID);
    sprintf (Timings , "%s%04d-\0", Timings, (int)(*itSpike)->Time);
  }
  
  Triggers[strlen(Triggers) - 1] = '\0';
  Timings[strlen(Timings) - 1] = '\0';
  
  sprintf (Timings, "%s__%04d\0", Timings, (int)(TriggeringSpikes.size() + Spikes.size ()));

  
  (*Filename) += string(Triggers) + "__" + string(Timings);

#ifdef __DEBUG  
  cout << "\"" << ("../../" + CParameters::PG_OUTPUT_DIR + (*Filename) + "." + CParameters::DATA_OUTPUT_EXT) << "\"" << endl;
#endif

  delete [] Triggers;
  delete [] Timings;
  
  return Filename;
}


/*****************************************************************************/
bool CPolyGroup::SavePG ()
{
  string * Filename = GenerateFilename ();
  
  // Check if file already exists
  ofstream * myPGFile = new ofstream ((CParameters::PG_OUTPUT_DIR + (*Filename) + "." + CParameters::DATA_OUTPUT_EXT).c_str(), ios::in);
  
  if (! (myPGFile->fail()))
  {
    myPGFile->close ();
    delete myPGFile;
    return false;
  }
  
  myPGFile->close ();
  delete myPGFile;
  
  // create PG file ...
  myPGFile = new ofstream ((CParameters::PG_OUTPUT_DIR + (*Filename) + "." + CParameters::DATA_OUTPUT_EXT).c_str(), ios::out|ios::binary|ios::trunc);
  
  vector <TSpike *>::iterator itSpike;
  vector <TSpike *>::iterator itLastSpike = TriggeringSpikes.end ();
  
  list <TEvent *>::iterator itPSP;
  list <TEvent *>::iterator itLastPSP;
  
  // write triggering spikes
  for (itSpike = TriggeringSpikes.begin (); itSpike != itLastSpike; itSpike++)
  {
    (*myPGFile) << (*itSpike)->Time << " " << (*itSpike)->EmittingNeuron->ID << endl << endl;
  }
  
  itLastSpike = Spikes.end ();
  
  // write triggered spikes
  for (itSpike = Spikes.begin (); itSpike != itLastSpike; itSpike++)
  {
    itLastPSP = (*itSpike)->PSPList->end ();
    
    for (itPSP = (*itSpike)->PSPList->begin (); itPSP != itLastPSP; itPSP++)
    {
      (*myPGFile) << (*itPSP)->Time - DelaysByNeuronID [(*itPSP)->EmittingNeuron->ID][(*itPSP)->ImpactedNeuron->ID] << " " << (*itPSP)->EmittingNeuron->ID << endl;
      (*myPGFile) << (*itSpike)->Time << " " << (*itSpike)->EmittingNeuron->ID << endl << endl;
    }
  }
  
  myPGFile->close ();
  
  if (Truncated)
    NbTruncatedWritten++;
  
  delete myPGFile;
  
  delete Filename;
  
  return true;
}


/*****************************************************************************/
void CPolyGroup::SavePlot (ofstream * myPGPlot)
{
  string * Filename = GenerateFilename ();
  
  (*myPGPlot) << "reset" << endl;

  (*myPGPlot) << "set terminal postscript eps enhanced color defaultplex \"Times-Roman\" 10" << endl;
  //(*myPGPlot) << "set size ratio 0.3" << endl;
  (*myPGPlot) << "set size 0.8,0.5" << endl;
  //(*myPGPlot) << "set size 0.4,0.25" << endl;

  (*myPGPlot) << "unset key" << endl;

  (*myPGPlot) << "set output \""<<  CParameters::PG_OUTPUT_DIR + (*Filename) + "." + CParameters::FIGURES_EXT << "\"" << endl;
  (*myPGPlot) << "set xlabel 'Time [ms]'" << endl;
  (*myPGPlot) << "set ylabel '# Neurons'" << endl;

  (*myPGPlot) << "set pointsize 0.5" << endl;
  
  //(*myPGPlot) << "set xtics 10" << endl;
  (*myPGPlot) << "set ytics 1" << endl;
  (*myPGPlot) << "set grid" << endl;
  
  //(*myPGPlot) << "set yrange [1:" << NeuronsByID.size () << "]" << endl;
  (*myPGPlot) << "set xrange [0:" << ((float)(CParameters::MAX_PG_TIME)) / (float)CParameters::TIMESCALE << "]" << endl;
  //(*myPGPlot) << "set xrange [0:*]" << endl;
  (*myPGPlot) << "plot '" << CParameters::PG_OUTPUT_DIR + (*Filename) + "." + CParameters::DATA_OUTPUT_EXT << "' using (($1)/" << CParameters::TIMESCALE << "):2 w lines, \\" << endl;
  (*myPGPlot) << "     '" << CParameters::PG_OUTPUT_DIR + (*Filename) + "." + CParameters::DATA_OUTPUT_EXT << "' using (($1)/" << CParameters::TIMESCALE << "):2 w p " << endl << endl;
}

/*****************************************************************************/
void CPolyGroup::SaveTriggers (ofstream * PGs)
{
  vector <TSpike *>::iterator itSpike;
  vector <TSpike *>::iterator itLastSpike = TriggeringSpikes.end ();
  
  (*PGs) << NbPGsWritten << " ";
  // write triggering spikes
  for (itSpike = TriggeringSpikes.begin (); itSpike != itLastSpike; itSpike++)
  {
    (*PGs) << (*itSpike)->EmittingNeuron->ID << "," << (*itSpike)->Time << " ";
  }
  
  (*PGs) << endl;
}

/*****************************************************************************/
bool CPolyGroup::isTruncated ()
{
  return Truncated;
}
