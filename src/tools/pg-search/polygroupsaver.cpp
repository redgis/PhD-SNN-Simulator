/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "polygroupsaver.h"

#include "polygroup.h"
#include "../../parameters.h"


/****************************************************************************/
CPolyGroupSaver::CPolyGroupSaver ()
{
  Exit = false;
  
  //Initialise the semaphore :
  init ();
  
  // Start the thread
  Create ();
}

/****************************************************************************/
CPolyGroupSaver::~CPolyGroupSaver ()
{
  Exit = true;
}

/****************************************************************************/
void CPolyGroupSaver::AddPolyGroup (CPolyGroup * newPG)
{
  PolyGroups.push_back (newPG);
  //cout << PolyGroups.size () << endl;
  sem_post (&semThread);
}

/****************************************************************************/
void CPolyGroupSaver::Run ()
{
  CPolyGroup * tmpPG;
  string Filename = "PGs";
  
  //ofstream * myPGPlot = new ofstream (("../../" + CParameters::FIGURE_SCRIPTS_DIR + Filename + "." + CParameters::FIGURE_SCRIPTS_EXT).c_str(), ios::out|ios::trunc);
  
  //ofstream * PGs = new ofstream (("../../" + CParameters::DATA_OUTPUT_DIR + Filename + "." + CParameters::DATA_OUTPUT_EXT).c_str(), ios::out|ios::trunc);
  
  ofstream * myPGPlot = new ofstream ((CParameters::PG_OUTPUT_DIR + Filename + "." + CParameters::FIGURE_SCRIPTS_EXT).c_str(), ios::out|ios::trunc);
  
  ofstream * PGs = new ofstream ((CParameters::PG_OUTPUT_DIR + Filename + "." + CParameters::DATA_OUTPUT_EXT).c_str(), ios::out|ios::trunc);
  
  while ((!Exit) || (!(PolyGroups.empty ())))
  {
    if(!(PolyGroups.empty ()))
    {
      tmpPG = PolyGroups.front ();
      
      if ( !(tmpPG->isTruncated ()) || !(CParameters::AVOID_CYCLICS) )
      {
        if (tmpPG->SavePG ())
        {
          tmpPG->SavePlot (myPGPlot);
          tmpPG->SaveTriggers (PGs);
          NbPGsWritten ++;
        }
      }
      
      delete tmpPG;
      PolyGroups.pop_front ();
      
    }
    else
      sem_wait (&semThread);
  }
  
  cout << "exiting ..." << endl;
  
  PGs->close ();
  myPGPlot->close ();
  delete myPGPlot;
  delete PGs;
}
