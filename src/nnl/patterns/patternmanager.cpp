/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
 
#include "patternmanager.h"

#include "parameters.h"
#include "patternset.h"


/*****************************************************************************/
CPatternManager::CPatternManager (string TrainingFile, string TestingFile)
{
/*
  //create a lua state
  lua_State* pLua = lua_open();

  //open the lua libaries - new in lua5.1
  luaL_openlibs(pLua);

  //open luabind
  open(pLua);

  //load and run the script
  RunLuaScript(pLua, "settings/ExposingCPPFunctionsToLua.lua");


  //tidy up
  lua_close(pLua);
*/

  TrainPatternSet = new CPatternSet (TrainingFile);
  TestPatternSet = new CPatternSet (TestingFile);
}

/*****************************************************************************/
CPatternManager::CPatternManager (const char * luaFilename)
{
  if (luaFilename != NULL)
    LoadFromLua (luaFilename);
}

/*****************************************************************************/
CPatternManager::~CPatternManager ()
{
  delete TrainPatternSet;
  delete TestPatternSet;
}

/*****************************************************************************/
void CPatternManager::LoadFromLua (const char * luaFilename)
{
  cout << "Loading patterns using \"" << luaFilename << "\" ... " << flush;

  //create a lua state
  lua_State* pLua = lua_open();

  //open the lua libaries - new in lua5.1
  luaL_openlibs(pLua);

  //open luabind
  //open(pLua);

  //load and run the script
  int hasError = luaL_dofile(pLua, luaFilename);
  if (hasError)
  {
    cerr << "-- " << lua_tostring(pLua, -1) << endl;
    luaL_error (pLua, "An error occured while loading %s", luaFilename);
    exit (hasError);
  }

  //reset the stack index
  lua_settop(pLua, 0);

  // Will we have to randomize presentation of patterns ?
  lua_getglobal (pLua, "RESHUFFLE_PATTERNS");
  bool bReshuffle = lua_toboolean(pLua, 1);
  lua_pop (pLua,1);

  // Find train dataset file name
  lua_getglobal (pLua, "TRAIN_PATTERN_FILE");
  string strTrainFile = lua_tostring(pLua, 1);
  lua_pop (pLua,1);

  // Find test dataset file name
  lua_getglobal (pLua, "TEST_PATTERN_FILE");
  string strTestFile = lua_tostring(pLua, 1);
  lua_pop (pLua,1);

  TrainPatternSet = new CPatternSet (strTrainFile);
  TestPatternSet = new CPatternSet (strTestFile);

  int iNbTrainClasses = lua_getTableLength(pLua, "TRAIN_CLASSES_TO_LOAD");
  int iNbTestClasses = lua_getTableLength(pLua, "TEST_CLASSES_TO_LOAD");

  int iClassNumber;
  int iNbPatterns;
  cout << endl;

  // Load train patterns (all if nb to load is -1)
  cout << "  training patterns: " << flush;
  for (int Index = 1; Index <= iNbTrainClasses; Index++)
  {
    iClassNumber = lua_getIntTableElement (pLua, "TRAIN_CLASSES_TO_LOAD", Index);
    iNbPatterns = lua_getIntTableElement (pLua, "NB_TRAIN_PATTERNS_TO_LOAD", Index);

    cout << "" << iClassNumber << ":" << flush;
    if (iNbPatterns == -1)
      TrainPatternSet->LoadClassXPatternsFromFile(iClassNumber);
    else
      TrainPatternSet->LoadNClassXPatternsFromFile(iNbPatterns,iClassNumber);
    cout << "... "<< flush;
  }
  cout << " = " << TrainPatternSet->getNbPatterns() << " - done." << endl;

  // Load test patterns (all if nb to load is -1)
  cout << "  testing patterns: " << flush;
  for (int Index = 1; Index <= iNbTestClasses; Index++)
  {
    iClassNumber = lua_getIntTableElement (pLua, "TEST_CLASSES_TO_LOAD", Index);
    iNbPatterns = lua_getIntTableElement (pLua, "NB_TEST_PATTERNS_TO_LOAD", Index);

    cout << "" << iClassNumber << ":" << flush;
    if (iNbPatterns == -1)
      TestPatternSet->LoadClassXPatternsFromFile(iClassNumber);
    else
      TestPatternSet->LoadNClassXPatternsFromFile(iNbPatterns,iClassNumber);
    cout << "... "<< flush;
  }
  cout << " = " << TestPatternSet->getNbPatterns() << " - done." << endl;

  // Reshuffle patterns if specified so
  if (bReshuffle)
  {
    TrainPatternSet->ReshufflePatterns();
    TestPatternSet->ReshufflePatterns();
  }

  //tidy up
  lua_close(pLua);

  NbClasses = iNbTrainClasses;
}

/*****************************************************************************/
CPattern * CPatternManager::getCurrentTrainPattern ()
{
  //testpres++;
  //cout << testpres%TrainPatternSet->getNbPatterns() << endl;
  return TrainPatternSet->getCurrentPattern ();
}

/*****************************************************************************/
CPattern * CPatternManager::getCurrentTestPattern ()
{
  //testpres++;
  //cout << testpres%TestPatternSet->getNbPatterns() << endl;
  return TestPatternSet->getCurrentPattern ();
}

/*****************************************************************************/
CPattern * CPatternManager::getNextTrainPattern ()
{
  //testpres++;
  //cout << testpres%TrainPatternSet->getNbPatterns() << endl;
  return TrainPatternSet->getNextPattern ();
}

/*****************************************************************************/
CPattern * CPatternManager::getNextTestPattern ()
{
  //testpres++;
  //cout << testpres%TestPatternSet->getNbPatterns() << endl;
  return TestPatternSet->getNextPattern ();
}

/*****************************************************************************/
CPattern * CPatternManager::getNextClassTrainPattern (int Class)
{
  //testpres++;
  //cout << testpres%TrainPatternSet->getNbPatterns() << endl;
  return TrainPatternSet->getNextClassPattern (Class);
}

/*****************************************************************************/
CPattern * CPatternManager::getNextClassTestPattern (int Class)
{
  //testpres++;
  //cout << testpres%TestPatternSet->getNbPatterns() << endl;
  return TestPatternSet->getNextClassPattern (Class);
}

/*****************************************************************************/
int CPatternManager::getNbFeatures ()
{
  return NbFeatures;
}

/*****************************************************************************/
int CPatternManager::getNbClasses ()
{
  return NbClasses;
}

/*****************************************************************************/
double CPatternManager::getMinTrainValue ()
{
  return TrainPatternSet->getMinValue ();
}

/*****************************************************************************/
double CPatternManager::getMaxTrainValue ()
{
  return TrainPatternSet->getMaxValue ();
}

/*****************************************************************************/
double CPatternManager::getMinTestValue ()
{
  return TestPatternSet->getMinValue ();
}

/*****************************************************************************/
double CPatternManager::getMaxTestValue ()
{
  return TestPatternSet->getMaxValue ();
}

/*****************************************************************************/
int CPatternManager::getNbTrainPatterns ()
{
  return TrainPatternSet->getNbPatterns ();
}

/*****************************************************************************/
int CPatternManager::getNbTestPatterns ()
{
  return TestPatternSet->getNbPatterns ();
}

/*****************************************************************************/
CPatternSet * CPatternManager::getTrainPatternSet ()
{
  return TrainPatternSet;
}

/*****************************************************************************/
CPatternSet * CPatternManager::getTestPatternSet ()
{
  return TestPatternSet;
}

/*****************************************************************************/
CPattern * CPatternManager::getNiemeTrainPattern (int N)
{
  return TrainPatternSet->getNiemePattern (N);
}

/*****************************************************************************/
CPattern * CPatternManager::getNiemeTestPattern (int N)
{
  return TestPatternSet->getNiemePattern (N);
}

