/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef _PATTERN_H
#define _PATTERN_H


#include "../simulator/globals.h"

/*****************************************************************************/
class CPattern
{
  protected:
    int PatternClass;   /* class id of the pattern */
    int NbFeatures;     /* Number of features in pattern */
    vector <double> Features;  /* Features in the pattern */
    CPatternSet * PatternSet;
  
  public:
    CPattern (int NbFeat, CPatternSet * pat);
    ~CPattern ();

    /* Access methods */
    int getPatternClass ();
    int getNbFeatures ();
    CPatternSet * getPatternSet ();
    vector <double> * getPatternVector ();
    
    friend istream& operator >> (istream &is, CPattern &obj);
};


#endif /* _PATTERN_H */
