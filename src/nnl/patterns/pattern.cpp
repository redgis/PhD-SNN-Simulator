/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "pattern.h"


/*****************************************************************************/
CPattern::CPattern (int NbFeat, CPatternSet * pat)
{
  NbFeatures = NbFeat;
  PatternSet = pat;
}

/*****************************************************************************/
CPattern::~CPattern ()
{
  
}


/*****************************************************************************/
istream& operator >> (istream &is, CPattern &obj)
{
  int Index = 0;
  double Feature;
  
  is >> obj.PatternClass;
  /*
  if (obj.PatternClass == 6)
    obj.PatternClass = 0;
  else if (obj.PatternClass == 0)
    obj.PatternClass = 6;
  else if (obj.PatternClass == 9)
    obj.PatternClass = 1;
  else if (obj.PatternClass == 1)
    obj.PatternClass = 9;/**/
           
  /*if (obj.PatternClass == 9)
    obj.PatternClass = 1;
  else if (obj.PatternClass == 1)
    obj.PatternClass = 9;/**/
  
  //if (!is.eof ())
  //cout << obj.PatternClass << " ";
  for (Index = 0; Index < obj.NbFeatures; Index++)
  {
    is >> Feature;
    
    //cout << Feature << " ";
    
    Feature = (double) Feature;
    //cout << "(" << Feature << "," << (int)(Feature*10.0) << ") ";
    //~ if (Feature == 0)
      //~ Feature = 2.02; // so that the stored feature is -1 (i.e. nothing)
    obj.Features.push_back ((double)Feature);
    //cout << Feature << endl;
    //~ cout << Feature << " : " << 20*CParameters::TIMESCALE << " - " << ((long int)(Feature*10.0*(float)CParameters::TIMESCALE)) << endl;
    //cout << (((long int)(20*CParameters::TIMESCALE))-((long int)(Feature*10.0*(float)CParameters::TIMESCALE))) << endl;
  }
  
  //cout << endl;
  
  return is;
}


/*****************************************************************************/
int CPattern::getPatternClass ()
{
  return PatternClass;
}


/*****************************************************************************/
int CPattern::getNbFeatures ()
{
  return NbFeatures;
}

/*****************************************************************************/
vector <double> * CPattern::getPatternVector ()
{
  return & Features;
}

/*****************************************************************************/
CPatternSet * CPattern::getPatternSet ()
{
  return PatternSet;
}
