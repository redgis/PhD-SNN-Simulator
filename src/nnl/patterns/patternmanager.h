/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef _PATTERNMANAGER_H
#define _PATTERNMANAGER_H

#include "../simulator/globals.h"

class CPatternManager
{
  private:
    CPatternSet * TrainPatternSet;       /* Training pattern set */
    CPatternSet * TestPatternSet;        /* Testing pattern set */
    
    int NbFeatures;
    int NbClasses;
    
  public:
    CPatternManager (string TrainingFile, string TestingFile);
    CPatternManager (const char * luaFilename);
    virtual ~CPatternManager ();
    
    /* Access methods */
    int getNbFeatures ();
    int getNbClasses ();
    int getNbTrainPatterns ();
    int getNbTestPatterns ();
    double getMinTrainValue ();
    double getMaxTrainValue ();
    double getMinTestValue ();
    double getMaxTestValue ();
    CPatternSet * getTrainPatternSet ();
    CPatternSet * getTestPatternSet ();
    
    /* Next pattern to be presented */
    CPattern * getNextTrainPattern ();
    CPattern * getNextTestPattern ();
    CPattern * getNextClassTrainPattern (int Class);
    CPattern * getNextClassTestPattern (int Class);
    CPattern * getCurrentTrainPattern ();
    CPattern * getCurrentTestPattern ();
    CPattern * getNiemeTrainPattern (int N);
    CPattern * getNiemeTestPattern (int N);
    
    void LoadFromLua (const char * luaFilename);
};




#endif /* _PATTERNMANAGER_H */
