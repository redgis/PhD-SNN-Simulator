/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "patternset.h"
#include "pattern.h"

/*****************************************************************************/
CPatternSet::CPatternSet (string iFile)
{
  /* We set a default value for the number of features */
  NbFeatures = -1;

  ResetPatternSet ();
  itPattern = PatternSet.begin ();
  
  InputFile = iFile;
  
  ClassLoaded = -1;
  NbPatternsLimit = -1;


}

/*****************************************************************************/
CPatternSet::~CPatternSet ()
{
  vector<CPattern *>::iterator itPattern = PatternSet.begin ();
  
  cout << "  Destroying pattern set ..." << endl;
  
  while (itPattern != PatternSet.end ())
  {
    delete *itPattern;
    itPattern++;
  }
}

/*****************************************************************************/
int CPatternSet::getNbFeatures ()
{
  return NbFeatures;
}

/*****************************************************************************/
int CPatternSet::getNbClasses ()
{
  return NbClasses;
}

/*****************************************************************************/
size_t CPatternSet::getNbPatterns ()
{
  return PatternSet.size ();
}

/*****************************************************************************/
double CPatternSet::getMinValue ()
{
  return MinFeatureValue;
}

/*****************************************************************************/
double CPatternSet::getMaxValue ()
{
  return MaxFeatureValue;
}

/*****************************************************************************/
void CPatternSet::ResetPatternSet ()
{
  PatternSet.clear ();
}

/*****************************************************************************/
CPattern * CPatternSet::getCurrentPattern ()
{  
  return (*itPattern);
}

/*****************************************************************************/
CPattern * CPatternSet::getNextPattern ()
{
  itPattern++;
  
  if (itPattern == PatternSet.end ())
    itPattern = PatternSet.begin ();  
  
  if (itPattern == PatternSet.end ())
      return NULL;
  
  return (*itPattern);
}

/*****************************************************************************/
CPattern * CPatternSet::getNextClassPattern (int Class)
{
  CPattern * PatternTmp = getNextPattern();
  
  while ((PatternTmp != NULL) && (PatternTmp->getPatternClass () != Class))
  {
    PatternTmp = getNextPattern ();
  }
  
  return PatternTmp;
}

/*****************************************************************************/
CPattern * CPatternSet::getNiemePattern (int N)
{
  return PatternSet.at (N);
}

/*****************************************************************************/
void CPatternSet::AddPattern (CPattern * newPattern)
{
  PatternSet.push_back (newPattern);
}

/*****************************************************************************/
void CPatternSet::InitPatternIterator ()
{
  itPattern = PatternSet.begin ();
}

/*****************************************************************************/
istream& operator >> (istream &is, CPatternSet &obj)
{
  CPattern * PatternTmp;

  PatternTmp = new CPattern (obj.NbFeatures, &obj);
  is >> (*PatternTmp);

  while (!is.eof ())
  {
    /*if ( (!is.eof ())
      && ( (PatternTmp->getPatternClass() == 0)
        || (PatternTmp->getPatternClass() == 1)
        //|| (PatternTmp->getPatternClass() == 2)
        //|| (PatternTmp->getPatternClass() == 3)
        //|| (PatternTmp->getPatternClass() == 4)
    ) )/**/
      obj.AddPattern (PatternTmp);
      //cout << "Added ! " << endl;
    PatternTmp = new CPattern (obj.NbFeatures, &obj);
    is >> (*PatternTmp);
  }
  
  return is;
}

/*****************************************************************************/
void CPatternSet::setNbFeatures (int NbFeat)
{
  NbFeatures = NbFeat;
}

/*****************************************************************************/
void CPatternSet::ReshufflePatterns ()
{
  random_shuffle(PatternSet.begin(), PatternSet.end());
}

/*****************************************************************************/
void CPatternSet::LoadPatternsFromFile ()
{
  /* Open stream with this filename */
  ifstream * DataFile = new ifstream (InputFile.c_str(), ios::in);

  /* Check if the opening was successful, otherwise, means the file doesn't exist */
  if (!(*DataFile))
  {
    cerr << "File \"" << InputFile << "\" could not be open, does it exist ? Is it the right filename ? Right permissions ?" << endl;
    exit (1);
  }
  
  (*DataFile) >> NbClasses >> NbFeatures >> MinFeatureValue >> MaxFeatureValue;
  
  //setNbFeatures (NbFeatures);
  
  (*DataFile) >> (*this);
  
  ClassLoaded = -1;
  NbPatternsLimit = -1;
  
  InitPatternIterator ();
  
  cout << getNbPatterns() << flush;

}

/*****************************************************************************/
void CPatternSet::LoadNPatternsFromFile (int atMostN)
{
  /* Loads all patterns, than truncate the list */
  LoadPatternsFromFile ();
  
  if (PatternSet.size() > (unsigned int) atMostN)
    PatternSet.resize (atMostN);
  
  ClassLoaded = -1;
  NbPatternsLimit = atMostN;
  
  InitPatternIterator ();
  
  cout << getNbPatterns() << flush;

}

/*****************************************************************************/
void CPatternSet::LoadClassXPatternsFromFile (int X)
{
  /* Open stream with this filename */
  ifstream * DataFile = new ifstream (InputFile.c_str(), ios::in);

  /* Check if the opening was successful, otherwise, means the file doesn't exist */
  if (!(*DataFile))
  {
    cerr << "File \"" << InputFile << "\" could not be open, does it exist ? Is it the right filename ?" << endl;
    exit (1);
  }
  
  (*DataFile) >> NbClasses >> NbFeatures >> MinFeatureValue >> MaxFeatureValue;
  
  //setNbFeatures (NbFeatures);
  
  CPattern * PatternTmp;
  unsigned int NbPatterns = 0;
  
  while (!(*DataFile).eof ())
  {
    PatternTmp = new CPattern (NbFeatures, this);
    (*DataFile) >> (*PatternTmp);
    
    if ( (!(*DataFile).eof ()) && (PatternTmp->getPatternClass() == X))
    {
      AddPattern (PatternTmp);
      NbPatterns++;
    }
  }
  
  ClassLoaded = X;
  NbPatternsLimit = -1;
  
  InitPatternIterator ();
  
  cout << NbPatterns << flush;

}

/*****************************************************************************/
void CPatternSet::LoadNClassXPatternsFromFile (unsigned int atMostN, int X)
{
  /* Open stream with this filename */
  ifstream * DataFile = new ifstream (InputFile.c_str(), ios::in);

  /* Check if the opening was successful, otherwise, means the file doesn't exist */
  if (!(*DataFile))
  {
    cerr << "File \"" << InputFile << "\" could not be open, does it exist ? Is it the right filename ?" << endl;
    exit (1);
  }

  (*DataFile) >> NbClasses >> NbFeatures >> MinFeatureValue >> MaxFeatureValue;

  //setNbFeatures (NbFeatures);
  
  CPattern * PatternTmp;
  unsigned int NbPatterns = 0;
  while (!((*DataFile).eof ()) && (NbPatterns < atMostN))
  {
    PatternTmp = new CPattern (NbFeatures, this);
    (*DataFile) >> (*PatternTmp);

    if ( (!(*DataFile).eof ()) && (PatternTmp->getPatternClass() == X))
    {
      AddPattern (PatternTmp);
      NbPatterns ++;
    }
  }


  
  ClassLoaded = X;
  NbPatternsLimit = atMostN;
  
  InitPatternIterator ();
  
  cout << NbPatterns << flush;

}
