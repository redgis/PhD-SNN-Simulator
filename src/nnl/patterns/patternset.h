/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef _PATTERNSET_H
#define _PATTERNSET_H

#include "../simulator/globals.h"

class CPatternSet
{
  private:
    int NbFeatures;                   /* Number of features in each pattern */
    int NbClasses;
    double MinFeatureValue;
    double MaxFeatureValue;
  
    /* to know how to load in setPresentationMethod */
    int ClassLoaded;     /* -1 : all */
    int NbPatternsLimit; /* -1 : no limit */
  
    vector<CPattern *> PatternSet;    /* Set of pointers to patterns */
    vector<CPattern *>::iterator itPattern;         /* Current iterator on patterns */

    string InputFile;
  
    NNLPresentationMethod PresentationMethod;
    
  public:
    CPatternSet (string inputFile);
    virtual ~CPatternSet ();

    /* access functions */
    int getNbFeatures ();
    int getNbClasses ();
    size_t getNbPatterns ();
    double getMinValue ();
    double getMaxValue ();
  
    void setNbFeatures (int NbFeat);

    /* Manage the pattern set */
    void AddPattern (CPattern * );
    void ResetPatternSet ();
    void InitPatternIterator ();
    
    /* increments the corresponding iterator and returns the element pointed */
    CPattern * getCurrentPattern ();
    CPattern * getNextPattern ();
    CPattern * getNextClassPattern (int Class);
    CPattern * getNiemePattern (int N);
  
    /* Loading methods */
    void LoadPatternsFromFile ();
    void LoadNPatternsFromFile (int atMostN);
    void LoadClassXPatternsFromFile (int X);
    void LoadNClassXPatternsFromFile (unsigned int atMostN, int X);

    void ReshufflePatterns ();
  
    friend istream& operator >> (istream &is,CPatternSet &obj);
};

#endif /* _PATTERNSET_H */
