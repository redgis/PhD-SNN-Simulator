/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

extern "C" {
#include "lua.hpp"
}

//#include <lualib.h>
//#include <lauxlib.h>
#include "parameters.h"

#include "../patterns/patternmanager.h"

using namespace std;

/*****************************************************************************/
CParameters::CParameters ()
{
  
}

/*****************************************************************************/
CParameters::~CParameters ()
{
}

/*****************************************************************************/
//bool CParameters::loadFromXmlFile (string InputSettingFile)
//{
//  cout << "CParameters::loadFromXmlFile -- methode a terminer." << endl;
//  return false;
//
//  //~ TiXmlDocument doc (InputSettingFile.c_str());
//  //~ if (!doc.LoadFile()) return false;
//
//  //~ TiXmlHandle hDoc(&doc);
//	//~ TiXmlElement* pElem;
//	//~ TiXmlHandle hRoot(0);
//
//	//~ TiXmlNode* pChild;
//	//~ TiXmlText* pText;
//
//  //~ /*
// //~ <?xml version="1.0" ?>
// //~ <HitchHikerApp>
//    //~ <!-- Settings for HitchHikerApp -->
//    //~ <Messages>
//        //~ <Farewell>Thanks for all the fish</Farewell>
//        //~ <Welcome>Don&apos;t Panic</Welcome>
//    //~ </Messages>
//    //~ <Windows>
//        //~ <Window name="BookFrame" x="15" y="25" w="300" h="250" />
//    //~ </Windows>
//    //~ <Connection ip="192.168.0.77" timeout="42.000000" />
// //~ </HitchHikerApp>
//  //~ */
//
//	//~ for ( pChild = doc.FirstChild(); pChild != 0; pChild = pChild->NextSibling())
//	//~ {
//    //~ // block: name
//    //~ {
//      //~ pElem=hDoc.FirstChildElement().Element();
//      //~ // should always have a valid root but handle gracefully if it does
//      //~ if (!pElem) return false;
//      //~ string m_name=pElem->Value();
//
//      //~ // save this for later
//      //~ hRoot=TiXmlHandle(pElem);
//    //~ }
//
//    //~ // block: string table
//    //~ {
//      //~ m_messages[50]; // trash existing table
//
//      //~ pElem=hRoot.FirstChild( "Messages" ).FirstChild().Element();
//      //~ for( pElem; pElem; pElem=pElem->NextSiblingElement())
//      //~ {
//        //~ const char *pKey=pElem->Value();
//        //~ const char *pText=pElem->GetText();
//        //~ if (pKey && pText)
//        //~ {
//          //~ m_messages[pKey]=pText;
//        //~ }
//      //~ }
//    //~ }
//
//    //~ // block: windows
//    //~ {
//      //~ m_windows.clear(); // trash existing list
//
//      //~ TiXmlElement* pWindowNode=hRoot.FirstChild( "Windows" ).FirstChild().Element();
//      //~ for( pWindowNode; pWindowNode; pWindowNode=pWindowNode->NextSiblingElement())
//      //~ {
//
//        //~ WindowSettings w;
//        //~ const char *pName=pWindowNode->Attribute("name");
//        //~ if (pName) w.name=pName;
//
//        //~ pWindowNode->QueryIntAttribute("x", &w.x); // If this fails, original value is left as-is
//        //~ pWindowNode->QueryIntAttribute("y", &w.y);
//        //~ pWindowNode->QueryIntAttribute("w", &w.w);
//        //~ pWindowNode->QueryIntAttribute("hh", &w.h);
//
//        //~ m_windows.push_back(w);
//
//      //~ }
//    //~ }
//
//    //~ // block: connection
//    //~ {
//      //~ pElem=hRoot.FirstChild("Connection").Element();
//      //~ if (pElem)
//      //~ {
//        //~ m_connection.ip=pElem->Attribute("ip");
//        //~ pElem->QueryDoubleAttribute("timeout",&m_connection.timeout);
//      //~ }
//    //~ }
//	//~ }
//  //~ return true;
//}

/*****************************************************************************/
bool CParameters::loadFromLuaFile (string InputSettingFile)
{  
  cout << "Loading settings \"" << InputSettingFile << "\" ... " << flush;
  
  // Create a lua state
  //lua_State * pLua = lua_open ();
  lua_State * pLua = luaL_newstate ();
  
  // Reset lua stack
  lua_settop (pLua, 0);
  
  int hasError;
  //load and run the script
  hasError = luaL_dofile(pLua, InputSettingFile.c_str ());
  if (hasError)
  {
    cerr << "-- " << lua_tostring(pLua, -1) << endl;
    luaL_error (pLua, "An error occured while loading %s", InputSettingFile.c_str());
    exit (hasError);
  }
  

  // FileSystemSettings

    // Parameters
    lua_getglobal (pLua, "PSP_STRENGHT");
    PSP_STRENGHT = (NNLpotential) lua_tonumber (pLua, 1);
    lua_pop (pLua,1);

    lua_getglobal (pLua, "TIMESCALE");
    TIMESCALE = (NNLtime) lua_tonumber (pLua, 1);
    lua_pop (pLua,1);

    // Synapse constants

    lua_getglobal (pLua, "MAX_DELAY");
    MAX_DELAY = (NNLtime) lua_tonumber (pLua, 1);
    lua_pop (pLua,1);

    lua_getglobal (pLua, "MIN_DELAY");
    MIN_DELAY = (NNLtime) lua_tonumber (pLua, 1);
    lua_pop (pLua,1);

    lua_getglobal (pLua, "MAX_WEIGHT");
    MAX_WEIGHT = (NNLweight) lua_tonumber (pLua, 1);
    lua_pop (pLua,1);

    lua_getglobal (pLua, "MIN_WEIGHT");
    MIN_WEIGHT = (NNLweight) lua_tonumber (pLua, 1);
    lua_pop (pLua,1);



    // Directories
    lua_getglobal (pLua, "ROOT");
    ROOT = lua_tostring (pLua, 1);
    lua_pop (pLua,1);
  
    lua_getglobal (pLua, "DATA_OUTPUT_DIR");
    DATA_OUTPUT_DIR = lua_tostring (pLua, 1);
    lua_pop (pLua,1); 
  
    lua_getglobal (pLua, "DATA_OUTPUT_EXT");
    DATA_OUTPUT_EXT = lua_tostring (pLua, 1);
    lua_pop (pLua,1); 
    
    lua_getglobal (pLua, "PATTERNS_DIR");
    PATTERNS_DIR = lua_tostring (pLua, 1);
    lua_pop (pLua,1); 
    
    lua_getglobal (pLua, "PATTERNS_EXT");
    PATTERNS_EXT = lua_tostring (pLua, 1);
    lua_pop (pLua,1); 
 
    lua_getglobal (pLua, "FIGURE_SCRIPTS_DIR");
    FIGURE_SCRIPTS_DIR = lua_tostring (pLua, 1);
    lua_pop (pLua,1); 

    lua_getglobal (pLua, "FIGURE_SCRIPTS_EXT");
    FIGURE_SCRIPTS_EXT = lua_tostring (pLua, 1);
    lua_pop (pLua,1); 
    
    lua_getglobal (pLua, "FIGURES_DIR");
    FIGURES_DIR = lua_tostring (pLua, 1);
    lua_pop (pLua,1); 

    lua_getglobal (pLua, "FIGURES_EXT");
    FIGURES_EXT = lua_tostring (pLua, 1);
    lua_pop (pLua,1); 
    
    lua_getglobal (pLua, "ARCHITECTURE_OUTPUT_DIR");
    ARCHITECTURE_OUTPUT_DIR = lua_tostring (pLua, 1);
    lua_pop (pLua,1); 
    
    lua_getglobal (pLua, "ARCHITECTURE_XML_FILE");
    ARCHITECTURE_XML_FILE = lua_tostring (pLua, 1);
    lua_pop (pLua,1); 


    lua_getglobal (pLua, "BUILDERS_DIR");
    BUILDERS_DIR = lua_tostring (pLua, 1);
    lua_pop (pLua,1);

    lua_getglobal (pLua, "SETTINGS_DIR");
    SETTINGS_DIR = lua_tostring (pLua, 1);
    lua_pop (pLua,1); 
    
    lua_getglobal (pLua, "SIMULATIONS_DIR");
    SIMULATIONS_DIR = lua_tostring (pLua, 1);
    lua_pop (pLua,1);


    // STDP constants
    
    lua_getglobal (pLua, "STDP_AINH");
    STDP_AINH = (float) lua_tonumber (pLua, 1);
    lua_pop (pLua,1); 

    lua_getglobal (pLua, "STDP_AEXC");
    STDP_AEXC = (float) lua_tonumber (pLua, 1);
    lua_pop (pLua,1); 
    
    lua_getglobal (pLua, "STDP_TYPE");
    STDP_TYPE = (NNLStdpType) lua_tonumber (pLua, 1);
    lua_pop (pLua,1); 

    // Delay learning parameters
    
    lua_getglobal (pLua, "DELAY_EPSILON");
    DELAY_EPSILON = (NNLtime) lua_tonumber (pLua, 1);
    lua_pop (pLua,1); 

    lua_getglobal (pLua, "DELAY_LEARNING_JITTER");
    DELAY_LEARNING_JITTER = (NNLtime) lua_tonumber (pLua, 1);
    lua_pop (pLua,1); 

    lua_getglobal (pLua, "DELAY_AINH"); // used for stdp-like delay learning. Currently unused.
    DELAY_AINH = (float) lua_tonumber (pLua, 1);
    lua_pop (pLua,1); 
    
    lua_getglobal (pLua, "DELAY_AEXC"); // used for stdp-like delay learning. Currently unused.
    DELAY_AEXC = (float) lua_tonumber (pLua, 1);
    lua_pop (pLua,1); 
    
  

  // close lua
  lua_close (pLua); //Theoretically, no need to close : automatically done by destructor of lua_State (i.e. pLua) class. But we do it anyway.

  cout << "done." << endl << flush;
  return true;
}

/*****************************************************************************/
//bool CParameters::saveToXmlFile (string OutputSettingFile)
//{
//  TiXmlDocument doc;
//	TiXmlDeclaration * decl = new TiXmlDeclaration( "1.0", "", "" );
//
//  TiXmlElement * Block;
//  TiXmlElement * SubBlock;
//  TiXmlComment * Comment;
//
//	doc.LinkEndChild( decl );
//
//  /***************/
//  Block = new TiXmlElement ("FileSystemSettings");
//
//  Comment = new TiXmlComment ();
//	Comment->SetValue(" Directories " );
//	Block->LinkEndChild (Comment);
//
//  SubBlock = new TiXmlElement ("ROOT");
//  SubBlock->SetAttribute("directory", ROOT.c_str());
//  Block->LinkEndChild (SubBlock);
//
//  SubBlock = new TiXmlElement ("DATA_OUTPUT");
//  SubBlock->SetAttribute("directory", DATA_OUTPUT_DIR.c_str());
//  SubBlock->SetAttribute("extention", DATA_OUTPUT_EXT.c_str());
//  Block->LinkEndChild (SubBlock);
//
//  //cout << " -- data_out : " << CParameters::DATA_OUTPUT_EXT << endl;
//  //cout << " -- pattern_out : " << CParameters::PATTERNS_DIR << endl;
//
//  SubBlock = new TiXmlElement ("PATTERNS");
//  SubBlock->SetAttribute("directory", PATTERNS_DIR.c_str());
//  SubBlock->SetAttribute("extention", PATTERNS_EXT.c_str());
//  Block->LinkEndChild (SubBlock);
//
//  SubBlock = new TiXmlElement ("FIGURE_SCRIPTS"); /* gnuplot .plot files */
//  SubBlock->SetAttribute("directory", FIGURE_SCRIPTS_DIR.c_str());
//  SubBlock->SetAttribute("extention", FIGURE_SCRIPTS_EXT.c_str());
//  Block->LinkEndChild (SubBlock);
//
//  SubBlock = new TiXmlElement ("FIGURES");
//  SubBlock->SetAttribute("directory", FIGURES_DIR.c_str());
//  SubBlock->SetAttribute("extention", FIGURES_EXT.c_str());
//  Block->LinkEndChild (SubBlock);
//
//  SubBlock = new TiXmlElement ("ARCHITECTURE");
//  SubBlock->SetAttribute("directory", ARCHITECTURE_OUTPUT_DIR.c_str());
//  SubBlock->SetAttribute("filename", ARCHITECTURE_XML_FILE.c_str());
//  Block->LinkEndChild (SubBlock);
//
//  SubBlock = new TiXmlElement ("SETTINGS_DIR");
//  SubBlock->SetAttribute("directory", SETTINGS_DIR.c_str());
//  Block->LinkEndChild (SubBlock);
//
//  doc.LinkEndChild (Block);
//  /***************/
//
//
//  /***************/
//  Block = new TiXmlElement ("ArchitectureSettings");
//
//  Comment = new TiXmlComment ();
//	Comment->SetValue(" Neuron constants " );
//	Block->LinkEndChild (Comment);
//
//  SubBlock = new TiXmlElement ("RESTING_POTENTIAL");
//  SubBlock->SetDoubleAttribute("value", RESTING_POTENTIAL);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("THRESHOLD");
//  SubBlock->SetDoubleAttribute("value", THRESHOLD);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("ABSOLUTE_REFRACTORY_POTENTIAL");
//  SubBlock->SetDoubleAttribute("value", ABSOLUTE_REFRACTORY_POTENTIAL);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("ABSOLUTE_REFRACTORY_PERIODE");
//  SubBlock->SetAttribute("value", ABSOLUTE_REFRACTORY_PERIODE);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("TAU_PSP");
//  SubBlock->SetAttribute("value", TAU_PSP);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("TAU_REFRACT");
//  SubBlock->SetAttribute("value", TAU_REFRACT);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("PSP_STRENGHT");
//  SubBlock->SetDoubleAttribute("value", PSP_STRENGHT);
//  Block->LinkEndChild (SubBlock);
//
//  Comment = new TiXmlComment ();
//	Comment->SetValue(" Synapse constants " );
//	Block->LinkEndChild (Comment);
//
//  SubBlock = new TiXmlElement ("MAX_DELAY");
//  SubBlock->SetAttribute("value", MAX_DELAY);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("MIN_DELAY");
//  SubBlock->SetAttribute("value", MIN_DELAY);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("MAX_WEIGHT");
//  SubBlock->SetDoubleAttribute("value", MAX_WEIGHT);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("MIN_WEIGHT");
//  SubBlock->SetDoubleAttribute("value", MIN_WEIGHT);
//  Block->LinkEndChild (SubBlock);
//
//  doc.LinkEndChild (Block);
//  /***************/
//
//  /***************/
//  Block = new TiXmlElement ("SimulationSettings");
//
//  Comment = new TiXmlComment ();
//	Comment->SetValue(" General settings " );
//	Block->LinkEndChild (Comment);
//
//  SubBlock = new TiXmlElement ("PRESENTATION_METHOD");
//  SubBlock->SetAttribute("value", PRESENTATION_METHOD);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("TIMESCALE");
//  SubBlock->SetAttribute("value", TIMESCALE);
//  Block->LinkEndChild (SubBlock);
//
//  Comment = new TiXmlComment ();
//	Comment->SetValue(" Initialisation, training and test phase " );
//	Block->LinkEndChild (Comment);
//
//  SubBlock = new TiXmlElement ("NB_TRAIN_EPOCS");
//  SubBlock->SetAttribute("value", NB_TRAIN_EPOCS);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("NB_RAND_STIM");
//  SubBlock->SetAttribute("value", NB_RAND_STIM);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("RAND_STIM_INTERVAL");
//  SubBlock->SetAttribute("value", RAND_STIM_INTERVAL);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("SEPARATION_INTERVAL");
//  SubBlock->SetAttribute("value", SEPARATION_INTERVAL);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("STIM_INTERVAL");
//  SubBlock->SetAttribute("value", STIM_INTERVAL);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("INITIALIZATION_START");
//  SubBlock->SetAttribute("value", INITIALIZATION_START);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("INITIALIZATION_LENGTH");
//  SubBlock->SetAttribute("value", INITIALIZATION_LENGTH);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("NB_STIM_PRESENTATIONS");
//  SubBlock->SetAttribute("value", NB_STIM_PRESENTATIONS);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("NB_STIM_REPETITION_IN_STIM_INTERVAL");
//  SubBlock->SetAttribute("value", NB_STIM_REPETITION_IN_STIM_INTERVAL);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("STIM_REPETITION_SEPERATION");
//  SubBlock->SetAttribute("value", STIM_REPETITION_SEPERATION);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("TRAINING_START");
//  SubBlock->SetAttribute("value", TRAINING_START);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("TRAINING_LENGTH");
//  SubBlock->SetAttribute("value", TRAINING_LENGTH);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("TRAIN_PERF_START");
//  SubBlock->SetAttribute("value", TRAIN_PERF_START);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("TRAIN_PERF_LENGTH");
//  SubBlock->SetAttribute("value", TRAIN_PERF_LENGTH);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("TEST_PERF_START");
//  SubBlock->SetAttribute("value", TEST_PERF_START);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("TEST_PERF_LENGTH");
//  SubBlock->SetAttribute("value", TEST_PERF_LENGTH);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("END_TIME");
//  SubBlock->SetAttribute("value", END_TIME);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("GENERALIZATION_PHASE");
//  SubBlock->SetAttribute("value", GENERALIZATION_PHASE);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("NOISE");
//  SubBlock->SetAttribute("value", NOISE);
//  Block->LinkEndChild (SubBlock);
//
//
//  Comment = new TiXmlComment ();
//	Comment->SetValue(" STDP constants " );
//	Block->LinkEndChild (Comment);
//
//  SubBlock = new TiXmlElement ("STDP_AINH");
//  SubBlock->SetDoubleAttribute("value", STDP_AINH);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("STDP_AEXC");
//  SubBlock->SetDoubleAttribute("value", STDP_AEXC);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("STDP_TYPE");
//  SubBlock->SetAttribute("value", STDP_TYPE);
//  Block->LinkEndChild (SubBlock);
//
//  Comment = new TiXmlComment ();
//	Comment->SetValue(" Delay learning parameters " );
//	Block->LinkEndChild (Comment);
//
//  SubBlock = new TiXmlElement ("DELAY_EPSILON");
//  SubBlock->SetAttribute("value", DELAY_EPSILON);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("DELAY_LEARNING_JITTER");
//  SubBlock->SetAttribute("value", DELAY_LEARNING_JITTER);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("DELAY_AINH");
//  SubBlock->SetDoubleAttribute("value", DELAY_AINH);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("DELAY_AEXC");
//  SubBlock->SetDoubleAttribute("value", DELAY_AEXC);
//  Block->LinkEndChild (SubBlock);
//
//  doc.LinkEndChild (Block);
//  /***************/
//
//  /***************/
//  Block = new TiXmlElement ("MeasuresSettings");
//
//  Comment = new TiXmlComment ();
//	Comment->SetValue(" Polychronous groups " );
//	Block->LinkEndChild (Comment);
//
//  SubBlock = new TiXmlElement ("PG_OUTPUT_DIR");
//  SubBlock->SetAttribute("value", PG_OUTPUT_DIR.c_str());
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("GENERATE_PGS");
//  SubBlock->SetAttribute("value", GENERATE_PGS);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("MAX_PG_TIME");
//  SubBlock->SetAttribute("value", MAX_PG_TIME);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("MIN_PG_TIME");
//  SubBlock->SetAttribute("value", MIN_PG_TIME);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("MIN_PG_SIZE");
//  SubBlock->SetAttribute("value", MIN_PG_SIZE);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("MAX_PG_SIZE");
//  SubBlock->SetAttribute("value", MAX_PG_SIZE);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("PG_JITTER");
//  SubBlock->SetAttribute("value", PG_JITTER);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("NB_TRIGGER_NEURONS");
//  SubBlock->SetAttribute("value", NB_TRIGGER_NEURONS);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("AVOID_CYCLICS");
//  SubBlock->SetAttribute("value", AVOID_CYCLICS);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("SEARCH_BY_NB_PSP");
//  SubBlock->SetAttribute("value", SEARCH_BY_NB_PSP);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("NB_PSP_TO_SPIKE");
//  SubBlock->SetAttribute("value", NB_PSP_TO_SPIKE);
//  Block->LinkEndChild (SubBlock);
//  SubBlock = new TiXmlElement ("WEIGHT_THRESHOLD");
//  SubBlock->SetDoubleAttribute("value", WEIGHT_THRESHOLD);
//  Block->LinkEndChild (SubBlock);
//
//
//  Comment = new TiXmlComment ();
//	Comment->SetValue(" Distribution measures " );
//	Block->LinkEndChild (Comment);
//
//  doc.LinkEndChild (Block);
//  /***************/
//
//  /*
//	TiXmlElement * element = new TiXmlElement( "Hello" );
//	TiXmlText * text = new TiXmlText( "World" );
//	element->LinkEndChild( text );
//	doc.LinkEndChild( element );
//
//  TiXmlComment * comment = new TiXmlComment();
//	comment->SetValue(" Settings for MyApp " );
//	root->LinkEndChild( comment );
//
//
//  TiXmlElement * xmlSynapse = new TiXmlElement("SynapseOut");
//
//  xmlSynapse->SetDoubleAttribute("weight", Weight);
//  xmlSynapse->SetAttribute("delay", Delay);
//  xmlSynapse->SetAttribute("inhibitory", (int) Inhibitory);
//  xmlSynapse->SetAttribute("weightSTDP", (int) WeightAdaptation);
//  xmlSynapse->SetAttribute("delaySTDP", (int) DelayAdaptation);
//  xmlSynapse->SetAttribute("inputNeuron", InputNeuron->getID());
//  xmlSynapse->SetAttribute("outputNeuron", OutputNeuron->getID());
//  */
//
//	return doc.SaveFile( OutputSettingFile.c_str() );
//
//}

/*****************************************************************************/
bool CParameters::saveToLuaFile (string OutputSettingFile)
{
  ofstream * OutputSettingsStream = new ofstream (OutputSettingFile.c_str(), ios::out|ios::trunc);
  
  if (!OutputSettingsStream)
  {
    cerr << "*** Error opening \"" << OutputSettingFile << "\"."<< endl;
    return false;
  }
  
  (*OutputSettingsStream) << "PSP_STRENGHT = \"" << PSP_STRENGHT << "\"" << endl;
  (*OutputSettingsStream) << "TIMESCALE = " << TIMESCALE << endl << endl;

  (*OutputSettingsStream) << " -- Synapse parameters" << endl << endl;
  (*OutputSettingsStream) << "MAX_DELAY = " << MAX_DELAY << endl;
  (*OutputSettingsStream) << "MIN_DELAY = " << MIN_DELAY << endl << endl;
  (*OutputSettingsStream) << "MAX_WEIGHT = " << MAX_WEIGHT << endl;
  (*OutputSettingsStream) << "MIN_WEIGHT = " << MIN_WEIGHT << endl << endl;

  (*OutputSettingsStream) << " -- Directories" << endl << endl;
  
  (*OutputSettingsStream) << "ROOT = \"" << ROOT << "\"" << endl << endl;
  
  (*OutputSettingsStream) << "DATA_OUTPUT_DIR = \"" << DATA_OUTPUT_DIR << "\"" << endl;
  (*OutputSettingsStream) << "DATA_OUTPUT_EXT = \"" << DATA_OUTPUT_EXT << "\"" << endl << endl;
  
  (*OutputSettingsStream) << "PATTERNS_DIR = \"" << PATTERNS_DIR << "\"" << endl;
  (*OutputSettingsStream) << "PATTERNS_EXT = \"" << PATTERNS_EXT << "\"" << endl << endl;
  
  (*OutputSettingsStream) << "FIGURE_SCRIPTS_DIR = \"" << FIGURE_SCRIPTS_DIR << "\"" << endl;
  (*OutputSettingsStream) << "FIGURE_SCRIPTS_EXT = \"" << FIGURE_SCRIPTS_EXT << "\"" << endl << endl;
  
  (*OutputSettingsStream) << "FIGURES_DIR = \"" << FIGURES_DIR << "\"" << endl;
  (*OutputSettingsStream) << "FIGURES_EXT = \"" << FIGURES_EXT << "\"" << endl << endl;
  
  (*OutputSettingsStream) << "ARCHITECTURE_OUTPUT_DIR = \"" << ARCHITECTURE_OUTPUT_DIR << "\"" << endl;
  (*OutputSettingsStream) << "ARCHITECTURE_XML_FILE = \"" << ARCHITECTURE_XML_FILE << "\"" << endl << endl;
  
  (*OutputSettingsStream) << "BUILDERS_DIR = \"" << BUILDERS_DIR << "\"" << endl;
  (*OutputSettingsStream) << "SETTINGS_DIR = \"" << SETTINGS_DIR << "\"" << endl;
  (*OutputSettingsStream) << "SIMULATIONS_DIR = \"" << SIMULATIONS_DIR << "\"" << endl;
  
  
  (*OutputSettingsStream) << "      -- STDP constants" << endl << endl;

  (*OutputSettingsStream) << "local CLASSICAL_STDP = " << CLASSICAL_STDP << endl;
  (*OutputSettingsStream) << "local MEUNIER_STDP = " << MEUNIER_STDP << endl << endl;
  
  (*OutputSettingsStream) << "STDP_AINH = " << STDP_AINH << endl;
  (*OutputSettingsStream) << "STDP_AEXC = " << STDP_AEXC << endl;
  (*OutputSettingsStream) << "STDP_TYPE = " << STDP_TYPE << endl << endl;
  
  (*OutputSettingsStream) << "      -- Delay learning parameters" << endl << endl;
  
  (*OutputSettingsStream) << "DELAY_EPSILON = " << DELAY_EPSILON << endl;
  (*OutputSettingsStream) << "DELAY_LEARNING_JITTER = " << DELAY_LEARNING_JITTER << endl << endl;
  
  (*OutputSettingsStream) << "DELAY_AINH = " << DELAY_AINH << " -- used for stdp-like delay learning. Currently unused." << endl;
  (*OutputSettingsStream) << "DELAY_AEXC = " << DELAY_AEXC << " -- used for stdp-like delay learning. Currently unused." << endl << endl;
  
  OutputSettingsStream->close ();
  
  //delete OutputSettingsStream;
  
  return true;
}


NNLtime CParameters::TIMESCALE = 10;
NNLpotential CParameters::PSP_STRENGHT = 12;
NNLtime CParameters::MAX_DELAY = 20 * TIMESCALE;
NNLtime CParameters::MIN_DELAY = 1;
NNLpotential CParameters::MAX_WEIGHT = 1.0;
NNLpotential CParameters::MIN_WEIGHT = 0;

string CParameters::ROOT = "./";
string CParameters::DATA_OUTPUT_DIR = "data/";
string CParameters::DATA_OUTPUT_EXT = "txt";
string CParameters::PATTERNS_DIR = "datasets/";
string CParameters::PATTERNS_EXT = "dat";
string CParameters::FIGURE_SCRIPTS_DIR = "plots/";
string CParameters::FIGURE_SCRIPTS_EXT = "plot";
string CParameters::FIGURES_DIR = "figures/";
string CParameters::FIGURES_EXT = "eps";
string CParameters::ARCHITECTURE_OUTPUT_DIR = "architectures/";
string CParameters::ARCHITECTURE_XML_FILE = "architecture-%07ld.xml";
string CParameters::BUILDERS_DIR = "builders/";
string CParameters::SETTINGS_DIR = "settings/";
string CParameters::SIMULATIONS_DIR = "simulations/";

float CParameters::STDP_AINH = 0.1;
float CParameters::STDP_AEXC = 0.1;

NNLStdpType CParameters::STDP_TYPE = MEUNIER_STDP;
NNLtime CParameters::DELAY_EPSILON = 5 * TIMESCALE;
NNLtime CParameters::DELAY_LEARNING_JITTER = 1 * TIMESCALE;
float CParameters::DELAY_AINH = 0.01; // used for stdp-like delay learning. Currently unused.
float CParameters::DELAY_AEXC = 0.01; // used for stdp-like delay learning. Currently unused.
