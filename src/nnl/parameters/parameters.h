/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
 
#ifndef _PARAMETERS_H
#define _PARAMETERS_H

#include "../simulator/globals.h"

#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>


class CParameters
{
  public:
    /* Parameters */  

    /**/
    // FileSystemSettings
    

      // Parameters
      static NNLtime TIMESCALE;
      static NNLpotential PSP_STRENGHT;

      // Synapse parameters
      static NNLtime MAX_DELAY;
      static NNLtime MIN_DELAY;
      static NNLweight MAX_WEIGHT;
      static NNLweight MIN_WEIGHT;

      // Directories
  
      static string ROOT;
  
      static string DATA_OUTPUT_DIR;
      static string DATA_OUTPUT_EXT;
  
      static string PATTERNS_DIR;
      static string PATTERNS_EXT;
  
      static string FIGURE_SCRIPTS_DIR;
      static string FIGURE_SCRIPTS_EXT;

      static string FIGURES_DIR;
      static string FIGURES_EXT;
  
      static string ARCHITECTURE_OUTPUT_DIR;
      static string ARCHITECTURE_XML_FILE;
      
      static string BUILDERS_DIR;
      static string SETTINGS_DIR;
      static string SIMULATIONS_DIR;
      
      // STDP constants
      
      static float STDP_AINH;
      static float STDP_AEXC;
      
      static NNLStdpType STDP_TYPE;
      
      // Delay learning parameters
      
      static NNLtime DELAY_EPSILON;
      static NNLtime DELAY_LEARNING_JITTER;
      
      static float DELAY_AINH; // used for stdp-like delay learning. Currently unused.
      static float DELAY_AEXC; // used for stdp-like delay learning. Currently unused.
  
    /**/

  private:
    /*Constructor / desctructor*/
    CParameters (/*string XMLSettingsFile = "sample-settings.xml" */);
    virtual ~CParameters ();
  
  public:
    /* Methods */
    //static bool loadFromXmlFile (string InputSettingFile);
    static bool loadFromLuaFile (string InputSettingFile);
  
    //static bool saveToXmlFile (string OutputSettingFile);
    static bool saveToLuaFile (string OutputSettingFile);
};

#endif /* _PARAMETERS_H */
