/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


#include "simulation.h"
#include "simulationphase.h"
#include "parameters.h"

CSimulation::CSimulation()
{
}

CSimulation::~CSimulation ()
{
  CSimulation::iterator itPhase;
  CSimulation::iterator itLastPhase = end ();

  itPhase = begin ();
  while (itPhase != itLastPhase)
  {
    delete (*itPhase);
    itPhase++;
  }

  clear ();
}

/****************************************************************************/
void CSimulation::LoadFromLua (const char * LuaFilename)
{
  lua_State * pLua = lua_open();
  cout << "Loading simulation \"" << LuaFilename << "\" ... " << endl << flush;

  //open the lua libaries - new in lua5.1
  luaL_openlibs(pLua);

  //load and run the script
  int error = luaL_dofile(pLua, LuaFilename);

  if (error)
  {
    cerr << "-- " << lua_tostring(pLua, -1) << endl;
    luaL_error (pLua, "An error occured while loading %s", LuaFilename);
    exit (error);
  }

  //reset the stack index
  lua_settop(pLua, 0);

  CSimulationPhase * tmpPhase;

  for (int Index = 1; Index <= lua_getTableLength (pLua, "Simulation"); Index ++)
  {
    tmpPhase = new CSimulationPhase ( lua_getStringTableElement (pLua, "Simulation", Index) );
    tmpPhase->LoadFromLua (pLua);
    push_back (tmpPhase);
  }

  //tidy up
  lua_close(pLua);

  //cout << "done." << endl << flush;
}


/****************************************************************************/
void CSimulation::SaveToLua (const char * LuaFilename)
{

  ofstream * LuaOutputFile = new ofstream (LuaFilename, ios::out|ios::trunc);

  CSimulation::iterator itPhase;
  CSimulation::iterator itLastPhase = end ();

  int numEvents = 0;

  (*LuaOutputFile) << "------------------- Simulation Phases -----------------" << endl << endl << endl;

  (*LuaOutputFile) << "TIMESCALE = " << (double) CParameters::TIMESCALE << endl << endl << endl;


  // Write Phases info
  itPhase = begin ();
  while (itPhase != itLastPhase)
  {
    numEvents = (*itPhase)->SaveToLua (LuaOutputFile, numEvents);
    itPhase++;
  }

  (*LuaOutputFile) << "------------------- Simulation Description ------------" << endl << endl;

  // Write simulation table
  (*LuaOutputFile) << "Simulation = { ";
  itPhase = begin ();
  while (itPhase != itLastPhase)
  {
    (*LuaOutputFile) << "\"" << (*itPhase)->getPhaseName () << "\" ; ";
    itPhase++;
  }
  (*LuaOutputFile) << " }" << endl;

  LuaOutputFile->close ();
}


