/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "simulationphase.h"


/*****************************************************************************/
CSimulationPhase::~CSimulationPhase ()
{
  list<NNLevent*>::iterator itEvent = EventList.begin();
  list<NNLevent*>::iterator itLastEvent = EventList.end();

  while (itEvent != itLastEvent)
  {
    delete (*itEvent);
    itEvent++;
  }

  EventList.clear();
}

/*****************************************************************************/
void CSimulationPhase::enqueueEvent (NNLevent * evt)
{
  EventList.push_back (evt);
}

/*****************************************************************************/
string & CSimulationPhase::getPhaseName ()
{
  return Name;
}

/*****************************************************************************/
string & CSimulationPhase::getPhaseDescription ()
{
  return Description;
}

/*****************************************************************************/
NNLtime CSimulationPhase::getPhaseLength ()
{
  
  list<NNLevent *>::iterator itEvt = EventList.begin ();
  list<NNLevent *>::iterator itLastEvt = EventList.end ();
 
  NNLtime PhaseLength = 0;
  NNLtime tmpTime;

  for (itEvt = EventList.begin (); itEvt != itLastEvt; itEvt++)
  {
    tmpTime = (*itEvt)->Time;
    if (tmpTime == -1)
      tmpTime = 0;

    if ( (tmpTime + (*itEvt)->Duration) > PhaseLength )
    {
      PhaseLength = tmpTime + (*itEvt)->Duration;
    }
  }
  
  return PhaseLength;
}

/*****************************************************************************/
void CSimulationPhase::LoadFromLua (lua_State * pLua)
{
  NNLevent * tmpEvt;

  cout << "  phase " << Name << ": " << flush ;
  Description = lua_getStringTableElement (pLua, Name.c_str (), 1);

  for (int Index = 2; Index <= lua_getTableLength (pLua, Name.c_str ()); Index ++)
  {
    tmpEvt = new NNLevent ();
    tmpEvt->LoadFromLua (pLua, lua_getStringTableElement (pLua, Name.c_str (), Index));
    cout << "... " << flush;
    enqueueEvent (tmpEvt);
  }

  cout << "done." << endl << flush;
}

/****************************************************************************/
int CSimulationPhase::SaveToLua (ofstream * LuaOutputFile, int numEvent)
{
  int tmpNumEvent = numEvent;

  list<NNLevent *>::iterator itEvent;
  list<NNLevent *>::iterator itLastEvent = EventList.end();

  (*LuaOutputFile) << "-------- Phase : " << Name << " ---------- " << endl << endl;

  //Writing events
  itEvent = EventList.begin();
  while (itEvent != itLastEvent)
  {
    std::stringstream ss;
    ss << tmpNumEvent;

    (*itEvent)->SaveToLua(LuaOutputFile, (string("Event") + ss.str ()).c_str());
    tmpNumEvent++;
    itEvent++;
  }

  //writing phase table
  (*LuaOutputFile) << Name << " = { \"" << Description << "\"; ";
  itEvent = EventList.begin();
  while (itEvent != itLastEvent)
  {
    (*LuaOutputFile) << "\"" << "Event" << numEvent << "\" ; ";
    numEvent++;
    itEvent++;
  }
  (*LuaOutputFile) << " }" << endl << endl << endl;

  return numEvent;
}






