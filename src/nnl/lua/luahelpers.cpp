/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <iostream>
using namespace std;

#include "luahelpers.h"

/****************************************************************************/
int lua_getTableLength (lua_State * pLua, const char * TableName)
{
  lua_getglobal(pLua, TableName);
  int tabPos = lua_gettop (pLua);
  int tabLen = lua_objlen (pLua, tabPos);
  lua_pop (pLua, 1);

  return tabLen;
}

/****************************************************************************/
bool lua_fieldExist (lua_State * pLua, const char * TableName, const char * FieldName)
{
  bool res = false;

  lua_getglobal(pLua, TableName);
  int tabPos = lua_gettop (pLua);

  lua_pushstring (pLua, FieldName);
  lua_gettable (pLua, tabPos);
  res = !(lua_isnoneornil (pLua, lua_gettop (pLua)));
  lua_pop (pLua, 2);

  return res;
}

/****************************************************************************/
int lua_getIntTableElement (lua_State * pLua, const char * TableName, int Index)
{
  int res = 0;

  lua_getglobal(pLua, TableName);
  int tabPos = lua_gettop (pLua);

  if (Index <= lua_getTableLength (pLua,TableName))
  {
    lua_pushinteger (pLua, Index);
    lua_gettable (pLua, tabPos);
    res = lua_tointeger (pLua, lua_gettop (pLua));
    lua_pop (pLua, 1);
  }
  lua_pop (pLua, 1);

  return res;
}

/****************************************************************************/
double lua_getDoubleTableElement (lua_State * pLua, const char * TableName, int Index)
{
  double res = 0;

  lua_getglobal(pLua, TableName);
  int tabPos = lua_gettop (pLua);

  if (Index <= lua_getTableLength (pLua,TableName))
  {
    lua_pushinteger (pLua, Index);
    lua_gettable (pLua, tabPos);
    res = lua_tonumber (pLua, lua_gettop (pLua));
    lua_pop (pLua, 1);
  }
  lua_pop (pLua, 1);

  return res;
}

/****************************************************************************/
const char * lua_getStringTableElement (lua_State * pLua, const char * TableName, int Index)
{
  char * res = 0;

  lua_getglobal(pLua, TableName);
  int tabPos = lua_gettop (pLua);

  if (Index <= lua_getTableLength (pLua,TableName))
  {
    lua_pushinteger (pLua, Index);
    lua_gettable (pLua, tabPos);
    res = (char *) lua_tostring (pLua, lua_gettop (pLua));
    lua_pop (pLua, 1);
  }
  lua_pop (pLua, 1);

  return res;
}

/****************************************************************************/
bool lua_getBooleanTableElement (lua_State * pLua, const char * TableName, int Index)
{
  int res = 0;

  lua_getglobal(pLua, TableName);
  bool tabPos = lua_gettop (pLua);

  if (Index <= lua_getTableLength (pLua,TableName))
  {
    lua_pushinteger (pLua, Index);
    lua_gettable (pLua, tabPos);
    res = lua_toboolean (pLua, lua_gettop (pLua));
    lua_pop (pLua, 1);
  }
  lua_pop (pLua, 1);

  return res;
}

/****************************************************************************/
int lua_getIntTableElement (lua_State * pLua, const char * TableName, const char * FieldName)
{
  int res = 0;

  lua_getglobal(pLua, TableName);
  int tabPos = lua_gettop (pLua);

  lua_pushstring (pLua, FieldName);
  lua_gettable (pLua, tabPos);
  res = lua_tointeger (pLua, lua_gettop (pLua));
  lua_pop (pLua, 2);

  return res;
}

/****************************************************************************/
double lua_getDoubleTableElement (lua_State * pLua, const char * TableName, const char * FieldName)
{
  double res = 0;

  lua_getglobal(pLua, TableName);
  int tabPos = lua_gettop (pLua);

  lua_pushstring (pLua, FieldName);
  lua_gettable (pLua, tabPos);
  res = lua_tonumber (pLua, lua_gettop (pLua));
  lua_pop (pLua, 2);

  return res;
}

/****************************************************************************/
const char * lua_getStringTableElement (lua_State * pLua, const char * TableName, const char * FieldName)
{
  char * res = 0;

  lua_getglobal(pLua, TableName);
  int tabPos = lua_gettop (pLua);

  lua_pushstring (pLua, FieldName);
  lua_gettable (pLua, tabPos);
  res = (char *) lua_tostring (pLua, lua_gettop (pLua));
  lua_pop (pLua, 2);

  return res;
}

/****************************************************************************/
bool lua_getBooleanTableElement (lua_State * pLua, const char * TableName, const char * FieldName)
{
  int res = 0;

  lua_getglobal(pLua, TableName);
  int tabPos = lua_gettop (pLua);

  lua_pushstring (pLua, FieldName);
  lua_gettable (pLua, tabPos);
  res = lua_toboolean (pLua, lua_gettop (pLua));
  lua_pop (pLua, 2);

  return res;
}
