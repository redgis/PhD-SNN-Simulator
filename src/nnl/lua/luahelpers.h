/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef LUAHELPERS_H_
#define LUAHELPERS_H_

extern "C"
{
  #include <lua.h>
  #include <lualib.h>
  #include <lauxlib.h>
}

bool lua_fieldExist (lua_State * pLua, const char * TableName, const char * FieldName);
int lua_getTableLength (lua_State * pLua, const char * TableName);

int lua_getIntTableElement (lua_State * pLua, const char * TableName, int Index);
double lua_getDoubleTableElement (lua_State * pLua, const char * TableName, int Index);
const char * lua_getStringTableElement (lua_State * pLua, const char * TableName, int Index);
bool lua_getBooleanTableElement (lua_State * pLua, const char * TableName, int Index);

int lua_getIntTableElement (lua_State * pLua, const char * TableName, const char *);
double lua_getDoubleTableElement (lua_State * pLua, const char * TableName, const char *);
const char * lua_getStringTableElement (lua_State * pLua, const char * TableName, const char *);
bool lua_getBooleanTableElement (lua_State * pLua, const char * TableName, const char *);

#endif /* LUAHELPERS_H_ */
