cmake_minimum_required(VERSION 3.2)

project(nnl)

   file(GLOB_RECURSE NNL_SOURCES
      parameters/*.cpp simulator/*.cpp architecture/*.cpp common/*.cpp
      events/*.cpp lua/*.cpp metastructure/*.cpp patterns/*.cpp
      simulation/*.cpp tinyxml/*.cpp)

   include_directories(parameters simulator architecture common events lua
      metastructure patterns simulation tinyxml eventfunctions)

   add_executable (nnl ${NNL_SOURCES})


project(eventfunctions)

   add_library(eventfunctions SHARED eventfunctions/eventfunctions.cpp)

target_link_libraries(nnl gsl igraph lua luabind dl pthread eventfunctions)
target_link_libraries(eventfunctions lua luabind dl pthread)
