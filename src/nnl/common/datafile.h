/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
 
#ifndef _DATAFILE_H
#define _DATAFILE_H

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iomanip>

using namespace std;

#include "../simulator/globals.h"

class CDataFile;

namespace std {
  //template <typename T> T& endl (T&);
  CDataFile& endl (CDataFile&);
};

class CDataFile 
{
  private:
    
    unsigned int ChunkMaxSize; // 0 if no limit
    int CurrentChunkNumber;
    //ostringstream OutputString;
    ofstream * OutputStream;
    string BaseName;
    string CurrentFilename;
    ios::openmode mode;
    
    string & GenerateName ();
    
    bool NameHasChanged;
    string previousName;
    
    string DataDescription;
    
  public:  
    CDataFile (const char * filename, ios::openmode mode, string desc = string (""), unsigned int ChunkSize = 10485760 /* 20971520 */);
    virtual ~CDataFile () { close (); };
    
    bool VerifyChunkSize (); /* return true if maximum size excedeed, and new file is created */
    string & getCurrentFilename ();
    string & getBaseName ();
    bool hasChangedName ();
    string & getPreviousName ();
    void FlushCloseReopen ();
    void close ();
    
    string getDescription ();
    void setDescription (string Desc);
    
    friend class CEventManager;
    
    friend CDataFile & operator << (CDataFile &, const bool &);
    friend CDataFile & operator << (CDataFile &, const char &);
    friend CDataFile & operator << (CDataFile &, const unsigned char &);
    friend CDataFile & operator << (CDataFile &, const signed char &);
    friend CDataFile & operator << (CDataFile &, const int &);
    friend CDataFile & operator << (CDataFile &, const unsigned int &);
    //friend CDataFile & operator << (CDataFile &, const signed int &);
    friend CDataFile & operator << (CDataFile &, const short int &);
    friend CDataFile & operator << (CDataFile &, const unsigned short int &);
    //friend CDataFile & operator << (CDataFile &, const signed short int &);
    friend CDataFile & operator << (CDataFile &, const long int &);
    //friend CDataFile & operator << (CDataFile &, const signed long int &);
    friend CDataFile & operator << (CDataFile &, const unsigned long int &);
    friend CDataFile & operator << (CDataFile &, const float &);
    friend CDataFile & operator << (CDataFile &, const double&);
    friend CDataFile & operator << (CDataFile &, const long double &);
    friend CDataFile & operator << (CDataFile &, const string &);
    friend CDataFile & operator << (CDataFile &, const char *);
    friend CDataFile & operator << (CDataFile & out, CDataFile & (*Dummy) (CDataFile &));
    friend CDataFile & operator << (CDataFile & out, const std::_Setw & manip);
    friend CDataFile & operator << (CDataFile & out, const std::_Setprecision & manip);
    friend CDataFile & operator << (CDataFile & out, const std::_Setbase & manip);
    friend CDataFile & operator << (CDataFile & out, const std::_Setiosflags & manip);
    friend CDataFile & operator << (CDataFile & out, const std::_Resetiosflags & manip);
    friend CDataFile & operator << (CDataFile & out, const std::_Setfill<char> & manip);
    
    
    template <typename T> 
    friend CDataFile& operator<<(CDataFile& out, const T& dummy);
    
    template <typename T> 
    friend CDataFile& operator<<(CDataFile& out, T& (*dummy)(T&));

    //template <typename T> 
    //friend T & std::endl (T& out);
    friend CDataFile & std::endl (CDataFile& out);
};




#endif /* _DATAFILE_H */
