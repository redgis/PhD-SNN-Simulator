/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "tools.h"
#include "parameters.h"

#define PI ((double) 3.141592)

int NbValPSP = 0;
int NbValRefract = 0;

/* Values for exp(-x/Tau) for x from 0 to 5*Tau */
NNLpotential * ExpTabIzhiSTDP = NULL;
//NNLpotential * ExpTabRefract = NULL;


/*****************************************************************************/
void InitTools ()
{
  /* Initialises the random seed */
  //srand (12345);
  //srand (54321);
  srand (RandSeed);
  
  cout << "Random seed : " << (unsigned int) RandSeed << endl;
  
  sem_init (&semThread, 0, 0);
  sem_init (&semMain, 0, 1);
  
  
  int t;
  
  /*****************************************************************************/
  /* Number of time steps between 0 and 5*Tau (Tau is the time constant of the neuron for PSPs) */
  int NbVal = (int) ((float)(20*CParameters::TIMESCALE)) + 1;
  
  /* Exponential values from 1 to 0 */
  ExpTabIzhiSTDP = (NNLpotential *) malloc ((NbVal+1) * sizeof(NNLpotential));
  
  for (t = 0; t <= NbVal; t++)
  {
    *(ExpTabIzhiSTDP+t) = (NNLpotential) exp((double)(-((float)t/(float)CParameters::TIMESCALE)/((float)20)));
      //cout <<  "Inhib : " << - 0.12* ExpTabIzhiSTDP[t] << endl;
      //cout <<  "Excit : " << 0.1 * ExpTabIzhiSTDP[t] << endl << endl;
  }
  
  
  //~ /*****************************************************************************/
  //~ /* Number of time steps between 0 and 5*Tau (Tau is the time constant of the neuron */
  //~ NbValRefract = (int) ((float)(5*TAU_REFRACT)) + 1;
  
  //~ /* Exponential values from 1 to 0 */
  //~ ExpTabRefract = (NNLpotential *) malloc ((NbValRefract+1) * sizeof(NNLpotential));
  
  //~ for (t = 0; t <= NbValRefract; t++)
    //~ *(ExpTabRefract+t) = (NNLpotential) exp((double)(-((float)t)/((float)TAU_REFRACT)));
}

  
//~ /*****************************************************************************/
//~ NNLpotential ExpPSP (int val)
//~ {
  //~ return *(ExpTabPSP+val);
//~ }

//~ /*****************************************************************************/
//~ NNLpotential ExpRefract (int val)
//~ {
  //~ return *(ExpTabRefract+val);
//~ }

/*****************************************************************************/
void TimeRemaining (float PercentDone, long int Elapsed, long int * Hours = NULL, long int * Minutes = NULL, long int * Seconds = NULL)
{ 
  int EstimatedEnd = (long int) ((100.0 / PercentDone) * (double) Elapsed - (double)Elapsed);
  
  if (Hours != NULL)
    *Hours = EstimatedEnd/(60*60);
  
  if (Minutes != NULL)
    *Minutes = (EstimatedEnd%(60*60))/60;
  
  if (Seconds != NULL)
    *Seconds = (EstimatedEnd%(60*60))%60;
}


/*****************************************************************************/
/* Mexican hat function */
double Wavelet (double x, double maxima, double ParaboleWidth, double GaussianHatWidth)
{
  // try this in gnuplot : 
  // maxima = 0.5; ParaboleWidth = 10; GaussianHatWidth = 12
  // plot (maxima - (x**2)/ParaboleWidth), exp ( - ( (x**2)/GaussianHatWidth) ), (maxima - (x**2)/ParaboleWidth) * exp ( - ( (x**2)/GaussianHatWidth) )
  
  double Parabole = ( maxima - x*x / ParaboleWidth );
  double GaussianHat = exp ( - ( x*x / GaussianHatWidth ) ); 
  
  return Parabole * GaussianHat;
}


/*****************************************************************************/
/* Normal law Gaussian hat function */
double Gaussian (double x, double sigma, double center)
{
  //try this in gnuplot :
  // sigma = 1; center = 0; PI = 3.141592
  // plot (1 / (sigma * sqrt (2 * PI))), exp ( - (x - center)**2 / (2 * sigma**2) ), (1 / (sigma * sqrt (2 * PI))) * exp ( - (x - center)**2 / (2 * sigma**2) )
  double coef = 1 / (sigma * sqrt(2 * PI));
  double exponential = exp ( - ( (x - center) * (x - center) ) / ( 2 * sigma * sigma ) );
  
  return coef * exponential;
}

/*****************************************************************************/
/* Normal law Gaussian hat function */
double GaussianProba (double x, double sigma, double center)
{
  //try this in gnuplot :
  // sigma = 1; center = 0;
  // plot exp ( - (x - center)**2 / (2 * sigma**2) )
  double exponential;

  if (sigma == 0)
    exponential = 1;
  else
    exponential = exp ( - ( (x - center) * (x - center) ) / ( 2 * sigma * sigma ) );

  return exponential;
}

/*****************************************************************************/
/* Gaussian hat function with specified amplitude */
double GaussianMaxima (double x, double amplitude, double sigma, double center)
{
  //try this in gnuplot :
  // amplitude = 0.7; sigma = 1; center = 0; PI = 3.141592
  // plot aplitude, exp ( - (x - center)**2 / (2 * sigma**2) ), amplitude * exp ( - (x - center)**2 / (2 * sigma**2) )
  double result = amplitude * exp ( - ( (x - center) * (x - center) ) / ( 2 * sigma * sigma ) );
  
  return result;
}


/*****************************************************************************/
/* Inverse log function with specified amplitude / range */
double InverseLogDistrib (double x, double Maxima, double BorneMax)
{
  //try this in gnuplot :
  // plot ((-(log(100-x) / log(10)) + log(100)/log(10) ) /2) * 5
  
  double decal = (log(BorneMax+1) / log(10));  //+1 to avoid log(0)
  double loga = - (log(BorneMax+1-x)/log(10));  //+1 to avoid log(0)
  
  double result = ((loga + decal) / 2) * Maxima;
  
  return result;
}
