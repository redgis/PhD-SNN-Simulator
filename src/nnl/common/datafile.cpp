/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
 
#include "datafile.h"


/*****************************************************************************/
CDataFile::CDataFile (const char * filename, ios::openmode newMode, string Desc, unsigned int ChunkSize)
{
  
  ChunkMaxSize = ChunkSize;
  CurrentChunkNumber = 0;
  
  //OutputString.clear ();
  //OutputString.str ("");
  //OutputString.str ().resize (ChunkMaxSize);

  mode = newMode;
  
  BaseName = string (filename);

  GenerateName ();
  
  OutputStream = new ofstream (CurrentFilename.c_str (), mode);
  
  if (OutputStream->fail ())
    cout << endl << "*** ERROR : Unable to create : \"" << CurrentFilename.c_str () << "\"" << endl;
  
  DataDescription = Desc;
  
  previousName = "";
  NameHasChanged = false;
    
  //OutputStream->setf (ios::fixed);
  OutputStream->precision (10);
  
}

/*****************************************************************************/
//~ CDataFile::~CDataFile ()
//~ {
  //~ close ();
//~ }

/*****************************************************************************/
bool CDataFile::hasChangedName ()
{
  bool toReturn = NameHasChanged;
  NameHasChanged = false;
  return toReturn;
}

/*****************************************************************************/
string & CDataFile::getPreviousName ()
{
  return previousName;
}

/*****************************************************************************/
string & CDataFile::GenerateName ()
{
  int ExtensionPosition = 0;
  char ChunkNumber[128];
  
  ExtensionPosition = BaseName.rfind (".", BaseName.size());
  
  //cout << "\"" << BaseName << "\" : " << ExtensionPosition << endl << flush;
  //cout << "extention : \"" << BaseName.substr (ExtensionPosition , BaseName.size() - ExtensionPosition) << "\"" << endl << flush;
  
  /* Base name */
  CurrentFilename = BaseName.substr(0, ExtensionPosition);
  
  sprintf (ChunkNumber, "-%05d", CurrentChunkNumber);
  
  CurrentFilename = CurrentFilename + ChunkNumber + BaseName.substr (ExtensionPosition , BaseName.size() - ExtensionPosition);
  
  NameHasChanged = true;
  //cout << "\"" << newFileName << "\"" << endl << flush;
  
  return CurrentFilename;
}


/*****************************************************************************/
bool CDataFile::VerifyChunkSize ()
{ 
  //cout << BaseName << endl;
  //if (BaseName == "data/SpikeRasterExcit.txt")
  //  cout << OutputStream->tellp () << " " << ChunkMaxSize << endl;
  
  if ( (ChunkMaxSize != 0) && (OutputStream->tellp () > ChunkMaxSize) )
  {
    FlushCloseReopen ();
    return true;
  }
  
  return false;
}

/*****************************************************************************/
void CDataFile::FlushCloseReopen ()
{
  //string outString = OutputString.str ();
  //unsigned int Separator = outString.rfind("\n");
  
  //(*OutputStream) << outString.substr (0, Separator);  
  CurrentChunkNumber++;
  GenerateName ();
  //cout << "New filename : " << CurrentFilename << endl << flush;

  close ();
  
  OutputStream = new ofstream (CurrentFilename.c_str(), mode);
  //OutputStream->setf (ios::fixed);
  OutputStream->precision (10);
  
  if (OutputStream->fail ())
    cout << endl << "*** ERROR : Unable to create : \"" << CurrentFilename.c_str () << "\"" << endl;
  
    
  
  //cout << "\"" << outString << "\" : " << Separator << endl << flush;
  //cout << "extention : \"" << outString.substr (0, Separator) << "\"" << endl << flush;
  
  //~ OutputString.clear ();
  //~ OutputString.str ("");
  //~ OutputString.str ().resize (ChunkMaxSize);
  
  //~ if (Separator == string::npos)
    //~ OutputString << outString.substr (Separator, string::npos);
  //~ else
    //~ OutputString << outString.substr (Separator, outString.size () - Separator);
}

/*****************************************************************************/
string & CDataFile::getCurrentFilename ()
{
  return CurrentFilename;
}

/*****************************************************************************/
string & CDataFile::getBaseName ()
{
  return BaseName;
}

/*****************************************************************************/
void CDataFile::close ()
{
  OutputStream->close ();
  delete OutputStream;
}


/*****************************************************************************/
string CDataFile::getDescription ()
{
  return DataDescription;
}

/*****************************************************************************/
void CDataFile::setDescription (string Desc)
{
  DataDescription = Desc;
}


/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/
/*****************************************************************************/
template <typename T> CDataFile& operator<<(CDataFile& out, const T& dummy)
{
  //out.VerifyChunkSize ();
  *(out.OutputStream) << dummy;
  return out;
}

/*****************************************************************************/
template <typename T> CDataFile& operator<<(CDataFile& out, T& (*dummy)(T&))
{
  //out.VerifyChunkSize ();
  *(out.OutputStream) << (*dummy);
  return out;
}


/*****************************************************************************/
CDataFile & std::endl(CDataFile & out)
{
  //cout << "ENDL"<< endl;
  *(out.OutputStream) << endl;
  out.VerifyChunkSize ();
  return out;
}

/*
boolalpha  	Turns on the boolalpha flag  	X  	X
dec 	Turns on the dec flag 	X 	X
endl 	Output a newline character, flush the stream 		X
ends 	Output a null character 		X
fixed 	Turns on the fixed flag 		X
flush 	Flushes the stream 		X
hex 	Turns on the hex flag 	X 	X
internal 	Turns on the internal flag 		X
left 	Turns on the left flag 		X
noboolalpha 	Turns off the boolalpha flag 	X 	X
noshowbase 	Turns off the showbase flag 		X
noshowpoint 	Turns off the showpoint flag 		X
noshowpos 	Turns off the showpos flag 		X
noskipws 	Turns off the skipws flag 	X 	
nounitbuf 	Turns off the unitbuf flag 		X
nouppercase 	Turns off the uppercase flag 		X
oct 	Turns on the oct flag 	X 	X
right 	Turns on the right flag 		X
scientific 	Turns on the scientific flag 		X
showbase 	Turns on the showbase flag 		X
showpoint 	Turns on the showpoint flag 		X
showpos 	Turns on the showpos flag 		X
skipws 	Turns on the skipws flag 	X 	
unitbuf 	Turns on the unitbuf flag 		X
uppercase 	Turns on the uppercase flag 		X
ws 	Skip any leading whitespace 	X 	
*/


/*****************************************************************************/
CDataFile & operator << (CDataFile & out, CDataFile & (*manip) (CDataFile &))
{
  //out.VerifyChunkSize ();
  //cout << "MANIP:\"" << (*manip) << "\"" << endl;
  //out.OutputString << (*manip);
  return manip(out);
}

/*****************************************************************************/
CDataFile& operator<< (CDataFile& out, const std::_Setw & manip)
{
  *(out.OutputStream) << manip;
  return out;
}

/*****************************************************************************/
CDataFile& operator<< (CDataFile& out, const std::_Setprecision & manip)
{
  *(out.OutputStream) << manip;
  return out;
}

/*****************************************************************************/
//template <typename T>
CDataFile& operator<< (CDataFile& out, const std::_Setfill<char> & manip)
{
  *(out.OutputStream) << manip;
  return out;
}

/*****************************************************************************/
CDataFile& operator<< (CDataFile& out, const std::_Setbase & manip)
{
  *(out.OutputStream) << manip;
  return out;
}

/*****************************************************************************/
CDataFile & operator << (CDataFile & out, const std::_Setiosflags & manip)
{
  *(out.OutputStream) << manip;
  return out;
}

/*****************************************************************************/
CDataFile & operator << (CDataFile & out, const std::_Resetiosflags & manip)
{
  *(out.OutputStream) << manip;
  return out;
}



/*****************************************************************************/
CDataFile & operator << (CDataFile & out, const string & data)
{
  //out.VerifyChunkSize ();
  *(out.OutputStream) << data;
  return out;
}

/*****************************************************************************/
CDataFile & operator << (CDataFile & out, const char * data)
{
  //out.VerifyChunkSize ();
  *(out.OutputStream) << data;
  return out;
}

/*****************************************************************************/
CDataFile & operator << (CDataFile & out, const bool & data)
{
  //out.VerifyChunkSize ();
  *(out.OutputStream) << data;
  return out;
}

/*****************************************************************************/
CDataFile & operator << (CDataFile & out, const char & data)
{
  //out.VerifyChunkSize ();
  *(out.OutputStream) << data;
  return out;
}

/*****************************************************************************/
CDataFile & operator << (CDataFile & out, const unsigned char & data)
{
  //out.VerifyChunkSize ();
  *(out.OutputStream) << data;
  return out;
}

/*****************************************************************************/
CDataFile & operator << (CDataFile & out, const signed char & data)
{
  //out.VerifyChunkSize ();
  *(out.OutputStream) << data;
  return out;
}

/*****************************************************************************/
CDataFile & operator << (CDataFile & out, const int & data)
{
  //out.VerifyChunkSize ();
  *(out.OutputStream) << data;
  return out;
}

/*****************************************************************************/
CDataFile & operator << (CDataFile & out, const unsigned int & data)
{
  //out.VerifyChunkSize ();
  *(out.OutputStream) << data;
  return out;
}


/*****************************************************************************/
CDataFile & operator << (CDataFile & out, const short int & data)
{
  //out.VerifyChunkSize ();
  *(out.OutputStream) << data;
  return out;
}

/*****************************************************************************/
CDataFile & operator << (CDataFile & out, const unsigned short int & data)
{
  //out.VerifyChunkSize ();
  *(out.OutputStream) << data;
  return out;
}

/*****************************************************************************/
CDataFile & operator << (CDataFile & out, const long int & data)
{
  //out.VerifyChunkSize ();
  *(out.OutputStream) << data;
  return out;
}


/*****************************************************************************/
CDataFile & operator << (CDataFile & out, const unsigned long int & data)
{
  //out.VerifyChunkSize ();
  *(out.OutputStream) << data;
  return out;
}

/*****************************************************************************/
CDataFile & operator << (CDataFile & out, const float & data)
{
  //out.VerifyChunkSize ();
  *(out.OutputStream) << data;
  return out;
}

/*****************************************************************************/
CDataFile & operator << (CDataFile & out, const double & data)
{
  //out.VerifyChunkSize ();
  *(out.OutputStream) << data;
  return out;
}

/*****************************************************************************/
CDataFile & operator << (CDataFile & out, const long double & data)
{
  //out.VerifyChunkSize ();
  *(out.OutputStream) << data;
  return out;
}
