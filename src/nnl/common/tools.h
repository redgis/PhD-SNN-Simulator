/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
 
#ifndef _TOOLS_H
#define _TOOLS_H

#include "../simulator/globals.h"


/* Initialises tools */
extern void InitTools ();

//~ /* Exponantial function */
//~ extern NNLpotential ExpPSP (int);
//~ extern NNLpotential ExpRefract (int);

//~ /* Tabbed exponential */
//~ extern NNLpotential * ExpTabRefract;

/* Number of value in the tabbed exponential */
extern int NbValPSP;
extern int NbValRefract;

/* Evaluation of remaining time for a job */
extern void TimeRemaining (float PercentDone, long int Elapsed, long int *, long int *, long int *);

/* Mexican hat function */
extern double Wavelet (double x, double maxima = 1, double ParaboleWidth = 1, double GaussianHatWidth = 2);

/* Normal law Gaussian hat function */
extern double Gaussian (double x, double sigma = 3, double center = 0);

/* Normal law Gaussian hat function */
extern double GaussianProba (double x, double sigma, double center);

/* Gaussian hat function with specified amplitude */
extern double GaussianMaxima (double x, double amplitude = 1, double sigma = 3, double center = 0);

/* Inverse log function with specified amplitude / range */
double InverseLogDistrib (double x, double Maxima = 1, double BorneMax = 10);


#endif /* _TOOLS_H */
