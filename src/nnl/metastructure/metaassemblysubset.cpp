/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "metaassemblysubset.h"
#include "assembly.h"

/****************************************************************************/
CMetaAssemblySubSet::CMetaAssemblySubSet(const char * Name, lua_State * pLua)
{
  if (pLua != NULL)
    LoadFromLua (Name, pLua);
}

/****************************************************************************/
CMetaAssemblySubSet::~CMetaAssemblySubSet()
{

}

/****************************************************************************/
void CMetaAssemblySubSet::LoadFromLua (const char * Name, lua_State * pLua)
{
  m_NbNeuron = lua_getIntTableElement (pLua, Name, 1);
  char * paramName = (char *) lua_getStringTableElement (pLua, Name, 2);

  m_nparam.Threshold = lua_getDoubleTableElement (pLua, paramName, "Threshold");
  m_nparam.MembranePotential = lua_getDoubleTableElement (pLua, paramName, "MembranePotential");
  m_nparam.AbsoluteRefractoryPotential = lua_getDoubleTableElement (pLua, paramName, "AbsoluteRefractoryPotential");
  m_nparam.AbsoluteRefractoryPeriode = lua_getDoubleTableElement (pLua, paramName, "AbsoluteRefractoryPeriode");
  m_nparam.Inhibitory = lua_getIntTableElement (pLua, paramName, "Inhibitory");
  m_nparam.TauPSP = lua_getDoubleTableElement (pLua, paramName, "TauPSP");
  m_nparam.TauRefract = lua_getDoubleTableElement (pLua, paramName, "TauRefract");
}


/****************************************************************************/
void CMetaAssemblySubSet::addNeurons (CAssembly * pNewAssembly)
{
  pNewAssembly->AddXNeurons(&m_nparam, m_NbNeuron);
}
