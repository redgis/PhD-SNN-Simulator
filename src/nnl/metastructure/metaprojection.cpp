/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "metaprojection.h"
#include "metaarchitecture.h"
#include "architecture.h"
#include "assembly.h"

/****************************************************************************/
CMetaProjection::CMetaProjection(CMetaArchitecture * myArchi, const char * Name, lua_State * pLua)
{
  m_MetaArchitecture = myArchi;

  m_Ass1 = m_Ass2 = NULL;
  m_id1 = -1;
  m_id2 = -1;
  m_size1 = -1;
  m_size2 = -1;

  m_k = m_p = -1;

  // Additional for Sale-free nets
  m_kin = -1;
  m_kout = -1;
  m_NbInitialNeurons = -1;
  m_alpha = -1;
  m_beta = -1;
  m_gamma = -1;

  if (pLua != NULL)
    LoadFromLua (Name, pLua);

}

/****************************************************************************/
CMetaProjection::~CMetaProjection()
{

}

/****************************************************************************/
void CMetaProjection::LoadFromLua (const char * Name, lua_State * pLua)
{
  m_ProjectionType = (projection_t) lua_getIntTableElement (pLua, Name, 1);

  switch (m_ProjectionType)
  {
    case ASS_TO_ASS:
      m_Ass1 = m_MetaArchitecture->getMetaAssemblyByName (lua_getStringTableElement(pLua, Name, 2));
      m_Ass2 = m_MetaArchitecture->getMetaAssemblyByName (lua_getStringTableElement(pLua, Name, 3));
      loadProjectionParams (pLua, lua_getStringTableElement (pLua, Name, 4), &m_pparam);
      break;

    case NEU_TO_ASS:
      m_Ass1 = m_MetaArchitecture->getMetaAssemblyByName (lua_getStringTableElement(pLua, Name, 2));
      m_id1 = lua_getIntTableElement( pLua, Name, 3);
      m_Ass2 = m_MetaArchitecture->getMetaAssemblyByName (lua_getStringTableElement(pLua, Name, 4));
      loadProjectionParams (pLua, lua_getStringTableElement (pLua, Name, 5), &m_pparam);
      break;

    case ASS_TO_NEU:
      m_Ass1 = m_MetaArchitecture->getMetaAssemblyByName (lua_getStringTableElement(pLua, Name, 2));
      m_Ass2 = m_MetaArchitecture->getMetaAssemblyByName (lua_getStringTableElement(pLua, Name, 3));
      m_id2 = lua_getIntTableElement( pLua, Name, 4);
      loadProjectionParams (pLua, lua_getStringTableElement (pLua, Name, 5), &m_pparam);
      break;

    case NEU_TO_NEU:
      m_Ass1 = m_MetaArchitecture->getMetaAssemblyByName (lua_getStringTableElement(pLua, Name, 2));
      m_id1 = lua_getIntTableElement( pLua, Name, 3);
      m_Ass2 = m_MetaArchitecture->getMetaAssemblyByName (lua_getStringTableElement(pLua, Name, 4));
      m_id2 = lua_getIntTableElement( pLua, Name, 5);
      loadProjectionParams (pLua, lua_getStringTableElement (pLua, Name, 6), &m_pparam);
      break;

    case SUBASS_TO_SUBASS:
      m_Ass1 = m_MetaArchitecture->getMetaAssemblyByName (lua_getStringTableElement(pLua, Name, 2));
      m_id1 = lua_getIntTableElement(pLua, Name, 3);
      m_size1 = lua_getIntTableElement(pLua, Name, 4);
      m_Ass2 = m_MetaArchitecture->getMetaAssemblyByName (lua_getStringTableElement(pLua, Name, 5));
      m_id2 = lua_getIntTableElement( pLua, Name, 6);
      m_size2 = lua_getIntTableElement(pLua, Name, 7);
      loadProjectionParams (pLua, lua_getStringTableElement (pLua, Name, 8), &m_pparam);
      break;

    case ONE_TO_ONE:
      m_Ass1 = m_MetaArchitecture->getMetaAssemblyByName (lua_getStringTableElement(pLua, Name, 2));
      m_Ass2 = m_MetaArchitecture->getMetaAssemblyByName (lua_getStringTableElement(pLua, Name, 3));
      loadProjectionParams (pLua, lua_getStringTableElement (pLua, Name, 4), &m_pparam);
      break;

    case REGULAR:
      m_Ass1 = m_MetaArchitecture->getMetaAssemblyByName (lua_getStringTableElement(pLua, Name, 2));
      m_k = lua_getIntTableElement (pLua, Name, 3);
      loadProjectionParams (pLua, lua_getStringTableElement (pLua, Name, 4), &m_pparam);
      break;

    case REGULAR_AND_REWIRE:
      m_Ass1 = m_MetaArchitecture->getMetaAssemblyByName (lua_getStringTableElement(pLua, Name, 2));
      m_k = lua_getIntTableElement (pLua, Name, 3);
      m_p = lua_getDoubleTableElement (pLua, Name, 4);
      loadProjectionParams (pLua, lua_getStringTableElement (pLua, Name, 5), &m_pparam);
      break;

    case SCALE_FREE:
      m_Ass1 = m_MetaArchitecture->getMetaAssemblyByName (lua_getStringTableElement(pLua, Name, 2));
      loadProjectionParams (pLua, lua_getStringTableElement (pLua, Name, 3), &m_pparamInitial);
      m_NbInitialNeurons = lua_getIntTableElement (pLua, Name, 4);
      loadProjectionParams (pLua, lua_getStringTableElement (pLua, Name, 5), &m_pparam);
      m_k = lua_getIntTableElement (pLua, Name, 6);
      m_kin = lua_getIntTableElement (pLua, Name, 7);
      m_kout = lua_getIntTableElement (pLua, Name, 8);
      m_alpha = lua_getDoubleTableElement (pLua, Name, 9);
      m_beta = lua_getDoubleTableElement (pLua, Name, 10);
      m_gamma = lua_getDoubleTableElement (pLua, Name, 11);
      break;
  }
}

/****************************************************************************/
void CMetaProjection::loadProjectionParams (lua_State * pLua, const char * Name, ProjectionParameters * pparams)
{
  pparams->ProjectionRate = lua_getDoubleTableElement (pLua, Name, "ProjectionRate");
  pparams->SynParam.Delay = lua_getDoubleTableElement (pLua, Name, "Delay");
  pparams->SynParam.DelayAdaptation = lua_getBooleanTableElement (pLua, Name, "DelayAdaptation");
  pparams->SynParam.DeltaDelay = lua_getDoubleTableElement (pLua, Name, "DeltaDelay");
  pparams->SynParam.DeltaWeight = lua_getDoubleTableElement (pLua, Name, "DeltaWeight");
  pparams->SynParam.Inhibitory = (NNLbool) lua_getIntTableElement (pLua, Name, "Inhibitory");
  pparams->SynParam.Weight = lua_getDoubleTableElement (pLua, Name, "Weight");
  pparams->SynParam.WeightAdaptation = lua_getBooleanTableElement (pLua, Name, "WeightAdaptation");

//  cout << endl;
//  cout << "  Loading projection \"" << Name << "\"" << endl;
//  cout << "    Projection Rate : " << pparams->ProjectionRate << endl;
//  cout << "    Delays          : " << pparams->SynParam.Delay << endl;
//  cout << "    Delay adapt.    : " << pparams->SynParam.DelayAdaptation << endl;
//  cout << "    Sigma delays    : " << pparams->SynParam.DeltaDelay << endl;
//  cout << "    Sigma weight    : " << pparams->SynParam.DeltaWeight << endl;
//  cout << "    Inhibitory      : " << pparams->SynParam.Inhibitory << endl;
//  cout << "    Weight          : " << pparams->SynParam.Weight << endl;
//  cout << "    Weight adapt.   : " << pparams->SynParam.WeightAdaptation << endl;
}

/*
    int Connect (CAssembly *, CAssembly *, ProjectionParameters *);
    int Connect (CAssembly *, CNeuron *, ProjectionParameters *);
    int Connect (CNeuron *, CAssembly *, ProjectionParameters *);
    int Connect (CNeuron *, CNeuron *, ProjectionParameters *);
    int ConnectSubAssemblies (CAssembly * Assembly1, int From1, int Nb1, CAssembly * Assembly2, int From2, int Nb2, ProjectionParameters * param);
    int ConnectOneToOne (CAssembly *, CAssembly *, ProjectionParameters *);
    void GaussianMapping (CAssembly *, CAssembly *, ProjectionParameters *);
    void LogarithmicMapping (CAssembly *, CAssembly *, ProjectionParameters *);

    void ConnectRegular (CAssembly *, int kNeighbour, ProjectionParameters * param);
    void ConnectRegularAndRewire (CAssembly *, int kNeighbour, double p, ProjectionParameters * param);
    void Rewire (CAssembly *, double p, ProjectionParameters * param);
    void ConnectScaleFree (CAssembly * Ass, ProjectionParameters * InitialRandNetParam, int InitialNumberNeurons, ProjectionParameters * param, int k, int kin, int kout, double alpha, double beta, double gamma);
 */

/****************************************************************************/
void CMetaProjection::BuildProjections (CArchitecture * pArchitecture)
{

  switch (m_ProjectionType)
  {
    case ASS_TO_ASS:
      pArchitecture->Connect ( (*pArchitecture)[m_Ass1->m_Name], (*pArchitecture)[m_Ass2->m_Name], &m_pparam);
      break;

    case NEU_TO_ASS:
      pArchitecture->Connect ( (*((*pArchitecture)[m_Ass1->m_Name]))[m_id1], (*pArchitecture)[m_Ass2->m_Name], &m_pparam);
      break;

    case ASS_TO_NEU:
      pArchitecture->Connect ( (*pArchitecture)[m_Ass1->m_Name], (*((*pArchitecture)[m_Ass2->m_Name]))[m_id2], &m_pparam);
      break;

    case NEU_TO_NEU:
      pArchitecture->Connect ( (*((*pArchitecture)[m_Ass1->m_Name]))[m_id1], (*((*pArchitecture)[m_Ass2->m_Name]))[m_id2], &m_pparam);
      break;

    case SUBASS_TO_SUBASS:
      pArchitecture->ConnectSubAssemblies ( (*pArchitecture)[m_Ass1->m_Name], m_id1, m_size1, (*pArchitecture)[m_Ass2->m_Name], m_id2, m_size2, &m_pparam);
      break;

    case ONE_TO_ONE:
      pArchitecture->ConnectOneToOne ( (*pArchitecture)[m_Ass1->m_Name], (*pArchitecture)[m_Ass2->m_Name], &m_pparam);
      break;

    case REGULAR:
      pArchitecture->ConnectRegular ( (*pArchitecture)[m_Ass1->m_Name], m_k, &m_pparam);
      break;

    case REGULAR_AND_REWIRE:
      pArchitecture->ConnectRegularAndRewire ( (*pArchitecture)[m_Ass1->m_Name], m_k, m_p, &m_pparam);
      break;

    case SCALE_FREE:
      pArchitecture->ConnectScaleFree ( (*pArchitecture)[m_Ass1->m_Name], &m_pparamInitial, m_NbInitialNeurons, &m_pparam, m_k, m_kin, m_kout, m_alpha, m_beta, m_gamma);
      break;
  }

}
