/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "metaarchitecture.h"
#include "architecture.h"
#include <list>
#include <iostream>
#include <stdlib.h>


using namespace std;

/****************************************************************************/
CMetaArchitecture::CMetaArchitecture (char * LuaFilename)
{
  if (LuaFilename != NULL)
    LoadFromLua (LuaFilename);
}

/****************************************************************************/
CMetaArchitecture::~CMetaArchitecture ()
{

}

/****************************************************************************/
CMetaAssembly * CMetaArchitecture::getMetaAssemblyByName (const char * Name)
{
  list<CMetaAssembly *>::iterator itAssembly = m_MetaAssemblies.begin();
  list<CMetaAssembly *>::iterator itLastAssembly = m_MetaAssemblies.end();

  while (itAssembly != itLastAssembly)
  {
    if ((*itAssembly)->m_Name == Name)
      break;
    itAssembly++;
  }

  return (*itAssembly);
}

/****************************************************************************/
void CMetaArchitecture::LoadFromLua (char * LuaFilename)
{
  cout << "Loading network \"" << LuaFilename << "\" ... " << endl << flush;

  //create a lua state
  lua_State* pLua = lua_open();

  //open the lua libaries - new in lua5.1
  luaL_openlibs(pLua);

  //open luabind
  //open(pLua);

  int hasError;

  //load and run the script
  hasError = luaL_dofile(pLua, LuaFilename);

  if (hasError)
  {
    cerr << "-- " << lua_tostring(pLua, -1) << endl;
    luaL_error (pLua, "An error occured while loading %s", LuaFilename);
    exit (hasError);
  }
  //reset the stack index
  lua_settop(pLua, 0);

  //Load assemblies
  CMetaAssembly * metaAssembly;
  cout << "  assemblies: " << flush;
  for (int Index = 1; Index <= lua_getTableLength (pLua, "Assemblies"); Index ++)
  {
    metaAssembly = new CMetaAssembly (lua_getStringTableElement (pLua, "Assemblies", Index), pLua);
    cout << metaAssembly->m_Name << "... " << flush;
    m_MetaAssemblies.push_back (metaAssembly);
  }
  cout << "done." << flush;

  //Load projections
  CMetaProjection * metaProjection;
  cout << endl << "  projections: " << flush;
  for (int Index = 1; Index <= lua_getTableLength (pLua, "Projections"); Index ++)
  {
    metaProjection = new CMetaProjection (this, lua_getStringTableElement (pLua, "Projections", Index), pLua);
    cout << lua_getStringTableElement (pLua, "Projections", Index) << "... " << flush;
    m_MetaProjections.push_back (metaProjection);
  }
  cout << "done." << endl << flush;


  //tidy up
  lua_close(pLua);

  //cout << "done." << endl << flush;
}

/****************************************************************************/
CArchitecture * CMetaArchitecture::BuildArchitecture (CArchitecture * pArchitecture)
{
  cout << "Building network from meta-architecture ... " << endl << flush;

  if(pArchitecture == NULL)
    pArchitecture = new CArchitecture ();

  // Create Assemblies
  cout << "  assemblies: " << flush;
  list<CMetaAssembly *>::iterator itMetaAssembly = m_MetaAssemblies.begin();
  list<CMetaAssembly *>::iterator itLastMetaAssembly = m_MetaAssemblies.end();
  while (itMetaAssembly != itLastMetaAssembly)
  {
    pArchitecture->AddAssembly((*itMetaAssembly)->BuildAssembly ());
    cout << (*itMetaAssembly)->m_Name << "... "<< flush;
    itMetaAssembly++;
  }
  cout << "done." << endl << flush;

  // Create projections
  cout << "  projections: " << flush;
  list<CMetaProjection *>::iterator itMetaProjection = m_MetaProjections.begin();
  list<CMetaProjection *>::iterator itLastMetaProjection = m_MetaProjections.end();
  while (itMetaProjection != itLastMetaProjection)
  {
    (*itMetaProjection)->BuildProjections (pArchitecture);
    cout << (*itMetaProjection)->m_Ass1->m_Name;
    if ((*itMetaProjection)->m_Ass2)
      cout << "->" << (*itMetaProjection)->m_Ass2->m_Name;
    cout << " ... "<< flush;
    itMetaProjection++;
  }
  cout << "done." << endl << flush;

  return pArchitecture;
}
