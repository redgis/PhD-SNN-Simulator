/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef METAPROJECTION_H_
#define METAPROJECTION_H_


#include "../lua/luahelpers.h"
#include "../simulator/globals.h"
#include "metaassembly.h"

class CMetaArchitecture;

typedef enum { ASS_TO_ASS, NEU_TO_ASS, ASS_TO_NEU, NEU_TO_NEU, SUBASS_TO_SUBASS, ONE_TO_ONE, REGULAR, REGULAR_AND_REWIRE, SCALE_FREE } projection_t;

class CMetaProjection
{
  public:

    CMetaArchitecture * m_MetaArchitecture;

    projection_t m_ProjectionType;
    CMetaAssembly * m_Ass1;
    CMetaAssembly * m_Ass2;
    int m_id1;
    int m_id2;
    int m_size1;
    int m_size2;

    ProjectionParameters m_pparam;

    int m_k;
    float m_p;

    // Additional for Sale-free nets
    int m_kin;
    int m_kout;
    ProjectionParameters m_pparamInitial;
    int m_NbInitialNeurons;
    float m_alpha;
    float m_beta;
    float m_gamma;

    CMetaProjection (CMetaArchitecture * myArchi, const char * Name = "", lua_State * pLua = NULL);
    virtual ~CMetaProjection();

    void LoadFromLua (const char * Name, lua_State * pLua);
    void loadProjectionParams (lua_State * pLua, const char * Name, ProjectionParameters * pparams);
    void BuildProjections (CArchitecture * pArchitecture);
};

#endif /* METAPROJECTION_H_ */
