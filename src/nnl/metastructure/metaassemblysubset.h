/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef METAASSEMBLYSUBSET_H_
#define METAASSEMBLYSUBSET_H_

#include "../lua/luahelpers.h"
#include "../simulator/globals.h"

class CMetaAssemblySubSet
{
  public:
    int m_NbNeuron;
    NeuronParameters m_nparam;

    CMetaAssemblySubSet(const char * Name, lua_State * pLua);
    virtual ~CMetaAssemblySubSet();

    void LoadFromLua (const char * Name, lua_State * pLua);
    void addNeurons (CAssembly * pNewAssembly);
};

#endif /* METAASSEMBLYSUBSET_H_ */
