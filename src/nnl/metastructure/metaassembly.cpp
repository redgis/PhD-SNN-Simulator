/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "metaassembly.h"
#include "assembly.h"

/****************************************************************************/
CMetaAssembly::CMetaAssembly (const char * newName, lua_State * pLua) : m_Name(newName)
{
  if (pLua != NULL)
    LoadFromLua (newName, pLua);
}

/****************************************************************************/
CMetaAssembly::~CMetaAssembly ()
{

}

/****************************************************************************/
void CMetaAssembly::LoadFromLua (const char * thisName, lua_State * pLua)
{
  CMetaAssemblySubSet * metaAssemblySubSet;

  for (int Index = 1; Index <= lua_getTableLength (pLua, thisName); Index ++)
  {
    metaAssemblySubSet = new CMetaAssemblySubSet (lua_getStringTableElement (pLua, thisName, Index), pLua);
    push_back (metaAssemblySubSet);
  }

}

/****************************************************************************/
CAssembly * CMetaAssembly::BuildAssembly ()
{
  CAssembly * pNewAssembly = new CAssembly(m_Name);

  list<CMetaAssemblySubSet *>::iterator itSubSet = begin ();
  list<CMetaAssemblySubSet *>::iterator itLastSubSet = end ();

  while (itSubSet != itLastSubSet)
  {
    (*itSubSet)->addNeurons (pNewAssembly);
    itSubSet ++;
  }

  return pNewAssembly;
}
