/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

 #ifndef _META_ARCHITECTURE_
 #define _META_ARCHITECTURE_

#include "metaassembly.h"
#include "metaprojection.h"
#include <list>

class CMetaArchitecture
{
  public:
    list<CMetaAssembly *> m_MetaAssemblies;
    list<CMetaProjection *> m_MetaProjections;

    CMetaArchitecture (char * LuaFilename = NULL);
    virtual ~CMetaArchitecture ();

    CMetaAssembly * getMetaAssemblyByName (const char *);
    void LoadFromLua (char * LuaFilename);
    CArchitecture * BuildArchitecture (CArchitecture * pArchitecture);
};



 #endif //_META_ARCHITECTURE_
