/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
 
#include "globals.h"

#include "eventmanager.h"
#include "datafile.h"
#include "parameters.h"
#include <iostream>

using namespace std;


unsigned int RandSeed;// = (unsigned int) RandSeed; //(unsigned int) 1235929323; // (unsigned int) time(NULL);//54321;//(unsigned int) time(NULL); //1153245669

sem_t semThread;
sem_t semMain;

int NbPresentations = 0;
int nbEvents = 0;

//int testpres = 0;

CPatternManager * PatternManagerGlobalPointer = NULL;
CArchitecture * ArchitectureGlobalPointer = NULL;
CManager * ManagerGlobalPointer = NULL;

bool UnsupervisedLearning = true;

NNLtime CurrentTime = (NNLtime) 0;

CEventManager EventQueue;
//CGraphManager GraphManager;

int SaturationSup = 0;
int SaturationInf = 0;

int CurrentClass = 0;
NNLtime CurrentClassTime = 0;


double WeightPositiveModificationAmount = 0;
long int WeightPositiveModificationCount = 0;
double WeightNegativeModificationAmount = 0;
long int WeightNegativeModificationCount = 0;

CDataFile * SpikePSPs = NULL;
CDataFile * SpikeRasterExcit = NULL;
CDataFile * SpikeRasterInhib = NULL;


CDataFile * Synapses = NULL;
CDataFile * DelayNeuron1 = NULL;
CDataFile * DelayNeuron2 = NULL;
CDataFile * PatternClasses = NULL;
CDataFile * PredictedClasses = NULL;

void * libeventfunctions = NULL;


void InitializeGlobals() {
   EventQueue = CEventManager();
   //GraphManager = CGraphManager ();

   SpikePSPs = new CDataFile((CParameters::DATA_OUTPUT_DIR + "SpikePSPs" + "." + CParameters::DATA_OUTPUT_EXT).c_str(), ios::out | ios::binary | ios::trunc);
   SpikeRasterExcit = new CDataFile((CParameters::DATA_OUTPUT_DIR + "SpikeRasterExcit" + "." + CParameters::DATA_OUTPUT_EXT).c_str(), ios::out | ios::binary | ios::trunc, "Excitatory spikes", 10485760/*5242880/*10485760/*52428800*/);
   SpikeRasterInhib = new CDataFile((CParameters::DATA_OUTPUT_DIR + "SpikeRasterInhib" + "." + CParameters::DATA_OUTPUT_EXT).c_str(), ios::out | ios::binary | ios::trunc, "Inhibitory spikes", 10485760/*5242880/*10485760/*52428800*/);


   Synapses = new CDataFile((CParameters::DATA_OUTPUT_DIR + "SynapsesLearning." + CParameters::DATA_OUTPUT_EXT).c_str(), ios::out | ios::trunc);
   DelayNeuron1 = new CDataFile((CParameters::DATA_OUTPUT_DIR + "DelayLearning1." + CParameters::DATA_OUTPUT_EXT).c_str(), ios::out | ios::trunc);
   DelayNeuron2 = new CDataFile((CParameters::DATA_OUTPUT_DIR + "DelayLearning2." + CParameters::DATA_OUTPUT_EXT).c_str(), ios::out | ios::trunc);
   PatternClasses = new CDataFile((CParameters::DATA_OUTPUT_DIR + "PatternClasses." + CParameters::DATA_OUTPUT_EXT).c_str(), ios::out | ios::trunc);
   PredictedClasses = new CDataFile((CParameters::DATA_OUTPUT_DIR + "PredictedClasses." + CParameters::DATA_OUTPUT_EXT).c_str(), ios::out | ios::trunc);

   libeventfunctions = dlopen("libeventfunctions.so", RTLD_NOW | RTLD_LOCAL);
}