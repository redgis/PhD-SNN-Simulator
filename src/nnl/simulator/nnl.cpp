/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
 
/* Testing program */

#include <iostream>
#include "manager.h"
#include "tools.h"
#include "eventfunctions.h"
#include "metaarchitecture.h"
#include "simulation.h"
#include "architecture.h"
#include "parameters.h"
#include "patternmanager.h"
#include <sys/time.h>

using namespace std;

int main (int argc, char * argv[])
{ 
	cout << "NNL - Neuron Network Library - Copyright (C) 2017 regis.martinez3@gmail.com" << endl;
	cout << "This program comes with ABSOLUTELY NO WARRANTY; This is free software, and you" << endl;
	cout << "are welcome to redistribute it under certain conditions; for details see the" << endl;
	cout << "GPL-3 file License.md provided with the source code." << endl;
	cout << endl;
	 
   InitializeGlobals();

  // Check for event functions library
  if (libeventfunctions == NULL)
  {
    cout << dlerror () << endl;
    exit (1);
  }
  registerFunctions ();

  struct timeval tv;
  gettimeofday(&tv, NULL);
  RandSeed = tv.tv_sec * tv.tv_usec;
  //cout << RandSeed << " " << tv.tv_sec << " " << tv.tv_usec << endl;

  // Check Arguments
  if (argc != 9)
  {
    cerr << "Usage : nnl  NETWORK  SETTINGS  PATTERNS  SIMULATION " << endl;
    cerr << "  SETTINGS :" << endl;
    cerr << "  --settings <settings.lua>                    Settings" << endl;
    cerr << endl;
    cerr << "  PATTERNS :" << endl;
    cerr << "  --patterns <patterns.lua>                    Patterns" << endl;
    cerr << endl;
    cerr << "  SIMULATION :" << endl;
    cerr << "  --simulation <simulation.lua>                Simulation protocol" << endl;
    cerr << endl;
    cerr << "  NETWORK :" << endl;
    cerr << "  --build <builder.lua>                        Network building script" << endl;
    cerr << "| --metaarchitecture <metaarchitecture.lua>    Network building via meta structure description" << endl;
    cerr << "| --architecture <architecture.xml>            Prebuilt network" << endl << endl;
    exit (1);
  }

  InitTools ();

  //Parse arguments
  CArchitecture * pArchitecture = NULL;
  CMetaArchitecture * pMetaArchitecture = NULL;
  CSimulation * pSimulation = NULL;
  CPatternManager * pPatternManager = NULL;
  int iSimuPos = 0;
  int iPatternPos = 0;
  int iParamPos = 0;
  bool isBuilding = false;
  bool hasSettings = false;
  bool hasSimulation = false;
  bool hasPatterns = false;
  int argIndex = 1;
  while (argIndex < argc)
  {
  // Garder le chargement du réseau, et les parametres MAIS la simulation et les patterns viennent après.

    if ((strcmp(argv[argIndex], "--settings") == 0) && (!hasSettings))
    {
      argIndex++;
      iParamPos = argIndex++;
      hasSettings = true;
    }
    else if ((strcmp(argv[argIndex], "--simulation") == 0) && (!hasSimulation))
    {
      argIndex++;
      pSimulation = new CSimulation ();
      iSimuPos = argIndex++;
      hasSimulation = true;
    }
    else if ((strcmp(argv[argIndex], "--patterns") == 0) && (!hasPatterns))
    {
      argIndex++;
      iPatternPos = argIndex++;
      hasPatterns = true;
    }
    else if ((strcmp(argv[argIndex], "--build") == 0) && (!isBuilding)) //////////////// NEXT CONDITIONS : BUILD THE ARCHITECTURE
    {
      argIndex++;
      pArchitecture = new CArchitecture ();
      pArchitecture->BuildFromLua(argv[argIndex++]);
      isBuilding = true;
    }
    else if ((strcmp(argv[argIndex], "--metaarchitecture") == 0) && (!isBuilding))
    {
      argIndex++;
      pMetaArchitecture = new CMetaArchitecture ();
      pMetaArchitecture->LoadFromLua(argv[argIndex++]);
      pArchitecture = new CArchitecture ();
      pArchitecture->BuildFromMetaLua(*pMetaArchitecture);
      isBuilding = true;
    }
    else if ((strcmp(argv[argIndex], "--architecture") == 0) && (!isBuilding))
    {
      argIndex++;
      pArchitecture = new CArchitecture ();
      pArchitecture->XMLload(string(argv[argIndex++]));
      isBuilding = true;
    }
    else
    {
      cerr << "unexpected or missing argument " << argIndex << endl << endl;
      cerr << "Usage : nnl  NETWORK  SETTINGS  PATTERNS  SIMULATION " << endl;
      cerr << "  SETTINGS :" << endl;
      cerr << "  --settings <settings.lua>                    Settings" << endl;
      cerr << "                           " << endl;
      cerr << "  PATTERNS :" << endl;
      cerr << "  --patterns <patterns.lua>                    Patterns" << endl;
      cerr << "                           " << endl;
      cerr << "  SIMULATION :" << endl;
      cerr << "  --simulation <simulation.lua>                Simulation protocol" << endl;
      cerr << "                           " << endl;
      cerr << "  NETWORK :" << endl;
      cerr << "  --build <builder.lua>                        Network building script" << endl;
      cerr << "| --metaarchitecture <metaarchitecture.lua>    Network building via meta structure description" << endl;
      cerr << "| --architecture <architecture.xml>            Prebuilt network" << endl << endl;
      exit (1);
    }

    //argIndex++;
  }
  
  // Load specified patterns
  if (iPatternPos)
    pPatternManager = new CPatternManager (argv[iPatternPos]);

  // Load specified parameters
  if (iParamPos)
    CParameters::loadFromLuaFile(string(argv[iParamPos]));

  // Load simulation
  if (pSimulation && iSimuPos)
    pSimulation->LoadFromLua(argv[iSimuPos]);

  CManager * manager = new CManager (pArchitecture, pSimulation, pPatternManager);
  ManagerGlobalPointer = manager;
  
  manager->Run ();


  dlclose (libeventfunctions);

  return 0;
}
