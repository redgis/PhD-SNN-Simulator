/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
 
#ifndef _GLOBALS_H
#define _GLOBALS_H


/********************** Headers includes ******************/

using namespace std;

// ANSI C++
#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
#include <sstream>
#include <exception>
#include <list>

// STL C++
#include <vector>
#include <map>
#include <algorithm>
#include <queue>

// ANSI C
#include <stdlib.h>
#include <math.h>

// UNIX Includes
#include <unistd.h>
#include <semaphore.h>
#include <pthread.h>
#include <dlfcn.h>

// LUA Includes
extern "C"
{
  #include <lua.h>
  #include <lualib.h>
  #include <lauxlib.h>
}
#include "../lua/luahelpers.h"

// Other headers
#include "../tinyxml/tinyxml.h"
#include "../common/tools.h"


#define __ANR_MAPS 0
#define __USPS_MULTI 0
#define __BI_CLASS 0
#define __TOY_NET 0
#define __DYNAMIC_STUDY 0
#define __RESERVOIR_COMPUTING 0
#define __IZHIKEVICH_NETWORK 0
#define __MAIER_MILLER_NETWORK 0
#define __ICANN_RANDOM_NETWORK 0
#define __ICANN_NETWORK 0
#define __TOPO_STUDY 0

class CNeuron;
class CSynapse;
class CAssembly;
class CArchitecture;
class CEventManager;
class CPatternManager;
class CPatternSet;
class CPattern;
class CDataFile;
class CManager;
class CParameters;
class CThread;
class CSimulation;
class CSimulationPhase;
class NNLEvent;
class CMetaArchitectures;
class CMetaAssembly;
class CMetaAssemblySubSet;
class CMetaProjection;


/************** type definitions **************/

/* usual types */  
typedef long int NNLtime;
typedef double NNLweight;
typedef double NNLpotential;
typedef int NNLeventMask;
  
typedef enum { FALSE, TRUE, UNSPECIFIED } NNLbool;


/* Parameters of a neuron */
typedef struct sNeuronParameters
{
  NNLpotential Threshold;           /* Neuron threshold */
  NNLpotential MembranePotential;   /* Neuron potential */
  NNLpotential AbsoluteRefractoryPotential; /* Neuron absolute refractory potential */
  NNLtime AbsoluteRefractoryPeriode;        /* Neuron absolute refractory periode */
  float Inhibitory;                 /* Probability for the neuron to be inhibitory */
  NNLtime TauPSP;                   /* Time constante for a PSP */
  NNLtime TauRefract;
} NeuronParameters;


/* Parameters of a synapse */
typedef struct sSynapseParameters
{
  NNLweight Weight;       /* Synaptic weight */
  NNLtime Delay;          /* Axonal delay before the synapse. -1 = random */
  NNLtime DeltaDelay;         /* Variations around Delay if not -1 */
  NNLweight DeltaWeight;      /* Variations around Weight if not -1 */
  bool WeightAdaptation;  /* Do we make weight adaptation ? */
  bool DelayAdaptation;   /* Do we make delay adaptation ? */
  NNLbool Inhibitory;     /* The synapse will be inhibitory ? */
} SynapseParameters;


/* Parameters of a projection */
typedef struct sProjectionParameters
{
  float ProjectionRate;       /* probability of connexion - between 0 and 1, -1 = randomized */
  SynapseParameters SynParam; /* parameters of synapses of the projection */
} ProjectionParameters;


/* Pattern presentation method, used by the pattern manager */
typedef enum { NNL_RAND, NNL_AS_IS } NNLPresentationMethod;

typedef enum { CLASSICAL_STDP, MEUNIER_STDP } NNLStdpType;



/****************** Data definition ********************/

extern unsigned int RandSeed;

//extern int testpres;

extern CPatternManager * PatternManagerGlobalPointer;
extern CArchitecture * ArchitectureGlobalPointer;
extern CManager * ManagerGlobalPointer;

extern bool UnsupervisedLearning;

extern NNLtime CurrentTime;

//extern CGraphManager GraphManager;
extern CEventManager EventQueue;

extern int SaturationSup;
extern int SaturationInf;

extern int CurrentClass;
extern NNLtime CurrentClassTime;

extern double WeightPositiveModificationAmount;
extern long int WeightPositiveModificationCount;
extern double WeightNegativeModificationAmount;
extern long int WeightNegativeModificationCount;

extern CDataFile * Synapses;
extern CDataFile * DelayNeuron1;
extern CDataFile * DelayNeuron2;
extern CDataFile * PatternClasses;
extern CDataFile * PredictedClasses;

extern CDataFile * SpikeRasterExcit;
extern CDataFile * SpikeRasterInhib;
extern CDataFile * SpikePSPs;

/* Semaphore to regulate thread activty (avoid while(1) loops using 100percent CPU */
extern sem_t semThread; //to say to graphmanager when there are new data to treat
extern sem_t semMain;   //to avoid conflictual writing/erasing in graphmanager.DataBlocks 

/* Count the number of times one stimulus has already been presented */
extern int NbPresentations;

extern int nbEvents;

extern NNLpotential * ExpTabIzhiSTDP;

extern void * libeventfunctions;

extern void InitializeGlobals();

#endif /* _GLOBALS_H */
