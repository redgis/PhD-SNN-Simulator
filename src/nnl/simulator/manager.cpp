/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
 
#include "manager.h"

#include "assembly.h"
#include "neuron.h"
#include "architecture.h"
#include "parameters.h"
#include "eventmanager.h"
#include "simulationphase.h"
#include "simulation.h"
#include "eventfunctions.h"

/*
Regular net : 
Real connectivity for each value of k, for each size of net N.

N=100
1		0.02020202020202
2	  0.04040404040404
4	  0.080808080808081
6	  0.12121212121212
8	  0.16161616161616
10  0.2020202020202
*/
/*
N=200
1	  0.010050251256281
2	  0.020100502512563
4	  0.040201005025126
6	  0.060301507537688
8	  0.080402010050251
10	0.10050251256281
*/
/*
N=500
1	  0.004008016032064
2	  0.008016032064128
4	  0.016032064128256
6	  0.024048096192385
8	  0.032064128256513
10	0.040080160320641
*/
/*
N=800
1	  0.002503128911139
2	  0.005006257822278
4	  0.010012515644556
6	  0.015018773466834
8	  0.020025031289111
10	0.025031289111389
*/
/*
N=1000
1	  0.002002002002002
2	  0.004004004004004
4	  0.008008008008008
6	  0.012012012012012
8	  0.016016016016016
10	0.02002002002002

*/

/* random * /
//{
#define NB_NEU 1000
#define PROG_RATE 1.0
#define NAME "n-0100-c-0_1-01"
//#define NAME "n-1000"
//} */

/* regular * /
//{
// for regular networks, the c in the name is the real connectivity, the connectivity induced by the parameters, not the proj rate above.
// ie : proj rate = 1.0, nb_neu = 100, k=10, c_real = 100*2*10 * 1.0
#define NB_NEU 1000
#define PROG_RATE 1.0
//1 2 4 6 8 10
#define K_REGUL 2
// 0.25 0.5 0.75 
#define P_REWIRE 0.0

//#define NAME "n-1000-c-0_02-k-10-10"
#define NAME "n-1000"
//} */

/* smallworld */
//{
#define PROG_RATE 1.0

#define NB_NEU 1000

//1 2 4 6 8 10 //
#define K_REGUL 2
// 0 0.25 0.5 0.75 1
#define P_REWIRE 0.05

#define NAME "n-1000-c-0_008-k-04-p-0_75-10"
//#define NAME "n-1000"
//} */



/* SF Params 

N = 100

INIT_SIZE = 5

k	kin	kout		c real
1	1	1	305	0.030808080808081 *
2	1	1	400	0.04040404040404 *
4	2	2	780	0.078787878787879 *
8	4	4	1540	0.15555555555556 *
10	5	5	1920	0.19393939393939 
16	8	8	3060	0.30909090909091 
24	12	12	4580	0.46262626262626 
32	16	16	6100	0.61616161616162 
48	24	24	9140	0.92323232323232 
52	26	26	9900	1 

*/
/*
N=200

INIT_SIZE = 10

k	kin	kout		c real
1	1	1	660	0.016582914572864
2	1	1	850	0.021356783919598
4	2	2	1610	0.040452261306533
8	4	4	3130	0.078643216080402
10	5	5	3890	0.097738693467337
16	8	8	6170	0.15502512562814
24	12	12	9210	0.23140703517588
32	16	16	12250	0.30778894472362
48	24	24	18330	0.4605527638191
52	26	26	19850	0.49874371859296

*/

/*
N=500

INIT_SIZE = 25

k	kin	kout		c real
1	1	1	2025	0.00811623246493
2	1	1	2500	0.01002004008016
4	2	2	4400	0.017635270541082
8	4	4	8200	0.032865731462926
10	5	5	10100	0.040480961923848
16	8	8	15800	0.063326653306613
24	12	12	23400	0.093787575150301
32	16	16	31000	0.12424849699399
48	24	24	46200	0.18517034068136
52	26	26	50000	0.20040080160321

*/

/*
N=800

INIT_SIZE = 40

k	kin	kout		c real
1	1	1	3840	0.006007509386733
2	1	1	4600	0.007196495619524
4	2	2	7640	0.011952440550688
8	4	4	13720	0.021464330413016
10	5	5	16760	0.02622027534418
16	8	8	25880	0.040488110137672
24	12	12	38040	0.059511889862328
32	16	16	50200	0.078535669586984
48	24	24	74520	0.1165832290363
52	26	26	80600	0.12609511889862

*/

/*
N=1000

INIT_SIZE = 50

k	kin	kout		c real
1	1	1	5300	0.005305305305305
2	1	1	6250	0.006256256256256
4	2	2	10050	0.01006006006006
8	4	4	17650	0.017667667667668
10	5	5	21450	0.021471471471472
16	8	8	32850	0.032882882882883
24	12	12	48050	0.048098098098098
32	16	16	63250	0.063313313313313
48	24	24	93650	0.093743743743744
52	26	26	101250	0.10135135135135
*/

/* scalefree * /
//{ 

#define PROG_RATE 1.0

#define NB_NEU 1000

#define K_SF 2
#define KIN 1
#define KOU 1

#define INIT_SIZE 20

#define NAME "n-0100-c-0_2-k-10-kin-05-kout-05-01"
//#define NAME "n-1000"

//} */



#if __ANR_MAPS == 1

/****************************************************************************/
CManager::CManager ()
{ 
  /* Initialising few parameters (random seed, tabbed exponential ... */
  InitTools ();
    
  NNLevent evtTmp;
  
  //~ cout << "Loading patterns ..." << flush;
  //~ /* Pattern presentation manager */
  //~ PatternManager = new CPatternManager (CParameters::PATTERNS_DIR+"train."+CParameters::PATTERNS_EXT, CParameters::PATTERNS_DIR+"test."+CParameters::PATTERNS_EXT);
  //~ PatternManagerGlobalPointer = PatternManager;
  //~ cout << "done." << endl;  
    
  cout << "Loading parameters from LUA script ...";
  CParameters::loadFromLuaFile (CParameters::SETTINGS_DIR+"settings-ANR.lua");
  cout << "done." << endl;
  
  //~ cout << "Saving parameters to XML...";
  //~ CParameters::saveToXmlFile (CParameters::SETTINGS_DIR+"settings-backup.xml");
  //~ cout << "done." << endl;
  
  cout << "Saving parameters to LUA script...";
  CParameters::saveToLuaFile (CParameters::SETTINGS_DIR+"settings-backup.lua");
  cout << "done." << endl;


  cout << "Creating and initializing network, please wait..." << flush;
  
  /* Builds the network */
  Architecture = new CArchitecture ();
  ArchitectureGlobalPointer = Architecture;
  
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0000000.xml");
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"saved-architecture-1153245669-200-10.xml");
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"saved-architecture-0000000000--RandSeed-1153245669--Size-200--Classes-10--timescale-10.xml");
  BuildArchitecture ();
  
  
  CAssembly * CS =  Architecture->getAssemblyByName ("CS");
  CAssembly * iCS = Architecture->getAssemblyByName ("iCS");
  CAssembly * EBN = Architecture->getAssemblyByName ("EBN");
  CAssembly * OPN = Architecture->getAssemblyByName ("OPN");
  CAssembly * iOPN = Architecture->getAssemblyByName ("iOPN");
  CAssembly * CMRF = Architecture->getAssemblyByName ("CMRF");
  CAssembly * iCMRF = Architecture->getAssemblyByName ("iCMRF");
  CAssembly * stimOPN = Architecture->getAssemblyByName ("stimOPN");


  cout << "done." << endl;  
  
  InitTools ();
  
  EventQueue.WatchEvent (EVT_SPIKE);
  EventQueue.WatchEvent (EVT_SPIKE_IMPACT);
  //EventQueue.WatchEvent (EVT_SPIKING_RATIO);
  EventQueue.WatchEvent (EVT_RECURSIVE);
  EventQueue.WatchEvent (EVT_SAVE_SPIKE_PSP);
  
  //cout << "Number of classes : " << Output->getNeurons ()->size() << endl;
  cout << "Posting events (stimulation), please wait..." << flush;

  /************************* PREMIER PAS */
  /* Stimulation */
  evtTmp.EventType = EVT_RECURSIVE;
  evtTmp.Assembly = CS;
  evtTmp.Time = 0; //starting at time 0
  evtTmp.RecurssionStep = 40;// * CParameters::TIMESCALE; // every 1 ms
  evtTmp.End = 20 * CParameters::TIMESCALE; // During the 20 first ms
  evtTmp.func = &CSInput;
  
  EventQueue.push (evtTmp);
  
  
  /* Stimulation */
  evtTmp.EventType = EVT_RECURSIVE;
  evtTmp.Assembly = CS;
  evtTmp.Time = 500; //starting at time 500.0 ms
  evtTmp.RecurssionStep = 20;// * CParameters::TIMESCALE; // every 1 ms
  evtTmp.End = 500 + 20 * CParameters::TIMESCALE; // During the 20 first ms
  evtTmp.func = &CSInput2;
  
  EventQueue.push (evtTmp);


  /* Stimulation */
  evtTmp.EventType = EVT_RECURSIVE;
  evtTmp.Assembly = CS;
  evtTmp.Time = 1000; //starting at time 500.0 ms
  evtTmp.RecurssionStep = 20;// * CParameters::TIMESCALE; // every 1 ms
  evtTmp.End = 1000 + 20 * CParameters::TIMESCALE; // During the 20 first ms
  evtTmp.func = &CSInput3;
  
  EventQueue.push (evtTmp);
  
  
  /* Stimulation */
  evtTmp.EventType = EVT_RECURSIVE;
  evtTmp.Assembly = CS;
  evtTmp.Time = 1500; //starting at time 500.0 ms
  evtTmp.RecurssionStep = 20;// * CParameters::TIMESCALE; // every 1 ms
  evtTmp.End = 1500 + 20 * CParameters::TIMESCALE; // During the 20 first ms
  evtTmp.func = &CSInput4;
  
  EventQueue.push (evtTmp);
  
  
  /* Stimulation */
  evtTmp.EventType = EVT_RECURSIVE;
  evtTmp.Assembly = CS;
  evtTmp.Time = 2000; //starting at time 500.0 ms
  evtTmp.RecurssionStep = 20;// * CParameters::TIMESCALE; // every 1 ms
  evtTmp.End = 2000 + 20 * CParameters::TIMESCALE; // During the 20 first ms
  evtTmp.func = &CSInput5;
  
  EventQueue.push (evtTmp);
  
  
  
  /******************** DEUXIEME PAS */
  
  /* Stimulation */
  evtTmp.EventType = EVT_RECURSIVE;
  evtTmp.Assembly = CS;
  evtTmp.Time = 2500; //starting at time 0
  evtTmp.RecurssionStep = 20;// * CParameters::TIMESCALE; // every 1 ms
  evtTmp.End = 2500 + 20 * CParameters::TIMESCALE; // During the 20 first ms
  evtTmp.func = &CSInput;
  
  EventQueue.push (evtTmp);
  
  
  /* Stimulation */
  evtTmp.EventType = EVT_RECURSIVE;
  evtTmp.Assembly = CS;
  evtTmp.Time = 2500 + 250; //starting at time 500.0 ms
  evtTmp.RecurssionStep = 20;// * CParameters::TIMESCALE; // every 1 ms
  evtTmp.End = 2500 + 250 + 20 * CParameters::TIMESCALE; // During the 20 first ms
  evtTmp.func = &CSInput2;
  
  EventQueue.push (evtTmp);


  /* Stimulation */
  evtTmp.EventType = EVT_RECURSIVE;
  evtTmp.Assembly = CS;
  evtTmp.Time = 2500 + 500; //starting at time 500.0 ms
  evtTmp.RecurssionStep = 20;// * CParameters::TIMESCALE; // every 1 ms
  evtTmp.End = 2500 + 500 + 20 * CParameters::TIMESCALE; // During the 20 first ms
  evtTmp.func = &CSInput3;
  
  EventQueue.push (evtTmp);
  
  
  /* Stimulation */
  evtTmp.EventType = EVT_RECURSIVE;
  evtTmp.Assembly = CS;
  evtTmp.Time = 2500 + 750; //starting at time 500.0 ms
  evtTmp.RecurssionStep = 20;// * CParameters::TIMESCALE; // every 1 ms
  evtTmp.End = 2500 + 750 + 20 * CParameters::TIMESCALE; // During the 20 first ms
  evtTmp.func = &CSInput4;
  
  EventQueue.push (evtTmp);
  
  
  /* Stimulation */
  evtTmp.EventType = EVT_RECURSIVE;
  evtTmp.Assembly = CS;
  evtTmp.Time = 2500 + 1000; //starting at time 500.0 ms
  evtTmp.RecurssionStep = 20;// * CParameters::TIMESCALE; // every 1 ms
  evtTmp.End = 2500 + 1000 + 20 * CParameters::TIMESCALE; // During the 20 first ms
  evtTmp.func = &CSInput5;
  
  EventQueue.push (evtTmp);
  
  /**************** STIM des OPN */
  
  /* Stimulation OPN (via stimOPN) */
  evtTmp.EventType = EVT_RECURSIVE;
  evtTmp.Assembly = stimOPN;
  evtTmp.Time = 0; //starting at time 0 ms
  evtTmp.RecurssionStep = 100;// * CParameters::TIMESCALE; // every 1 ms
  evtTmp.End = 1000 * CParameters::TIMESCALE; // During the 20 first ms
  evtTmp.func = &CSInputOPN;
  
  EventQueue.push (evtTmp);
  
/************************/

  cout << "done." << endl;
  
  char XMLOutputFilename[256];  
  //architecture-%07ld-seed%d-size%d-classes%d-timescale%d.xml
  sprintf (XMLOutputFilename, (CParameters::ARCHITECTURE_OUTPUT_DIR + CParameters::ARCHITECTURE_XML_FILE).c_str(), CurrentTime, RandSeed, CParameters::TIMESCALE);
  
  cout << "Saving network to " << XMLOutputFilename << "..." << flush;
  //Architecture->XMLsave (XMLOutputFilename);
  cout << "done." << endl;
    
  //exit (0);
  
  //cout << "Queue : " << EventQueue.size () << " Input : " << Input->getNeurons ()->size() << " Assembly : " << Reservoir->getNeurons ()->size () << endl;
}

/****************************************************************************/
void CManager::BuildArchitecture ()
{
  ProjectionParameters pparam;
  NeuronParameters nparam;
  
  /**************** Creating assemblies ***************/
      
  /* CS assembly */
  nparam.MembranePotential = CParameters::RESTING_POTENTIAL;
  nparam.Threshold = CParameters::THRESHOLD;
  nparam.AbsoluteRefractoryPeriode = CParameters::ABSOLUTE_REFRACTORY_PERIODE;
  nparam.AbsoluteRefractoryPotential = CParameters::ABSOLUTE_REFRACTORY_POTENTIAL;
  nparam.TauPSP = CParameters::TAU_PSP;
  nparam.TauRefract = CParameters::TAU_REFRACT;
  
  CAssembly * CS = new CAssembly ("CS");
  nparam.Inhibitory = 0.0;
  CS->AddXNeurons (& nparam, 1000);
  Architecture->AddAssembly (CS);
  
  
  /* iCS assembly */
  nparam.MembranePotential = CParameters::RESTING_POTENTIAL;
  nparam.Threshold = CParameters::THRESHOLD;
  nparam.AbsoluteRefractoryPeriode = CParameters::ABSOLUTE_REFRACTORY_PERIODE;
  nparam.AbsoluteRefractoryPotential = CParameters::ABSOLUTE_REFRACTORY_POTENTIAL;
  nparam.TauPSP = CParameters::TAU_PSP;
  nparam.TauRefract = CParameters::TAU_REFRACT;
  
  CAssembly * iCS = new CAssembly ("iCS");
  nparam.Inhibitory = 1.0;
  iCS->AddXNeurons (& nparam, 100);
  Architecture->AddAssembly (iCS);
  
  
  /* EBN assembly */
  nparam.MembranePotential = CParameters::RESTING_POTENTIAL;
  nparam.Threshold = CParameters::THRESHOLD;
  nparam.AbsoluteRefractoryPeriode = CParameters::ABSOLUTE_REFRACTORY_PERIODE;
  nparam.AbsoluteRefractoryPotential = CParameters::ABSOLUTE_REFRACTORY_POTENTIAL;
  nparam.TauPSP = CParameters::TAU_PSP;
  nparam.TauRefract = CParameters::TAU_REFRACT;
  
  CAssembly * EBN = new CAssembly ("EBN");
  nparam.Inhibitory = 0.0;
  EBN->AddXNeurons (& nparam, 100);
  Architecture->AddAssembly (EBN);
  
  
  /* iCMRF assembly */

  nparam.MembranePotential = CParameters::RESTING_POTENTIAL;
  nparam.Threshold = CParameters::THRESHOLD;
  nparam.AbsoluteRefractoryPeriode = CParameters::ABSOLUTE_REFRACTORY_PERIODE;
  nparam.AbsoluteRefractoryPotential = CParameters::ABSOLUTE_REFRACTORY_POTENTIAL;
  nparam.TauPSP = CParameters::TAU_PSP;
  nparam.TauRefract = CParameters::TAU_REFRACT;
  
  CAssembly * iCMRF = new CAssembly ("iCMRF");
  nparam.Inhibitory = 1.0;
  iCMRF->AddXNeurons (& nparam, 100);
  Architecture->AddAssembly (iCMRF);
  

  /* CMRF assembly */
  nparam.MembranePotential = CParameters::RESTING_POTENTIAL;
  nparam.Threshold = CParameters::THRESHOLD;
  nparam.AbsoluteRefractoryPeriode = CParameters::ABSOLUTE_REFRACTORY_PERIODE;
  nparam.AbsoluteRefractoryPotential = CParameters::ABSOLUTE_REFRACTORY_POTENTIAL;
  nparam.TauPSP = CParameters::TAU_PSP;
  nparam.TauRefract = CParameters::TAU_REFRACT;
  
  CAssembly * CMRF = new CAssembly ("CMRF");
  nparam.Inhibitory = 0.0;
  CMRF->AddXNeurons (& nparam, 100);
  Architecture->AddAssembly (CMRF);


  /* iOPN assembly */

  nparam.MembranePotential = CParameters::RESTING_POTENTIAL;
  nparam.Threshold = CParameters::THRESHOLD;
  nparam.AbsoluteRefractoryPeriode = CParameters::ABSOLUTE_REFRACTORY_PERIODE;
  nparam.AbsoluteRefractoryPotential = CParameters::ABSOLUTE_REFRACTORY_POTENTIAL;
  nparam.TauPSP = CParameters::TAU_PSP;
  nparam.TauRefract = CParameters::TAU_REFRACT;
  
  CAssembly * iOPN = new CAssembly ("iOPN");
  nparam.Inhibitory = 1.0;
  iOPN->AddXNeurons (& nparam, 100);
  Architecture->AddAssembly (iOPN);
  

  /* OPN assembly */
  nparam.MembranePotential = CParameters::RESTING_POTENTIAL;
  nparam.Threshold = CParameters::THRESHOLD;
  nparam.AbsoluteRefractoryPeriode = 5 * CParameters::TIMESCALE;//CParameters::ABSOLUTE_REFRACTORY_PERIODE;
  nparam.AbsoluteRefractoryPotential = CParameters::ABSOLUTE_REFRACTORY_POTENTIAL;
  nparam.TauPSP = CParameters::TAU_PSP;
  nparam.TauRefract = CParameters::TAU_REFRACT;
  
  CAssembly * OPN = new CAssembly ("OPN");
  nparam.Inhibitory = 0.0;
  OPN->AddXNeurons (& nparam, 100);
  Architecture->AddAssembly (OPN);
  
  
  /* stimOPN assembly */
  nparam.MembranePotential = CParameters::RESTING_POTENTIAL;
  nparam.Threshold = CParameters::THRESHOLD;
  nparam.AbsoluteRefractoryPeriode = CParameters::ABSOLUTE_REFRACTORY_PERIODE;
  nparam.AbsoluteRefractoryPotential = CParameters::ABSOLUTE_REFRACTORY_POTENTIAL;
  nparam.TauPSP = CParameters::TAU_PSP;
  nparam.TauRefract = CParameters::TAU_REFRACT;
  
  CAssembly * stimOPN = new CAssembly ("stimOPN");
  nparam.Inhibitory = 0.0;
  stimOPN->AddXNeurons (& nparam, 50);
  Architecture->AddAssembly (stimOPN);
  
  /****************** Connecting assemblies *****************/
  pparam.ProjectionRate = 0.4;//0.82;
  pparam.SynParam.DeltaDelay = 5 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0;
  pparam.SynParam.Weight = 0; // set by mapping
  pparam.SynParam.Delay = 6*CParameters::TIMESCALE;//CParameters::MAX_DELAY/2;//0;//-1;
  pparam.SynParam.WeightAdaptation = false;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;
  
  Architecture->GaussianMapping (CS, CS, &pparam);
  
  
  pparam.ProjectionRate = 1.0;
  pparam.SynParam.DeltaDelay = 1 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0;
  pparam.SynParam.Weight = 0.2;
  pparam.SynParam.Delay = 6*CParameters::TIMESCALE;//CParameters::MAX_DELAY/2;//0;//-1;
  pparam.SynParam.WeightAdaptation = false;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) TRUE;
  
  //Architecture->Connect (CS, CS, &pparam);
  
 
  pparam.ProjectionRate = 0.5;
  pparam.SynParam.DeltaDelay = 5 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0.2;
  pparam.SynParam.Weight = 0.5;
  pparam.SynParam.Delay = 6*CParameters::TIMESCALE;//CParameters::MAX_DELAY/2;//0;//-1;
  pparam.SynParam.WeightAdaptation = false;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;

  Architecture->Connect (CS, iCS, &pparam);


  pparam.ProjectionRate = 0.8;
  pparam.SynParam.DeltaDelay = 5 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0.2;
  pparam.SynParam.Weight = 0.5;
  pparam.SynParam.Delay = 6*CParameters::TIMESCALE;//CParameters::MAX_DELAY/2;//0;//-1;
  pparam.SynParam.WeightAdaptation = false;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;

  Architecture->Connect (iCS, CS, &pparam);


  pparam.ProjectionRate = 0.5;
  pparam.SynParam.DeltaDelay = 5 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0;
  pparam.SynParam.Weight = 1.0;
  pparam.SynParam.Delay = -1; //6*CParameters::TIMESCALE;//CParameters::MAX_DELAY/2;//0;//-1;
  pparam.SynParam.WeightAdaptation = false;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;

  //Architecture->ConnectOneToOne (CS, EBN, &pparam);
  Architecture->LogarithmicMapping (CS, EBN, &pparam);
  
  
  pparam.ProjectionRate = 0.9;
  pparam.SynParam.DeltaDelay = 1 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0.2;
  pparam.SynParam.Weight = 1.0;
  pparam.SynParam.Delay = 6*CParameters::TIMESCALE;//CParameters::MAX_DELAY/2;//0;//-1;
  pparam.SynParam.WeightAdaptation = false;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;

  ////Architecture->LogarithmicMapping (CS, iOPN, &pparam);
  //Architecture->Connect (CS, iOPN, &pparam);

  
  pparam.ProjectionRate = 0.3;
  pparam.SynParam.DeltaDelay = 1 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0.2;
  pparam.SynParam.Weight = 0.2;
  pparam.SynParam.Delay = 6*CParameters::TIMESCALE;//CParameters::MAX_DELAY/2;//0;//-1;
  pparam.SynParam.WeightAdaptation = false;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) TRUE;

  //Architecture->Connect (OPN, EBN, &pparam);

  
  pparam.ProjectionRate = 1.0;
  pparam.SynParam.DeltaDelay = 1 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0.2;
  pparam.SynParam.Weight = 0.5;
  pparam.SynParam.Delay = 6*CParameters::TIMESCALE;//CParameters::MAX_DELAY/2;//0;//-1;
  pparam.SynParam.WeightAdaptation = false;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;

  //Architecture->Connect (EBN, iOPN, &pparam);
  
  
  pparam.ProjectionRate = 1.0;
  pparam.SynParam.DeltaDelay = 1 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0.2;
  pparam.SynParam.Weight = 1.0;
  pparam.SynParam.Delay = 6*CParameters::TIMESCALE;//CParameters::MAX_DELAY/2;//0;//-1;
  pparam.SynParam.WeightAdaptation = false;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;

  //Architecture->Connect (iOPN, OPN, &pparam);
  
  
  pparam.ProjectionRate = 1.0;
     pparam.SynParam.DeltaDelay = 0;//5 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0.2;
  pparam.SynParam.Weight = 0.5;
  pparam.SynParam.Delay = -1;//6*CParameters::TIMESCALE;//CParameters::MAX_DELAY/2;//0;//-1;
  pparam.SynParam.WeightAdaptation = false;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;

  Architecture->Connect (stimOPN, OPN, &pparam);
  
  
  pparam.ProjectionRate = 0.5;
  pparam.SynParam.DeltaDelay = 1 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0.2;
  pparam.SynParam.Weight = 0.5;
  pparam.SynParam.Delay = 6*CParameters::TIMESCALE;//CParameters::MAX_DELAY/2;//0;//-1;
  pparam.SynParam.WeightAdaptation = false;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;

  //Architecture->Connect (EBN, iCMRF, &pparam);
  
  
  pparam.ProjectionRate = 1.0;
  pparam.SynParam.DeltaDelay = 1 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0.2;
  pparam.SynParam.Weight = 0.5;
  pparam.SynParam.Delay = 6*CParameters::TIMESCALE;//CParameters::MAX_DELAY/2;//0;//-1;
  pparam.SynParam.WeightAdaptation = false;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;

  //Architecture->Connect (iCMRF, CMRF, &pparam);
  
  
  pparam.ProjectionRate = 0.5;
  pparam.SynParam.DeltaDelay = 1 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0.2;
  pparam.SynParam.Weight = 0.5;
  pparam.SynParam.Delay = 6*CParameters::TIMESCALE;//CParameters::MAX_DELAY/2;//0;//-1;
  pparam.SynParam.WeightAdaptation = false;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;

  //Architecture->Connect (CMRF, CMRF, &pparam);
  
  
  pparam.ProjectionRate = 0.5;
  pparam.SynParam.DeltaDelay = 1 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0.2;
  pparam.SynParam.Weight = 0.5;
  pparam.SynParam.Delay = 6*CParameters::TIMESCALE;//CParameters::MAX_DELAY/2;//0;//-1;
  pparam.SynParam.WeightAdaptation = false;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;

  //Architecture->Connect (CMRF, iOPN, &pparam);
  
  
  pparam.ProjectionRate = 1.0;
  pparam.SynParam.DeltaDelay = 1 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0.2;
  pparam.SynParam.Weight = 1.0;
  pparam.SynParam.Delay = 6*CParameters::TIMESCALE;//CParameters::MAX_DELAY/2;//0;//-1;
  pparam.SynParam.WeightAdaptation = false;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;

  //Architecture->LogarithmicMapping (CS, CMRF, &pparam);
}

//#else
#endif

#if __USPS_MULTI == 1

#include "patternmanager.h"

/****************************************************************************/
CManager::CManager (char * settingsFile, char * archiFile)
{
  SettingsFile = settingsFile;
  ArchiFile = archiFile;

  /* Initialising few parameters (random seed, tabbed exponential ... */
  InitTools ();
    
  NNLevent evtTmp;
  
  cout << "Loading patterns ..." << flush;
  
  /* Pattern presentation manager */
  PatternManager = new CPatternManager (CParameters::PATTERNS_DIR+"train."+CParameters::PATTERNS_EXT, CParameters::PATTERNS_DIR+"test."+CParameters::PATTERNS_EXT);
  PatternManagerGlobalPointer = PatternManager;
  cout << "done." << endl;
  
  cout << "Loading parameters from LUA script ...";
  if (settingsFile == NULL)
    CParameters::loadFromLuaFile (CParameters::SETTINGS_DIR+"settings-USPS.lua");
  else
    CParameters::loadFromLuaFile (string(settingsFile));
  cout << "done." << endl;

  //~ cout << "Saving parameters to XML...";
  //~ CParameters::saveToXmlFile (CParameters::SETTINGS_DIR+"settings-backup.xml");
  //~ cout << "done." << endl;
  
  cout << "Saving parameters to LUA script...";
  CParameters::saveToLuaFile (CParameters::SETTINGS_DIR+"settings-backup.lua");
  cout << "done." << endl;


  cout << "Creating and initializing network, please wait..." << flush;
  
  /* Builds the network */
  Architecture = new CArchitecture ();
  ArchitectureGlobalPointer = Architecture;
  
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0000000.xml");
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"saved-architecture-1153245669-200-10.xml");
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"saved-architecture-0000000000--RandSeed-1153245669--Size-200--Classes-10--timescale-10.xml");
    if (archiFile == NULL)
    BuildArchitecture ();
  else
    Architecture->XMLload (string(archiFile));
  
  CAssembly * Input =     Architecture->getAssemblyByName ("Input");
  CAssembly * Reservoir = Architecture->getAssemblyByName ("Reservoir");
  CAssembly * Output =    Architecture->getAssemblyByName ("Output");
  
  cout << "done." << endl;  
  
  InitTools ();
  
  EventQueue.WatchEvent (EVT_SPIKE);
  EventQueue.WatchEvent (EVT_SPIKE_IMPACT);
  //EventQueue.WatchEvent (EVT_SPIKING_RATIO);
  EventQueue.WatchEvent (EVT_RECURSIVE);
  EventQueue.WatchEvent (EVT_SAVE_SPIKE_PSP);
  
  cout << "Number of classes : " << Output->getNeurons ()->size() << endl;
  cout << "Posting events (stimulation), please wait..." << flush;

  
  /* Initial stimulation */
  evtTmp.EventType = EVT_RECURSIVE;
  evtTmp.Assembly = Input;
  evtTmp.Time = CParameters::INITIALIZATION_START;
  evtTmp.RecurssionStep = CParameters::RAND_STIM_INTERVAL;
  evtTmp.End = CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL;
  evtTmp.func = &RandomInputs;
  
  EventQueue.push (evtTmp);
  
  /* Training input */
  evtTmp.EventType = EVT_RECURSIVE;
  evtTmp.Assembly = Input;
  evtTmp.Time = CParameters::TRAINING_START;
  evtTmp.RecurssionStep = CParameters::STIM_INTERVAL;
  evtTmp.End = CParameters::TEST_PERF_START - 1;
  evtTmp.func = &PostTrainInput;
  
  EventQueue.push (evtTmp);

  /* Test input */
  evtTmp.EventType = EVT_RECURSIVE;
  evtTmp.Assembly = Input;
  evtTmp.Time = CParameters::TEST_PERF_START;
  evtTmp.RecurssionStep = CParameters::STIM_INTERVAL;
  evtTmp.End = CParameters::END_TIME - 1;
  evtTmp.func = &PostTestInput;
  
  EventQueue.push (evtTmp);

  /* Stop STDP learning during performance evaluation phases */
  /**/ evtTmp.EventType = EVT_RECURSIVE;
  evtTmp.Assembly = NULL;
  evtTmp.Time = CParameters::TRAIN_PERF_START - 1 ;
  evtTmp.RecurssionStep = 10;
  evtTmp.End = CParameters::TRAIN_PERF_START;
  evtTmp.func = &StopSTDP;
  
  EventQueue.push (evtTmp); /**/
  
  /* Stop STDP learning after 1st epoch */
  /* evtTmp.EventType = EVT_RECURSIVE;
  evtTmp.Assembly = NULL;
  evtTmp.Time = CParameters::TRAINING_START + PatternManagerGlobalPointer->getNbTrainPatterns () * CParameters::STIM_INTERVAL - 1 ;
  evtTmp.RecurssionStep = 10;
  evtTmp.End = CParameters::TRAINING_START + PatternManagerGlobalPointer->getNbTrainPatterns () * CParameters::STIM_INTERVAL;
  evtTmp.func = &StopLearning;
  
  EventQueue.push (evtTmp); /**/
    
  /* Setting first delay learning */
  evtTmp.EventType = EVT_RECURSIVE;
  evtTmp.Assembly = Output;
  evtTmp.Time = CParameters::TRAINING_START + (CParameters::STIM_INTERVAL-5);
  evtTmp.RecurssionStep = CParameters::STIM_INTERVAL;
  evtTmp.End = CParameters::TRAIN_PERF_START;
  evtTmp.func = &LearnDelays;
  
  EventQueue.push (evtTmp);
  
  /* Performance evaluation on patterns */
  evtTmp.EventType = EVT_RECURSIVE;
  evtTmp.Assembly = Output;
  evtTmp.Time =  CParameters::TRAINING_START + (CParameters::STIM_INTERVAL-5);
  evtTmp.RecurssionStep = CParameters::STIM_INTERVAL;
  evtTmp.End = CParameters::END_TIME;
  evtTmp.func = &GenePerfo;

  EventQueue.push (evtTmp);
  
  /* Performance evaluations during training phase (TRAINING_START -> TRAIN_PERF_START) */
  evtTmp.EventType = EVT_RECURSIVE;
  evtTmp.Assembly = Output;
  evtTmp.Time =  CParameters::TRAINING_START + (PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL) -1;
  evtTmp.RecurssionStep = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL;
  evtTmp.End = CParameters::TRAIN_PERF_START;
  evtTmp.func = &InitPerfo;

  EventQueue.push (evtTmp);
  
  /* Performance evaluation on TRAIN set (TRAIN_PERF_START -> TEST_PERF_START) */
  evtTmp.EventType = EVT_RECURSIVE;
  evtTmp.Assembly = Output;
  evtTmp.Time =  CParameters::TEST_PERF_START - 1;
  evtTmp.RecurssionStep = CParameters::STIM_INTERVAL;
  evtTmp.End = CParameters::TEST_PERF_START;
  evtTmp.func = &InitPerfo;

  EventQueue.push (evtTmp);
  
  /* Performance evaluation on TEST set (TEST_PERF_START -> END_TIME) */
  evtTmp.EventType = EVT_RECURSIVE;
  evtTmp.Assembly = Output;
  evtTmp.Time = CParameters::END_TIME - 1;
  evtTmp.RecurssionStep = CParameters::STIM_INTERVAL;
  evtTmp.End = CParameters::END_TIME;
  evtTmp.func = &InitPerfo;

  EventQueue.push (evtTmp);

  /* Weight variance checking */
  /*evtTmp.EventType = EVT_RECURSIVE;
  evtTmp.Assembly = Reservoir;
  evtTmp.Time = CParameters::TRAINING_START + (PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL) -1;
  evtTmp.RecurssionStep = CParameters::STIM_INTERVAL;
  evtTmp.End = CParameters::END_TIME;
  evtTmp.func = &InitWeightModif;
  
  EventQueue.push (evtTmp);/**/

  /* Setting first weight distribution evaluation */
  /*evtTmp.EventType = EVT_RECURSIVE;
  evtTmp.Assembly = Reservoir;
  evtTmp.Time = CurrentTime;
  evtTmp.RecurssionStep = 2000*CParameters::TIMESCALE; //i.e. every 10 stimulation, as StimInterval == 200*TIMESCALE
  evtTmp.End = CParameters::END_TIME;
  evtTmp.func = &PostWeightDistributionCalculation;
  
  EventQueue.push (evtTmp);/**/
  
  /* Setting first delay distribution evaluation */
  /*evtTmp.EventType = EVT_RECURSIVE;
  evtTmp.Assembly = Output;
  evtTmp.Time = CurrentTime;
  evtTmp.RecurssionStep = 1000;
  evtTmp.End = END_TIME;
  evtTmp.func = &PostDelayDistributionCalculation;
  
  EventQueue.push (evtTmp);/**/

  /**  evtTmp.EventType = EVT_RECURSIVE;
  evtTmp.Assembly = Input;
  evtTmp.Time = NB_RAND_STIM * RAND_STIM_INTERVAL + SEPARATION_INTERVAL;
  evtTmp.RecurssionStep = STIM_INTERVAL; //* 2;
  evtTmp.End = END_TIME-GENERALIZATION_PHASE;
  evtTmp.func = &PostInputs1;
  
  EventQueue.push (evtTmp);/**/

  /**evtTmp.EventType = EVT_RECURSIVE;
  evtTmp.Assembly = Input;
  evtTmp.Time = NB_RAND_STIM * RAND_STIM_INTERVAL + SEPARATION_INTERVAL + STIM_INTERVAL;
  evtTmp.RecurssionStep = STIM_INTERVAL * 2;
  evtTmp.End = END_TIME-GENERALIZATION_PHASE;
  evtTmp.func = &PostInputs2;
  
  EventQueue.push (evtTmp);/**/
  
  /* Random noisy input for generalization */
  /* evtTmp.EventType = EVT_RECURSIVE;
  evtTmp.Assembly = Input;
  evtTmp.Time = END_TIME-GENERALIZATION_PHASE + STIM_INTERVAL;
  evtTmp.RecurssionStep = STIM_INTERVAL;
  evtTmp.End = END_TIME;
  evtTmp.func = &RandomNoisyInputClass;
  
  EventQueue.push (evtTmp);
  */

  /* Alterned noisy inputs */
  /* evtTmp.EventType = EVT_RECURSIVE;
  evtTmp.Assembly = Input;
  evtTmp.Time = END_TIME-GENERALIZATION_PHASE + STIM_INTERVAL;
  evtTmp.RecurssionStep = STIM_INTERVAL * 2;
  evtTmp.End = END_TIME;
  evtTmp.func = &PostNoisyInputs1;
  
  EventQueue.push (evtTmp);

  evtTmp.EventType = EVT_RECURSIVE;
  evtTmp.Assembly = Input;
  evtTmp.Time = END_TIME-GENERALIZATION_PHASE + STIM_INTERVAL * 2;
  evtTmp.RecurssionStep = STIM_INTERVAL * 2;
  evtTmp.End = END_TIME;
  evtTmp.func = &PostNoisyInputs2;
  
  EventQueue.push (evtTmp);*/
  
  /* Performance evaluation on TRAIN patterns (2300 -> 400000) */
  /* evtTmp.EventType = EVT_RECURSIVE;
  evtTmp.Assembly = Output;
  evtTmp.Time =  400000;
  evtTmp.RecurssionStep = STIM_INTERVAL;
  evtTmp.End = 400000 + 1;
  evtTmp.func = &InitPerfo;

  EventQueue.push (evtTmp);*/
  
  /* Initialize stimulation */
  
  /*  evtTmp.EventType = EVT_RECURSIVE;
  evtTmp.Assembly = Reservoir;
  evtTmp.Time = 0;
  evtTmp.RecurssionStep = 30000;
  evtTmp.End = END_TIME-GENERALIZATION_PHASE;
  evtTmp.func = &PostInputs1;
  
  EventQueue.push (evtTmp);*/
  

/************************/

  cout << "done." << endl;
  
  char XMLOutputFilename[256];  
  //architecture-%07ld-seed%d-size%d-classes%d-timescale%d.xml
  sprintf (XMLOutputFilename, (CParameters::ARCHITECTURE_OUTPUT_DIR + CParameters::ARCHITECTURE_XML_FILE).c_str(), CurrentTime, RandSeed, Reservoir->getNeurons ()->size (), Output->getNeurons ()->size (), CParameters::TIMESCALE);
  
  cout << "Saving network to " << XMLOutputFilename << "..." << flush;
  Architecture->XMLsave (XMLOutputFilename);
  cout << "done." << endl;
    
  //exit (0);
  
  //cout << "Queue : " << EventQueue.size () << " Input : " << Input->getNeurons ()->size() << " Assembly : " << Reservoir->getNeurons ()->size () << endl;
}

/****************************************************************************/
void CManager::BuildArchitecture ()
{
  ProjectionParameters pparam;
  NeuronParameters nparam;
  
  nparam.MembranePotential = CParameters::RESTING_POTENTIAL;
  nparam.Threshold = CParameters::THRESHOLD;
  nparam.AbsoluteRefractoryPeriode = CParameters::ABSOLUTE_REFRACTORY_PERIODE;
  nparam.AbsoluteRefractoryPotential = CParameters::ABSOLUTE_REFRACTORY_POTENTIAL;
  nparam.TauPSP = 1*CParameters::TIMESCALE;
  nparam.TauRefract = 1;
  
  /* Input assembly */
  CAssembly * Input = new CAssembly ("Input");
  nparam.Inhibitory = 0.0;
  Input->AddXNeurons (& nparam, 256);
  //Input->AddXNeurons (& nparam, 5);
  Architecture->AddAssembly (Input);
  
  /* Central assembly */
  CAssembly * Reservoir = new CAssembly ("Reservoir");
  nparam.Inhibitory = 0.0;
  //Reservoir->AddXNeurons (& nparam, 10);
  Reservoir->AddXNeurons (& nparam, 160); //80 160 240 400 800 1600
  nparam.Inhibitory = 1.0;
  Reservoir->AddXNeurons (& nparam, 40);  //20 40  60  100 200 400
  Architecture->AddAssembly (Reservoir);

  CNeuron::IDcounter += 2;
  CNeuron::IDcounter += 2;
  CNeuron::IDcounter += 2;

  /* Output assembly */
  CAssembly * Output = new CAssembly ("Output");
  nparam.Inhibitory = 0.0;
  nparam.AbsoluteRefractoryPeriode = 70*CParameters::TIMESCALE;
  //nparam.Threshold = -40;
  nparam.TauPSP = 20*CParameters::TIMESCALE;
  nparam.TauRefract = 1;
  Output->AddXNeurons (& nparam, 1);
  CNeuron::IDcounter += 2;
  Output->AddXNeurons (& nparam, 1);
  CNeuron::IDcounter += 2;
  Output->AddXNeurons (& nparam, 1);
  CNeuron::IDcounter += 2;
  Output->AddXNeurons (& nparam, 1);
  CNeuron::IDcounter += 2;
  Output->AddXNeurons (& nparam, 1);
  CNeuron::IDcounter += 2;
  Output->AddXNeurons (& nparam, 1);
  CNeuron::IDcounter += 2;
  Output->AddXNeurons (& nparam, 1);
  CNeuron::IDcounter += 2;
  Output->AddXNeurons (& nparam, 1);
  CNeuron::IDcounter += 2;
  Output->AddXNeurons (& nparam, 1);
  CNeuron::IDcounter += 2;
  Output->AddXNeurons (& nparam, 1);/**/
  
  Architecture->AddAssembly (Output);
  
  
  /***************************************************************************/

  /* Connecting assemblies */
  pparam.ProjectionRate = 1.0;
  pparam.SynParam.DeltaDelay = 0 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0;
  pparam.SynParam.Weight = 0.2; // 0.4; // 0.2; // 0.13; // 0.08; // 0.04; // 0.02;
  pparam.SynParam.Delay = CParameters::MAX_DELAY/2;//0;//-1;
  pparam.SynParam.WeightAdaptation = false;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;
  
  Architecture->Connect (Reservoir, Output, &pparam);
  
  pparam.ProjectionRate = 0.1;//0.145; // 0.29; // 0.145; // 0.096; // 0.068; // 0.029; // 0.0145;
  pparam.SynParam.DeltaDelay = 0 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0;
  pparam.SynParam.Weight = CParameters::MAX_WEIGHT/2.0;
  pparam.SynParam.Delay = -1;
  pparam.SynParam.WeightAdaptation = true;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;

  //Architecture->Connect (Reservoir, Reservoir, &pparam);
  
  pparam.ProjectionRate = 1.0;
  
  //Architecture->ConnectRegular (Reservoir, 10, &pparam);
  Architecture->ConnectRegularAndRewire (Reservoir, 10, 0.75, &pparam); //15

  
  pparam.ProjectionRate = 0.01;
  pparam.SynParam.DeltaDelay = 0 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0;
  pparam.SynParam.Weight = CParameters::MAX_WEIGHT*3;
  pparam.SynParam.Delay = -1;
  pparam.SynParam.WeightAdaptation = false;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;
  
  Architecture->Connect (Input, Reservoir, &pparam);
  
}

//#  else
#endif

#if __BI_CLASS == 1

#include "patternmanager.h"

/****************************************************************************/
CManager::CManager ()
{
  /* Initialising few parameters (random seed, tabbed exponential ... */
  InitTools ();
  
  NNLevent evtTmp;
  NNLevent * pEvtTmp;
  
  cout << "Loading patterns ..." << flush;
  
  /* Pattern presentation manager */
  PatternManager = new CPatternManager (CParameters::PATTERNS_DIR+"train."+CParameters::PATTERNS_EXT, CParameters::PATTERNS_DIR+"test."+CParameters::PATTERNS_EXT);
  PatternManagerGlobalPointer = PatternManager;
  cout << "done." << endl;
  
  cout << "Loading parameters from LUA script ...";
  CParameters::loadFromLuaFile (CParameters::SETTINGS_DIR+"settings-USPS.lua");
  cout << "done." << endl;
  
  //~ cout << "Saving parameters to XML...";
  //~ CParameters::saveToXmlFile (CParameters::SETTINGS_DIR+"settings-backup.xml");
  //~ cout << "done." << endl;
  
  cout << "Saving parameters to LUA script...";
  CParameters::saveToLuaFile (CParameters::SETTINGS_DIR+"settings-backup.lua");
  cout << "done." << endl;


  cout << "Creating and initializing network, please wait..." << flush;
  
  /* Builds the network */
  Architecture = new CArchitecture ();
  ArchitectureGlobalPointer = Architecture;
  
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0000000.xml");
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"saved-architecture-1153245669-200-10.xml");
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"saved-architecture-0000000000--RandSeed-1153245669--Size-200--Classes-10--timescale-10.xml");
  BuildArchitecture ();
  
  CAssembly * Input =     Architecture->getAssemblyByName ("Input");
  CAssembly * Reservoir = Architecture->getAssemblyByName ("Reservoir");
  CAssembly * Output =    Architecture->getAssemblyByName ("Output");
  
  cout << "done." << endl;

  InitTools ();
  
  EventQueue.WatchEvent (EVT_SPIKE);
  EventQueue.WatchEvent (EVT_SPIKE_IMPACT);
  //EventQueue.WatchEvent (EVT_SPIKING_RATIO);
  EventQueue.WatchEvent (EVT_RECURSIVE);
  EventQueue.WatchEvent (EVT_SAVE_SPIKE_PSP);
  EventQueue.WatchEvent (EVT_NEXT_PHASE);
  
  
  cout << "Number of classes : " << Output->getNeurons ()->size() << endl;
  cout << "Posting events (stimulation), please wait..." << flush;

  CSimulationPhase * Initialization = new CSimulationPhase ("Initialization");
  CSimulationPhase * BiclassTraining = new CSimulationPhase ("Bi-class Training");
  CSimulationPhase * Class1Training = new CSimulationPhase ("Class 1 Training");
  CSimulationPhase * Class2Training = new CSimulationPhase ("Class 2 Training");
  CSimulationPhase * StopSTDPCommand = new CSimulationPhase ("Stop learning");
  
  /******* Initialization phase *******/
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Input;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = CParameters::RAND_STIM_INTERVAL;
  pEvtTmp->End = CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL;
  pEvtTmp->func = &RandomInputs;
  
  Initialization->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL + CParameters::SEPARATION_INTERVAL;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL + CParameters::SEPARATION_INTERVAL;
  pEvtTmp->func = NULL;
  
  Initialization->enqueueEvent (pEvtTmp);
  
  
  /******* Bi-Class Training *******/
  CParameters::NB_STIM_PRESENTATIONS = 1;
  CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL = 1;
  CParameters::STIM_REPETITION_SEPERATION = 0;
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Input;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * CParameters::NB_TRAIN_EPOCS;
  pEvtTmp->func = &PostTrainInput;
  pEvtTmp->fparam.NbStimPresentations = CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->fparam.NbStimRepetitionsInInterval = CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL;
  pEvtTmp->fparam.StimRepetitionSeparation = CParameters::STIM_REPETITION_SEPERATION;
  
  
  BiclassTraining->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = CParameters::STIM_INTERVAL-5;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * CParameters::NB_TRAIN_EPOCS;
  pEvtTmp->func = &LearnDelays;
  
  BiclassTraining->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = CParameters::STIM_INTERVAL-5;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * CParameters::NB_TRAIN_EPOCS;
  pEvtTmp->func = &GenePerfo;
  
  BiclassTraining->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * CParameters::NB_TRAIN_EPOCS;
  pEvtTmp->func = &InitPerfo;
  
  BiclassTraining->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * CParameters::NB_TRAIN_EPOCS;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  BiclassTraining->enqueueEvent (pEvtTmp);
  
  
  /******* Class 1 Training *******/
  CParameters::NB_STIM_PRESENTATIONS = 1;
  CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL = 1;
  CParameters::STIM_REPETITION_SEPERATION = 0;
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Input;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = NbTrainPatternsByClass[6] * 1 * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->func = &PostClass1TrainInput;
  pEvtTmp->fparam.NbStimPresentations = CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->fparam.NbStimRepetitionsInInterval = CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL;
  pEvtTmp->fparam.StimRepetitionSeparation = CParameters::STIM_REPETITION_SEPERATION;
  
  Class1Training->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = CParameters::STIM_INTERVAL-5;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = NbTrainPatternsByClass[6] * 1 * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->func = &GenePerfo;
  
  Class1Training->enqueueEvent (pEvtTmp);

  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (NbTrainPatternsByClass[6] * 1 * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = 10;
  pEvtTmp->End =            NbTrainPatternsByClass[6] * 1 * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->func = &InitPerfo;
  
  Class1Training->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = NbTrainPatternsByClass[6] * 1 * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  Class1Training->enqueueEvent (pEvtTmp);
  
  
  
  /******* Class 2 Training *******/
  CParameters::NB_STIM_PRESENTATIONS = 1;
  CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL = 1;
  CParameters::STIM_REPETITION_SEPERATION = 0;
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Input;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = NbTrainPatternsByClass[9] * 1 * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->func = &PostClass2TrainInput;
  pEvtTmp->fparam.NbStimPresentations = CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->fparam.NbStimRepetitionsInInterval = CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL;
  pEvtTmp->fparam.StimRepetitionSeparation = CParameters::STIM_REPETITION_SEPERATION;
  
  Class2Training->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = CParameters::STIM_INTERVAL-5;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = NbTrainPatternsByClass[9] * 1 * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->func = &GenePerfo;
  
  Class2Training->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = (NbTrainPatternsByClass[9] * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = 10;
  pEvtTmp->End = NbTrainPatternsByClass[9] * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->func = &InitPerfo;
  
  Class2Training->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = NbTrainPatternsByClass[9] * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  Class2Training->enqueueEvent (pEvtTmp);
    

  /******* Stop learning *******/
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = 10;
  pEvtTmp->End = 1;
  pEvtTmp->func = &StopSTDP;
  
  StopSTDPCommand->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  StopSTDPCommand->enqueueEvent (pEvtTmp);
  
  
  EventQueue.enqueueSimulationPhase (Initialization);
  EventQueue.enqueueSimulationPhase (BiclassTraining);
  EventQueue.enqueueSimulationPhase (StopSTDPCommand);
  EventQueue.enqueueSimulationPhase (Class1Training);
  EventQueue.enqueueSimulationPhase (Class2Training);
  
  CParameters::END_TIME = EventQueue.getTotalSimulationLength ();
  
/************************/

  cout << "done." << endl;
  
  char XMLOutputFilename[256];  
  //architecture-%07ld-seed%d-size%d-classes%d-timescale%d.xml
  sprintf (XMLOutputFilename, (CParameters::ARCHITECTURE_OUTPUT_DIR + CParameters::ARCHITECTURE_XML_FILE).c_str(), CurrentTime, RandSeed, Reservoir->getNeurons ()->size (), Output->getNeurons ()->size (), CParameters::TIMESCALE);
  
  cout << "Saving network to " << XMLOutputFilename << "..." << flush;
  Architecture->XMLsave (XMLOutputFilename);
  cout << "done." << endl;
  
}

/****************************************************************************/
void CManager::BuildArchitecture ()
{
  ProjectionParameters pparam;
  NeuronParameters nparam;
  
  nparam.MembranePotential = CParameters::RESTING_POTENTIAL;
  nparam.Threshold = CParameters::THRESHOLD;
  nparam.AbsoluteRefractoryPeriode = CParameters::ABSOLUTE_REFRACTORY_PERIODE;
  nparam.AbsoluteRefractoryPotential = CParameters::ABSOLUTE_REFRACTORY_POTENTIAL;
  nparam.TauPSP = CParameters::TAU_PSP;//3*CParameters::TIMESCALE;
  nparam.TauRefract = CParameters::TAU_REFRACT;//1;
  
  /* Input assembly */
  CAssembly * Input = new CAssembly ("Input");
  nparam.Inhibitory = 0.0;
  Input->AddXNeurons (& nparam, 256);
  //Input->AddXNeurons (& nparam, 5);
  Architecture->AddAssembly (Input);
  
  /* Central assembly */
  CAssembly * Reservoir = new CAssembly ("Reservoir");
  nparam.Inhibitory = 0.0;
  //Reservoir->AddXNeurons (& nparam, 10);
  Reservoir->AddXNeurons (& nparam, 160); //80 160 240 400 800 1600
  nparam.Inhibitory = 1.0;
  Reservoir->AddXNeurons (& nparam, 40);  //20 40  60  100 200 400
  Architecture->AddAssembly (Reservoir);

  CNeuron::IDcounter += 2;
  CNeuron::IDcounter += 2;
  CNeuron::IDcounter += 2;

  /* Output assembly */
  CAssembly * Output = new CAssembly ("Output");
  nparam.Inhibitory = 0.0;
  nparam.AbsoluteRefractoryPeriode = 170*CParameters::TIMESCALE;
  //nparam.Threshold = -40;
  nparam.TauPSP = 20*CParameters::TIMESCALE;
  nparam.TauRefract = 1;
  
  Output->AddXNeurons (& nparam, 1);
  CNeuron::IDcounter += 2;
  Output->AddXNeurons (& nparam, 1);
  /*CNeuron::IDcounter += 2;
  Output->AddXNeurons (& nparam, 1);
  CNeuron::IDcounter += 2;
  Output->AddXNeurons (& nparam, 1);
  CNeuron::IDcounter += 2;
  Output->AddXNeurons (& nparam, 1);
  CNeuron::IDcounter += 2;
  Output->AddXNeurons (& nparam, 1);
  CNeuron::IDcounter += 2;
  Output->AddXNeurons (& nparam, 1);
  CNeuron::IDcounter += 2;
  Output->AddXNeurons (& nparam, 1);
  CNeuron::IDcounter += 2;
  Output->AddXNeurons (& nparam, 1);
  CNeuron::IDcounter += 2;
  Output->AddXNeurons (& nparam, 1);/**/
  
  Architecture->AddAssembly (Output);
  
  
  /***************************************************************************/
  
  /* Connecting assemblies */
  pparam.ProjectionRate = 1.0;
  pparam.SynParam.DeltaDelay = 0 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0;
  pparam.SynParam.Weight = 0.2; // 0.4; // 0.2; // 0.13; // 0.08; // 0.04; // 0.02;
  pparam.SynParam.Delay = CParameters::MAX_DELAY/2;//0;//-1;
  pparam.SynParam.WeightAdaptation = false;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;
  
  Architecture->Connect (Reservoir, Output, &pparam);
  
  pparam.ProjectionRate = 0.145; // 0.29; // 0.145; // 0.096; // 0.068; // 0.029; // 0.0145;
  pparam.SynParam.DeltaDelay = 0 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0;
  pparam.SynParam.Weight = CParameters::MAX_WEIGHT/2.0;
  pparam.SynParam.Delay = -1;
  pparam.SynParam.WeightAdaptation = true;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;

  Architecture->Connect (Reservoir, Reservoir, &pparam);
    
  pparam.ProjectionRate = 0.01;
  pparam.SynParam.DeltaDelay = 0 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0;
  pparam.SynParam.Weight = CParameters::MAX_WEIGHT*3;
  pparam.SynParam.Delay = -1;
  pparam.SynParam.WeightAdaptation = false;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;
  
  Architecture->Connect (Input, Reservoir, &pparam); 
}

//#    else 
#endif

#if __TOY_NET == 1

#include "patternmanager.h"

/****************************************************************************/
CManager::CManager ()
{
  /* Initialising few parameters (random seed, tabbed exponential ... */
  InitTools ();
  
  //NNLevent evtTmp;
  //NNLevent * pEvtTmp;
  
  cout << "Loading patterns ..." << flush;
  
  /* Pattern presentation manager */
  PatternManager = new CPatternManager (CParameters::PATTERNS_DIR+"train."+CParameters::PATTERNS_EXT, CParameters::PATTERNS_DIR+"test."+CParameters::PATTERNS_EXT);
  PatternManagerGlobalPointer = PatternManager;
  cout << "done." << endl;
  
  cout << "Loading parameters from LUA script ...";
  CParameters::loadFromLuaFile (CParameters::SETTINGS_DIR+"settings-USPS.lua");
  cout << "done." << endl;
  
  //~ cout << "Saving parameters to XML...";
  //~ CParameters::saveToXmlFile (CParameters::SETTINGS_DIR+"settings-backup.xml");
  //~ cout << "done." << endl;
  
  cout << "Saving parameters to LUA script...";
  CParameters::saveToLuaFile (CParameters::SETTINGS_DIR+"settings-backup.lua");
  cout << "done." << endl;


  cout << "Creating and initializing network, please wait..." << flush;
  
  /* Builds the network */
  Architecture = new CArchitecture ();
  ArchitectureGlobalPointer = Architecture;
  
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0000000.xml");
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"saved-architecture-1153245669-200-10.xml");
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"saved-architecture-0000000000--RandSeed-1153245669--Size-200--Classes-10--timescale-10.xml");
  BuildArchitecture ();
  
  cout << "done." << endl;
  
  CAssembly * ToyNet = Architecture->getAssemblyByName ("Reservoir");
  
  char XMLOutputFilename[256];  
  //architecture-%07ld-seed%d-size%d-classes%d-timescale%d.xml
  //sprintf (XMLOutputFilename, (CParameters::ARCHITECTURE_OUTPUT_DIR + CParameters::ARCHITECTURE_XML_FILE).c_str(), CurrentTime, RandSeed, ToyNet->getNeurons ()->size (), 0, CParameters::TIMESCALE);
  sprintf (XMLOutputFilename, (CParameters::ARCHITECTURE_OUTPUT_DIR + "Izhikevich-toy-network.xml").c_str());
  
  cout << "Saving network to " << XMLOutputFilename << "..." << flush;
  Architecture->XMLsave (XMLOutputFilename);
  cout << "done." << endl;
  
}

/****************************************************************************/
void CManager::BuildArchitecture ()
{
  ProjectionParameters pparam;
  NeuronParameters nparam;
  
  nparam.MembranePotential = -65;//CParameters::RESTING_POTENTIAL;
  nparam.Threshold = -52;//CParameters::THRESHOLD;
  nparam.AbsoluteRefractoryPeriode = 7 * CParameters::TIMESCALE;//CParameters::ABSOLUTE_REFRACTORY_PERIODE;
  nparam.AbsoluteRefractoryPotential = CParameters::ABSOLUTE_REFRACTORY_POTENTIAL;
  nparam.TauPSP = 10 * CParameters::TIMESCALE;//CParameters::TAU_PSP;//3*CParameters::TIMESCALE;
  nparam.TauRefract = 0.1 * CParameters::TIMESCALE;//CParameters::TAU_REFRACT;
  
  /* Input assembly */
  CAssembly * ToyNet = new CAssembly ("Reservoir");
  nparam.Inhibitory = 0.0;
  
  ///CNeuron::CNeuron (NULL);
  CNeuron * NeuronBidon = ToyNet->AddNeuron (&nparam);
  CNeuron * Neuron1 = ToyNet->AddNeuron (&nparam);
  CNeuron * Neuron2 = ToyNet->AddNeuron (&nparam);
  CNeuron * Neuron3 = ToyNet->AddNeuron (&nparam);
  CNeuron * Neuron4 = ToyNet->AddNeuron (&nparam);
  CNeuron * Neuron5 = ToyNet->AddNeuron (&nparam);

  Architecture->AddAssembly (ToyNet);
    
  
  /***************************************************************************/
  
  /* Connecting Neurons */
  pparam.ProjectionRate = 1.0;
  pparam.SynParam.DeltaDelay = 0 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0;
  pparam.SynParam.Weight = 1.0;
  
  pparam.SynParam.WeightAdaptation = false;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;
  
  pparam.SynParam.Delay = 6 * CParameters::TIMESCALE;
  Architecture->Connect (Neuron1, Neuron2, &pparam);

  pparam.SynParam.Delay = 4 * CParameters::TIMESCALE;
  Architecture->Connect (Neuron1, Neuron3, &pparam);

  pparam.SynParam.Delay = 2 * CParameters::TIMESCALE;
  Architecture->Connect (Neuron1, Neuron4, &pparam);

  pparam.SynParam.Delay = 2 * CParameters::TIMESCALE;
  Architecture->Connect (Neuron1, Neuron5, &pparam);
  
  pparam.SynParam.Delay = 2 * CParameters::TIMESCALE;
  Architecture->Connect (Neuron2, Neuron1, &pparam);

  pparam.SynParam.Delay = 7 * CParameters::TIMESCALE;
  Architecture->Connect (Neuron2, Neuron3, &pparam);

  pparam.SynParam.Delay = 7 * CParameters::TIMESCALE;
  Architecture->Connect (Neuron2, Neuron4, &pparam);

  pparam.SynParam.Delay = 2 * CParameters::TIMESCALE;
  Architecture->Connect (Neuron2, Neuron5, &pparam);

  pparam.SynParam.Delay = 4 * CParameters::TIMESCALE;
  Architecture->Connect (Neuron3, Neuron1, &pparam);

  pparam.SynParam.Delay = 2 * CParameters::TIMESCALE;
  Architecture->Connect (Neuron3, Neuron2, &pparam);

  pparam.SynParam.Delay = 2 * CParameters::TIMESCALE;
  Architecture->Connect (Neuron3, Neuron4, &pparam);

  pparam.SynParam.Delay = 7 * CParameters::TIMESCALE;
  Architecture->Connect (Neuron3, Neuron5, &pparam);

  pparam.SynParam.Delay = 10 * CParameters::TIMESCALE;
  Architecture->Connect (Neuron4, Neuron1, &pparam);

  pparam.SynParam.Delay = 3 * CParameters::TIMESCALE;
  Architecture->Connect (Neuron4, Neuron2, &pparam);

  pparam.SynParam.Delay = 5 * CParameters::TIMESCALE;
  Architecture->Connect (Neuron4, Neuron3, &pparam);

  pparam.SynParam.Delay = 6 * CParameters::TIMESCALE;
  Architecture->Connect (Neuron4, Neuron5, &pparam);

  pparam.SynParam.Delay = 5 * CParameters::TIMESCALE;
  Architecture->Connect (Neuron5, Neuron1, &pparam);

  pparam.SynParam.Delay = 4 * CParameters::TIMESCALE;
  Architecture->Connect (Neuron5, Neuron2, &pparam);

  pparam.SynParam.Delay = 4 * CParameters::TIMESCALE;
  Architecture->Connect (Neuron5, Neuron3, &pparam);

  pparam.SynParam.Delay = 4 * CParameters::TIMESCALE;
  Architecture->Connect (Neuron5, Neuron4, &pparam);

}




//#      else
#endif

#if __DYNAMIC_STUDY == 1

#include "patternmanager.h"

/****************************************************************************/
CManager::CManager ()
{
  /* Initialising few parameters (random seed, tabbed exponential ... */
  InitTools ();
  
  NNLevent evtTmp;
  NNLevent * pEvtTmp;
  
  cout << "Loading patterns ..." << flush;
  
  /* Pattern presentation manager */
  PatternManager = new CPatternManager (CParameters::PATTERNS_DIR+"train."+CParameters::PATTERNS_EXT, CParameters::PATTERNS_DIR+"test."+CParameters::PATTERNS_EXT);
  PatternManagerGlobalPointer = PatternManager;
  cout << "done." << endl;
  
  cout << "Loading parameters from LUA script ...";
  CParameters::loadFromLuaFile (CParameters::SETTINGS_DIR+"settings-USPS.lua");
  cout << "done." << endl;
  
  //~ cout << "Saving parameters to XML...";
  //~ CParameters::saveToXmlFile (CParameters::SETTINGS_DIR+"settings-backup.xml");
  //~ cout << "done." << endl;
  
  cout << "Saving parameters to LUA script...";
  CParameters::saveToLuaFile (CParameters::SETTINGS_DIR+"settings-backup.lua");
  cout << "done." << endl;


  cout << "Creating and initializing network, please wait..." << flush;
  
  /* Builds the network */
  Architecture = new CArchitecture ();
  ArchitectureGlobalPointer = Architecture;
  
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0000000.xml");
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"saved-architecture-1153245669-200-10.xml");
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"baseline.xml");
  BuildArchitecture ();
  
  CAssembly * Input =     Architecture->getAssemblyByName ("Input");
  CAssembly * Reservoir = Architecture->getAssemblyByName ("Reservoir");
  CAssembly * Output =    Architecture->getAssemblyByName ("Output");
  
  cout << "done." << endl;

  InitTools ();
  
  EventQueue.WatchEvent (EVT_SPIKE);
  EventQueue.WatchEvent (EVT_SPIKE_IMPACT);
  //EventQueue.WatchEvent (EVT_SPIKING_RATIO);
  EventQueue.WatchEvent (EVT_RECURSIVE);
  EventQueue.WatchEvent (EVT_SAVE_SPIKE_PSP);
  EventQueue.WatchEvent (EVT_NEXT_PHASE);
  
  cout << "Number of classes : " << Output->getNeurons ()->size() << endl;
  cout << "Posting events (stimulation), please wait..." << flush;

  CSimulationPhase * Initialization   = new CSimulationPhase ("Initialization");
  CSimulationPhase * BiclassTraining  = new CSimulationPhase ("Bi-class training");
  CSimulationPhase * BiclassPerfEval  = new CSimulationPhase ("Bi-class performance evaluation");
  CSimulationPhase * Particular6Pres  = new CSimulationPhase ("Particular 6 presentation");
  CSimulationPhase * Particular9Pres  = new CSimulationPhase ("Particular 9 presentation");
  CSimulationPhase * ParticularPres   = new CSimulationPhase ("Particular stim presentation");
  CSimulationPhase * StartSTDPCommand = new CSimulationPhase ("Start STDP learning");
  CSimulationPhase * StopSTDPCommand  = new CSimulationPhase ("Stop STDP learning");
  CSimulationPhase * SaveArchi        = new CSimulationPhase ("Save current architecture");
  
  /******* Initialization phase *******/
  //{
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Input;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = CParameters::RAND_STIM_INTERVAL;
  pEvtTmp->End = CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL;
  pEvtTmp->func = &RandomInputs;
  
  Initialization->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL + CParameters::SEPARATION_INTERVAL;
  pEvtTmp->RecurssionStep = 10;
  pEvtTmp->End = CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL + CParameters::SEPARATION_INTERVAL + 1;
  pEvtTmp->func = &InitWeightModif;
  
  Initialization->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL + CParameters::SEPARATION_INTERVAL;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL + CParameters::SEPARATION_INTERVAL;
  pEvtTmp->func = NULL;
  
  Initialization->enqueueEvent (pEvtTmp);
  //}
  
  /******* Bi-Class Training *******/
  //{
  CParameters::NB_STIM_PRESENTATIONS = 1;
  CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL = 1;
  CParameters::STIM_REPETITION_SEPERATION = 0;
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Input;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * CParameters::NB_TRAIN_EPOCS;
  pEvtTmp->func = &PostTrainInput;
  pEvtTmp->fparam.NbStimPresentations = CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->fparam.NbStimRepetitionsInInterval = CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL;
  pEvtTmp->fparam.StimRepetitionSeparation = CParameters::STIM_REPETITION_SEPERATION;
  
  
  BiclassTraining->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = CParameters::STIM_INTERVAL-5;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * CParameters::NB_TRAIN_EPOCS;
  pEvtTmp->func = &LearnDelays;
  
  BiclassTraining->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = CParameters::STIM_INTERVAL-5;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * CParameters::NB_TRAIN_EPOCS;
  pEvtTmp->func = &GenePerfo;
  
  BiclassTraining->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * CParameters::NB_TRAIN_EPOCS;
  pEvtTmp->func = &InitWeightModif;
  
  BiclassTraining->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * CParameters::NB_TRAIN_EPOCS;
  pEvtTmp->func = &InitPerfo;
  
  BiclassTraining->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * CParameters::NB_TRAIN_EPOCS;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  BiclassTraining->enqueueEvent (pEvtTmp);
  //}
  
  /******* Bi-class performance evaluation *******/
  //{
  CParameters::NB_STIM_PRESENTATIONS = 1;
  CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL = 1;
  CParameters::STIM_REPETITION_SEPERATION = 0;
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Input;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &PostTrainInput;
  pEvtTmp->fparam.NbStimPresentations = CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->fparam.NbStimRepetitionsInInterval = CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL;
  pEvtTmp->fparam.StimRepetitionSeparation = CParameters::STIM_REPETITION_SEPERATION;
  
  
  BiclassPerfEval->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = CParameters::STIM_INTERVAL-5;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &GenePerfo;
  
  BiclassPerfEval->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &InitPerfo;
  
  BiclassPerfEval->enqueueEvent (pEvtTmp);
  
  
  //~ pEvtTmp = new NNLevent ();
  //~ pEvtTmp->EventType = EVT_RECURSIVE;
  //~ pEvtTmp->Assembly = Output;
  //~ pEvtTmp->Time =          (PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  //~ pEvtTmp->RecurssionStep = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  //~ pEvtTmp->End =            PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  //~ pEvtTmp->func = &InitWeightModif;
  
  //~ BiclassPerfEval->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  BiclassPerfEval->enqueueEvent (pEvtTmp);
  //}
  
  /******* Particular 6 presentation *******/
  //{
  CParameters::NB_STIM_PRESENTATIONS = 50;
  CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL = 1;
  CParameters::STIM_REPETITION_SEPERATION = 0;
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Input;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &PostParticularTrainInput;
  //pEvtTmp->func = &PostClass1TrainInput;
  pEvtTmp->fparam.NbStimPresentations = CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->fparam.NbStimRepetitionsInInterval = CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL;
  pEvtTmp->fparam.StimRepetitionSeparation = CParameters::STIM_REPETITION_SEPERATION;
  pEvtTmp->fparam.PatternNumber = 1;
  
  
  Particular6Pres->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = CParameters::STIM_INTERVAL-5;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &GenePerfo;
  
  Particular6Pres->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &InitPerfo;
  
  Particular6Pres->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &InitWeightModif;
  
  Particular6Pres->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  Particular6Pres->enqueueEvent (pEvtTmp);
  //}
  
  /******* Particular 9 presentation *******/
  //{
  CParameters::NB_STIM_PRESENTATIONS = 50;
  CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL = 1;
  CParameters::STIM_REPETITION_SEPERATION = 0;
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Input;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &PostParticularTrainInput;
  //pEvtTmp->func = &PostClass2TrainInput;
  pEvtTmp->fparam.NbStimPresentations = CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->fparam.NbStimRepetitionsInInterval = CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL;
  pEvtTmp->fparam.StimRepetitionSeparation = CParameters::STIM_REPETITION_SEPERATION;
  pEvtTmp->fparam.PatternNumber = 6;
  
  
  Particular9Pres->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = CParameters::STIM_INTERVAL-5;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &GenePerfo;
  
  Particular9Pres->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &InitPerfo;
  
  Particular9Pres->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &InitWeightModif;
  
  Particular9Pres->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  Particular9Pres->enqueueEvent (pEvtTmp);
  //}
  
  
  /******* Particular stim presentation *******/
  //{
  CParameters::NB_STIM_PRESENTATIONS = 50;
  CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL = 1;
  CParameters::STIM_REPETITION_SEPERATION = 0;
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Input;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &PostParticularTrainInput;
  //pEvtTmp->func = &PostClass2TrainInput;
  pEvtTmp->fparam.NbStimPresentations = CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->fparam.NbStimRepetitionsInInterval = CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL;
  pEvtTmp->fparam.StimRepetitionSeparation = CParameters::STIM_REPETITION_SEPERATION;
  pEvtTmp->fparam.PatternNumber = 1;
  
  
  ParticularPres->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = CParameters::STIM_INTERVAL-5;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &GenePerfo;
  
  ParticularPres->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &InitPerfo;
  
  ParticularPres->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &InitWeightModif;
  
  ParticularPres->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  ParticularPres->enqueueEvent (pEvtTmp);
  //}
  
  
  /******* Start learning *******/
  //{
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = 10;
  pEvtTmp->End = 1;
  pEvtTmp->func = &StartSTDP;
  
  StartSTDPCommand->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  StartSTDPCommand->enqueueEvent (pEvtTmp);
  //}
  
  /******* Stop learning *******/
  //{
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = 10;
  pEvtTmp->End = 1;
  pEvtTmp->func = &StopSTDP;
  
  StopSTDPCommand->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  StopSTDPCommand->enqueueEvent (pEvtTmp);
  //}
    
  
  /******* Save current architecture *******/
  //{
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = 10;
  pEvtTmp->End = 1;
  pEvtTmp->func = &SaveArchitecture;
  
  SaveArchi->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  SaveArchi->enqueueEvent (pEvtTmp);
  //}
  
  //~ EventQueue.enqueueSimulationPhase (Initialization);
  //~ EventQueue.enqueueSimulationPhase (SaveArchi);
  //~ EventQueue.enqueueSimulationPhase (BiclassTraining);
  //~ EventQueue.enqueueSimulationPhase (SaveArchi);
  //~ EventQueue.enqueueSimulationPhase (StopSTDPCommand);
  //~ EventQueue.enqueueSimulationPhase (BiclassPerfEval);
  //~ EventQueue.enqueueSimulationPhase (SaveArchi);
  
  //~ EventQueue.enqueueSimulationPhase (StartSTDPCommand);
  //~ EventQueue.enqueueSimulationPhase (ParticularPres);
  //~ EventQueue.enqueueSimulationPhase (SaveArchi);
  
  //~ EventQueue.enqueueSimulationPhase (Initialization);
  //~ EventQueue.enqueueSimulationPhase (SaveArchi);
  //~ EventQueue.enqueueSimulationPhase (ParticularPres);
  //~ EventQueue.enqueueSimulationPhase (SaveArchi);
  
  //~ EventQueue.enqueueSimulationPhase (Initialization);
  //~ EventQueue.enqueueSimulationPhase (SaveArchi);
  //~ EventQueue.enqueueSimulationPhase (ParticularPres);
  //~ EventQueue.enqueueSimulationPhase (SaveArchi);
  
  //~ EventQueue.enqueueSimulationPhase (Initialization);
  //~ EventQueue.enqueueSimulationPhase (SaveArchi);
  //~ EventQueue.enqueueSimulationPhase (ParticularPres);
  //~ EventQueue.enqueueSimulationPhase (SaveArchi);
  


  CParameters::END_TIME = EventQueue.getTotalSimulationLength ();
  
/************************/

  cout << "done." << endl;
  
  char XMLOutputFilename[256];  
  //architecture-%07ld-seed%d-size%d-classes%d-timescale%d.xml
  sprintf (XMLOutputFilename, (CParameters::ARCHITECTURE_OUTPUT_DIR + CParameters::ARCHITECTURE_XML_FILE).c_str(), CurrentTime, RandSeed, Reservoir->getNeurons ()->size (), Output->getNeurons ()->size (), CParameters::TIMESCALE);
  
  cout << "Saving network to " << XMLOutputFilename << "..." << flush;
  Architecture->XMLsave (XMLOutputFilename);
  cout << "done." << endl;
  
}

/****************************************************************************/
void CManager::BuildArchitecture ()
{
  ProjectionParameters pparam;
  NeuronParameters nparam;
  
  nparam.MembranePotential = CParameters::RESTING_POTENTIAL;
  nparam.Threshold = CParameters::THRESHOLD;
  nparam.AbsoluteRefractoryPeriode = CParameters::ABSOLUTE_REFRACTORY_PERIODE;
  nparam.AbsoluteRefractoryPotential = CParameters::ABSOLUTE_REFRACTORY_POTENTIAL;
  nparam.TauPSP = CParameters::TAU_PSP;//3*CParameters::TIMESCALE;
  nparam.TauRefract = CParameters::TAU_REFRACT;//1;
  

  /* Central assembly */
  CAssembly * Reservoir = new CAssembly ("Reservoir");
  nparam.Inhibitory = 0.0;
  Reservoir->AddXNeurons (& nparam, 200); //80 160 240 400 800 1600
  Architecture->AddAssembly (Reservoir);

  
  /***************************************************************************/
  
  /* Connecting assemblies */
  pparam.ProjectionRate = 1.0;
  pparam.SynParam.DeltaDelay = 0 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0;
  pparam.SynParam.Weight = 0.5; // 0.4; // 0.2; // 0.13; // 0.08; // 0.04; // 0.02;
  pparam.SynParam.Delay = -1;
  pparam.SynParam.WeightAdaptation = false;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;
  
  /** /
  ProjectionParameters InitialRandNetParam = pparam;
  InitialRandNetParam.ProjectionRate = 0.02;
  InitialRandNetParam.SynParam.DeltaDelay = 0 * CParameters::TIMESCALE;
  InitialRandNetParam.SynParam.DeltaWeight = 0;
  InitialRandNetParam.SynParam.Weight = 0.5; // 0.4; // 0.2; // 0.13; // 0.08; // 0.04; // 0.02;
  InitialRandNetParam.SynParam.Delay = -1;
  InitialRandNetParam.SynParam.WeightAdaptation = false;
  InitialRandNetParam.SynParam.DelayAdaptation = false;
  InitialRandNetParam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;
  
  int InitialNumberNeurons = 10;
  
  int k = 19; //32
  int kin = 1; //16
  int kout = 1; //16
  
  double alpha = 0.5;
  double beta = 0.5;
  double gamma = 0.0;//0.005;
  
  Architecture->ConnectScaleFree (Reservoir, &InitialRandNetParam, InitialNumberNeurons, &pparam, k, kin, kout, alpha, beta, gamma);
  Architecture->XMLsave ("architectures/scalefree.xml");
  /**/
  
  /** /
  //Make a regular connectivity
  Architecture->ConnectRegular (Reservoir, 10, &pparam); //15
  Architecture->XMLsave ("architectures/regular.xml");
  /**/
  
  /** /
  //Make small world connectivity :
  Architecture->ConnectRegularAndRewire (Reservoir, 10, 0.1, &pparam); //15
  Architecture->XMLsave ("architectures/smallworld.xml");
  /**/
  
  /** /
  //Make random connectivity :
  Architecture->ConnectRegularAndRewire (Reservoir, 10, 1.0, &pparam); //15
  Architecture->XMLsave ("architectures/random.xml");
  /**/
  
  //pparam.ProjectionRate = 0.012;
  Architecture->Connect (Reservoir, Reservoir, &pparam);
  Architecture->XMLsave ("architectures/random.xml");
}




//#        else
#endif

#if __RESERVOIR_COMPUTING == 1

#include "patternmanager.h"

/****************************************************************************/
CManager::CManager ()
{
  /* Initialising few parameters (random seed, tabbed exponential ... */
  InitTools ();
  
  NNLevent evtTmp;
  NNLevent * pEvtTmp;
  
  cout << "Loading patterns ..." << flush;
  
  /* Pattern presentation manager */
  PatternManager = new CPatternManager (CParameters::PATTERNS_DIR+"train."+CParameters::PATTERNS_EXT, CParameters::PATTERNS_DIR+"test."+CParameters::PATTERNS_EXT);
  PatternManagerGlobalPointer = PatternManager;
  cout << "done." << endl;
  
  cout << "Loading parameters from LUA script ...";
  CParameters::loadFromLuaFile (CParameters::SETTINGS_DIR+"settings-USPS.lua");
  cout << "done." << endl;
  
  //~ cout << "Saving parameters to XML...";
  //~ CParameters::saveToXmlFile (CParameters::SETTINGS_DIR+"settings-backup.xml");
  //~ cout << "done." << endl;
  
  cout << "Saving parameters to LUA script...";
  CParameters::saveToLuaFile (CParameters::SETTINGS_DIR+"settings-backup.lua");
  cout << "done." << endl;


  cout << "Creating and initializing network, please wait..." << flush;
  
  /* Builds the network */
  Architecture = new CArchitecture ();
  ArchitectureGlobalPointer = Architecture;
  
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0000000.xml");
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"saved-architecture-1153245669-200-10.xml");
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"baseline.xml");
  BuildArchitecture ();
  
  CAssembly * Input =     Architecture->getAssemblyByName ("Input");
  CAssembly * Reservoir = Architecture->getAssemblyByName ("Reservoir");
  CAssembly * Output =    Architecture->getAssemblyByName ("Output");
  
  cout << "done." << endl;

  InitTools ();
  
  EventQueue.WatchEvent (EVT_SPIKE);
  EventQueue.WatchEvent (EVT_SPIKE_IMPACT);
  //EventQueue.WatchEvent (EVT_SPIKING_RATIO);
  EventQueue.WatchEdit jeudi soir.vent (EVT_RECURSIVE);
  EventQueue.WatchEvent (EVT_SAVE_SPIKE_PSP);
  EventQueue.WatchEvent (EVT_NEXT_PHASE);
  
  cout << "Number of classes : " << Output->getNeurons ()->size() << endl;
  cout << "Posting events (stimulation), please wait..." << flush;

  CSimulationPhase * Initialization   = new CSimulationPhase ("Initialization");
  CSimulationPhase * BiclassTraining  = new CSimulationPhase ("Bi-class training");
  CSimulationPhase * BiclassPerfEval  = new CSimulationPhase ("Bi-class performance evaluation");
  CSimulationPhase * Particular6Pres  = new CSimulationPhase ("Particular 6 presentation");
  CSimulationPhase * Particular9Pres  = new CSimulationPhase ("Particular 9 presentation");
  CSimulationPhase * ParticularPres   = new CSimulationPhase ("Particular stim presentation");
  CSimulationPhase * StartSTDPCommand = new CSimulationPhase ("Start STDP learning");
  CSimulationPhase * StopSTDPCommand  = new CSimulationPhase ("Stop STDP learning");
  CSimulationPhase * SaveArchi        = new CSimulationPhase ("Save current architecture");
  
  /******* Initialization phase *******/
  //{
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Input;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = CParameters::RAND_STIM_INTERVAL;
  pEvtTmp->End = CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL;
  pEvtTmp->func = &RandomInputs;
  
  Initialization->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL + CParameters::SEPARATION_INTERVAL;
  pEvtTmp->RecurssionStep = 10;
  pEvtTmp->End = CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL + CParameters::SEPARATION_INTERVAL + 1;
  pEvtTmp->func = &InitWeightModif;
  
  Initialization->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL + CParameters::SEPARATION_INTERVAL;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL + CParameters::SEPARATION_INTERVAL;
  pEvtTmp->func = NULL;
  
  Initialization->enqueueEvent (pEvtTmp);
  //}
  
  /******* Bi-Class Training *******/
  //{
  CParameters::NB_STIM_PRESENTATIONS = 1;
  CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL = 1;
  CParameters::STIM_REPETITION_SEPERATION = 0;
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Input;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * CParameters::NB_TRAIN_EPOCS;
  pEvtTmp->func = &PostTrainInput;
  pEvtTmp->fparam.NbStimPresentations = CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->fparam.NbStimRepetitionsInInterval = CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL;
  pEvtTmp->fparam.StimRepetitionSeparation = CParameters::STIM_REPETITION_SEPERATION;
  
  
  BiclassTraining->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = CParameters::STIM_INTERVAL-5;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * CParameters::NB_TRAIN_EPOCS;
  pEvtTmp->func = &LearnDelays;
  
  BiclassTraining->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = CParameters::STIM_INTERVAL-5;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * CParameters::NB_TRAIN_EPOCS;
  pEvtTmp->func = &GenePerfo;
  
  BiclassTraining->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * CParameters::NB_TRAIN_EPOCS;
  pEvtTmp->func = &InitWeightModif;
  
  BiclassTraining->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * CParameters::NB_TRAIN_EPOCS;
  pEvtTmp->func = &InitPerfo;
  
  BiclassTraining->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * CParameters::NB_TRAIN_EPOCS;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  BiclassTraining->enqueueEvent (pEvtTmp);
  //}
  
  /******* Bi-class performance evaluation *******/
  //{
  CParameters::NB_STIM_PRESENTATIONS = 1;
  CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL = 1;
  CParameters::STIM_REPETITION_SEPERATION = 0;
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Input;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &PostTrainInput;
  pEvtTmp->fparam.NbStimPresentations = CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->fparam.NbStimRepetitionsInInterval = CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL;
  pEvtTmp->fparam.StimRepetitionSeparation = CParameters::STIM_REPETITION_SEPERATION;
  
  
  BiclassPerfEval->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = CParameters::STIM_INTERVAL-5;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &GenePerfo;
  
  BiclassPerfEval->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &InitPerfo;
  
  BiclassPerfEval->enqueueEvent (pEvtTmp);
  
  
  //~ pEvtTmp = new NNLevent ();
  //~ pEvtTmp->EventType = EVT_RECURSIVE;
  //~ pEvtTmp->Assembly = Output;
  //~ pEvtTmp->Time =          (PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  //~ pEvtTmp->RecurssionStep = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  //~ pEvtTmp->End =            PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  //~ pEvtTmp->func = &InitWeightModif;
  
  //~ BiclassPerfEval->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  BiclassPerfEval->enqueueEvent (pEvtTmp);
  //}
  
  /******* Particular 6 presentation *******/
  //{
  CParameters::NB_STIM_PRESENTATIONS = 50;
  CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL = 1;
  CParameters::STIM_REPETITION_SEPERATION = 0;
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Input;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &PostParticularTrainInput;
  //pEvtTmp->func = &PostClass1TrainInput;
  pEvtTmp->fparam.NbStimPresentations = CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->fparam.NbStimRepetitionsInInterval = CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL;
  pEvtTmp->fparam.StimRepetitionSeparation = CParameters::STIM_REPETITION_SEPERATION;
  pEvtTmp->fparam.PatternNumber = 1;
  
  
  Particular6Pres->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = CParameters::STIM_INTERVAL-5;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &GenePerfo;
  
  Particular6Pres->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &InitPerfo;
  
  Particular6Pres->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &InitWeightModif;
  
  Particular6Pres->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  Particular6Pres->enqueueEvent (pEvtTmp);
  //}
  
  /******* Particular 9 presentation *******/
  //{
  CParameters::NB_STIM_PRESENTATIONS = 50;
  CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL = 1;
  CParameters::STIM_REPETITION_SEPERATION = 0;
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Input;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &PostParticularTrainInput;
  //pEvtTmp->func = &PostClass2TrainInput;
  pEvtTmp->fparam.NbStimPresentations = CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->fparam.NbStimRepetitionsInInterval = CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL;
  pEvtTmp->fparam.StimRepetitionSeparation = CParameters::STIM_REPETITION_SEPERATION;
  pEvtTmp->fparam.PatternNumber = 6;
  
  
  Particular9Pres->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = CParameters::STIM_INTERVAL-5;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &GenePerfo;
  
  Particular9Pres->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &InitPerfo;
  
  Particular9Pres->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &InitWeightModif;
  
  Particular9Pres->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  Particular9Pres->enqueueEvent (pEvtTmp);
  //}
  
  
  /******* Particular stim presentation *******/
  //{
  CParameters::NB_STIM_PRESENTATIONS = 50;
  CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL = 1;
  CParameters::STIM_REPETITION_SEPERATION = 0;
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Input;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &PostParticularTrainInput;
  //pEvtTmp->func = &PostClass2TrainInput;
  pEvtTmp->fparam.NbStimPresentations = CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->fparam.NbStimRepetitionsInInterval = CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL;
  pEvtTmp->fparam.StimRepetitionSeparation = CParameters::STIM_REPETITION_SEPERATION;
  pEvtTmp->fparam.PatternNumber = 1;
  
  
  ParticularPres->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = CParameters::STIM_INTERVAL-5;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &GenePerfo;
  
  ParticularPres->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &InitPerfo;
  
  ParticularPres->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &InitWeightModif;
  
  ParticularPres->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  ParticularPres->enqueueEvent (pEvtTmp);
  //}
  
  
  /******* Start learning *******/
  //{
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = 10;
  pEvtTmp->End = 1;
  pEvtTmp->func = &StartSTDP;
  
  StartSTDPCommand->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  StartSTDPCommand->enqueueEvent (pEvtTmp);
  //}
  
  /******* Stop learning *******/
  //{
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = 10;
  pEvtTmp->End = 1;
  pEvtTmp->func = &StopSTDP;
  
  StopSTDPCommand->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  StopSTDPCommand->enqueueEvent (pEvtTmp);
  //}
    
  
  /******* Save current architecture *******/
  //{
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = 10;
  pEvtTmp->End = 1;
  pEvtTmp->func = &SaveArchitecture;
  
  SaveArchi->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  SaveArchi->enqueueEvent (pEvtTmp);
  //}
  
  EventQueue.enqueueSimulationPhase (Initialization);
  EventQueue.enqueueSimulationPhase (SaveArchi);
  EventQueue.enqueueSimulationPhase (BiclassTraining);
  EventQueue.enqueueSimulationPhase (SaveArchi);
  EventQueue.enqueueSimulationPhase (StopSTDPCommand);
  EventQueue.enqueueSimulationPhase (BiclassPerfEval);
  EventQueue.enqueueSimulationPhase (SaveArchi);
  
  EventQueue.enqueueSimulationPhase (StartSTDPCommand);
  EventQueue.enqueueSimulationPhase (ParticularPres);
  EventQueue.enqueueSimulationPhase (SaveArchi);
  
  EventQueue.enqueueSimulationPhase (Initialization);
  EventQueue.enqueueSimulationPhase (SaveArchi);
  EventQueue.enqueueSimulationPhase (ParticularPres);
  EventQueue.enqueueSimulationPhase (SaveArchi);
  
  EventQueue.enqueueSimulationPhase (Initialization);
  EventQueue.enqueueSimulationPhase (SaveArchi);
  EventQueue.enqueueSimulationPhase (ParticularPres);
  EventQueue.enqueueSimulationPhase (SaveArchi);
  
  EventQueue.enqueueSimulationPhase (Initialization);
  EventQueue.enqueueSimulationPhase (SaveArchi);
  EventQueue.enqueueSimulationPhase (ParticularPres);
  EventQueue.enqueueSimulationPhase (SaveArchi);
  


  CParameters::END_TIME = EventQueue.getTotalSimulationLength ();
  
/************************/

  cout << "done." << endl;
  
  char XMLOutputFilename[256];  
  //architecture-%07ld-seed%d-size%d-classes%d-timescale%d.xml
  sprintf (XMLOutputFilename, (CParameters::ARCHITECTURE_OUTPUT_DIR + CParameters::ARCHITECTURE_XML_FILE).c_str(), CurrentTime, RandSeed, Reservoir->getNeurons ()->size (), Output->getNeurons ()->size (), CParameters::TIMESCALE);
  
  cout << "Saving network to " << XMLOutputFilename << "..." << flush;
  Architecture->XMLsave (XMLOutputFilename);
  cout << "done." << endl;
  
}

/****************************************************************************/
void CManager::BuildArchitecture ()
{
  ProjectionParameters pparam;
  NeuronParameters nparam;
  
  nparam.MembranePotential = CParameters::RESTING_POTENTIAL;
  nparam.Threshold = CParameters::THRESHOLD;
  nparam.AbsoluteRefractoryPeriode = CParameters::ABSOLUTE_REFRACTORY_PERIODE;
  nparam.AbsoluteRefractoryPotential = CParameters::ABSOLUTE_REFRACTORY_POTENTIAL;
  nparam.TauPSP = CParameters::TAU_PSP;//3*CParameters::TIMESCALE;
  nparam.TauRefract = CParameters::TAU_REFRACT;//1;
  
  /* Input assembly */
  CAssembly * Input = new CAssembly ("Input");
  nparam.Inhibitory = 0.0;
  Input->AddXNeurons (& nparam, 256);
  //Input->AddXNeurons (& nparam, 5);
  Architecture->AddAssembly (Input);
  
  /* Central assembly */
  CAssembly * Reservoir = new CAssembly ("Reservoir");
  nparam.Inhibitory = 0.0;
  //Reservoir->AddXNeurons (& nparam, 10);
  Reservoir->AddXNeurons (& nparam, 160); //80 160 240 400 800 1600
  nparam.Inhibitory = 1.0;
  Reservoir->AddXNeurons (& nparam, 40);  //20 40  60  100 200 400
  Architecture->AddAssembly (Reservoir);

  CNeuron::IDcounter += 2;
  CNeuron::IDcounter += 2;
  CNeuron::IDcounter += 2;

  /* Output assembly */
  CAssembly * Output = new CAssembly ("Output");
  nparam.Inhibitory = 0.0;
  nparam.AbsoluteRefractoryPeriode = 170*CParameters::TIMESCALE;
  //nparam.Threshold = -40;
  nparam.TauPSP = 20*CParameters::TIMESCALE;
  nparam.TauRefract = 1;
  
  Output->AddXNeurons (& nparam, 1);
  CNeuron::IDcounter += 2;
  Output->AddXNeurons (& nparam, 1);
  /*CNeuron::IDcounter += 2;
  Output->AddXNeurons (& nparam, 1);
  CNeuron::IDcounter += 2;
  Output->AddXNeurons (& nparam, 1);
  CNeuron::IDcounter += 2;
  Output->AddXNeurons (& nparam, 1);
  CNeuron::IDcounter += 2;
  Output->AddXNeurons (& nparam, 1);
  CNeuron::IDcounter += 2;
  Output->AddXNeurons (& nparam, 1);
  CNeuron::IDcounter += 2;
  Output->AddXNeurons (& nparam, 1);
  CNeuron::IDcounter += 2;
  Output->AddXNeurons (& nparam, 1);
  CNeuron::IDcounter += 2;
  Output->AddXNeurons (& nparam, 1);/**/
  
  Architecture->AddAssembly (Output);
  
  
  /***************************************************************************/
  
  /* Connecting assemblies */
  pparam.ProjectionRate = 1.0;
  pparam.SynParam.DeltaDelay = 0 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0;
  pparam.SynParam.Weight = 0.2; // 0.4; // 0.2; // 0.13; // 0.08; // 0.04; // 0.02;
  pparam.SynParam.Delay = CParameters::MAX_DELAY/2;//0;//-1;
  pparam.SynParam.WeightAdaptation = false;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;
  
  Architecture->Connect (Reservoir, Output, &pparam);
  
  pparam.ProjectionRate = 0.145; // 0.29; // 0.145; // 0.096; // 0.068; // 0.029; // 0.0145;
  pparam.SynParam.DeltaDelay = 0 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0;
  pparam.SynParam.Weight = -1;//CParameters::MAX_WEIGHT/2.0;
  pparam.SynParam.Delay = -1;
  pparam.SynParam.WeightAdaptation = true;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;

  Architecture->Connect (Reservoir, Reservoir, &pparam);
    
  pparam.ProjectionRate = 0.01;
  pparam.SynParam.DeltaDelay = 0 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0;
  pparam.SynParam.Weight = CParameters::MAX_WEIGHT*3;
  pparam.SynParam.Delay = -1;
  pparam.SynParam.WeightAdaptation = false;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;
  
  Architecture->Connect (Input, Reservoir, &pparam); 
}





//#        endif
//#      endif
//#    endif
//#  endif
#endif



#if __IZHIKEVICH_NETWORK == 1

#include "patternmanager.h"

/****************************************************************************/
CManager::CManager ()
{
  /* Initialising few parameters (random seed, tabbed exponential ... */
  InitTools ();
  
  NNLevent evtTmp;
  NNLevent * pEvtTmp;
  
  cout << "Loading patterns ..." << flush;
  
  /* Pattern presentation manager */
  PatternManager = new CPatternManager (CParameters::PATTERNS_DIR+"train."+CParameters::PATTERNS_EXT, CParameters::PATTERNS_DIR+"test."+CParameters::PATTERNS_EXT);
  PatternManagerGlobalPointer = PatternManager;
  cout << "done." << endl;
  
  cout << "Loading parameters from LUA script ...";
  CParameters::loadFromLuaFile (CParameters::SETTINGS_DIR+"settings-USPS.lua");
  cout << "done." << endl;
  
  //~ cout << "Saving parameters to XML...";
  //~ CParameters::saveToXmlFile (CParameters::SETTINGS_DIR+"settings-backup.xml");
  //~ cout << "done." << endl;
  
  cout << "Saving parameters to LUA script...";
  CParameters::saveToLuaFile (CParameters::SETTINGS_DIR+"settings-backup.lua");
  cout << "done." << endl;


  cout << "Creating and initializing network, please wait..." << flush;
  
  /* Builds the network */
  Architecture = new CArchitecture ();
  ArchitectureGlobalPointer = Architecture;
  
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"izhi-net-aftersim-200-01.xml");
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"saved-architecture-1153245669-200-10.xml");
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"baseline.xml");
  BuildArchitecture ();
  
  CAssembly * Input =     Architecture->getAssemblyByName ("Input");
  CAssembly * Reservoir = Architecture->getAssemblyByName ("Reservoir");
  CAssembly * Output =    Architecture->getAssemblyByName ("Output");
  
  cout << "done." << endl;

  InitTools ();
  
  EventQueue.WatchEvent (EVT_SPIKE);
  EventQueue.WatchEvent (EVT_SPIKE_IMPACT);
  //EventQueue.WatchEvent (EVT_SPIKING_RATIO);
  EventQueue.WatchEvent (EVT_RECURSIVE);
  EventQueue.WatchEvent (EVT_SAVE_SPIKE_PSP);
  EventQueue.WatchEvent (EVT_NEXT_PHASE);
  
  cout << "Number of classes : " << Output->getNeurons ()->size() << endl;
  cout << "Posting events (stimulation), please wait..." << flush;

  CSimulationPhase * Initialization   = new CSimulationPhase ("Initialization");
  //~ CSimulationPhase * BiclassTraining  = new CSimulationPhase ("Bi-class training");
  //~ CSimulationPhase * BiclassPerfEval  = new CSimulationPhase ("Bi-class performance evaluation");
  CSimulationPhase * Particular6Pres  = new CSimulationPhase ("Particular 6 presentation");
  CSimulationPhase * Particular9Pres  = new CSimulationPhase ("Particular 9 presentation");
  //~ CSimulationPhase * ParticularPres   = new CSimulationPhase ("Particular stim presentation");
  //~ CSimulationPhase * StartSTDPCommand = new CSimulationPhase ("Start STDP learning");
  CSimulationPhase * StopSTDPCommand  = new CSimulationPhase ("Stop STDP learning");
  CSimulationPhase * SaveArchi        = new CSimulationPhase ("Save current architecture");
  
  /******* Initialization phase ********/
  //{
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Input; // Input
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = CParameters::RAND_STIM_INTERVAL;
  pEvtTmp->End = CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL;
  pEvtTmp->func = &RandomInputs;
  
  Initialization->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL + CParameters::SEPARATION_INTERVAL;
  pEvtTmp->RecurssionStep = 10;
  pEvtTmp->End = CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL + CParameters::SEPARATION_INTERVAL + 1;
  pEvtTmp->func = &InitWeightModif;
  
  Initialization->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;//CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL + CParameters::SEPARATION_INTERVAL;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 1;//CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL + CParameters::SEPARATION_INTERVAL;
  pEvtTmp->func = NULL;
  
  Initialization->enqueueEvent (pEvtTmp);
  //}
  
  /******* Bi-Class Training *******
  //{
  CParameters::NB_STIM_PRESENTATIONS = 1;
  CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL = 1;
  CParameters::STIM_REPETITION_SEPERATION = 0;
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Input;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * CParameters::NB_TRAIN_EPOCS;
  pEvtTmp->func = &PostTrainInput;
  pEvtTmp->fparam.NbStimPresentations = CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->fparam.NbStimRepetitionsInInterval = CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL;
  pEvtTmp->fparam.StimRepetitionSeparation = CParameters::STIM_REPETITION_SEPERATION;
  
  
  BiclassTraining->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = CParameters::STIM_INTERVAL-5;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * CParameters::NB_TRAIN_EPOCS;
  pEvtTmp->func = &LearnDelays;
  
  BiclassTraining->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = CParameters::STIM_INTERVAL-5;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * CParameters::NB_TRAIN_EPOCS;
  pEvtTmp->func = &GenePerfo;
  
  BiclassTraining->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * CParameters::NB_TRAIN_EPOCS;
  pEvtTmp->func = &InitWeightModif;
  
  BiclassTraining->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * CParameters::NB_TRAIN_EPOCS;
  pEvtTmp->func = &InitPerfo;
  
  BiclassTraining->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * CParameters::NB_TRAIN_EPOCS;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  BiclassTraining->enqueueEvent (pEvtTmp);
  //} */
  
  /******* Bi-class performance evaluation *******
  //{
  CParameters::NB_STIM_PRESENTATIONS = 1;
  CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL = 1;
  CParameters::STIM_REPETITION_SEPERATION = 0;
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Input;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &PostTrainInput;
  pEvtTmp->fparam.NbStimPresentations = CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->fparam.NbStimRepetitionsInInterval = CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL;
  pEvtTmp->fparam.StimRepetitionSeparation = CParameters::STIM_REPETITION_SEPERATION;
  
  
  BiclassPerfEval->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = CParameters::STIM_INTERVAL-5;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &GenePerfo;
  
  BiclassPerfEval->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &InitPerfo;
  
  BiclassPerfEval->enqueueEvent (pEvtTmp);
  
  
  //~ pEvtTmp = new NNLevent ();
  //~ pEvtTmp->EventType = EVT_RECURSIVE;
  //~ pEvtTmp->Assembly = Output;
  //~ pEvtTmp->Time =          (PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  //~ pEvtTmp->RecurssionStep = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  //~ pEvtTmp->End =            PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  //~ pEvtTmp->func = &InitWeightModif;
  
  //~ BiclassPerfEval->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  BiclassPerfEval->enqueueEvent (pEvtTmp);
  //} */
  
  /******* Particular 6 presentation *******/
  //{
  CParameters::NB_STIM_PRESENTATIONS = 50;
  CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL = 1;
  CParameters::STIM_REPETITION_SEPERATION = 0;
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Input;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &PostParticularTrainInput;
  //pEvtTmp->func = &PostClass1TrainInput;
  pEvtTmp->fparam.NbStimPresentations = CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->fparam.NbStimRepetitionsInInterval = CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL;
  pEvtTmp->fparam.StimRepetitionSeparation = CParameters::STIM_REPETITION_SEPERATION;
  pEvtTmp->fparam.PatternNumber = 1;
  
  
  Particular6Pres->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = CParameters::STIM_INTERVAL-5;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &GenePerfo;
  
  Particular6Pres->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &InitPerfo;
  
  Particular6Pres->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &InitWeightModif;
  
  Particular6Pres->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  Particular6Pres->enqueueEvent (pEvtTmp);
  //}
  
  /******* Particular 9 presentation *******/
  //{
  CParameters::NB_STIM_PRESENTATIONS = 50;
  CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL = 1;
  CParameters::STIM_REPETITION_SEPERATION = 0;
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Input;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &PostParticularTrainInput;
  //pEvtTmp->func = &PostClass2TrainInput;
  pEvtTmp->fparam.NbStimPresentations = CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->fparam.NbStimRepetitionsInInterval = CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL;
  pEvtTmp->fparam.StimRepetitionSeparation = CParameters::STIM_REPETITION_SEPERATION;
  pEvtTmp->fparam.PatternNumber = 6;
  
  
  Particular9Pres->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = CParameters::STIM_INTERVAL-5;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &GenePerfo;
  
  Particular9Pres->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &InitPerfo;
  
  Particular9Pres->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &InitWeightModif;
  
  Particular9Pres->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  Particular9Pres->enqueueEvent (pEvtTmp);
  //}
  
  
  /******* Particular stim presentation *******
  //{
  CParameters::NB_STIM_PRESENTATIONS = 50;
  CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL = 1;
  CParameters::STIM_REPETITION_SEPERATION = 0;
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Input;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &PostParticularTrainInput;
  //pEvtTmp->func = &PostClass2TrainInput;
  pEvtTmp->fparam.NbStimPresentations = CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->fparam.NbStimRepetitionsInInterval = CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL;
  pEvtTmp->fparam.StimRepetitionSeparation = CParameters::STIM_REPETITION_SEPERATION;
  pEvtTmp->fparam.PatternNumber = 1;
  
  ParticularPres->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = CParameters::STIM_INTERVAL-5;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &GenePerfo;
  
  ParticularPres->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &InitPerfo;
  
  ParticularPres->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &InitWeightModif;
  
  ParticularPres->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  ParticularPres->enqueueEvent (pEvtTmp);
  //} */
  
  
  /******* Start learning *******
  //{
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = 10;
  pEvtTmp->End = 1;
  pEvtTmp->func = &StartSTDP;
  
  StartSTDPCommand->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  StartSTDPCommand->enqueueEvent (pEvtTmp);
  //} */
  
  /******* Stop learning *******/
  //{
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = 10;
  pEvtTmp->End = 1;
  pEvtTmp->func = &StopSTDP;
  
  StopSTDPCommand->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  StopSTDPCommand->enqueueEvent (pEvtTmp);
  //} */
    
  
  /******* Save current architecture *******/
  //{
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = 10;
  pEvtTmp->End = 1;
  pEvtTmp->func = &SaveArchitecture;
  
  SaveArchi->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  SaveArchi->enqueueEvent (pEvtTmp);
  //} */

//EventQueue.enqueueSimulationPhase (StopSTDPCommand);
EventQueue.enqueueSimulationPhase (Initialization);
  //~ EventQueue.enqueueSimulationPhase (SaveArchi);
  //~ EventQueue.enqueueSimulationPhase (BiclassTraining);
  //~ EventQueue.enqueueSimulationPhase (SaveArchi);

  //~ EventQueue.enqueueSimulationPhase (BiclassPerfEval);
  //~ EventQueue.enqueueSimulationPhase (SaveArchi);
  
  //~ EventQueue.enqueueSimulationPhase (StartSTDPCommand);
  //~ EventQueue.enqueueSimulationPhase (ParticularPres);
  //~ EventQueue.enqueueSimulationPhase (SaveArchi);
  
  //~ EventQueue.enqueueSimulationPhase (Initialization);
  //~ EventQueue.enqueueSimulationPhase (SaveArchi);
  //~ EventQueue.enqueueSimulationPhase (ParticularPres);
  //~ EventQueue.enqueueSimulationPhase (SaveArchi);
  
  //~ EventQueue.enqueueSimulationPhase (Initialization);
  //~ EventQueue.enqueueSimulationPhase (SaveArchi);
  //~ EventQueue.enqueueSimulationPhase (ParticularPres);
  //~ EventQueue.enqueueSimulationPhase (SaveArchi);
  
  //~ EventQueue.enqueueSimulationPhase (Initialization);
  //~ EventQueue.enqueueSimulationPhase (SaveArchi);
  //~ EventQueue.enqueueSimulationPhase (ParticularPres);
  //~ EventQueue.enqueueSimulationPhase (SaveArchi);
  
EventQueue.enqueueSimulationPhase (Particular6Pres);
EventQueue.enqueueSimulationPhase (Particular9Pres);


//EventQueue.enqueueSimulationPhase (SaveArchi);
//EventQueue.enqueueSimulationPhase (ParticularPres);
//EventQueue.enqueueSimulationPhase (SaveArchi);

/**/

  CParameters::END_TIME = EventQueue.getTotalSimulationLength ();
  
/************************/

  cout << "done." << endl;
  
  char XMLOutputFilename[256];  
  //architecture-%07ld-seed%d-size%d-classes%d-timescale%d.xml
  sprintf (XMLOutputFilename, (CParameters::ARCHITECTURE_OUTPUT_DIR + CParameters::ARCHITECTURE_XML_FILE).c_str(), CurrentTime, RandSeed, Reservoir->getNeurons ()->size (), Output->getNeurons ()->size (), CParameters::TIMESCALE);
  
  cout << "Saving network to " << XMLOutputFilename << "..." << flush;
  Architecture->XMLsave (XMLOutputFilename);
  cout << "done." << endl;
  
}

/****************************************************************************/
void CManager::BuildArchitecture ()
{
  ProjectionParameters pparam;
  NeuronParameters nparam;
  
  nparam.MembranePotential = CParameters::RESTING_POTENTIAL;
  nparam.Threshold = CParameters::THRESHOLD;
  nparam.AbsoluteRefractoryPeriode = CParameters::ABSOLUTE_REFRACTORY_PERIODE;
  nparam.AbsoluteRefractoryPotential = CParameters::ABSOLUTE_REFRACTORY_POTENTIAL;
  nparam.TauPSP = CParameters::TAU_PSP;//3*CParameters::TIMESCALE;
  nparam.TauRefract = CParameters::TAU_REFRACT;//1;
  
  Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"izhi-net-aftersim-400-01.xml");
  CAssembly * Reservoir = Architecture->getAssemblyByName ("Reservoir");

  /* Central assembly */
  CAssembly * Input = new CAssembly ("Input");
  nparam.Inhibitory = 0.0;
  Input->AddXNeurons (& nparam, 256);

  /*
  CAssembly * Reservoir = new CAssembly ("Reservoir");
  nparam.Inhibitory = 0.0;
  Reservoir->AddXNeurons (& nparam, int(NB_NEU*0.8)); //160
  nparam.Inhibitory = 1.0;
  Reservoir->AddXNeurons (& nparam, int(NB_NEU*0.2)); //40
  */
    
  CAssembly * Output = new CAssembly ("Output");
  nparam.Inhibitory = 0.0;
  Output->AddXNeurons (& nparam, 10);
   
  Architecture->AddAssembly (Input);
  //Architecture->AddAssembly (Reservoir);
  Architecture->AddAssembly (Output);
  
  /***************************************************************************/
  
  /* Connecting assemblies */
  pparam.ProjectionRate = 1.0;
  pparam.SynParam.DeltaDelay = 0 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0;
  pparam.SynParam.Weight = 0.2; // 0.4; // 0.2; // 0.13; // 0.08; // 0.04; // 0.02;
  pparam.SynParam.Delay = CParameters::MAX_DELAY/2;//0;//-1;
  pparam.SynParam.WeightAdaptation = false;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;
  
  Architecture->Connect (Reservoir, Output, &pparam);
  

  /*pparam.ProjectionRate = 0.1;
  pparam.SynParam.DeltaDelay = 0 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0;
  pparam.SynParam.Weight = 0.5; // 0.4; // 0.2; // 0.13; // 0.08; // 0.04; // 0.02;
  pparam.SynParam.Delay = -1;
  pparam.SynParam.WeightAdaptation = true;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;
  
  //Architecture->Connect (Reservoir, Reservoir, &pparam);
  Architecture->ConnectSubAssemblies (Reservoir, 0, int(NB_NEU*0.8), Reservoir, 0, NB_NEU, &pparam);
  Architecture->ConnectSubAssemblies (Reservoir, int(NB_NEU*0.8), int(NB_NEU*0.2), Reservoir, 0, int(NB_NEU*0.8), &pparam);
  */
  
  //cout << CParameters::MAX_WEIGHT*10 << endl;
    
  pparam.ProjectionRate = 2.0/(((float)(NB_NEU))); //0.01;
  pparam.SynParam.DeltaDelay = 0 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0;
  pparam.SynParam.Weight = CParameters::MAX_WEIGHT*2;
  pparam.SynParam.Delay = 1;
  pparam.SynParam.WeightAdaptation = false;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;
  
  Architecture->Connect (Input, Reservoir, &pparam); 
  //Architecture->ConnectSubAssemblies (Input, 0, 256, Reservoir, 0, 160, &pparam); 
  
  
  Architecture->XMLsave ((string("architectures/izhi-net-simu-beforesim-")+NAME+".xml").c_str());
  
}

//#        else
#endif





#if __MAIER_MILLER_NETWORK == 1

#include "patternmanager.h"

/****************************************************************************/
CManager::CManager ()
{
  /* Initialising few parameters (random seed, tabbed exponential ... */
  InitTools ();
  
  NNLevent evtTmp;
  NNLevent * pEvtTmp;
  
  cout << "Loading patterns ..." << flush;
  
  /* Pattern presentation manager */
  PatternManager = new CPatternManager (CParameters::PATTERNS_DIR+"train."+CParameters::PATTERNS_EXT, CParameters::PATTERNS_DIR+"test."+CParameters::PATTERNS_EXT);
  PatternManagerGlobalPointer = PatternManager;
  cout << "done." << endl;
  
  cout << "Loading parameters from LUA script ...";
  CParameters::loadFromLuaFile (CParameters::SETTINGS_DIR+"settings-Maier-Miller.lua");
  cout << "done." << endl;
  
  //~ cout << "Saving parameters to XML...";
  //~ CParameters::saveToXmlFile (CParameters::SETTINGS_DIR+"settings-backup.xml");
  //~ cout << "done." << endl;
  
  cout << "Saving parameters to LUA script...";
  CParameters::saveToLuaFile (CParameters::SETTINGS_DIR+"settings-backup.lua");
  cout << "done." << endl;


  cout << "Creating and initializing network, please wait..." << flush;
  
  /* Builds the network */
  Architecture = new CArchitecture ();
  ArchitectureGlobalPointer = Architecture;
  
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0000000.xml");
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"saved-architecture-1153245669-200-10.xml");
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"baseline.xml");
  BuildArchitecture ();
  
  CAssembly * Reservoir = Architecture->getAssemblyByName ("Reservoir");
  
  cout << "done." << endl;

  InitTools ();
  
  EventQueue.WatchEvent (EVT_SPIKE);
  EventQueue.WatchEvent (EVT_SPIKE_IMPACT);
  //EventQueue.WatchEvent (EVT_SPIKING_RATIO);
  EventQueue.WatchEvent (EVT_RECURSIVE);
  EventQueue.WatchEvent (EVT_SAVE_SPIKE_PSP);
  EventQueue.WatchEvent (EVT_NEXT_PHASE);
  
  cout << "Posting events (stimulation), please wait..." << flush;

  CSimulationPhase * Initialization   = new CSimulationPhase ("Initialization");
  CSimulationPhase * SaveArchi        = new CSimulationPhase ("Save current architecture");
  
  /******* Initialization phase ********/
  //{
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Reservoir;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = CParameters::RAND_STIM_INTERVAL;
  pEvtTmp->End = CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL;
  pEvtTmp->func = &RandomInputs;
  
  Initialization->enqueueEvent (pEvtTmp);
    
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL + CParameters::SEPARATION_INTERVAL;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL + CParameters::SEPARATION_INTERVAL;
  pEvtTmp->func = NULL;
  
  Initialization->enqueueEvent (pEvtTmp);
  //}
      
  
  /******* Save current architecture *******/
  //{
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = 10;
  pEvtTmp->End = 1;
  pEvtTmp->func = &SaveArchitecture;
  
  SaveArchi->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  SaveArchi->enqueueEvent (pEvtTmp);
  //}
  
EventQueue.enqueueSimulationPhase (Initialization);
EventQueue.enqueueSimulationPhase (SaveArchi);

/**/

  CParameters::END_TIME = EventQueue.getTotalSimulationLength ();
  
/************************/

  cout << "done." << endl;
  
  char XMLOutputFilename[256];  
  //architecture-%07ld-seed%d-size%d-classes%d-timescale%d.xml
  sprintf (XMLOutputFilename, (CParameters::ARCHITECTURE_OUTPUT_DIR + CParameters::ARCHITECTURE_XML_FILE).c_str(), CurrentTime, RandSeed, Reservoir->getNeurons ()->size (), 0, CParameters::TIMESCALE);
  
  cout << "Saving network to " << XMLOutputFilename << "..." << flush;
  Architecture->XMLsave (XMLOutputFilename);
  cout << "done." << endl;
  
}

/****************************************************************************/
void CManager::BuildArchitecture ()
{
  ProjectionParameters pparam;
  NeuronParameters nparam;
  
  nparam.MembranePotential = CParameters::RESTING_POTENTIAL;
  nparam.Threshold = CParameters::THRESHOLD;
  nparam.AbsoluteRefractoryPeriode = CParameters::ABSOLUTE_REFRACTORY_PERIODE;
  nparam.AbsoluteRefractoryPotential = CParameters::ABSOLUTE_REFRACTORY_POTENTIAL;
  nparam.TauPSP = CParameters::TAU_PSP;//3*CParameters::TIMESCALE;
  nparam.TauRefract = CParameters::TAU_REFRACT;//1;
  

  /* Central assembly */
  CAssembly * Reservoir = new CAssembly ("Reservoir");
  nparam.Inhibitory = 0.0;
  Reservoir->AddXNeurons (& nparam, NB_NEU); //80 160 240 400 800 1600 
  Architecture->AddAssembly (Reservoir);
    
  /***************************************************************************/
  
  /* Connecting assemblies */
  pparam.ProjectionRate = 0.5;
  pparam.SynParam.DeltaDelay = 0 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0;
  pparam.SynParam.Weight = 0.5; // 0.4; // 0.2; // 0.13; // 0.08; // 0.04; // 0.02;
  pparam.SynParam.Delay = -1;
  pparam.SynParam.WeightAdaptation = false;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;
  
  /** /
  ProjectionParameters InitialRandNetParam = pparam;
  InitialRandNetParam.ProjectionRate = 0.02;
  InitialRandNetParam.SynParam.DeltaDelay = 0 * CParameters::TIMESCALE;
  InitialRandNetParam.SynParam.DeltaWeight = 0;
  InitialRandNetParam.SynParam.Weight = 0.5; // 0.4; // 0.2; // 0.13; // 0.08; // 0.04; // 0.02;
  InitialRandNetParam.SynParam.Delay = -1;
  InitialRandNetParam.SynParam.WeightAdaptation = false;
  InitialRandNetParam.SynParam.DelayAdaptation = false;
  InitialRandNetParam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;
    
  int InitialNumberNeurons = 10;
  
  int k = 19; //32
  int kin = 1; //16
  int kout = 1; //16
  
  double alpha = 0.5;
  double beta = 0.5;
  double gamma = 0.0;//0.005;
  
  
  Architecture->ConnectScaleFree (Reservoir, &InitialRandNetParam, InitialNumberNeurons, &pparam, k, kin, kout, alpha, beta, gamma);
  Architecture->XMLsave ("architectures/scalefree.xml");
  /**/
  
  /** /
  //Make a regular connectivity
  Architecture->ConnectRegular (Reservoir, 5, &pparam); //15
  Architecture->XMLsave ((string("architectures/maier-miller-1-")+NAME+".xml").c_str());
  /**/
  
  /**/
  //Make random connectivity :
    
  pparam.ProjectionRate = 0.05;
  Architecture->Connect (Reservoir, Reservoir, &pparam);
    
  //Architecture->ConnectRegularAndRewire (Reservoir, 5, 1.0, &pparam); //15
  
  //Architecture->XMLsave ("architectures/reservoir-200-01.xml");
  //Architecture->XMLsave ((string("architectures/maier-miller-rand-")+NAME+".xml").c_str());
  //Architecture->XMLsave ((string("architectures/random-")+NAME+".xml").c_str());
  Architecture->XMLsave ((string("architectures/random-c-0_05-")+NAME+".xml").c_str());
  /**/
  
}

//#        else
#endif

#if __ICANN_RANDOM_NETWORK == 1

#include "patternmanager.h"

/****************************************************************************/
CManager::CManager ()
{
  /* Initialising few parameters (random seed, tabbed exponential ... */
  InitTools ();
  
  NNLevent evtTmp;
  NNLevent * pEvtTmp;
  
  cout << "Loading patterns ..." << flush;
  
  /* Pattern presentation manager */
  PatternManager = new CPatternManager (CParameters::PATTERNS_DIR+"train."+CParameters::PATTERNS_EXT, CParameters::PATTERNS_DIR+"test."+CParameters::PATTERNS_EXT);
  PatternManagerGlobalPointer = PatternManager;
  cout << "done." << endl;
  
  cout << "Loading parameters from LUA script ...";
  CParameters::loadFromLuaFile (CParameters::SETTINGS_DIR+"settings-ICANN-final.lua");
  cout << "done." << endl;
  
  //~ cout << "Saving parameters to XML...";
  //~ CParameters::saveToXmlFile (CParameters::SETTINGS_DIR+"settings-backup.xml");
  //~ cout << "done." << endl;
  
  cout << "Saving parameters to LUA script...";
  CParameters::saveToLuaFile (CParameters::SETTINGS_DIR+"settings-backup.lua");
  cout << "done." << endl;


  cout << "Creating and initializing network, please wait..." << flush;
  
  /* Builds the network */
  Architecture = new CArchitecture ();
  ArchitectureGlobalPointer = Architecture;
  
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0000000.xml");
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"saved-architecture-1153245669-200-10.xml");
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"baseline.xml");
  BuildArchitecture ();
  
  CAssembly * Reservoir = Architecture->getAssemblyByName ("Reservoir");
  
  cout << "done." << endl;

  InitTools ();
  
  EventQueue.WatchEvent (EVT_SPIKE);
  EventQueue.WatchEvent (EVT_SPIKE_IMPACT);
  //EventQueue.WatchEvent (EVT_SPIKING_RATIO);
  EventQueue.WatchEvent (EVT_RECURSIVE);
  EventQueue.WatchEvent (EVT_SAVE_SPIKE_PSP);
  EventQueue.WatchEvent (EVT_NEXT_PHASE);
  
  cout << "Posting events (stimulation), please wait..." << flush;

  CSimulationPhase * Initialization   = new CSimulationPhase ("Initialization");
  CSimulationPhase * SaveArchi        = new CSimulationPhase ("Save current architecture");
  CSimulationPhase * Life = new CSimulationPhase ("Life");
    
  /******* Initialization phase ********/
  //{
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Reservoir;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = CParameters::RAND_STIM_INTERVAL;
  pEvtTmp->End = CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL;
  pEvtTmp->func = &RandomInputs;
  
  Initialization->enqueueEvent (pEvtTmp);
    
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL + CParameters::SEPARATION_INTERVAL;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL + CParameters::SEPARATION_INTERVAL;
  pEvtTmp->func = NULL;
  
  Initialization->enqueueEvent (pEvtTmp);
  //}
      
  
  /******* Save current architecture *******/
  //{
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = 10;
  pEvtTmp->End = 1;
  pEvtTmp->func = &SaveArchitecture;
  
  SaveArchi->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  SaveArchi->enqueueEvent (pEvtTmp);
  //}
    
  /******* Life phase ********/
  //{
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 100 * CParameters::TIMESCALE;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = pEvtTmp->Time;
  pEvtTmp->func = NULL;
  
  Life->enqueueEvent (pEvtTmp);
  //}
  
EventQueue.enqueueSimulationPhase (Initialization);
EventQueue.enqueueSimulationPhase (SaveArchi);
EventQueue.enqueueSimulationPhase (Life);

/**/

  CParameters::END_TIME = EventQueue.getTotalSimulationLength ();
  
/************************/

  cout << "done." << endl;
  
  char XMLOutputFilename[256];  
  //architecture-%07ld-seed%d-size%d-classes%d-timescale%d.xml
  sprintf (XMLOutputFilename, (CParameters::ARCHITECTURE_OUTPUT_DIR + CParameters::ARCHITECTURE_XML_FILE).c_str(), CurrentTime, RandSeed, Reservoir->getNeurons ()->size (), 0, CParameters::TIMESCALE);
  
  cout << "Saving network to " << XMLOutputFilename << "..." << flush;
  Architecture->XMLsave (XMLOutputFilename);
  cout << "done." << endl;
  
}

/****************************************************************************/
void CManager::BuildArchitecture ()
{
  ProjectionParameters pparam;
  NeuronParameters nparam;
  
  nparam.MembranePotential = CParameters::RESTING_POTENTIAL;
  nparam.Threshold = CParameters::THRESHOLD;
  nparam.AbsoluteRefractoryPeriode = CParameters::ABSOLUTE_REFRACTORY_PERIODE;
  nparam.AbsoluteRefractoryPotential = CParameters::ABSOLUTE_REFRACTORY_POTENTIAL;
  nparam.TauPSP = CParameters::TAU_PSP;//3*CParameters::TIMESCALE;
  nparam.TauRefract = CParameters::TAU_REFRACT;//1;
  

  /* Central assembly */
  CAssembly * Reservoir = new CAssembly ("Reservoir");
  nparam.Inhibitory = 0.0;
  Reservoir->AddXNeurons (& nparam, NB_NEU); //80 160 240 400 800 1600 
  Architecture->AddAssembly (Reservoir);
    
  /***************************************************************************/
  
  /* Connecting assemblies */
  pparam.ProjectionRate = 0.18;
  pparam.SynParam.DeltaDelay = 0 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0;
  pparam.SynParam.Weight = 0.5; // 0.4; // 0.2; // 0.13; // 0.08; // 0.04; // 0.02;
  pparam.SynParam.Delay = -1;
  pparam.SynParam.WeightAdaptation = false;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;
  
  /** /
  ProjectionParameters InitialRandNetParam = pparam;
  InitialRandNetParam.ProjectionRate = 0.02;
  InitialRandNetParam.SynParam.DeltaDelay = 0 * CParameters::TIMESCALE;
  InitialRandNetParam.SynParam.DeltaWeight = 0;
  InitialRandNetParam.SynParam.Weight = 0.5; // 0.4; // 0.2; // 0.13; // 0.08; // 0.04; // 0.02;
  InitialRandNetParam.SynParam.Delay = -1;
  InitialRandNetParam.SynParam.WeightAdaptation = false;
  InitialRandNetParam.SynParam.DelayAdaptation = false;
  InitialRandNetParam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;
    
  int InitialNumberNeurons = 10;
  
  int k = 19; //32
  int kin = 1; //16
  int kout = 1; //16
  
  double alpha = 0.5;
  double beta = 0.5;
  double gamma = 0.0;//0.005;
  
  
  Architecture->ConnectScaleFree (Reservoir, &InitialRandNetParam, InitialNumberNeurons, &pparam, k, kin, kout, alpha, beta, gamma);
  Architecture->XMLsave ("architectures/scalefree.xml");
  /**/
  
  /** /
  //Make a regular connectivity
  Architecture->ConnectRegular (Reservoir, 5, &pparam); //15
  Architecture->XMLsave ((string("architectures/maier-miller-1-")+NAME+".xml").c_str());
  /**/
  
  /**/
  //Make random connectivity :
    
  //pparam.ProjectionRate = 0.3;
  Architecture->Connect (Reservoir, Reservoir, &pparam);
    
  //Architecture->ConnectRegularAndRewire (Reservoir, 5, 1.0, &pparam); //15
  
  //Architecture->XMLsave ("architectures/reservoir-200-01.xml");
  //Architecture->XMLsave ((string("architectures/maier-miller-rand-")+NAME+".xml").c_str());
  //Architecture->XMLsave ((string("architectures/random-")+NAME+".xml").c_str());
  Architecture->XMLsave ((string("architectures/random-")+NAME+".xml").c_str());
  /**/
  
}

//#        else
#endif


#if __ICANN_NETWORK == 1

#include "patternmanager.h"

/****************************************************************************/
CManager::CManager ()
{
  /* Initialising few parameters (random seed, tabbed exponential ... */
  InitTools ();
  
  NNLevent evtTmp;
  NNLevent * pEvtTmp;
  
  cout << "Loading patterns ..." << flush;
  
  /* Pattern presentation manager */
  PatternManager = new CPatternManager (CParameters::PATTERNS_DIR+"train."+CParameters::PATTERNS_EXT, CParameters::PATTERNS_DIR+"test."+CParameters::PATTERNS_EXT);
  PatternManagerGlobalPointer = PatternManager;
  cout << "done." << endl;
  
  cout << "Loading parameters from LUA script ...";
  CParameters::loadFromLuaFile (CParameters::SETTINGS_DIR+"settings-USPS.lua");
  cout << "done." << endl;
  
  //~ cout << "Saving parameters to XML...";
  //~ CParameters::saveToXmlFile (CParameters::SETTINGS_DIR+"settings-backup.xml");
  //~ cout << "done." << endl;
  
  cout << "Saving parameters to LUA script...";
  CParameters::saveToLuaFile (CParameters::SETTINGS_DIR+"settings-backup.lua");
  cout << "done." << endl;


  cout << "Creating and initializing network, please wait..." << flush;
  
  /* Builds the network */
  Architecture = new CArchitecture ();
  ArchitectureGlobalPointer = Architecture;
  
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"izhi-net-aftersim-200-01.xml");
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"saved-architecture-1153245669-200-10.xml");
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"baseline.xml");
  BuildArchitecture ();
  
  CAssembly * Input =     Architecture->getAssemblyByName ("Input");
  CAssembly * Reservoir = Architecture->getAssemblyByName ("Reservoir");
  CAssembly * Output =    Architecture->getAssemblyByName ("Output");
  
  cout << "done." << endl;

  InitTools ();
  
  EventQueue.WatchEvent (EVT_SPIKE);
  EventQueue.WatchEvent (EVT_SPIKE_IMPACT);
  //EventQueue.WatchEvent (EVT_SPIKING_RATIO);
  EventQueue.WatchEvent (EVT_RECURSIVE);
  EventQueue.WatchEvent (EVT_SAVE_SPIKE_PSP);
  EventQueue.WatchEvent (EVT_NEXT_PHASE);
  
  cout << "Number of classes : " << Output->getNeurons ()->size() << endl;
  cout << "Posting events (stimulation), please wait..." << flush;

  CSimulationPhase * Initialization   = new CSimulationPhase ("Initialization");
  CSimulationPhase * BiclassTraining  = new CSimulationPhase ("Bi-class training");
  CSimulationPhase * BiclassPerfEval  = new CSimulationPhase ("Bi-class performance evaluation");
  CSimulationPhase * Particular6Pres  = new CSimulationPhase ("Particular 6 presentation");
  CSimulationPhase * Particular9Pres  = new CSimulationPhase ("Particular 9 presentation");
  CSimulationPhase * ParticularPres   = new CSimulationPhase ("Particular stim presentation");
  CSimulationPhase * StartSTDPCommand = new CSimulationPhase ("Start STDP learning");
  CSimulationPhase * StopSTDPCommand  = new CSimulationPhase ("Stop STDP learning");
  CSimulationPhase * SaveArchi        = new CSimulationPhase ("Save current architecture");
  
  /******* Initialization phase ********/
  //{
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Input;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = CParameters::RAND_STIM_INTERVAL;
  pEvtTmp->End = CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL;
  pEvtTmp->func = &RandomInputs;
  
  Initialization->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL + CParameters::SEPARATION_INTERVAL;
  pEvtTmp->RecurssionStep = 10;
  pEvtTmp->End = CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL + CParameters::SEPARATION_INTERVAL + 1;
  pEvtTmp->func = &InitWeightModif;
  
  Initialization->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL + CParameters::SEPARATION_INTERVAL;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL + CParameters::SEPARATION_INTERVAL;
  pEvtTmp->func = NULL;
  
  Initialization->enqueueEvent (pEvtTmp);
  //}
  
  /******* Bi-Class Training *******/
  //{
  CParameters::NB_STIM_PRESENTATIONS = 1;
  CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL = 1;
  CParameters::STIM_REPETITION_SEPERATION = 0;
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Input;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * CParameters::NB_TRAIN_EPOCS;
  pEvtTmp->func = &PostTrainInput;
  pEvtTmp->fparam.NbStimPresentations = CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->fparam.NbStimRepetitionsInInterval = CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL;
  pEvtTmp->fparam.StimRepetitionSeparation = CParameters::STIM_REPETITION_SEPERATION;
  
  
  BiclassTraining->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = CParameters::STIM_INTERVAL-5;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * CParameters::NB_TRAIN_EPOCS;
  pEvtTmp->func = &LearnDelays;
  
  BiclassTraining->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = CParameters::STIM_INTERVAL-5;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * CParameters::NB_TRAIN_EPOCS;
  pEvtTmp->func = &GenePerfo;
  
  BiclassTraining->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * CParameters::NB_TRAIN_EPOCS;
  pEvtTmp->func = &InitWeightModif;
  
  BiclassTraining->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * CParameters::NB_TRAIN_EPOCS;
  pEvtTmp->func = &InitPerfo;
  
  BiclassTraining->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * CParameters::NB_TRAIN_EPOCS;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  BiclassTraining->enqueueEvent (pEvtTmp);
  //} */
  
  /******* Bi-class performance evaluation *******/
  //{
  CParameters::NB_STIM_PRESENTATIONS = 1;
  CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL = 1;
  CParameters::STIM_REPETITION_SEPERATION = 0;
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Input;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &PostTrainInput;
  pEvtTmp->fparam.NbStimPresentations = CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->fparam.NbStimRepetitionsInInterval = CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL;
  pEvtTmp->fparam.StimRepetitionSeparation = CParameters::STIM_REPETITION_SEPERATION;
  
  
  BiclassPerfEval->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = CParameters::STIM_INTERVAL-5;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &GenePerfo;
  
  BiclassPerfEval->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &InitPerfo;
  
  BiclassPerfEval->enqueueEvent (pEvtTmp);
  
  
  //~ pEvtTmp = new NNLevent ();
  //~ pEvtTmp->EventType = EVT_RECURSIVE;
  //~ pEvtTmp->Assembly = Output;
  //~ pEvtTmp->Time =          (PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  //~ pEvtTmp->RecurssionStep = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  //~ pEvtTmp->End =            PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  //~ pEvtTmp->func = &InitWeightModif;
  
  //~ BiclassPerfEval->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = PatternManager->getNbTrainPatterns () * CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  BiclassPerfEval->enqueueEvent (pEvtTmp);
  //} */
  
  /******* Particular 6 presentation *******/
  //{
  CParameters::NB_STIM_PRESENTATIONS = 500;
  CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL = 1; //1
  CParameters::STIM_REPETITION_SEPERATION = 0; //0
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Input;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &PostParticularTrainInput;
  //pEvtTmp->func = &PostClass1TrainInput;
  pEvtTmp->fparam.NbStimPresentations = CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->fparam.NbStimRepetitionsInInterval = CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL;
  pEvtTmp->fparam.StimRepetitionSeparation = CParameters::STIM_REPETITION_SEPERATION;
  pEvtTmp->fparam.PatternNumber = 1;
  
  
  Particular6Pres->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = CParameters::STIM_INTERVAL-5;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &GenePerfo;
  
  Particular6Pres->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &InitPerfo;
  
  Particular6Pres->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &InitWeightModif;
  
  Particular6Pres->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  Particular6Pres->enqueueEvent (pEvtTmp);
  //}
  
  /******* Particular 9 presentation *******/
  //{
  CParameters::NB_STIM_PRESENTATIONS = 500;
  CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL = 1; //1
  CParameters::STIM_REPETITION_SEPERATION = 0; //0
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Input;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &PostParticularTrainInput;
  //pEvtTmp->func = &PostClass2TrainInput;
  pEvtTmp->fparam.NbStimPresentations = CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->fparam.NbStimRepetitionsInInterval = CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL;
  pEvtTmp->fparam.StimRepetitionSeparation = CParameters::STIM_REPETITION_SEPERATION;
  pEvtTmp->fparam.PatternNumber = 6;
  
  
  Particular9Pres->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = CParameters::STIM_INTERVAL-5;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &GenePerfo;
  
  Particular9Pres->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &InitPerfo;
  
  Particular9Pres->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &InitWeightModif;
  
  Particular9Pres->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  Particular9Pres->enqueueEvent (pEvtTmp);
  //}
  
  
  /******* Particular stim presentation *******/
  //{
  CParameters::NB_STIM_PRESENTATIONS = 50;
  CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL = 1;
  CParameters::STIM_REPETITION_SEPERATION = 0;
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Input;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &PostParticularTrainInput;
  //pEvtTmp->func = &PostClass2TrainInput;
  pEvtTmp->fparam.NbStimPresentations = CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->fparam.NbStimRepetitionsInInterval = CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL;
  pEvtTmp->fparam.StimRepetitionSeparation = CParameters::STIM_REPETITION_SEPERATION;
  pEvtTmp->fparam.PatternNumber = 1;
  
  ParticularPres->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time = CParameters::STIM_INTERVAL-5;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &GenePerfo;
  
  ParticularPres->enqueueEvent (pEvtTmp);
  

  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &InitPerfo;
  
  ParticularPres->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Output;
  pEvtTmp->Time =          (CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS) -1;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->End =            CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &InitWeightModif;
  
  ParticularPres->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  ParticularPres->enqueueEvent (pEvtTmp);
  //} */
  
  
  /******* Start learning *******/
  //{
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = 10;
  pEvtTmp->End = 1;
  pEvtTmp->func = &StartSTDP;
  
  StartSTDPCommand->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  StartSTDPCommand->enqueueEvent (pEvtTmp);
  //} */
  
  /******* Stop learning *******/
  //{
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = 10;
  pEvtTmp->End = 1;
  pEvtTmp->func = &StopSTDP;
  
  StopSTDPCommand->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  StopSTDPCommand->enqueueEvent (pEvtTmp);
  //} */
    
  
  /******* Save current architecture *******/
  //{
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = 10;
  pEvtTmp->End = 1;
  pEvtTmp->func = &SaveArchitecture;
  
  SaveArchi->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  SaveArchi->enqueueEvent (pEvtTmp);
  //} */



  EventQueue.enqueueSimulationPhase (Initialization);
  EventQueue.enqueueSimulationPhase (SaveArchi);
  EventQueue.enqueueSimulationPhase (BiclassTraining);
  EventQueue.enqueueSimulationPhase (SaveArchi);
  
  //~ EventQueue.enqueueSimulationPhase (StopSTDPCommand);
  //~ EventQueue.enqueueSimulationPhase (BiclassPerfEval);
  //~ EventQueue.enqueueSimulationPhase (SaveArchi);
  
  //~ EventQueue.enqueueSimulationPhase (StartSTDPCommand);
  //~ EventQueue.enqueueSimulationPhase (ParticularPres);
  //~ EventQueue.enqueueSimulationPhase (SaveArchi);
  
  EventQueue.enqueueSimulationPhase (Particular6Pres);
  EventQueue.enqueueSimulationPhase (Particular9Pres);
  //EventQueue.enqueueSimulationPhase (SaveArchi);
  
  //~ EventQueue.enqueueSimulationPhase (Initialization);
  //~ EventQueue.enqueueSimulationPhase (SaveArchi);
  //~ EventQueue.enqueueSimulationPhase (ParticularPres);
  //~ EventQueue.enqueueSimulationPhase (SaveArchi);
  
  //~ EventQueue.enqueueSimulationPhase (Initialization);
  //~ EventQueue.enqueueSimulationPhase (SaveArchi);
  //~ EventQueue.enqueueSimulationPhase (ParticularPres);
  //~ EventQueue.enqueueSimulationPhase (SaveArchi);
  
  //~ EventQueue.enqueueSimulationPhase (Initialization);
  //~ EventQueue.enqueueSimulationPhase (SaveArchi);
  //~ EventQueue.enqueueSimulationPhase (ParticularPres);
  //~ EventQueue.enqueueSimulationPhase (SaveArchi);

  //EventQueue.enqueueSimulationPhase (SaveArchi);
  //EventQueue.enqueueSimulationPhase (ParticularPres);
  //EventQueue.enqueueSimulationPhase (SaveArchi);

/**/

  CParameters::END_TIME = EventQueue.getTotalSimulationLength ();
  
/************************/

  cout << "done." << endl;
  
  char XMLOutputFilename[256];  
  //architecture-%07ld-seed%d-size%d-classes%d-timescale%d.xml
  sprintf (XMLOutputFilename, (CParameters::ARCHITECTURE_OUTPUT_DIR + CParameters::ARCHITECTURE_XML_FILE).c_str(), CurrentTime, RandSeed, Reservoir->getNeurons ()->size (), Output->getNeurons ()->size (), CParameters::TIMESCALE);
  
  cout << "Saving network to " << XMLOutputFilename << "..." << flush;
  Architecture->XMLsave (XMLOutputFilename);
  cout << "done." << endl;
  
}




/****************************************************************************/
void CManager::BuildArchitecture ()
{
  ProjectionParameters pparam;
  NeuronParameters nparam;
  
  nparam.MembranePotential = CParameters::RESTING_POTENTIAL;
  nparam.Threshold = CParameters::THRESHOLD;
  nparam.AbsoluteRefractoryPeriode = CParameters::ABSOLUTE_REFRACTORY_PERIODE;
  nparam.AbsoluteRefractoryPotential = CParameters::ABSOLUTE_REFRACTORY_POTENTIAL;
  nparam.TauPSP = CParameters::TAU_PSP;//3*CParameters::TIMESCALE;
  nparam.TauRefract = CParameters::TAU_REFRACT;//1;
  
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"izhi-net-aftersim-400-01.xml");
  //CAssembly * Reservoir = Architecture->getAssemblyByName ("Reservoir");

  /* Central assembly */
  CAssembly * Input = new CAssembly ("Input");
  nparam.Inhibitory = 0.0;
  Input->AddXNeurons (& nparam, 256);

  
  CAssembly * Reservoir = new CAssembly ("Reservoir");
  nparam.Inhibitory = 0.0;
  Reservoir->AddXNeurons (& nparam, int(NB_NEU*0.8)); //160
  nparam.Inhibitory = 1.0;
  nparam.AbsoluteRefractoryPeriode = 3 * CParameters::TIMESCALE;
  Reservoir->AddXNeurons (& nparam, int(NB_NEU*0.2)); //40
  
  CAssembly * Output = new CAssembly ("Output");
  nparam.Inhibitory = 0.0;
  nparam.AbsoluteRefractoryPeriode = CParameters::STIM_INTERVAL-50;
  Output->AddXNeurons (& nparam, 10);
  
  Architecture->AddAssembly (Input);
  Architecture->AddAssembly (Reservoir);
  Architecture->AddAssembly (Output);
  
  /***************************************************************************/
  
  /* Connecting assemblies */
  pparam.ProjectionRate = 1.0;
  pparam.SynParam.DeltaDelay = 0 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0;
  pparam.SynParam.Weight = 0.2; // 0.4; // 0.2; // 0.13; // 0.08; // 0.04; // 0.02;
  pparam.SynParam.Delay = CParameters::MAX_DELAY/2;//0;//-1;
  pparam.SynParam.WeightAdaptation = false;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;
  
  Architecture->Connect (Reservoir, Output, &pparam);
  

  /**/
  pparam.ProjectionRate = 0.05;
  pparam.SynParam.DeltaDelay = 0 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0;
  pparam.SynParam.Weight = CParameters::MAX_WEIGHT/2; // 0.4; // 0.2; // 0.13; // 0.08; // 0.04; // 0.02;
  pparam.SynParam.Delay = -1;
  pparam.SynParam.WeightAdaptation = true;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;
  
  Architecture->Connect (Reservoir, Reservoir, &pparam);
  /**/
  
  //cout << CParameters::MAX_WEIGHT*10 << endl;
    
  pparam.ProjectionRate = 1.0/(((float)(NB_NEU))); //0.01;
  pparam.SynParam.DeltaDelay = 0 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0;
  pparam.SynParam.Weight = CParameters::MAX_WEIGHT*2;
  pparam.SynParam.Delay = -1;
  pparam.SynParam.WeightAdaptation = false;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;
  
  Architecture->Connect (Input, Reservoir, &pparam); 
  //Architecture->ConnectSubAssemblies (Input, 0, 256, Reservoir, 0, 160, &pparam); 
  
  
  Architecture->XMLsave ((string("architectures/icann-net-simu-beforesim-")+NAME+".xml").c_str());
  
}

//#        else
#endif


#if __TOPO_STUDY == 1

#include "patternmanager.h"

/****************************************************************************/
CManager::CManager (char * settingsFile, char * archiFile)
{
  SettingsFile = settingsFile;
  ArchiFile = archiFile;
  
  /* Initialising few parameters (random seed, tabbed exponential ... */
  InitTools ();
  
  NNLevent evtTmp;
  NNLevent * pEvtTmp;

  cout << "Loading patterns ..." << flush;
  
  /* Pattern presentation manager */
  PatternManager = new CPatternManager (CParameters::PATTERNS_DIR+"train."+CParameters::PATTERNS_EXT, CParameters::PATTERNS_DIR+"test."+CParameters::PATTERNS_EXT);
  PatternManagerGlobalPointer = PatternManager;
  cout << "done." << endl;
  

  cout << "Loading parameters from LUA script ...";
  if (settingsFile == NULL)
    CParameters::loadFromLuaFile (CParameters::SETTINGS_DIR+"settings-Topo-Study.lua");
  else
    CParameters::loadFromLuaFile (string(settingsFile));
  cout << "done." << endl;
  
  //~ cout << "Saving parameters to XML...";
  //~ CParameters::saveToXmlFile (CParameters::SETTINGS_DIR+"settings-backup.xml");
  //~ cout << "done." << endl;
  
  cout << "Saving parameters to LUA script...";
  CParameters::saveToLuaFile (CParameters::SETTINGS_DIR+"settings-backup.lua");
  cout << "done." << endl;


  cout << "Creating and initializing network, please wait..." << flush;
  
  /* Builds the network */
  Architecture = new CArchitecture ();
  ArchitectureGlobalPointer = Architecture;
  
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"architecture-0000000.xml");
  //Architecture->XMLload (CParameters::ARCHITECTURE_OUTPUT_DIR+"saved-architecture-1153245669-200-10.xml");
  if (archiFile == NULL)
    {
      //BuildArchitecture ();
      Architecture->BuildFromLua("builders/builder-procedural.lua");
      Architecture->XMLsave ((string("architectures/smallworld-")+NAME+".xml").c_str());
    }
  else
    Architecture->XMLload (string(archiFile));
  
  
  CAssembly * Reservoir = Architecture->getAssemblyByName ("Reservoir");
  
  cout << "done." << endl;

  InitTools ();
  
  EventQueue.WatchEvent (EVT_SPIKE);
  EventQueue.WatchEvent (EVT_SPIKE_IMPACT);
  //EventQueue.WatchEvent (EVT_SPIKING_RATIO);
  EventQueue.WatchEvent (EVT_RECURSIVE);
  EventQueue.WatchEvent (EVT_SAVE_SPIKE_PSP);
  EventQueue.WatchEvent (EVT_NEXT_PHASE);
  
  cout << "Posting events (stimulation), please wait..." << flush;


  CSimulationPhase * Initialization   = new CSimulationPhase ("Initialization", "Initialization");
  CSimulationPhase * SaveArchi        = new CSimulationPhase ("SaveCurrentArchitecture", "Save current archi");
  CSimulationPhase * Particular6Pres  = new CSimulationPhase ("Particular6Presentation", "Particular 6 Presentation");
  CSimulationPhase * Life = new CSimulationPhase ("Life", "Let it be");

  /******* Initialization phase ********/
  //{
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Reservoir;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = CParameters::RAND_STIM_INTERVAL;
  pEvtTmp->End = CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL;
  pEvtTmp->func = &RandomInputs;
  
  Initialization->enqueueEvent (pEvtTmp);
    
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL + CParameters::SEPARATION_INTERVAL;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = CParameters::NB_RAND_STIM * CParameters::RAND_STIM_INTERVAL + CParameters::SEPARATION_INTERVAL;
  pEvtTmp->func = NULL;
  
  Initialization->enqueueEvent (pEvtTmp);
  //}
      
  
  /******* Save current architecture *******/
  //{
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = 10;
  pEvtTmp->End = 1;
  pEvtTmp->func = &SaveArchitecture;
  
  SaveArchi->enqueueEvent (pEvtTmp);
  
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  SaveArchi->enqueueEvent (pEvtTmp);
  //}
    
  
  /******* Particular 6 presentation *******/
  //{
  CParameters::NB_STIM_PRESENTATIONS = 50;
  CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL = 1;
  CParameters::STIM_REPETITION_SEPERATION = 0;
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_RECURSIVE;
  pEvtTmp->Assembly = Reservoir;
  pEvtTmp->Time = 0;
  pEvtTmp->RecurssionStep = CParameters::STIM_INTERVAL;
  pEvtTmp->End = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->func = &PostParticularTrainInput;
  //pEvtTmp->func = &PostClass1TrainInput;
  pEvtTmp->fparam.NbStimPresentations = CParameters::NB_STIM_PRESENTATIONS;
  pEvtTmp->fparam.NbStimRepetitionsInInterval = CParameters::NB_STIM_REPETITION_IN_STIM_INTERVAL;
  pEvtTmp->fparam.StimRepetitionSeparation = CParameters::STIM_REPETITION_SEPERATION;
  pEvtTmp->fparam.PatternNumber = 1;
  
  
  Particular6Pres->enqueueEvent (pEvtTmp);
    
  
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = CParameters::STIM_INTERVAL * CParameters::NB_STIM_PRESENTATIONS * 1;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = 0;
  pEvtTmp->func = NULL;

  Particular6Pres->enqueueEvent (pEvtTmp);
  //}

  
  /******* Life phase ********/
  //{
  pEvtTmp = new NNLevent ();
  pEvtTmp->EventType = EVT_NEXT_PHASE;
  pEvtTmp->Assembly = NULL;
  pEvtTmp->Time = 100 * CParameters::TIMESCALE;
  pEvtTmp->RecurssionStep = 0;
  pEvtTmp->End = pEvtTmp->Time;
  pEvtTmp->func = NULL;
  
  Life->enqueueEvent (pEvtTmp);
  //}
  

  CParameters::END_TIME = EventQueue.getTotalSimulationLength ();
  
  CSimulation * MySimulation = new CSimulation ();
  MySimulation->push_back(Initialization);
  MySimulation->push_back(SaveArchi);
  MySimulation->push_back(Particular6Pres);
  MySimulation->push_back(Life);

  MySimulation->SaveToLua("simulations/test-out.lua");
  delete MySimulation;

  MySimulation = new CSimulation ();
  MySimulation->LoadFromLua("simulations/test-out.lua");
  MySimulation->SaveToLua("simulations/test-out2.lua");


  //EventQueue.enqueueSimulationPhase (Initialization);
  //EventQueue.enqueueSimulationPhase (Particular6Pres);
  //EventQueue.enqueueSimulationPhase (SaveArchi);
  //EventQueue.enqueueSimulationPhase (Life);


  EventQueue.enqueueSimulation (MySimulation);

  CParameters::END_TIME = EventQueue.getTotalSimulationLength ();

/************************/

  cout << "done." << endl;
  
  char XMLOutputFilename[256];  
  
  if (ArchiFile == NULL)
  {
    //architecture-%07ld-seed%d-size%d-classes%d-timescale%d.xml
    sprintf (XMLOutputFilename, (CParameters::ARCHITECTURE_OUTPUT_DIR + CParameters::ARCHITECTURE_XML_FILE).c_str(), CurrentTime, RandSeed, Reservoir->getNeurons ()->size (), 0, CParameters::TIMESCALE);
    cout << "Saving network to " << XMLOutputFilename << " ... " << flush;
    Architecture->XMLsave (XMLOutputFilename);
    cout << "done." << endl;
  }
  else
  {
    sprintf (XMLOutputFilename, (CParameters::ARCHITECTURE_OUTPUT_DIR + string("00-") + (strstr(ArchiFile, "/")+1)).c_str());
    cout << "Saving network to " << XMLOutputFilename << " ... " << flush;
    Architecture->XMLsave (XMLOutputFilename);
    cout << "done." << endl;
  }
  
}

/****************************************************************************/
void CManager::BuildArchitecture ()
{
  ProjectionParameters pparam;
  NeuronParameters nparam;
  
  nparam.MembranePotential = CParameters::RESTING_POTENTIAL;
  nparam.Threshold = CParameters::THRESHOLD;
  nparam.AbsoluteRefractoryPeriode = CParameters::ABSOLUTE_REFRACTORY_PERIODE;
  nparam.AbsoluteRefractoryPotential = CParameters::ABSOLUTE_REFRACTORY_POTENTIAL;
  nparam.TauPSP = CParameters::TAU_PSP;//3*CParameters::TIMESCALE;
  nparam.TauRefract = CParameters::TAU_REFRACT;//1;
  

  /* Central assembly */
  CAssembly * Reservoir = new CAssembly ("Reservoir");
  nparam.Inhibitory = 0.0;
  Reservoir->AddXNeurons (& nparam, NB_NEU); //80 160 240 400 800 1600 
  Architecture->AddAssembly (Reservoir);
    
  /***************************************************************************/
  
  /* Connecting assemblies */
  pparam.ProjectionRate = PROG_RATE;
  pparam.SynParam.DeltaDelay = 0 * CParameters::TIMESCALE;
  pparam.SynParam.DeltaWeight = 0;
  pparam.SynParam.Weight = -1 ; // 0.4; // 0.2; // 0.13; // 0.08; // 0.04; // 0.02;
  pparam.SynParam.Delay = -1;
  pparam.SynParam.WeightAdaptation = false;
  pparam.SynParam.DelayAdaptation = false;
  pparam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;
  
  /* Scalefree * /
  ProjectionParameters InitialRandNetParam = pparam;
  InitialRandNetParam.ProjectionRate = 1.0;
  InitialRandNetParam.SynParam.DeltaDelay = 0 * CParameters::TIMESCALE;
  InitialRandNetParam.SynParam.DeltaWeight = 0;
  InitialRandNetParam.SynParam.Weight = -1; // 0.4; // 0.2; // 0.13; // 0.08; // 0.04; // 0.02;
  InitialRandNetParam.SynParam.Delay = -1;
  InitialRandNetParam.SynParam.WeightAdaptation = false;
  InitialRandNetParam.SynParam.DelayAdaptation = false;
  InitialRandNetParam.SynParam.Inhibitory = (NNLbool) UNSPECIFIED;
  
  int InitialNumberNeurons = INIT_SIZE;
  
  int k = K_SF; //19 //32
  int kin = KIN; //1 //16
  int kout = KOU; //1 //16
  
  double alpha = 0.5;
  double beta = 0.5;
  double gamma = 0.0;//0.005;
  
  
  Architecture->ConnectScaleFree (Reservoir, &InitialRandNetParam, InitialNumberNeurons, &pparam, k, kin, kout, alpha, beta, gamma);
  Architecture->XMLsave ((string("architectures/scalefree-")+NAME+".xml").c_str());
  exit (0);
  /**/
  
  /* Random * /
  //Make random connectivity
  //pparam.ProjectionRate = 0.3;
  Architecture->Connect (Reservoir, Reservoir, &pparam);
//  Architecture->ConnectRegularAndRewire (Reservoir, 2, 1, &pparam); //
  Architecture->XMLsave ((string("architectures/random-")+NAME+".xml").c_str());
  exit (0);
  /**/
  
  /* Regular * /
  //Make a regular connectivity
  Architecture->ConnectRegular (Reservoir, K_REGUL, &pparam); //15
  //Architecture->XMLsave ((string("architectures/maier-miller-1-")+NAME+".xml").c_str());
  //Architecture->XMLsave ((string("architectures/maier-miller-rand-")+NAME+".xml").c_str());
  Architecture->XMLsave ((string("architectures/regular-")+NAME+".xml").c_str());
  exit (0);
  /**/
  
  /* SmallWorld */    
  Architecture->ConnectRegularAndRewire (Reservoir, K_REGUL, P_REWIRE, &pparam); //
  Architecture->XMLsave ((string("architectures/smallworld-")+NAME+".xml").c_str());
  //exit (0);
  /**/
  
}

//#        else
#endif





/****************************************************************************/
CManager::CManager (CArchitecture * pArchitecture, CSimulation * pSimulation, CPatternManager * pPatternManager)
{
  /* Initialising few parameters (random seed, tabbed exponential ... */

  InitTools ();

  PatternManager = PatternManagerGlobalPointer = pPatternManager;
  Architecture = ArchitectureGlobalPointer = pArchitecture;


  // Setting simulation
  EventQueue.WatchEvent (EVT_SPIKE);
  EventQueue.WatchEvent (EVT_SPIKE_IMPACT);
  //EventQueue.WatchEvent (EVT_SPIKING_RATIO);
  EventQueue.WatchEvent (EVT_RECURSIVE);
  EventQueue.WatchEvent (EVT_SAVE_SPIKE_PSP);
  EventQueue.WatchEvent (EVT_NEXT_PHASE);

  EventQueue.enqueueSimulation (pSimulation);

}


/****************************************************************************/
CManager::~CManager ()
{
  delete Architecture;
}

/****************************************************************************/
void CManager::generateGnuplotFile ()
{
  cout << "Generating gnuplot command file, please wait..." << flush;
  
  /* Writes gnuplot command files */

  NNLtime Current = 0;
  int Gabarit = 9;
  NNLtime SpikeRasterRange = 2000*CParameters::TIMESCALE;
  NNLtime WeightDistRange = 2000*CParameters::TIMESCALE*10;
  NNLtime DelayDistRange = 2000*CParameters::TIMESCALE*10;
                        //200000
  
  ofstream * gnuplotFile = new ofstream ((CParameters::FIGURE_SCRIPTS_DIR + "autoGeneratedWeightDistributions" + "." + CParameters::FIGURE_SCRIPTS_EXT).c_str(), ios::out|ios::binary|ios::trunc);
  (*gnuplotFile) << setfill ('0');
  for (Current = 0; Current < EventQueue.getTotalSimulationLength (); Current += WeightDistRange)
  { 
    (*gnuplotFile) << (int) 5;
    (*gnuplotFile) << endl;//"";// << endl; 
    (*gnuplotFile) << "################ Weight distributions ##############" << endl;
    (*gnuplotFile) << "reset" << endl;
    (*gnuplotFile) << "" << endl;
    (*gnuplotFile) << "set terminal postscript eps enhanced color defaultplex \"Times-Roman\" 10" << endl;
    (*gnuplotFile) << "set size 1.3,1.3" << endl;
    (*gnuplotFile) << "" << endl;
    (*gnuplotFile) << "set yrange [" << Current << ":" << Current+WeightDistRange << "]" << endl;
    (*gnuplotFile) << "set ticslevel 0" << endl;
    (*gnuplotFile) << "set grid xtics ytics ztics" << endl;
    (*gnuplotFile) << "set border 240" << endl;
    //(*gnuplotFile) << "set border 32" << endl;
    //(*gnuplotFile) << "set border 64" << endl;
    //(*gnuplotFile) << "set border 128" << endl;
    (*gnuplotFile) << "" << endl;
    (*gnuplotFile) << "set output \"" << CParameters::FIGURES_DIR << "WeightDistribution-Excit-" << setw(Gabarit) << Current << "-" << setw(Gabarit) << Current+WeightDistRange << "." << CParameters::FIGURES_EXT << "\"" << endl;
    (*gnuplotFile) << "set title 'Distribution des poids excitateurs au cours du temps'" << endl;
    (*gnuplotFile) << "set xlabel 'Tranches de poids'" << endl;
    (*gnuplotFile) << "set ylabel 'Temps'" << endl;
    (*gnuplotFile) << "set zlabel 'Nombre de poids'" << endl;
    (*gnuplotFile) << "splot \"" << CParameters::DATA_OUTPUT_DIR << "WeightDistribution-Excit-Reservoir." << CParameters::DATA_OUTPUT_EXT << "\" w lines" << endl;
    (*gnuplotFile) << "" << endl;
    (*gnuplotFile) << "set output \"" << CParameters::FIGURES_DIR << "WeightDistribution-Inhib-" << setw(Gabarit) << Current << "-" << setw(Gabarit) << Current+WeightDistRange << "." << CParameters::FIGURES_EXT << "\"" << endl;
    (*gnuplotFile) << "set title 'Distribution des poids inhibiteurs au cours du temps'" << endl;
    (*gnuplotFile) << "splot \"" << CParameters::DATA_OUTPUT_DIR << "WeightDistribution-Inhib-Reservoir." << CParameters::DATA_OUTPUT_EXT << "\" w lines" << endl;
  }
  gnuplotFile->close ();
  delete gnuplotFile;

  gnuplotFile = new ofstream ((CParameters::FIGURE_SCRIPTS_DIR + "autoGeneratedDelayDistributions" + "." + CParameters::FIGURE_SCRIPTS_EXT).c_str(), ios::out|ios::binary|ios::trunc);
  (*gnuplotFile) << setfill ('0');
  for (Current = 0; Current < EventQueue.getTotalSimulationLength (); Current += DelayDistRange)
  {
    (*gnuplotFile) << "" << endl; 
    (*gnuplotFile) << "################ Delay distributions ##############" << endl;
    (*gnuplotFile) << "reset" << endl;
    (*gnuplotFile) << "" << endl;
    (*gnuplotFile) << "set terminal postscript eps enhanced color defaultplex \"Times-Roman\" 10" << endl;
    (*gnuplotFile) << "set size 1.3,1.3" << endl;
    (*gnuplotFile) << "" << endl;
    (*gnuplotFile) << "set yrange [" << Current << ":" << Current+DelayDistRange << "]" << endl;
    (*gnuplotFile) << "set ticslevel 0" << endl;
    (*gnuplotFile) << "set grid xtics ytics ztics" << endl;
    (*gnuplotFile) << "set border 240" << endl;
    (*gnuplotFile) << "" << endl;
    (*gnuplotFile) << "set output \"" << CParameters::FIGURES_DIR << "DelayDistributionExcit-" << setw(Gabarit) << Current << "-" << setw(Gabarit) << Current+DelayDistRange << "." << CParameters::FIGURES_EXT << "\"" << endl;
    (*gnuplotFile) << "set title 'Distribution des delais excitateurs au cours du temps'" << endl;
    (*gnuplotFile) << "set xlabel 'Tranches de delais'" << endl;
    (*gnuplotFile) << "set ylabel 'Temps'" << endl;
    (*gnuplotFile) << "set zlabel 'Nombre de delais'" << endl;
    (*gnuplotFile) << "#splot \"" << CParameters::DATA_OUTPUT_DIR << "DelayDistribution-ExcitReservoir." << CParameters::DATA_OUTPUT_EXT << "\" w lines" << endl;
    (*gnuplotFile) << "splot \"" << CParameters::DATA_OUTPUT_DIR << "DelayDistribution-Output." << CParameters::DATA_OUTPUT_EXT << "\" w lines" << endl;
    (*gnuplotFile) << "" << endl;
    (*gnuplotFile) << "#set output \"" << CParameters::FIGURES_DIR << "DelayDistributionInhib-" << setw(Gabarit) << Current << "-" << setw(Gabarit) << Current+DelayDistRange << "." << CParameters::FIGURES_EXT << "\"" << endl;
    (*gnuplotFile) << "#set title 'Distribution des delais inhibiteurs au cours du temps'" << endl;
    (*gnuplotFile) << "#splot \"" << CParameters::DATA_OUTPUT_DIR << "DelayDistribution-InhibReservoir." << CParameters::DATA_OUTPUT_EXT << "\" w lines" << endl;
  }
  gnuplotFile->close ();
  delete gnuplotFile;
  
  gnuplotFile = new ofstream ((CParameters::FIGURE_SCRIPTS_DIR + "autoGeneratedSpikeRasters" + "." + CParameters::FIGURE_SCRIPTS_EXT).c_str(), ios::out|ios::binary|ios::trunc);
  (*gnuplotFile) << setfill ('0');
  for (Current = (NNLtime) 0; Current < EventQueue.getTotalSimulationLength (); Current += SpikeRasterRange)
  {
    (*gnuplotFile) << "################ Spike raster plot with spike ratio ##############" << endl;
    (*gnuplotFile) << "reset" << endl;
    (*gnuplotFile) << "" << endl;
    (*gnuplotFile) << "set terminal postscript eps enhanced color defaultplex \"Times-Roman\" 10" << endl;
    (*gnuplotFile) << "" << endl;
    (*gnuplotFile) << "unset key" << endl;
    (*gnuplotFile) << "set output \"" << CParameters::FIGURES_DIR << "SpikeRasterPlot-" << setw(Gabarit) << Current << "-" << setw(Gabarit) << Current+SpikeRasterRange << "." << CParameters::FIGURES_EXT << "\"" << endl;
    (*gnuplotFile) << "set xlabel 'Temps'" << endl;
    (*gnuplotFile) << "set ylabel 'Neurones'" << endl;
    (*gnuplotFile) << "set pointsize 0.2" << endl;
    (*gnuplotFile) << "set xrange ["<< Current << ":" << Current + SpikeRasterRange << "]" << endl;
    (*gnuplotFile) << "plot \"" << CParameters::DATA_OUTPUT_DIR << "SpikeRasterExcit." << CParameters::DATA_OUTPUT_EXT << "\" w points, \"" << CParameters::DATA_OUTPUT_DIR << "SpikeRasterInhib." << CParameters::DATA_OUTPUT_EXT << "\" w points" << endl;
    
    /*(*gnuplotFile) << "plot \"SpikeRasterExcit-aa\" w points lt 1,\\" << endl;
    (*gnuplotFile) << "     \"SpikeRasterExcit-ab\" w points lt 1,\\" << endl;
    (*gnuplotFile) << "     \"SpikeRasterExcit-ac\" w points lt 1,\\" << endl;
    (*gnuplotFile) << "     \"SpikeRasterExcit-ad\" w points lt 1,\\" << endl;
    (*gnuplotFile) << "     \"SpikeRasterExcit-ae\" w points lt 1,\\" << endl;
    (*gnuplotFile) << "     \"SpikeRasterExcit-af\" w points lt 1,\\" << endl;
    (*gnuplotFile) << "     \"SpikeRasterExcit-ag\" w points lt 1,\\" << endl;
    (*gnuplotFile) << "     \"SpikeRasterInhib-aa\" w points lt 2,\\" << endl;
    (*gnuplotFile) << "     \"SpikeRasterInhib-ab\" w points lt 2,\\" << endl;
    (*gnuplotFile) << "     \"SpikeRasterInhib-ac\" w points lt 2,\\" << endl;
    (*gnuplotFile) << "     \"SpikeRasterInhib-ad\" w points lt 2,\\" << endl;
    (*gnuplotFile) << "     \"SpikeRasterInhib-ae\" w points lt 2,\\" << endl;
    (*gnuplotFile) << "     \"SpikeRasterInhib-af\" w points lt 2,\\" << endl;
    (*gnuplotFile) << "     \"SpikeRasterInhib-ag\" w points lt 2" << endl;*/
    
    /*(*gnuplotFile) << "set origin 0.000,0.7" << endl;
    (*gnuplotFile) << "set size  1.0,0.3" << endl;
    (*gnuplotFile) << "set noxtics" << endl;
    (*gnuplotFile) << "set yrange [0:1.0]" << endl;
    (*gnuplotFile) << "set ytics 0.1" << endl;
    (*gnuplotFile) << "set style line 1 lw 1 lt 1" << endl;
    (*gnuplotFile) << "#set style line 2 lw 1 lt 0" << endl;
    (*gnuplotFile) << "set title 'Diagramme de Spike'" << endl;
    (*gnuplotFile) << "unset xlabel" << endl;
    (*gnuplotFile) << "set ylabel 'Ratio de neurones actifs'" << endl;
    (*gnuplotFile) << "#plot \"" << CParameters::DATA_OUTPUT_DIR << "SpikeRatio-Output." << CParameters::DATA_OUTPUT_EXT << "\" w lines ls 1" << endl;
    (*gnuplotFile) << "plot \"" << CParameters::DATA_OUTPUT_DIR << "SpikeRatio-Reservoir." << CParameters::DATA_OUTPUT_EXT << "\" w lines ls 1" << endl;
    (*gnuplotFile) << "#plot \"" << CParameters::DATA_OUTPUT_DIR << "SpikeRatio-ExcitReservoir." << CParameters::DATA_OUTPUT_EXT << "\" w lines ls 1, \"" << CParameters::DATA_OUTPUT_DIR << "SpikeRatio-InhibReservoir." << CParameters::DATA_OUTPUT_EXT << "\" w lines ls 2" << endl;
    (*gnuplotFile) << "#plot \"" << CParameters::DATA_OUTPUT_DIR << "SpikeRatio-ExcitReservoir." << CParameters::DATA_OUTPUT_EXT << "\" w dots, \"" << CParameters::DATA_OUTPUT_DIR << "SpikeRatio-InhibReservoir." << CParameters::DATA_OUTPUT_EXT << "\" w dots" << endl;
    (*gnuplotFile) << "" << endl;
    (*gnuplotFile) << "unset multiplot" << endl;*/
    (*gnuplotFile) << "" << endl;
  }
  gnuplotFile->close ();
  delete gnuplotFile;
  
  /* Say goodbye */
  cout << "done." << endl;
}

/****************************************************************************/
void CManager::Run ()
{
  time_t Start = time (NULL);
  time_t End;
  
  NNLevent evt;
  
  bool Finished = false;
  
  vector<CAssembly *>::iterator Current;
  vector<CAssembly *>::iterator EndAss;
  
  generateGnuplotFile ();
  
  cout.fill(' ');
  
  long int total = EventQueue.getTotalSimulationLength ()/CParameters::TIMESCALE; // convert time to ms timescale
  long int Hours = total/(1000*60*60);
  long int Minutes = (total%(1000*60*60))/(1000*60);
  long int Seconds = ((total%(1000*60*60))%(1000*60))/1000;
  long int Millieme = (((total%(1000*60*60)))%(1000*60))%1000;

  cout << "Starting simulation with virtual time duration : " << Hours << "h " << Minutes << "m " << Seconds << "." << Millieme << "s (" << EventQueue.getTotalSimulationLength () << "ms)" << endl;
  
  //Starting first phase
  EventQueue.PostNextPhaseEvents ();
  
  while (!Finished)
  { 
    if (CurrentTime % 100 == 0)
      cout << " \r*** Time " << setw(6) << CurrentTime << " *** \t Estimated remaining time: " << setw(2) << Hours << "h " << setw(2) << Minutes << "m " << setw(2) << Seconds << "s (" << setw (2) << (unsigned long int) ((((double)(CurrentTime)) / ((double)EventQueue.getTotalSimulationLength ()))*100) << "% done) " << flush;
    
    //Current = Architecture->getAssemblies ()->begin ();
    //EndAss = Architecture->getAssemblies ()->end ();
    
    //~ /* Say the spike ratio for each assembly at this time, only if the last calculation did not 
    //~ occur at CurrentTime-1 : the calculation request for this time would already have been posted (see 17 lines below) */
    //~ while (Current != EndAss)
    //~ {
      //~ if ((*Current)->getLastSpike () != CurrentTime - 1)
      //~ {
        //~ evt.EventType = EVT_SPIKING_RATIO;
        //~ evt.Assembly = (*Current);
        //~ evt.Time = CurrentTime;
        //~ evt.Ratio = (float) ((float)(*Current)->getNbSpikes () / (float)(*Current)->getNeurons ()->size ());
        
        //~ EventQueue.push (evt);
      //~ }
      //~ Current++;
    //~ }
    
    Architecture->Run ();
  
    //~ /* Say we want to calculate spike ratio for each assembly at the next time */
    //~ Current = Architecture->getAssemblies ()->begin ();
    //~ while (Current != EndAss)
    //~ {
      
      //~ if ((*Current)->hasSpiked ())
      //~ {
        //~ evt.EventType = EVT_SPIKING_RATIO;
        //~ evt.Assembly = (*Current);
        //~ evt.Time = CurrentTime + 1;
        //~ evt.Ratio = -1;
        
        //~ EventQueue.push (evt);
        
        //~ (*Current)->setLastSpike (CurrentTime);
      //~ }
      
      //~ Current++;
    //~ }
    
    NNLevent evtTmp = EventQueue.top ();
    
    if (!EventQueue.empty ())
    {
      
      if (evtTmp.Time < CurrentTime) 
      {
        cout << endl << " *** WARNING *** EventTime < CurrentTime: " << evtTmp.Time << " Current time: " << CurrentTime << " event type: " << evtTmp.EventType << endl;
        if (evtTmp.EmittingNeuron != NULL)
          cout << " *** WARNING *** EmittingNeuron: " << evtTmp.EmittingNeuron->getID() << "(" << evtTmp.EmittingNeuron->getLocalID () << ")" << endl;
        if (evtTmp.Assembly != NULL)
          cout << "from " << evtTmp.Assembly->getName () << endl;
      }
      CurrentTime = EventQueue.top ().Time;
    }
    else
    {
      //cout << "bla1" << endl;
      Finished = true;
    }
    
    if (CurrentTime > EventQueue.getTotalSimulationLength ())
    {
      //cout << "bla2" << endl;
      Finished = true;
    }
    
    if (CurrentTime && (CurrentTime%100 == 0))
      TimeRemaining ( (((float)CurrentTime) / ((float)EventQueue.getTotalSimulationLength ()))*100.0, (long int) difftime (time (NULL), Start), &Hours, &Minutes, &Seconds);
  }
  
  End = time (NULL);

  total = (long int) difftime (End, Start);
  Hours = total/(60*60);
  Minutes = (total%(60*60))/60;
  Seconds = (total%(60*60))%60;
  
  
  //Current = Architecture->getAssemblies ()->begin ();
  //Current += 2;
  //(*Current)->ShowStats ();
  
  cout << endl << "Execution time: " << Hours << "h " << Minutes << "m " << Seconds << "s\a" << endl;

  //char XMLOutputFilename[256];
  
  //if (ManagerGlobalPointer->ArchiFile == NULL)
  //{
  //  sprintf (XMLOutputFilename, (CParameters::ARCHITECTURE_OUTPUT_DIR + CParameters::ARCHITECTURE_XML_FILE).c_str(), CurrentTime, RandSeed, Architecture->getAssemblyByName("Reservoir")->getNeurons ()->size (), Architecture->getAssemblyByName("Output")->getNeurons ()->size (), CParameters::TIMESCALE);
  //  Architecture->XMLsave (XMLOutputFilename);
  //}
  //else
  //{
  //  sprintf (XMLOutputFilename, (CParameters::ARCHITECTURE_OUTPUT_DIR + string("02-") + (strstr(ArchiFile, "/")+1)).c_str());
  //  cout << "Saving network to " << XMLOutputFilename << " ... " << flush;
  //  Architecture->XMLsave (XMLOutputFilename);
  //  cout << "done." << endl;
 // }
  
  //Architecture->XMLsave ((string("architectures/icann-net-simu-aftersim-")+NAME+".xml").c_str());
    
  //assTmp->GenerateWeightMatrix (CParameters::DATA_OUTPUT_DIR+string("WeightMatrix-fin.txt"));
  
}
