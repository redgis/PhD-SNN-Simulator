/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef _EVENTFUNCTIONS_H
#define _EVENTFUNCTIONS_H


#include "../simulator/globals.h"
#include "../events/event.h"

using namespace std;

extern "C"
{


typedef void (*TFunc) (CAssembly *, TFuncParam *);

extern const void * Functions[][2];

/****************************************************************************/
TFunc getFunctionByName (const char * FuncName);

/****************************************************************************/
const char * getFunctionByPtr (TFunc func);

/****************************************************************************/
void registerFunctions ();





/****************************************************************************/
void StopSTDP (CAssembly * ass, TFuncParam * fparam = NULL);

/****************************************************************************/
void StartSTDP (CAssembly * ass, TFuncParam * fparam = NULL);

/****************************************************************************/
void ResetTrainPatternIterator (CAssembly * ass, TFuncParam * fparam = NULL);

/****************************************************************************/
void ResetTestPatternIterator (CAssembly * ass, TFuncParam * fparam = NULL);

/****************************************************************************/
void RandomInputs (CAssembly * ass, TFuncParam * fparam = NULL);

/****************************************************************************/
void PostInputs1 (CAssembly * ass, TFuncParam * fparam = NULL);

/****************************************************************************/
void PostInputs2 (CAssembly * ass, TFuncParam * fparam = NULL);

/****************************************************************************/
void PostNoisyInputs1 (CAssembly * ass, TFuncParam * fparam = NULL);

/****************************************************************************/
void PostNoisyInputs2 (CAssembly * ass, TFuncParam * fparam = NULL);

/****************************************************************************/
void RandomNoisyInputClass (CAssembly * ass, TFuncParam * fparam = NULL);

/****************************************************************************/
void PostTrainInput (CAssembly * ass, TFuncParam * fparam = NULL);

/****************************************************************************/
void PostTestInput (CAssembly * ass, TFuncParam * fparam = NULL);

/****************************************************************************/
void PostClass1TrainInput (CAssembly * ass, TFuncParam * fparam = NULL);

/****************************************************************************/
void PostClass1TestInput (CAssembly * ass, TFuncParam * fparam = NULL);

/****************************************************************************/
void PostClass2TrainInput (CAssembly * ass, TFuncParam * fparam = NULL);

/****************************************************************************/
void PostClass2TestInput (CAssembly * ass, TFuncParam * fparam = NULL);

/****************************************************************************/
void LearnDelays (CAssembly * ass, TFuncParam * fparam = NULL);

/****************************************************************************/
void InitWeightModif (CAssembly * ass, TFuncParam * fparam = NULL);

/****************************************************************************/
void GenePerfo (CAssembly * ass, TFuncParam * fparam = NULL);

/****************************************************************************/
void InitPerfo (CAssembly * ass, TFuncParam * fparam = NULL);

/****************************************************************************/
void CSInput (CAssembly * ass, TFuncParam * fparam = NULL);
void CSInput2 (CAssembly * ass, TFuncParam * fparam = NULL);
void CSInput3 (CAssembly * ass, TFuncParam * fparam = NULL);
void CSInput4 (CAssembly * ass, TFuncParam * fparam = NULL);
void CSInput5 (CAssembly * ass, TFuncParam * fparam = NULL);
void CSInputOPN (CAssembly * ass, TFuncParam * fparam = NULL);

/****************************************************************************/
void SaveArchitecture (CAssembly * assembly, TFuncParam * fparam = NULL);

/****************************************************************************/
void PostParticularTrainInput (CAssembly * ass, TFuncParam * fparam = NULL);

/****************************************************************************/
void PostParticularTestInput (CAssembly * ass, TFuncParam * fparam = NULL);

}

#endif /* _EVENTFUNCTIONS_H */
