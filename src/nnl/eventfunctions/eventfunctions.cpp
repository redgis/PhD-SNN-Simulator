/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
 
 
#include "eventfunctions.h"

#include "assembly.h"
#include "parameters.h"
#include "neuron.h"
#include "datafile.h"
#include "patternmanager.h"
#include "pattern.h"
#include "architecture.h"
#include "patternset.h"
#include "manager.h"

#include <dlfcn.h>

extern "C"
{

const void * Functions[][2] = {
    { "StopSTDP", (const void*) &StopSTDP },
    { "StartSTDP", (const void*) &StartSTDP },
    { "ResetTrainPatternIterator", (const void*) &ResetTrainPatternIterator },
    { "ResetTestPatternIterator", (const void*) &ResetTestPatternIterator },
    { "RandomInputs", (const void*) &RandomInputs},
    { "PostInputs1", (const void*) &PostInputs1 },
    { "PostInputs2", (const void*) &PostInputs2 },
    { "PostNoisyInputs1", (const void*) &PostNoisyInputs1 },
    { "PostNoisyInputs2", (const void*) &PostNoisyInputs2 },
    { "RandomNoisyInputClass", (const void*) &RandomNoisyInputClass },
    { "PostTrainInput", (const void*) &RandomNoisyInputClass },
    { "PostTestInput", (const void*) &PostTestInput },
    { "PostClass1TrainInput", (const void*) &PostClass1TrainInput },
    { "PostClass1TestInput", (const void*) &PostClass1TestInput },
    { "PostClass2TrainInput", (const void*) &PostClass2TrainInput },
    { "PostClass2TestInput", (const void*) &PostClass2TestInput },
    { "LearnDelays", (const void*) &LearnDelays },
    { "InitWeightModif", (const void*) &InitWeightModif },
    { "GenePerfo", (const void*) &GenePerfo },
    { "InitPerfo", (const void*) &InitPerfo },
    { "CSInput", (const void*) &CSInput },
    { "CSInput2", (const void*) &CSInput2},
    { "CSInput3", (const void*) &CSInput3 },
    { "CSInput4", (const void*) &CSInput4 },
    { "CSInput5", (const void*) &CSInput5 },
    { "CSInputOPN", (const void*) &CSInputOPN },
    { "SaveArchitecture", (const void*) &SaveArchitecture },
    { "PostParticularTrainInput", (const void*) &PostParticularTrainInput },
    { "PostParticularTestInput", (const void*) &PostParticularTestInput },
    { "NULL", (const void*) 0 },
};


/****************************************************************************/
TFunc getFunctionByName (const char * FuncName)
{
  for (int Index = 0; Index < (int) sizeof (Functions); Index ++)
  {
    if (strcmp((char *) Functions[Index][0], FuncName) == 0 )
    {
      return (TFunc) Functions[Index][1];
    }
  }
  return NULL;
}

/****************************************************************************/
const char * getFunctionByPtr (TFunc func)
{
  int Index = 0;

  while( strcmp ((const char *) Functions[Index][0], "NULL") != 0)
  {
    if ((Functions[Index][1]) == (void*) func)
    {
      return (const char *) Functions[Index][0];
    }

    Index++;
  }

  return NULL;
}

/****************************************************************************/
void registerFunctions ()
{
  int Index = 0;
  while (strcmp((const char*) Functions[Index][0], "NULL") != 0)
  {
    Functions[Index][1] = dlsym (RTLD_DEFAULT, (const char*) Functions[Index][0]);
    if (Functions[Index][1] == NULL)
    {
      cout << dlerror () << endl;
      exit (1);
    }

    Index ++;
  }
}

/****************************************************************************/
void StopSTDP (CAssembly * ass, TFuncParam * fparam)
{
  //cout << endl << "*** Stop Learning ***" << endl;
  UnsupervisedLearning = false; //false;
}

/****************************************************************************/
void StartSTDP (CAssembly * ass, TFuncParam * fparam)
{
  //cout << endl << "*** Stop Learning ***" << endl;
  UnsupervisedLearning = true; //false;
}

/****************************************************************************/
void RandomInputs (CAssembly * ass, TFuncParam * fparam)
{
  //cout << endl << "*** Random inputs ***" << endl;
  vector<CNeuron *>::iterator itNeurons = ass->getNeurons()->begin();
  NNLtime emissionTime;
  double Alea;
    
    
  while (itNeurons != ass->getNeurons()->end())
  {
    Alea = (double)(round(((float)rand() / (float)RAND_MAX)*20.0));
    if (Alea > 15)
    {
      emissionTime = CurrentTime + (int) ( (((float)rand()) / ((float)RAND_MAX)) * 20.0 * CParameters::TIMESCALE );
      ((CNeuron *)(*itNeurons))->emitSpike (emissionTime);
    }
    
    itNeurons++;
  }
  
}

/****************************************************************************/
void ResetTrainPatternIterator (CAssembly * ass, TFuncParam * fparam)
{
  PatternManagerGlobalPointer->getTrainPatternSet ()->InitPatternIterator ();
}

/****************************************************************************/
void ResetTestPatternIterator (CAssembly * ass, TFuncParam * fparam)
{
  PatternManagerGlobalPointer->getTestPatternSet ()->InitPatternIterator ();
}

/****************************************************************************/
void PostInputs1 (CAssembly * ass, TFuncParam * fparam)
{

  /* toy net tests */
  /* 
  CurrentTime = BackupTime + 0;
  (*(itNeurons+0))->emitSpike();
  
  CurrentTime = BackupTime + 10;
  (*(itNeurons+1))->emitSpike();
  
  CurrentTime = BackupTime + 10;
  (*(itNeurons+2))->emitSpike();
  */
  /*
  CurrentTime = BackupTime + 0;
  (*(itNeurons+57))->emitSpike();
  
  CurrentTime = BackupTime + 4;
  (*(itNeurons+62))->emitSpike();
  
  CurrentTime = BackupTime + 5;
  (*(itNeurons+71))->emitSpike();
  */

  /* dim 10  front montant
  vector<CNeuron *>::iterator itNeurons = ass->getNeurons()->begin();
  NNLtime BackupTime = CurrentTime;
  
  while (itNeurons != ass->getNeurons()->end())
  {
    ((CNeuron *)(*itNeurons))->emitSpike();

    CurrentTime += 2;
    
    itNeurons++;
  }
  
  CurrentTime = BackupTime;
  CurrentClass = 1;
  */
  
  
  /* dim 10 a 100 escalier montant 
  vector<CNeuron *>::iterator itNeurons = ass->getNeurons()->begin();
  NNLtime BackupTime = CurrentTime;
  
  unsigned int Index = 0;
  
  while (itNeurons != ass->getNeurons()->end())
  {
    ((CNeuron *)(*itNeurons))->emitSpike();

    if (Index == ass->getNeurons ()->size () / 2)
      CurrentTime += 20;
    
    Index ++;
    itNeurons++;
  }
  
  CurrentTime = BackupTime;
  CurrentClass = 1;
  */
  
  /* dim 100  dents de scie montante 
  vector<CNeuron *>::iterator itNeurons = ass->getNeurons()->begin();
  NNLtime BackupTime = CurrentTime;
  
  unsigned int Index = 0;
  
  while (itNeurons != ass->getNeurons()->end())
  {
    ((CNeuron *)(*itNeurons))->emitSpike();

    if ((Index/10)%2)
      CurrentTime -= 2;
    else
      CurrentTime += 2;
    
    Index ++;
    itNeurons++;
  }
  
  CurrentTime = BackupTime;
  CurrentClass = 1;
  */
  
  /* dim 100   diagonale montante 
  vector<CNeuron *>::iterator itNeurons = ass->getNeurons()->begin();
  NNLtime BackupTime = CurrentTime;
  
  unsigned int Index = 0;
  
  while (itNeurons != ass->getNeurons()->end())
  {
    ((CNeuron *)(*itNeurons))->emitSpike();

    CurrentTime += 1;
    
    Index ++;
    itNeurons++;
  }
  
  CurrentTime = BackupTime;
  CurrentClass = 1;
  */
}

/****************************************************************************/
void PostInputs2 (CAssembly * ass, TFuncParam * fparam)
{
  /* dim 10   front descendant
  vector<CNeuron *>::iterator itNeurons = ass->getNeurons()->begin();
  NNLtime BackupTime = CurrentTime;
  
  CurrentTime += 20;
  
  while (itNeurons != ass->getNeurons()->end())
  {
    CurrentTime -= 2;
    
    ((CNeuron *)(*itNeurons))->emitSpike();
    
    itNeurons++;
  }
  
  CurrentTime = BackupTime;
  CurrentClass = 2; */
  
  /* dim 10 �� 100 escalier descendant
  vector<CNeuron *>::iterator itNeurons = ass->getNeurons()->begin();
  NNLtime BackupTime = CurrentTime;
  
  unsigned int Index = 0;
  
  CurrentTime += 20;
  
  while (itNeurons != ass->getNeurons()->end())
  {
    if (Index == ass->getNeurons ()->size () / 2)
      CurrentTime -= 20;
    
    ((CNeuron *)(*itNeurons))->emitSpike();
    
    Index++;
    itNeurons++;
  }
  
  CurrentTime = BackupTime;
  CurrentClass = 2;*/
  
  /* dim 100  dents de scie descendante
  vector<CNeuron *>::iterator itNeurons = ass->getNeurons()->begin();
  NNLtime BackupTime = CurrentTime;
  
  CurrentTime += 20;
  
  unsigned int Index = 0;
  
  while (itNeurons != ass->getNeurons()->end())
  {
    ((CNeuron *)(*itNeurons))->emitSpike();

    if ((Index/10)%2)
      CurrentTime += 2;
    else
      CurrentTime -= 2;
    
    Index ++;
    itNeurons++;
  }
  
  CurrentTime = BackupTime;
  CurrentClass = 2; */
  
  /* dim 100  diagonale descendante
  /*vector<CNeuron *>::iterator itNeurons = ass->getNeurons()->begin();
  NNLtime BackupTime = CurrentTime;
  
  CurrentTime += 100;
  
  unsigned int Index = 0;
  
  while (itNeurons != ass->getNeurons()->end())
  {
    ((CNeuron *)(*itNeurons))->emitSpike();

    CurrentTime -= 1;
    
    Index ++;
    itNeurons++;
  }
  
  CurrentTime = BackupTime;
  CurrentClass = 2;*/
  
}

/****************************************************************************/
void PostNoisyInputs1 (CAssembly * ass, TFuncParam * fparam)
{
  /* dim 10   front montant
  vector<CNeuron *>::iterator itNeurons = ass->getNeurons()->begin();
  NNLtime BackupTime = CurrentTime;

  int Alea;
  

  
  while (itNeurons != ass->getNeurons()->end())
  {
    CurrentTime += 2;
    // Alea = (int)(round(((float)rand() / (float)RAND_MAX)*2.0*NOISE-NOISE));
    Alea = (int)(round(((float)rand() / (float)RAND_MAX)*2.0*NOISE));
    CurrentTime += Alea;

    // cout << "aleatoire = " << Alea-NOISE << "\t noisy pattern 1 input time : " << setw(6) << CurrentTime << endl;
    
    ((CNeuron *)(*itNeurons))->emitSpike();

    CurrentTime -= Alea;

    itNeurons++;
  }
  
  CurrentTime = BackupTime;
  CurrentClass = 1; */
  
  
  /* dim 10 �� 100  escalier montant
  vector<CNeuron *>::iterator itNeurons = ass->getNeurons()->begin();
  NNLtime BackupTime = CurrentTime;

  int Alea;
  
  unsigned int Index = 0;
  
  while (itNeurons != ass->getNeurons()->end())
  {
    if (Index == ass->getNeurons ()->size () / 2)
    CurrentTime += 20;
    // Alea = (int)(round(((float)rand() / (float)RAND_MAX)*2.0*NOISE-NOISE));
    Alea = (int)(round(((float)rand() / (float)RAND_MAX)*2.0*NOISE));
    CurrentTime += Alea;

    // cout << "aleatoire = " << Alea-NOISE << "\t noisy pattern 1 input time : " << setw(6) << CurrentTime << endl;
    
    ((CNeuron *)(*itNeurons))->emitSpike();

    CurrentTime -= Alea;

    Index ++;
    itNeurons++;
  }
  
  CurrentTime = BackupTime;
  CurrentClass = 1; */
  
  
  /* dim 100  dents de scie descendant 
  vector<CNeuron *>::iterator itNeurons = ass->getNeurons()->begin();
  NNLtime BackupTime = CurrentTime;

  int Alea;
  
  unsigned int Index = 0;
  
  while (itNeurons != ass->getNeurons()->end())
  {
    
    if ((Index/10)%2)
      CurrentTime -= 2;
    else
      CurrentTime += 2;
    
    // Alea = (int)(round(((float)rand() / (float)RAND_MAX)*2.0*NOISE-NOISE));
    Alea = (int)(round(((float)rand() / (float)RAND_MAX)*2.0*NOISE));
    CurrentTime += Alea;

    // cout << "aleatoire = " << Alea-NOISE << "\t noisy pattern 1 input time : " << setw(6) << CurrentTime << endl;
    
    ((CNeuron *)(*itNeurons))->emitSpike();

    CurrentTime -= Alea;

    Index ++;
    itNeurons++;
  }
  
  CurrentTime = BackupTime;
  CurrentClass = 1;*/
  
  /* dim 100  diagonale descendant */
  vector<CNeuron *>::iterator itNeurons = ass->getNeurons()->begin();
  NNLtime emissionTime = CurrentTime;

  int Alea;
  
  unsigned int Index = 0;
  
  while (itNeurons != ass->getNeurons()->end())
  {
    
    emissionTime += 1;
    
    // Alea = (int)(round(((float)rand() / (float)RAND_MAX)*2.0*NOISE-NOISE));
    Alea = (int)(round(((float)rand() / (float)RAND_MAX)*2.0*(fparam->Param1)));
    emissionTime += Alea;

    // cout << "aleatoire = " << Alea-NOISE << "\t noisy pattern 1 input time : " << setw(6) << CurrentTime << endl;
    
    ((CNeuron *)(*itNeurons))->emitSpike(emissionTime);

    emissionTime -= Alea;

    Index ++;
    itNeurons++;
  }
  
  CurrentClass = 1;
  CurrentClassTime = CurrentTime;
}

/****************************************************************************/
void PostNoisyInputs2 (CAssembly * ass, TFuncParam * fparam)
{
  /* dim : 10   front descendant
  vector<CNeuron *>::iterator itNeurons = ass->getNeurons()->begin();
  NNLtime BackupTime = CurrentTime;

  int Alea;

  CurrentTime += ass->getNeurons()->size() * 2 + 2;
  
  while (itNeurons != ass->getNeurons()->end())
  {
    CurrentTime -= 2;
    // Alea = (int)(round(((float)rand() / (float)RAND_MAX)*2.0*NOISE-NOISE));
    Alea = (int)(round(((float)rand() / (float)RAND_MAX)*2.0*NOISE));
    CurrentTime += Alea;

    // cout << "noisy pattern 2 input time : " << setw(6) << CurrentTime << endl;
    
    ((CNeuron *)(*itNeurons))->emitSpike();
    CurrentTime -= Alea;

    itNeurons++;
  }

  CurrentTime = BackupTime;
  CurrentClass = 2; */
  
  
  /* dim 10 �� 100   escalier descendant
  vector<CNeuron *>::iterator itNeurons = ass->getNeurons()->begin();
  NNLtime BackupTime = CurrentTime;

  unsigned int Index = 0;

  int Alea;

  CurrentTime += 20;
  
  while (itNeurons != ass->getNeurons()->end())
  {
    if (Index == ass->getNeurons ()->size () / 2)
      CurrentTime -= 20;
    // Alea = (int)(round(((float)rand() / (float)RAND_MAX)*2.0*NOISE-NOISE));
    Alea = (int)(round(((float)rand() / (float)RAND_MAX)*2.0*NOISE));
    CurrentTime += Alea;

    // cout << "noisy pattern 2 input time : " << setw(6) << CurrentTime << endl;
    
    ((CNeuron *)(*itNeurons))->emitSpike();
    CurrentTime -= Alea;

    Index ++;
    itNeurons++;
  }

  CurrentTime = BackupTime;
  CurrentClass = 2;*/
  
  /* dim 100  dents de scie descendant 
  vector<CNeuron *>::iterator itNeurons = ass->getNeurons()->begin();
  NNLtime BackupTime = CurrentTime;

  int Alea;
  
  unsigned int Index = 0;
  
  CurrentTime += 20;
  
  while (itNeurons != ass->getNeurons()->end())
  {
    
    if ((Index/10)%2)
      CurrentTime += 2;
    else
      CurrentTime -= 2;
    
    // Alea = (int)(round(((float)rand() / (float)RAND_MAX)*2.0*NOISE-NOISE));
    Alea = (int)(round(((float)rand() / (float)RAND_MAX)*2.0*NOISE));
    CurrentTime += Alea;

    // cout << "aleatoire = " << Alea-NOISE << "\t noisy pattern 1 input time : " << setw(6) << CurrentTime << endl;
    
    ((CNeuron *)(*itNeurons))->emitSpike();

    CurrentTime -= Alea;

    Index ++;
    itNeurons++;
  }
  
  CurrentTime = BackupTime;
  CurrentClass = 2;*/
  
  /* dim 100  diagonale descendante */
  vector<CNeuron *>::iterator itNeurons = ass->getNeurons()->begin();
  NNLtime emissionTime = CurrentTime;

  int Alea;
  
  unsigned int Index = 0;
  
  emissionTime += 100;
  
  while (itNeurons != ass->getNeurons()->end())
  {
    
    emissionTime -= 1;
    
    // Alea = (int)(round(((float)rand() / (float)RAND_MAX)*2.0*NOISE-NOISE));
    Alea = (int)(round(((float)rand() / (float)RAND_MAX)*2.0*(fparam->Param1)));
    emissionTime += Alea;

    // cout << "aleatoire = " << Alea-NOISE << "\t noisy pattern 1 input time : " << setw(6) << CurrentTime << endl;
    
    ((CNeuron *)(*itNeurons))->emitSpike(emissionTime);

    emissionTime -= Alea;

    Index ++;
    itNeurons++;
  }

  CurrentClass = 2;
  CurrentClassTime = CurrentTime;
}

/****************************************************************************/
void RandomNoisyInputClass (CAssembly * ass, TFuncParam * fparam)
{
  if ( rand () > (RAND_MAX / 2) )
    PostNoisyInputs1 (ass);
  else
    PostNoisyInputs2 (ass);
}

/****************************************************************************/
void PostTrainInput (CAssembly * ass, TFuncParam * fparam)
{
  //cout << endl << "*** Training inputs ***" << endl;
  
  /* dim 256 USPS dataset */
    
  double MinValue;
  double MaxValue;
  
  vector<CNeuron *>::iterator itNeurons = ass->getNeurons()->begin();
  NNLtime emissionTime = CurrentTime;
  
  CPattern * nextPattern;
  
  if (NbPresentations <= 0) // when NbPresentations reaches 0 => next pattern and reset to 10
  {
    nextPattern = PatternManagerGlobalPointer->getNextTrainPattern ();
    NbPresentations = fparam->NbStimPresentations - 1;
  }
  else
  {
    nextPattern = PatternManagerGlobalPointer->getCurrentTrainPattern ();
    NbPresentations--;
  }
  
  /* We scale input between 0 and MaxValue-MinValue */
  MinValue = PatternManagerGlobalPointer->getMinTrainValue ();
  MaxValue = PatternManagerGlobalPointer->getMaxTrainValue ();
  MaxValue -= MinValue;
  MinValue = 0;
  
  if (nextPattern == NULL)
  {
    if (NbPresentations <= 0) // when NbPresentations reaches 0 => next pattern and reset to 10
    {
      nextPattern = PatternManagerGlobalPointer->getNextTrainPattern ();
      NbPresentations = fparam->NbStimPresentations - 1;
    }
    else
    {
      nextPattern = PatternManagerGlobalPointer->getCurrentTrainPattern ();
      NbPresentations--;
    }
  }
  
  if (nextPattern == NULL)
  {
    cerr << "\nCannot get next pattern for training !" << endl;
    exit (1);
  }
  
  vector<double>::iterator itFeatures = nextPattern->getPatternVector()->begin ();
  
  while (itNeurons != ass->getNeurons()->end())
  {
    /* we dont spike for minimum values */
    if ((*itFeatures) > MinValue)
    {
      emissionTime = (NNLtime) (CurrentTime + ((MaxValue - (double)(*itFeatures)) * 20 * CParameters::TIMESCALE)/MaxValue);
      if (emissionTime < CurrentTime)
      {
        cout << "MinValue : " << MinValue << endl;
        cout << "MaxValue : " << MaxValue << endl;
        cout << "CurrentTime : " << CurrentTime << endl;
        cout << "*itFeatures : " << *itFeatures << endl;
        cout << "CParameters::TIMESCALE : " << CParameters::TIMESCALE << endl;
        cout << "emissionTime : " << emissionTime << endl;
        
        cout << "(MaxValue - (float)(*itFeatures)) : " << (MaxValue - (double)(*itFeatures)) << endl;
        
        cout << "((MaxValue - (float)(*itFeatures)) * 20 * CParameters::TIMESCALE) : " << ((MaxValue - (double)(*itFeatures)) * 20 * CParameters::TIMESCALE) << endl;
        
        cout << "((MaxValue - (float)(*itFeatures)) * 20 * CParameters::TIMESCALE)/MaxValue : " << ((MaxValue - (double)(*itFeatures)) * 20 * CParameters::TIMESCALE)/MaxValue << endl;
        
        cout << "(NNLtime)(((MaxValue - (float)(*itFeatures)) * 20 * CParameters::TIMESCALE)/MaxValue) : " << (NNLtime)(((MaxValue - (float)(*itFeatures)) * 20 * CParameters::TIMESCALE)/MaxValue) << endl;
        
        cout << "(CurrentTime + (NNLtime)(((MaxValue - (float)(*itFeatures)) * 20 * CParameters::TIMESCALE)/MaxValue)) : " << (CurrentTime + (NNLtime)(((MaxValue - (double)(*itFeatures)) * 20 * CParameters::TIMESCALE)/MaxValue)) << endl;
        
        cout << "(NNLtime) (CurrentTime + ((MaxValue - (float)(*itFeatures)) * 20 * CParameters::TIMESCALE)/MaxValue) : " << (NNLtime) (CurrentTime + (NNLtime)(((MaxValue - (double)(*itFeatures)) * 20 * CParameters::TIMESCALE)/MaxValue)) << endl;
        
        //cout << ((*itFeatures) * 20 * CParameters::TIMESCALE)/MaxValue << " ";
      }
      
      for (int NbRepetition = 0; NbRepetition < fparam->NbStimRepetitionsInInterval; NbRepetition++)
      {
        NNLtime Shift = ((20 * CParameters::TIMESCALE) + fparam->StimRepetitionSeparation) * NbRepetition;
        ((CNeuron *)(*itNeurons))->emitSpike(emissionTime+Shift);
      }
    }

    itFeatures++;
    itNeurons++;
  }
  
  CurrentClass = nextPattern->getPatternClass ();
  CurrentClassTime = CurrentTime;
  
 (*PatternClasses) << CurrentTime << " " << CurrentClass << endl;
}

/****************************************************************************/
void PostTestInput (CAssembly * ass, TFuncParam * fparam)
{
  //cout << endl << "*** Test inputs ***" << endl;
  /* dim 256 USPS dataset */
  
  double MinValue;
  double MaxValue;

  vector<CNeuron *>::iterator itNeurons = ass->getNeurons()->begin();
  NNLtime emissionTime = CurrentTime;
  
  CPattern * nextPattern;
    
  if (NbPresentations <= 0) // when NbPresentations reaches 0 => next pattern and reset to 10
  {
    nextPattern = PatternManagerGlobalPointer->getNextTestPattern ();
    NbPresentations = fparam->NbStimPresentations - 1;
  }
  else
  {
    nextPattern = PatternManagerGlobalPointer->getCurrentTestPattern ();
    NbPresentations--;
  }
  
  /* We scale input between 0 and MaxValue-MinValue */
  MinValue = PatternManagerGlobalPointer->getMinTrainValue ();
  MaxValue = PatternManagerGlobalPointer->getMaxTrainValue ();
  MaxValue -= MinValue;
  MinValue = 0;
  
  if (nextPattern == NULL)
  {
    if (NbPresentations <= 0) // when NbPresentations reaches 0 => next pattern and reset to 10
    {
      nextPattern = PatternManagerGlobalPointer->getNextTestPattern ();
      NbPresentations = fparam->NbStimPresentations - 1;
    }
    else
    {
      nextPattern = PatternManagerGlobalPointer->getCurrentTestPattern ();
      NbPresentations--;
    }
  }
  
  if (nextPattern == NULL)
  {
    cerr << "\nCannot get next pattern for generalization !" << endl;
    exit (1);
  }
  
  vector<double>::iterator itFeatures = nextPattern->getPatternVector()->begin ();
    
  while (itNeurons != ass->getNeurons()->end())
  {
    if ((*itFeatures) > MinValue)
    {
      emissionTime = (NNLtime) (CurrentTime + ((MaxValue - ((*itFeatures) - MinValue)) * 20 * CParameters::TIMESCALE)/MaxValue);
      
      for (int NbRepetition = 0; NbRepetition < fparam->NbStimRepetitionsInInterval; NbRepetition++)
      {
        NNLtime Shift = ((20 * CParameters::TIMESCALE) + fparam->StimRepetitionSeparation) * NbRepetition;
        ((CNeuron *)(*itNeurons))->emitSpike(emissionTime+Shift);
      }
    }

    itFeatures++;
    itNeurons++;
  }
  
  CurrentClass = nextPattern->getPatternClass ();
  CurrentClassTime = CurrentTime;
  
  (*PatternClasses) << CurrentTime << " " << CurrentClass << endl;
}

/****************************************************************************/
void PostClass1TrainInput (CAssembly * ass, TFuncParam * fparam)
{
  //cout << endl << "*** Training inputs ***" << endl;
  
  /* dim 256 USPS dataset */
    
  double MinValue;
  double MaxValue;
  
  vector<CNeuron *>::iterator itNeurons = ass->getNeurons()->begin();
  NNLtime emissionTime = CurrentTime;
  
  CPattern * nextPattern;
  
  if (NbPresentations <= 0) // when NbPresentations reaches 0 => next pattern and reset to 10
  {
    nextPattern = PatternManagerGlobalPointer->getNextClassTrainPattern (0);
    NbPresentations = fparam->NbStimPresentations - 1;
  }
  else
  {
    nextPattern = PatternManagerGlobalPointer->getCurrentTrainPattern ();
    NbPresentations--;
  }

  /* We scale input between 0 and MaxValue-MinValue */
  MinValue = PatternManagerGlobalPointer->getMinTrainValue ();
  MaxValue = PatternManagerGlobalPointer->getMaxTrainValue ();
  MaxValue -= MinValue;
  MinValue = 0;
  
  if (nextPattern == NULL)
  {
    if (NbPresentations <= 0) // when NbPresentations reaches 0 => next pattern and reset to 10
    {
      nextPattern = PatternManagerGlobalPointer->getNextClassTrainPattern (0);
      NbPresentations = fparam->NbStimPresentations - 1;
    }
    else
    {
      nextPattern = PatternManagerGlobalPointer->getCurrentTrainPattern ();
      NbPresentations--;
    }
  }
  
  if (nextPattern == NULL)
  {
    cerr << "\nCannot get next pattern for training !" << endl;
    exit (1);
  }
  
  vector<double>::iterator itFeatures = nextPattern->getPatternVector()->begin ();
  
  while (itNeurons != ass->getNeurons()->end())
  {
    /* we dont spike for minimum values */
    if ((*itFeatures) > MinValue)
    {
      emissionTime = (NNLtime) (CurrentTime + ((MaxValue - (double)(*itFeatures)) * 20 * CParameters::TIMESCALE)/MaxValue);
      if (emissionTime < CurrentTime)
      {
        cout << "MinValue : " << MinValue << endl;
        cout << "MaxValue : " << MaxValue << endl;
        cout << "CurrentTime : " << CurrentTime << endl;
        cout << "*itFeatures : " << *itFeatures << endl;
        cout << "CParameters::TIMESCALE : " << CParameters::TIMESCALE << endl;
        cout << "emissionTime : " << emissionTime << endl;
        
        cout << "(MaxValue - (float)(*itFeatures)) : " << (MaxValue - (double)(*itFeatures)) << endl;
        
        cout << "((MaxValue - (float)(*itFeatures)) * 20 * CParameters::TIMESCALE) : " << ((MaxValue - (double)(*itFeatures)) * 20 * CParameters::TIMESCALE) << endl;
        
        cout << "((MaxValue - (float)(*itFeatures)) * 20 * CParameters::TIMESCALE)/MaxValue : " << ((MaxValue - (double)(*itFeatures)) * 20 * CParameters::TIMESCALE)/MaxValue << endl;
        
        cout << "(NNLtime)(((MaxValue - (float)(*itFeatures)) * 20 * CParameters::TIMESCALE)/MaxValue) : " << (NNLtime)(((MaxValue - (float)(*itFeatures)) * 20 * CParameters::TIMESCALE)/MaxValue) << endl;
        
        cout << "(CurrentTime + (NNLtime)(((MaxValue - (float)(*itFeatures)) * 20 * CParameters::TIMESCALE)/MaxValue)) : " << (CurrentTime + (NNLtime)(((MaxValue - (double)(*itFeatures)) * 20 * CParameters::TIMESCALE)/MaxValue)) << endl;
        
        cout << "(NNLtime) (CurrentTime + ((MaxValue - (float)(*itFeatures)) * 20 * CParameters::TIMESCALE)/MaxValue) : " << (NNLtime) (CurrentTime + (NNLtime)(((MaxValue - (double)(*itFeatures)) * 20 * CParameters::TIMESCALE)/MaxValue)) << endl;
        
        //cout << ((*itFeatures) * 20 * CParameters::TIMESCALE)/MaxValue << " ";
      }
      
      for (int NbRepetition = 0; NbRepetition < fparam->NbStimRepetitionsInInterval; NbRepetition++)
      {
        NNLtime Shift = ((20 * CParameters::TIMESCALE) + fparam->StimRepetitionSeparation) * NbRepetition;
        ((CNeuron *)(*itNeurons))->emitSpike(emissionTime+Shift);
      }
    }

    itFeatures++;
    itNeurons++;
  }
  
  CurrentClass = nextPattern->getPatternClass ();
  CurrentClassTime = CurrentTime;
  
 (*PatternClasses) << CurrentTime << " " << CurrentClass << endl;
}

/****************************************************************************/
void PostClass1TestInput (CAssembly * ass, TFuncParam * fparam)
{
  //cout << endl << "*** Test inputs ***" << endl;
  /* dim 256 USPS dataset */
  
  double MinValue;
  double MaxValue;

  vector<CNeuron *>::iterator itNeurons = ass->getNeurons()->begin();
  NNLtime emissionTime = CurrentTime;
  
  CPattern * nextPattern;
  
  if (NbPresentations <= 0) // when NbPresentations reaches 0 => next pattern and reset to 10
  {
    nextPattern = PatternManagerGlobalPointer->getNextClassTestPattern (0);
    NbPresentations = fparam->NbStimPresentations - 1;
  }
  else
  {
    nextPattern = PatternManagerGlobalPointer->getCurrentTestPattern ();
    NbPresentations--;
  }
  
  
  /* We scale input between 0 and MaxValue-MinValue */
  MinValue = PatternManagerGlobalPointer->getMinTrainValue ();
  MaxValue = PatternManagerGlobalPointer->getMaxTrainValue ();
  MaxValue -= MinValue;
  MinValue = 0;
  
  if (nextPattern == NULL)
  {
    if (NbPresentations <= 0) // when NbPresentations reaches 0 => next pattern and reset to 10
    {
      nextPattern = PatternManagerGlobalPointer->getNextClassTestPattern (0);
      NbPresentations = fparam->NbStimPresentations - 1;
    }
    else
    {
      nextPattern = PatternManagerGlobalPointer->getCurrentTestPattern ();
      NbPresentations--;
    }
  }
  
  if (nextPattern == NULL)
  {
    cerr << "\nCannot get next pattern for generalization !" << endl;
    exit (1);
  }
  
  vector<double>::iterator itFeatures = nextPattern->getPatternVector()->begin ();
    
  while (itNeurons != ass->getNeurons()->end())
  {
    if ((*itFeatures) > MinValue)
    {
      emissionTime = (NNLtime) (CurrentTime + ((MaxValue - ((*itFeatures) - MinValue)) * 20 * CParameters::TIMESCALE)/MaxValue);
      //cout << (*itFeatures) << " ";
      for (int NbRepetition = 0; NbRepetition < fparam->NbStimRepetitionsInInterval; NbRepetition++)
      {
        NNLtime Shift = ((20 * CParameters::TIMESCALE) + fparam->StimRepetitionSeparation) * NbRepetition;
        ((CNeuron *)(*itNeurons))->emitSpike(emissionTime+Shift);
      }
    }

    itFeatures++;
    itNeurons++;
  }
  
  CurrentClass = nextPattern->getPatternClass ();
  CurrentClassTime = CurrentTime;
  
  (*PatternClasses) << CurrentTime << " " << CurrentClass << endl;
}

/****************************************************************************/
void PostClass2TrainInput (CAssembly * ass, TFuncParam * fparam)
{
  //cout << endl << "*** Training inputs ***" << endl;
  
  /* dim 256 USPS dataset */
    
  double MinValue;
  double MaxValue;
  
  vector<CNeuron *>::iterator itNeurons = ass->getNeurons()->begin();
  NNLtime emissionTime = CurrentTime;
  
  CPattern * nextPattern;
  
  if (NbPresentations <= 0) // when NbPresentations reaches 0 => next pattern and reset to 10
  {
    nextPattern = PatternManagerGlobalPointer->getNextClassTrainPattern (1);
    NbPresentations = fparam->NbStimPresentations - 1;
  }
  else
  {
    nextPattern = PatternManagerGlobalPointer->getCurrentTrainPattern ();
    NbPresentations--;
  }
  
  /* We scale input between 0 and MaxValue-MinValue */
  MinValue = PatternManagerGlobalPointer->getMinTrainValue ();
  MaxValue = PatternManagerGlobalPointer->getMaxTrainValue ();
  MaxValue -= MinValue;
  MinValue = 0;
  
  if (nextPattern == NULL)
  {
    if (NbPresentations <= 0) // when NbPresentations reaches 0 => next pattern and reset to 10
    {
      nextPattern = PatternManagerGlobalPointer->getNextClassTrainPattern (1);
      NbPresentations = fparam->NbStimPresentations - 1;
    }
    else
    {
      nextPattern = PatternManagerGlobalPointer->getCurrentTrainPattern ();
      NbPresentations--;
    }
  }
  
  if (nextPattern == NULL)
  {
    cerr << "\nCannot get next pattern for training !" << endl;
    exit (1);
  }
  
  vector<double>::iterator itFeatures = nextPattern->getPatternVector()->begin ();
  
  while (itNeurons != ass->getNeurons()->end())
  {
    /* we dont spike for minimum values */
    if ((*itFeatures) > MinValue)
    {
      emissionTime = (NNLtime) (CurrentTime + ((MaxValue - (double)(*itFeatures)) * 20 * CParameters::TIMESCALE)/MaxValue);
      if (emissionTime < CurrentTime)
      {
        cout << "MinValue : " << MinValue << endl;
        cout << "MaxValue : " << MaxValue << endl;
        cout << "CurrentTime : " << CurrentTime << endl;
        cout << "*itFeatures : " << *itFeatures << endl;
        cout << "CParameters::TIMESCALE : " << CParameters::TIMESCALE << endl;
        cout << "emissionTime : " << emissionTime << endl;
        
        cout << "(MaxValue - (float)(*itFeatures)) : " << (MaxValue - (double)(*itFeatures)) << endl;
        
        cout << "((MaxValue - (float)(*itFeatures)) * 20 * CParameters::TIMESCALE) : " << ((MaxValue - (double)(*itFeatures)) * 20 * CParameters::TIMESCALE) << endl;
        
        cout << "((MaxValue - (float)(*itFeatures)) * 20 * CParameters::TIMESCALE)/MaxValue : " << ((MaxValue - (double)(*itFeatures)) * 20 * CParameters::TIMESCALE)/MaxValue << endl;
        
        cout << "(NNLtime)(((MaxValue - (float)(*itFeatures)) * 20 * CParameters::TIMESCALE)/MaxValue) : " << (NNLtime)(((MaxValue - (float)(*itFeatures)) * 20 * CParameters::TIMESCALE)/MaxValue) << endl;
        
        cout << "(CurrentTime + (NNLtime)(((MaxValue - (float)(*itFeatures)) * 20 * CParameters::TIMESCALE)/MaxValue)) : " << (CurrentTime + (NNLtime)(((MaxValue - (double)(*itFeatures)) * 20 * CParameters::TIMESCALE)/MaxValue)) << endl;
        
        cout << "(NNLtime) (CurrentTime + ((MaxValue - (float)(*itFeatures)) * 20 * CParameters::TIMESCALE)/MaxValue) : " << (NNLtime) (CurrentTime + (NNLtime)(((MaxValue - (double)(*itFeatures)) * 20 * CParameters::TIMESCALE)/MaxValue)) << endl;
        
        //cout << ((*itFeatures) * 20 * CParameters::TIMESCALE)/MaxValue << " ";
      }
      
      for (int NbRepetition = 0; NbRepetition < fparam->NbStimRepetitionsInInterval; NbRepetition++)
      {
        NNLtime Shift = ((20 * CParameters::TIMESCALE) + fparam->StimRepetitionSeparation) * NbRepetition;
        ((CNeuron *)(*itNeurons))->emitSpike(emissionTime+Shift);
      }
    }

    itFeatures++;
    itNeurons++;
  }
  
  CurrentClass = nextPattern->getPatternClass ();
  CurrentClassTime = CurrentTime;
  
 (*PatternClasses) << CurrentTime << " " << CurrentClass << endl;
}

/****************************************************************************/
void PostClass2TestInput (CAssembly * ass, TFuncParam * fparam)
{
  //cout << endl << "*** Test inputs ***" << endl;
  /* dim 256 USPS dataset */
  
  double MinValue;
  double MaxValue;

  vector<CNeuron *>::iterator itNeurons = ass->getNeurons()->begin();
  NNLtime emissionTime = CurrentTime;
  
  CPattern * nextPattern;
  
  if (NbPresentations <= 0) // when NbPresentations reaches 0 => next pattern and reset to 10
  {
    nextPattern = PatternManagerGlobalPointer->getNextClassTestPattern (1);
    NbPresentations = fparam->NbStimPresentations - 1;
  }
  else
  {
    nextPattern = PatternManagerGlobalPointer->getCurrentTestPattern ();
    NbPresentations--;
  }
  
  /* We scale input between 0 and MaxValue-MinValue */
  MinValue = PatternManagerGlobalPointer->getMinTrainValue ();
  MaxValue = PatternManagerGlobalPointer->getMaxTrainValue ();
  MaxValue -= MinValue;
  MinValue = 0;
  
  if (nextPattern == NULL)
  {
    if (NbPresentations <= 0) // when NbPresentations reaches 0 => next pattern and reset to 10
    {
      nextPattern = PatternManagerGlobalPointer->getNextClassTestPattern (1);
      NbPresentations = fparam->NbStimPresentations - 1;
    }
    else
    {
      nextPattern = PatternManagerGlobalPointer->getCurrentTestPattern ();
      NbPresentations--;
    }
  }
  
  if (nextPattern == NULL)
  {
    cerr << "\nCannot get next pattern for generalization !" << endl;
    exit (1);
  }
  
  vector<double>::iterator itFeatures = nextPattern->getPatternVector()->begin ();
    
  while (itNeurons != ass->getNeurons()->end())
  {
    if ((*itFeatures) > MinValue)
    {
      emissionTime = (NNLtime) (CurrentTime + ((MaxValue - ((*itFeatures) - MinValue)) * 20 * CParameters::TIMESCALE)/MaxValue);
      //cout << (*itFeatures) << " ";
      
      for (int NbRepetition = 0; NbRepetition < fparam->NbStimRepetitionsInInterval; NbRepetition++)
      {
        NNLtime Shift = ((20 * CParameters::TIMESCALE) + fparam->StimRepetitionSeparation) * NbRepetition;
        ((CNeuron *)(*itNeurons))->emitSpike(emissionTime+Shift);
      }
    }

    itFeatures++;
    itNeurons++;
  }
  
  CurrentClass = nextPattern->getPatternClass ();
  CurrentClassTime = CurrentTime;
  
  (*PatternClasses) << CurrentTime << " " << CurrentClass << endl;
}

/****************************************************************************/
void LearnDelays (CAssembly * ass, TFuncParam * fparam)
{
  //cout << endl << "*** Learn delays ***" << endl;
  int resMul;
  //int resBi;
  
  //resBi = ass->LearnDelays ();
  //resBi = ass->LearnDelays2 ();
  resMul = ass->LearnDelaysMulticlass (fparam->Param1);
  
  /*if (resMul != resBi)
  {
    cout << endl << "AAAAAAHHHHhhh !! CA CHIE DANS LA COLLE !!!!!" << endl;
    cout << "Mul:" << resMul << endl << "Bic:" << resBi << endl;
  }/**/
}

/****************************************************************************/
void InitWeightModif (CAssembly * ass, TFuncParam * fparam)
{
  cout << endl << "Weigth increase - Total: " << WeightPositiveModificationAmount << " Count: " << WeightPositiveModificationCount << " Average: " << WeightPositiveModificationAmount/(float)WeightPositiveModificationCount << endl;
  cout << "Weigth decrease - Total: " << WeightNegativeModificationAmount << " Count: " << WeightNegativeModificationCount << " Average: " << WeightNegativeModificationAmount/(float)WeightNegativeModificationCount << endl;
  cout << "Weigth delta - Total: " << WeightPositiveModificationAmount-WeightNegativeModificationAmount << " Count: " << WeightPositiveModificationAmount+WeightNegativeModificationCount << " Average: " << (float) ((WeightPositiveModificationAmount-WeightNegativeModificationAmount)/(double)(WeightPositiveModificationAmount+WeightNegativeModificationCount)) << endl;
    
  //(WeightNegativeModificationAmount/(double)WeightNegativeModificationCount)-(WeightNegativeModificationAmount/(double)WeightNegativeModificationCount) << endl;
  WeightPositiveModificationAmount = 0;
  WeightPositiveModificationCount = 0;
  WeightNegativeModificationAmount = 0;
  WeightNegativeModificationCount = 0;
}

/****************************************************************************/
void GenePerfo (CAssembly * ass, TFuncParam * fparam)
{
  //cout << endl << "*** Gene perfo ***" << endl;
  //cout << endl;
  int resMul;
  //int resBi;
  resMul = ass->GenePerfoMulticlass (fparam->Param1);
  //resBi = ass->GenePerfo ();
  
  /*if (resMul != resBi)
  {
    cout << endl << endl << "AAAAAAHHHHhhh !! CA CHIE DANS LA COLLE !!!!!" << endl;
    cout << "neurone 0 : " << (*(ass->getNeurons ()))[0]->getLastSpike () << endl;
    cout << "neurone 1 : " << (*(ass->getNeurons ()))[1]->getLastSpike () << endl << endl;
  }*/

}

/****************************************************************************/
void InitPerfo (CAssembly * ass, TFuncParam * fparam)
{
  //cout << endl << "*** Inits perfo ***" << endl;
  ass->ShowStats ();
  ass->InitPerfo ();
  //InitWeightModif (ass);
  //SaveArchitecture (ass);
}

/****************************************************************************/
void CSInput (CAssembly * ass, TFuncParam * fparam)
{
  //100 neurones dans le CS
  vector<CNeuron *> * Neurons = ass->getNeurons();
  
  (*Neurons)[895]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[896]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[897]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[898]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[899]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[900]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[901]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[902]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[903]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[904]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[905]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[906]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
/*  (*Neurons)[57]->emitSpike (CurrentTime);
  (*Neurons)[58]->emitSpike (CurrentTime);
  (*Neurons)[59]->emitSpike (CurrentTime);
  (*Neurons)[60]->emitSpike (CurrentTime);*/
  
}

/****************************************************************************/
void CSInput2 (CAssembly * ass, TFuncParam * fparam)
{
  //100 neurones dans le CS
  vector<CNeuron *> * Neurons = ass->getNeurons();
  
  (*Neurons)[695]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[696]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[697]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[698]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[699]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[700]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[701]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[702]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[703]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[704]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[705]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[706]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
/*  (*Neurons)[57]->emitSpike (CurrentTime);
  (*Neurons)[58]->emitSpike (CurrentTime);
  (*Neurons)[59]->emitSpike (CurrentTime);
  (*Neurons)[60]->emitSpike (CurrentTime);*/
  
}

/****************************************************************************/
void CSInput3 (CAssembly * ass, TFuncParam * fparam)
{
    //100 neurones dans le CS
  vector<CNeuron *> * Neurons = ass->getNeurons();

  (*Neurons)[495]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[496]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[497]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[498]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[499]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[500]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[501]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[502]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[503]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[504]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[505]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[506]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
}

/****************************************************************************/
void CSInput4 (CAssembly * ass, TFuncParam * fparam)
{
  //100 neurones dans le CS
  vector<CNeuron *> * Neurons = ass->getNeurons();
  
  (*Neurons)[295]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[296]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[297]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[298]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[299]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[300]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[301]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[302]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[303]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[304]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[305]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[306]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
/*  (*Neurons)[57]->emitSpike (CurrentTime);
  (*Neurons)[58]->emitSpike (CurrentTime);
  (*Neurons)[59]->emitSpike (CurrentTime);
  (*Neurons)[60]->emitSpike (CurrentTime);*/
  
}

/****************************************************************************/
void CSInput5 (CAssembly * ass, TFuncParam * fparam)
{
  //100 neurones dans le CS
  vector<CNeuron *> * Neurons = ass->getNeurons();
  
  (*Neurons)[95]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[96]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[97]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[98]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[99]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[100]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[101]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[102]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[103]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[104]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[105]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
  (*Neurons)[106]->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
/*  (*Neurons)[57]->emitSpike (CurrentTime);
  (*Neurons)[58]->emitSpike (CurrentTime);
  (*Neurons)[59]->emitSpike (CurrentTime);
  (*Neurons)[60]->emitSpike (CurrentTime);*/
  
}

/****************************************************************************/
void CSInputOPN (CAssembly * ass, TFuncParam * fparam)
{
  //100 neurones dans le CS
  vector<CNeuron *>::iterator itNeuron = ass->getNeurons()->begin ();
  vector<CNeuron *>::iterator itLastNeuron = ass->getNeurons()->end ();
  
  while (itNeuron != itLastNeuron)
  {
    (*itNeuron)->emitSpike (CurrentTime + (double) ( (((double)rand()) / ((double)RAND_MAX)) * 20.0 * CParameters::TIMESCALE ));
    itNeuron++ ;
  }
  
}

/****************************************************************************/
void SaveArchitecture (CAssembly * assembly, TFuncParam * fparam)
{
  char XMLOutputFilename[256];  
  
  if (fparam->strParam1 == "")
    sprintf (XMLOutputFilename, (CParameters::ARCHITECTURE_OUTPUT_DIR + CParameters::ARCHITECTURE_XML_FILE).c_str(), CurrentTime, RandSeed, CNeuron::IDcounter, PatternManagerGlobalPointer->getNbClasses(), CParameters::TIMESCALE);//CurrentTime, RandSeed, ArchitectureGlobalPointer->getAssemblyByName("Reservoir")->getNeurons ()->size (), ArchitectureGlobalPointer->getAssemblyByName("Output")->getNeurons ()->size (), CParameters::TIMESCALE);
  else
    sprintf (XMLOutputFilename, (fparam->strParam1).c_str(), CurrentTime, RandSeed, CNeuron::IDcounter, PatternManagerGlobalPointer->getNbClasses(), CParameters::TIMESCALE);
  ArchitectureGlobalPointer->XMLsave (XMLOutputFilename);

}

/****************************************************************************/
void PostParticularTrainInput (CAssembly * ass, TFuncParam * fparam)
{
  //cout << endl << "*** Test inputs ***" << endl;
  /* dim 256 USPS dataset */
  
  double MinValue;
  double MaxValue;

  vector<CNeuron *>::iterator itNeurons = ass->getNeurons()->begin();
  NNLtime emissionTime = CurrentTime;
  
  CPattern * nextPattern = PatternManagerGlobalPointer->getNiemeTrainPattern (fparam->PatternNumber);
  
  
  /* We scale input between 0 and MaxValue-MinValue */
  MinValue = PatternManagerGlobalPointer->getMinTrainValue ();
  MaxValue = PatternManagerGlobalPointer->getMaxTrainValue ();
  MaxValue -= MinValue;
  MinValue = 0;
  
  if (nextPattern == NULL)
  {
    nextPattern = PatternManagerGlobalPointer->getNiemeTrainPattern (fparam->PatternNumber);
  }
  
  if (nextPattern == NULL)
  {
    cerr << "\nCannot get next pattern for training !" << endl;
    exit (1);
  }
  
  vector<double>::iterator itFeatures = nextPattern->getPatternVector()->begin ();
    
  while (itNeurons != ass->getNeurons()->end())
  {
    if ((*itFeatures) > MinValue)
    {
      emissionTime = (NNLtime) (CurrentTime + ((MaxValue - ((*itFeatures) - MinValue)) * 40 * CParameters::TIMESCALE)/MaxValue);
      
      for (int NbRepetition = 0; NbRepetition < fparam->NbStimRepetitionsInInterval; NbRepetition++)
      {
        NNLtime Shift = ((20 * CParameters::TIMESCALE) + fparam->StimRepetitionSeparation) * NbRepetition;
        ((CNeuron *)(*itNeurons))->emitSpike(emissionTime+Shift);
      } 
    }

    itFeatures++;
    itNeurons++;
  }
  
  CurrentClass = nextPattern->getPatternClass ();
  
  (*PatternClasses) << CurrentTime << " " << CurrentClass << endl;
}

/****************************************************************************/
void PostParticularTestInput (CAssembly * ass, TFuncParam * fparam)
{
  //cout << endl << "*** Test inputs ***" << endl;
  /* dim 256 USPS dataset */
  
  double MinValue;
  double MaxValue;

  vector<CNeuron *>::iterator itNeurons = ass->getNeurons()->begin();
  NNLtime emissionTime = CurrentTime;
  
  CPattern * nextPattern = PatternManagerGlobalPointer->getNiemeTestPattern (fparam->PatternNumber);
  
  
  /* We scale input between 0 and MaxValue-MinValue */
  MinValue = PatternManagerGlobalPointer->getMinTrainValue ();
  MaxValue = PatternManagerGlobalPointer->getMaxTrainValue ();
  MaxValue -= MinValue;
  MinValue = 0;
  
  if (nextPattern == NULL)
  {
    nextPattern = PatternManagerGlobalPointer->getNiemeTestPattern (fparam->PatternNumber);
  }
  
  if (nextPattern == NULL)
  {
    cerr << "\nCannot get next pattern for generalization !" << endl;
    exit (1);
  }
  
  vector<double>::iterator itFeatures = nextPattern->getPatternVector()->begin ();
    
  while (itNeurons != ass->getNeurons()->end())
  {
    if ((*itFeatures) > MinValue)
    {
      emissionTime = (NNLtime) (CurrentTime + ((MaxValue - ((*itFeatures) - MinValue)) * 40 * CParameters::TIMESCALE)/MaxValue);
      
      for (int NbRepetition = 0; NbRepetition < fparam->NbStimRepetitionsInInterval; NbRepetition++)
      {
        NNLtime Shift = ((20 * CParameters::TIMESCALE) + fparam->StimRepetitionSeparation) * NbRepetition;
        ((CNeuron *)(*itNeurons))->emitSpike(emissionTime+Shift);
      }
    }

    itFeatures++;
    itNeurons++;
  }
  
  CurrentClass = nextPattern->getPatternClass ();
  
  (*PatternClasses) << CurrentTime << " " << CurrentClass << endl;
}


}
