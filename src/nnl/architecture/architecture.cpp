/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
 

#include "architecture.h"

#include "assembly.h"
#include "neuron.h"
#include "synapse.h"
#include "eventmanager.h"

#include <luabind/luabind.hpp>


using namespace luabind;



/*****************************************************************************/
CArchitecture::CArchitecture ()
{
  ArchitectureGlobalPointer = this;
}

/*****************************************************************************/
CArchitecture::~CArchitecture ()
{
  vector<CAssembly *>::iterator itAss = Assemblies.begin();
  
  cout << "Cleaning architecture ..." << endl;
  
  while (itAss != Assemblies.end ())
  {
    delete *itAss;
    itAss++;
  }
}

/*****************************************************************************/
int CArchitecture::Connect (CAssembly * Assembly1, CAssembly * Assembly2, ProjectionParameters * param)
{
  int NbConnex = 0;
  
  CSynapse * synTemp;
  
  vector<CNeuron *>::iterator it1;
  vector<CNeuron *>::iterator it2;
  
  //cout << "assemblee 1 : " << Assembly1->getNeurons ()->size () << " Assemble 2 : " << Assembly2->getNeurons ()->size () << endl;
  
  it1 = Assembly1->getNeurons ()->begin ();
  while (it1 != Assembly1->getNeurons()->end())
  {
    //cout << "neuone " << ((CNeuron *)(*it1))->getID () << endl;
    
    it2 = Assembly2->getNeurons ()->begin ();
    while (it2 != Assembly2->getNeurons()->end())
    {
      //cout << "neuone " << ((CNeuron *)(*it1))->getID ();
      if ((*it1 != *it2) && ( (((float)rand()) / ((float)RAND_MAX) ) <= param->ProjectionRate))
      {
        //cout << " -> neurone " << ((CNeuron *)(*it2))->getID ();
        synTemp = new CSynapse (&(param->SynParam), ((CNeuron *)(*it1)), ((CNeuron *)(*it2)));
        ((CNeuron *)(*it1))->AddOutputSynapse (synTemp);
        ((CNeuron *)(*it2))->AddInputSynapse (synTemp);
        NbConnex++;
      }
      //cout << endl;
      it2++;
    }
    it1++;
  }
  
  return NbConnex;
}

/*****************************************************************************/
int CArchitecture::Connect (CAssembly * Assembly, CNeuron * Neuron, ProjectionParameters * param)
{
  int NbConnex = 0;
  CSynapse * synTemp;
  
  vector<CNeuron *>::iterator it1 = Assembly->getNeurons ()->begin ();
  
  while (it1 != Assembly->getNeurons ()->end ())
  {
    if ((*it1 != Neuron) && (( ((float)rand()) / ((float)RAND_MAX) ) <= param->ProjectionRate))
    {
      synTemp = new CSynapse (& (param->SynParam), ((CNeuron *)(*it1)), Neuron);
      ((CNeuron *)(*it1))->AddOutputSynapse (synTemp);
      Neuron->AddInputSynapse (synTemp);
      NbConnex++;
    }
    it1++;
  }
  
  return NbConnex;
}

/*****************************************************************************/
int CArchitecture::Connect (CNeuron * Neuron, CAssembly * Assembly, ProjectionParameters * param)
{
  int NbConnex = 0;
  
  CSynapse * synTemp;
  
  vector<CNeuron *>::iterator it1 = Assembly->getNeurons ()->begin ();
  
  while (it1 != Assembly->getNeurons ()->end ())
  {
    if ((*it1 != Neuron) && (( ((float)rand()) / ((float)RAND_MAX) ) <= param->ProjectionRate))
    {
      synTemp = new CSynapse (& (param->SynParam), Neuron, ((CNeuron *)(*it1)));
      Neuron->AddOutputSynapse (synTemp);
      ((CNeuron *)(*it1))->AddInputSynapse (synTemp);
      NbConnex++;
    }
    it1++;
  }
  
  return NbConnex;
}

/*****************************************************************************/
int CArchitecture::Connect (CNeuron * Neuron1, CNeuron * Neuron2, ProjectionParameters * param)
{
  int NbConnex = 0;
  
  CSynapse * synTemp;
  
  //cout << (Neuron1 != Neuron2) << endl;
  
  if ( (Neuron1 != Neuron2) && ((((float)rand()) / ((float)RAND_MAX) ) <= param->ProjectionRate) )
  {
    synTemp = new CSynapse (& (param->SynParam), Neuron1, Neuron2);
    Neuron1->AddOutputSynapse (synTemp);
    Neuron2->AddInputSynapse (synTemp);
    NbConnex++;
  }
  
  return NbConnex;
}

/*****************************************************************************/
int CArchitecture::ConnectSubAssemblies (CAssembly * Assembly1, int From1, int Nb1, CAssembly * Assembly2, int From2, int Nb2, ProjectionParameters * param)
{
  int NbConnex = 0;
  int NbInputNeurons = 0;
  int NbOutputNeurons = 0;
    
  CSynapse * synTemp;
  
  vector<CNeuron *>::iterator it1;
  vector<CNeuron *>::iterator it2;
  
  //cout << "assemblee 1 : " << Assembly1->getNeurons ()->size () << " Assemble 2 : " << Assembly2->getNeurons ()->size () << endl;
  //cout << From1 << " " << Nb1 << " " << From2 << " " << Nb2 << endl << flush;
    
  it1 = find(Assembly1->getNeurons ()->begin (), Assembly1->getNeurons()->end(), Assembly1->getNeurons()->at (From1));
  while ( (it1 != Assembly1->getNeurons()->end()) && (NbInputNeurons < Nb1) )
  {
    //cout << "neuone " << ((CNeuron *)(*it1))->getID () << endl;
    NbOutputNeurons = 0;
      
    it2 = find(Assembly2->getNeurons ()->begin (), Assembly2->getNeurons()->end(), Assembly2->getNeurons()->at (From2));;
    while ( (it2 != Assembly2->getNeurons()->end()) && (NbOutputNeurons < Nb2) )
    {
      //cout << "neuone " << ((CNeuron *)(*it1))->getID ();
      if ((*it1 != *it2) && ( (((float)rand()) / ((float)RAND_MAX) ) <= param->ProjectionRate))
      {
        //cout << " -> neurone " << ((CNeuron *)(*it2))->getID ();
        synTemp = new CSynapse (&(param->SynParam), ((CNeuron *)(*it1)), ((CNeuron *)(*it2)));
        ((CNeuron *)(*it1))->AddOutputSynapse (synTemp);
        ((CNeuron *)(*it2))->AddInputSynapse (synTemp);
        NbConnex++;
      }
      //cout << endl;
      it2++;
      NbOutputNeurons ++;
    }
    it1++;
    NbInputNeurons++;
  }
  
  return NbConnex;
}

/*****************************************************************************/
int CArchitecture::ConnectOneToOne (CAssembly * Assembly1, CAssembly * Assembly2, ProjectionParameters * param)
{
  int NbConnex = 0;
  
  CSynapse * synTemp;
  
  vector<CNeuron *>::iterator it1;
  vector<CNeuron *>::iterator it2;
  
  it1 = Assembly1->getNeurons ()->begin ();
  it2 = Assembly2->getNeurons ()->begin ();
  
  while ( (it1 != Assembly1->getNeurons()->end()) && (it2 != Assembly2->getNeurons()->end()) )
  {/* We have disabled the test for reccurence (a neuron connected to itself) */
    if (/*(*it1 != *it2) && */ ( (((float)rand()) / ((float)RAND_MAX) ) <= param->ProjectionRate))
    {
      synTemp = new CSynapse (&(param->SynParam), ((CNeuron *)(*it1)), ((CNeuron *)(*it2)));
      ((CNeuron *)(*it1))->AddOutputSynapse (synTemp);
      ((CNeuron *)(*it2))->AddInputSynapse (synTemp);
      NbConnex++;
    }
    
    it2++;
    it1++;
  }
  
  return NbConnex;
}

/*****************************************************************************/
void CArchitecture::ConnectRegular (CAssembly * Ass, int kNeighbour, ProjectionParameters * param)
{
  
  vector<CNeuron *>::iterator it1;
  vector<CNeuron *>::iterator it2;
  vector<CNeuron *>::iterator it3;
  
  it1 = Ass->getNeurons ()->begin ();
  
  while (it1 != Ass->getNeurons()->end())
  {
    it2 = it3 = it1;
    for (int NbNeighbour = 1; NbNeighbour <= kNeighbour; NbNeighbour++)
    {
      
      if (it2 == Ass->getNeurons ()->begin () )
        it2 = Ass->getNeurons ()->end();
      it2--;
      
      it3++;
      if ( it3 == Ass->getNeurons ()->end() )
        it3 = Ass->getNeurons ()->begin ();
      
      
      Connect (*it1, *it2, param);
      Connect (*it1, *it3, param);
    }
    
    it1++;
  }
}

/*****************************************************************************/
void CArchitecture::ConnectRegularAndRewire (CAssembly * Ass, int kNeighbour, double p, ProjectionParameters * param)
{
  ConnectRegular (Ass, kNeighbour, param);
  
  float ProjSave = param->ProjectionRate;
  param->ProjectionRate = 1.0;
  
  Rewire (Ass, p, param);
  
  param->ProjectionRate = ProjSave;
}

/*****************************************************************************/
void CArchitecture::Rewire (CAssembly * Ass, double p, ProjectionParameters * param)
{
  /* Reshuffle connections with probability p */

  
  /* Store synapses to be rewired */
  vector<CSynapse *> AllSynapses;
  
  vector<CSynapse *>::iterator itOutputSynapse;
  vector<CSynapse *>::iterator itLastOutputSynapse;
  
  vector<CNeuron *>::iterator itNeuron = Ass->getNeurons ()->begin ();
  vector<CNeuron *>::iterator itLastNeuron = Ass->getNeurons ()->end ();
  
  while (itNeuron != itLastNeuron)
  {
    itOutputSynapse = (*itNeuron)->getOutputSynapses()->begin ();
    itLastOutputSynapse = (*itNeuron)->getOutputSynapses()->end ();
    
    while (itOutputSynapse != itLastOutputSynapse)
    {
      if ( (*itOutputSynapse)->getOutputNeuron ()->getAssembly () == Ass)
        AllSynapses.push_back (*itOutputSynapse);
      itOutputSynapse++;
    }
    itNeuron++;
  }
  
  /* Rewire synapses */
  bool duplicated = true;

  CNeuron * InputNeuron;
  CNeuron * OutputNeuron;
  
  vector<CSynapse *>::iterator itRewiredSynapse = AllSynapses.begin ();
  vector<CSynapse *>::iterator itLastRewiredSynapse = AllSynapses.end ();
  
  while (itRewiredSynapse != itLastRewiredSynapse)
  {
    if ( (((double)rand()) / ((double)RAND_MAX) ) <= p)
    {
      (*itRewiredSynapse)->DisconnectOutput ();
      (*itRewiredSynapse)->DisconnectInput ();
      
      InputNeuron = Ass->getNeurons ()->at ( (unsigned int) ((((double)rand()) / ((double)RAND_MAX) ) * Ass->getNeurons()->size()) );
      
      while (duplicated)
      {
        OutputNeuron = Ass->getNeurons ()->at ( (unsigned int) ((((double)rand()) / ((double)RAND_MAX) ) * Ass->getNeurons()->size()) );
       
        duplicated = false;
        
        itOutputSynapse = InputNeuron->getOutputSynapses ()->begin ();
        itLastOutputSynapse = InputNeuron->getOutputSynapses()->end ();
        
        if (OutputNeuron == InputNeuron)
          duplicated = true;
        
        while ( (itOutputSynapse != itLastOutputSynapse) && (!duplicated) )
        {
          if (((*itOutputSynapse)->getOutputNeuron () == OutputNeuron))
            duplicated = true;
          
          itOutputSynapse++;
        }
        
      }
      
      duplicated = true;
      
      //if (InputNeuron->getID() == 199)
      //cout << Ass->getNeurons ()->at (199)-> getOutputSynapses ()-> size () << endl;
        
      Connect (InputNeuron, OutputNeuron, param);
        
      //if (InputNeuron->getID() == 199)
      //cout << Ass->getNeurons ()->at (199)-> getOutputSynapses ()-> size () << endl << endl;
        
      //nbRealRewiring += Connect (InputNeuron, OutputNeuron, param);
      //nbRewired++;
    }
    
    itRewiredSynapse++;
  }
  
  //cout << Ass->getNeurons ()->at (199)-> getOutputSynapses ()-> size () << endl;
  
  //cout << endl << nbRewired << " " << nbRealRewiring << endl;
}

/*****************************************************************************/
void CArchitecture::ConnectScaleFree (CAssembly * Ass, ProjectionParameters * InitialRandNetParam, int InitialNumberNeurons, ProjectionParameters * param, int k, int kin, int kout, double alpha, double beta, double gamma)
{
  int nbEdges = 0;
  vector<CNeuron *>::iterator it1;
  vector<CNeuron *>::iterator it2;
  vector<CNeuron *>::iterator itLastNeuron;
  
  CNeuron * Neuron1;
  CNeuron * Neuron2;
    
  it1 = Ass->getNeurons ()->begin ();
  itLastNeuron = Ass->getNeurons ()->end ();

  int i;

  /** /
  // Create initial random network - Method 1
  for (i = 0; i < InitialNumberNeurons; i++) // Create initial random network
  {
    it2 = Ass->getNeurons ()->begin ();
    
    for (j = 0; j < InitialNumberNeurons; j++)
    {
      nbEdges += Connect (*it1, *it2, InitialRandNetParam);
      //cout << nbEdges << " ";
      it2++;
    }
    
    it1++;
  }
  /**/
  /** /
  // Create initial random network - Method 2
  ConnectSubAssemblies (Ass, 0, InitialNumberNeurons, Ass, 0, InitialNumberNeurons, InitialRandNetParam)
  /**/
  /**/
  // Create initial random network - Method 3
  CAssembly * DummyAss = new CAssembly ("DummyAssembly from CArchitecture::ConnectScaleFree");
  for (i = 0; i < InitialNumberNeurons; i++) // Create dummy assembly with the InitialNumberNeurons first neurons
  {
    DummyAss->AddNeuron (*it1);
    it1++;
  }
  // Randomly wire this dummy assembly. k/2 because in the next step of the algo, k is the total amount of edged added with each new node.
  ConnectRegularAndRewire (DummyAss, k/2, 1.0, InitialRandNetParam);
  /**/


  
  it1 += InitialNumberNeurons;
  i = InitialNumberNeurons;

  // Create additionnale nodes (infact they already exist, just wire them, based on preferential attachment
  cout << endl << "wiring ... " << flush;
  while (it1 != itLastNeuron)
  {
    // Add one vertex and k edges.
    i++;

    /**/
    for (int l = 0; l < kin; l++)
    {
      //Choose input randomly for kin edges
      do
      {
        Neuron1 = (*(Ass->getNeurons()))[(int)((((double)rand()) / ((double)RAND_MAX)) * ((double)i))];
      } while ( ((((double)rand()) / ((double)RAND_MAX)) > ScaleFreeProba (Neuron1, alpha, beta, gamma, i, nbEdges)) || \
                (Neuron1 == (*it1)) || Neuron1->hasOutput ((*it1)) );
      
      nbEdges += Connect (Neuron1, (*it1), param);
    }

    for (int l = 0; l < kout; l++)
    {
      //Choose output randomly for kout edges
      do
      {
        Neuron2 = (*(Ass->getNeurons()))[(int)((((double)rand()) / ((double)RAND_MAX)) * ((double)i))];
      } while ( ((((double)rand()) / ((double)RAND_MAX)) > ScaleFreeProba (Neuron2, alpha, beta, gamma, i, nbEdges)) || \
                (Neuron2 == (*it1)) || (*it1)->hasOutput (Neuron2) );
      
      nbEdges += Connect ((*it1), Neuron2, param);
    }
    /**/
    
    /**/ // k is the total number of edges at each step. In this number, kin are input of current node, kou are output of current node, the reste is fully preferentially attached
    for (int l = 0; l < k- (kin+kout); l++)
    {
      do
      {
        // Randomly choose input neuron, than decide to keep it or not depending on its degrees
        do
        {
          //cout << round((((double)rand()) / ((double)RAND_MAX)) * ((double)i)) << endl << flush;
          Neuron1 = (*(Ass->getNeurons()))[(int)((((double)rand()) / ((double)RAND_MAX)) * ((double)i))];
        } while ( (((double)rand()) / ((double)RAND_MAX)) > ScaleFreeProba (Neuron1, alpha, beta, gamma, i, nbEdges));
        
        do
        {
          Neuron2 = (*(Ass->getNeurons()))[(int)((((double)rand()) / ((double)RAND_MAX)) * ((double)i))];
        } while ( (((double)rand()) / ((double)RAND_MAX)) > ScaleFreeProba (Neuron2, alpha, beta, gamma, i, nbEdges) );
        
      } while ( (Neuron1 == Neuron2) || Neuron1->hasOutput (Neuron2) );
      
      nbEdges += Connect (Neuron1, Neuron2, param);
    }
    /**/
    
    it1++;
  }
  
  cout << "done." << endl;
  
}

/*****************************************************************************/
double CArchitecture::ScaleFreeProba (CNeuron * Neuron, double alpha, double beta, double gamma, int NbNeurons, int NbEdges)
{
  double result;
  
  result = ( alpha * (Neuron->getInputSynapses ()->size())/((double) NbEdges) ) + \
    ( beta * (Neuron->getOutputSynapses ()->size())/((double) NbEdges) ) + \
    ( gamma * 1.0/((double) NbNeurons) );
  
  //cout << Neuron->getID () << " " << Neuron->getInputSynapses ()->size() << " " << Neuron->getOutputSynapses ()->size() << " " << gamma << " " << result << endl;
  
  return result;
}

/*****************************************************************************/
void CArchitecture::AddAssembly (CAssembly * ass)
{
  Assemblies.push_back (ass);
}

/*****************************************************************************/
void CArchitecture::Run ()
{
  NNLevent itEvt ;
  
  vector<CAssembly *>::iterator Current = Assemblies.begin ();
  vector<CAssembly *>::iterator End = Assemblies.end ();
  
  while (Current != End)
  {    
    (*Current)->ResetNbSpikes ();
    Current ++;   
  }
  
  if (!EventQueue.empty ())
  {
    itEvt = EventQueue.top ();
  
    while (!(EventQueue.empty ()) && (itEvt.Time == CurrentTime))
    {
      EventQueue.pop ();
      itEvt = EventQueue.top ();  
    }
  }
}

/*****************************************************************************/
vector<CAssembly *> * CArchitecture::getAssemblies ()
{
  return & Assemblies;
}

/*****************************************************************************/
CAssembly * CArchitecture::getAssemblyByName ( string SearchName )
{
  vector<CAssembly *>::iterator Current = Assemblies.begin ();
  vector<CAssembly *>::iterator End = Assemblies.end ();
  
  while (Current != End)
  {    
    if ( (*Current)->getName() == SearchName )
      break;
    
    Current ++;   
  }
  
  return (*Current);
}

/*****************************************************************************/
bool CArchitecture::XMLsave (string OutputXMLFile)
{
  TiXmlDocument doc;
  TiXmlDeclaration * decl = new TiXmlDeclaration ("1.0", "", "");
  doc.LinkEndChild( decl );

  cout << endl << "Saving network to \"" << OutputXMLFile << "\" ... " << flush;

  vector<CAssembly *>::iterator itAssembly = Assemblies.begin ();;
  vector<CAssembly *>::iterator lastAssembly = Assemblies.end ();
  
  /* For each assembly */
  while (itAssembly != lastAssembly)
  {
    TiXmlElement * xmlAssembly = (*(itAssembly))->XMLsave ();
    doc.LinkEndChild (xmlAssembly);
    
    itAssembly++;
  }
    
  bool res = doc.SaveFile(OutputXMLFile.c_str());
  
  if (res)
    cout << "done." << endl << flush;
  else
    cout << "Failed." << endl;

  return res;
}

/*****************************************************************************/
bool CArchitecture::XMLload (string InputXMLFile)
{
  CAssembly * NewAssembly;
  
  map<int, CNeuron *> NeuronByID;
  
  TiXmlDocument doc (InputXMLFile.c_str());
  if (!doc.LoadFile()) return false;

  cout << "Loading network \"" << InputXMLFile << "\" ... " << flush;
  
  //cout << NeuronByID[30] << endl;
  
  
  /*
	<Document>
		<Element attributeA = "valueA">
			<Child attributeB = "value1" />
			<Child attributeB = "value2" />
		</Element>
	<Document>
	

Assuming you want the value of "attributeB" in the 2nd "Child" element, it's very easy to write a *lot* of code that looks like:

	TiXmlElement* root = document.FirstChildElement( "Document" );
	if ( root )
	{
		TiXmlElement* element = root->FirstChildElement( "Element" );
		if ( element )
		{
			TiXmlElement* child = element->FirstChildElement( "Child" );
			if ( child )
			{
				TiXmlElement* child2 = child->NextSiblingElement( "Child" );
				if ( child2 )
				{
					// Finally do something useful.
	*/
  
	TiXmlElement* root = doc.FirstChildElement( "Assembly" );
  
  while (root)
  {
    //cout << root->Value() << endl;
    NewAssembly = new CAssembly ();
    NewAssembly->XMLload (root, &NeuronByID);
    cout << NewAssembly->getName() << "... " << flush;
    AddAssembly (NewAssembly);
    root = root->NextSiblingElement( "Assembly" );
  }
  
  cout << "done." << endl << flush;
  
  return true;  
  
  
}

/*****************************************************************************/
void CArchitecture::GaussianMapping (CAssembly * Assembly1, CAssembly * Assembly2, ProjectionParameters * param)
{
  CSynapse * synTemp;
  
  vector<CNeuron *>::iterator it1;
  vector<CNeuron *>::iterator it2;
  
  it1 = Assembly1->getNeurons ()->begin ();
  it2 = Assembly2->getNeurons ()->begin ();
  
  int Index1 = 0;
  int Index2 = 0;
  
  
  
  while ( it1 != Assembly1->getNeurons()->end())
  { 
    it2 = Assembly2->getNeurons ()->begin ();
    Index2 = 0;
    
    while ( it2 != Assembly2->getNeurons()->end() )
    {
      /* We have disabled the test for reccurence (a neuron connected to itself) */
      if (/*(*it1 != *it2) && */ ( (((float)rand()) / ((float)RAND_MAX) ) <= param->ProjectionRate)) 
      {
        //plot (0.5 - (x**2))/10, exp ( - ( (x**2)/12) ), (0.5 - (x**2)/10) * exp ( - ( (x**2)/12) )
        //param->SynParam.Weight = Wavelet (abs(Index1-Index2), 0.7, 150, 200);
        param->SynParam.Weight = Wavelet (abs(Index1-Index2), 1, 20000, 60000);
        
        // amplitude = 0.9; sigma = 2.5; center = 0; PI = 3.141592
        // plot aplitude, exp ( - (x - center)**2 / (2 * sigma**2) ), amplitude * exp ( - (x - center)**2 / (2 * sigma**2) )
        //param->SynParam.Weight = GaussianMaxima (abs(Index1-Index2), 0.9, 2.5, 0);
        
        if (param->SynParam.Weight < 0)
        {
          param->SynParam.Weight = -param->SynParam.Weight;
          param->SynParam.Inhibitory = TRUE;
        }
        else
          param->SynParam.Inhibitory = FALSE;
        
        synTemp = new CSynapse (&(param->SynParam), ((CNeuron *)(*it1)), ((CNeuron *)(*it2)));
        ((CNeuron *)(*it1))->AddOutputSynapse (synTemp);
        ((CNeuron *)(*it2))->AddInputSynapse (synTemp);
      }
      Index2++;
      it2++;
    }
    Index1++;
    it1++;
  }
}

/*****************************************************************************/
void CArchitecture::LogarithmicMapping (CAssembly * Assembly1, CAssembly * Assembly2, ProjectionParameters * param)
{
  CSynapse * synTemp;
  
  vector<CNeuron *>::iterator it1;
  vector<CNeuron *>::iterator it2;
  
  it1 = Assembly1->getNeurons ()->begin ();
  it2 = Assembly2->getNeurons ()->begin ();
  
  int Index1 = 0;
  int Index2 = 0;
  
  NNLweight Amplitude = param->SynParam.Weight;
  
  while ( it1 != Assembly1->getNeurons()->end())
  { 
    it2 = Assembly2->getNeurons ()->begin ();
    Index2 = 0;
    
    while ( it2 != Assembly2->getNeurons()->end() )
    {
      /* We have disabled the test for reccurence (a neuron connected to itself) */
      if (/*(*it1 != *it2) && */ ( (((float)rand()) / ((float)RAND_MAX) ) <= param->ProjectionRate)) 
      {
        //param->SynParam.Weight = Wavelet (abs(Index1-Index2), 0.7, 150, 200);
        param->SynParam.Weight = InverseLogDistrib (Index1, Amplitude, Assembly1->getNeurons()->size());
        
        synTemp = new CSynapse (&(param->SynParam), ((CNeuron *)(*it1)), ((CNeuron *)(*it2)));
        ((CNeuron *)(*it1))->AddOutputSynapse (synTemp);
        ((CNeuron *)(*it2))->AddInputSynapse (synTemp);
      }
      Index2++;
      it2++;
    }
    Index1++;
    it1++;
  }
}

/****************************************************************************/
//Lua Bindings

/****************************************************************************/
void luaAddAssembly (const char * Name)
{

  cout << Name << "... " << flush;
  CAssembly * Ass = new CAssembly (string(Name));
  ArchitectureGlobalPointer->AddAssembly(Ass);
}

/****************************************************************************/
void luaAddXNeurons (int X, const char * Ass, NEURON_PARAMETERS NParamTable)
{
  NeuronParameters nparam;

  nparam.Threshold = object_cast<NNLpotential>(NParamTable["Threshold"]);
  nparam.MembranePotential = object_cast<NNLpotential>(NParamTable["MembranePotential"]);
  nparam.AbsoluteRefractoryPotential = object_cast<NNLpotential>(NParamTable["AbsoluteRefractoryPotential"]);
  nparam.AbsoluteRefractoryPeriode = object_cast<NNLtime>(NParamTable["AbsoluteRefractoryPeriode"]);
  nparam.Inhibitory = object_cast<float>(NParamTable["Inhibitory"]);
  nparam.TauPSP = object_cast<NNLtime>(NParamTable["TauPSP"]);
  nparam.TauRefract = object_cast<NNLtime>(NParamTable["TauRefract"]);

  ArchitectureGlobalPointer->getAssemblyByName(Ass)->AddXNeurons(&nparam, X);
}

/****************************************************************************/
int luaConnectAssToAss (const char * Ass1, const char * Ass2, PROJECTION_PARAMETERS PParamTable)
{
  ProjectionParameters pparam;

  pparam.ProjectionRate = object_cast<float>(PParamTable["ProjectionRate"]);
  pparam.SynParam.Weight = object_cast<NNLweight>(PParamTable["Weight"]);
  pparam.SynParam.DeltaWeight = object_cast<NNLweight>(PParamTable["DeltaWeight"]);
  pparam.SynParam.Delay = object_cast<NNLtime>(PParamTable["Delay"]);
  pparam.SynParam.DeltaDelay = object_cast<NNLtime>(PParamTable["DeltaDelay"]);
  pparam.SynParam.WeightAdaptation = object_cast<bool>(PParamTable["WeightAdaptation"]);
  pparam.SynParam.DelayAdaptation = object_cast<bool>(PParamTable["DelayAdaptation"]);
  pparam.SynParam.Inhibitory = object_cast<NNLbool>(PParamTable["Inhibitory"]);

  return ArchitectureGlobalPointer->Connect (ArchitectureGlobalPointer->getAssemblyByName(Ass1), ArchitectureGlobalPointer->getAssemblyByName(Ass2), &pparam);
}

/****************************************************************************/
int luaConnectAssToNeu (const char * Ass1, const char * Ass2, int NeuronID, PROJECTION_PARAMETERS PParamTable)
{
  ProjectionParameters pparam;

  pparam.ProjectionRate = object_cast<float>(PParamTable["ProjectionRate"]);
  pparam.SynParam.Weight = object_cast<NNLweight>(PParamTable["Weight"]);
  pparam.SynParam.DeltaWeight = object_cast<NNLweight>(PParamTable["DeltaWeight"]);
  pparam.SynParam.Delay = object_cast<NNLtime>(PParamTable["Delay"]);
  pparam.SynParam.DeltaDelay = object_cast<NNLtime>(PParamTable["DeltaDelay"]);
  pparam.SynParam.WeightAdaptation = object_cast<bool>(PParamTable["WeightAdaptation"]);
  pparam.SynParam.DelayAdaptation = object_cast<bool>(PParamTable["DelayAdaptation"]);
  pparam.SynParam.Inhibitory = object_cast<NNLbool>(PParamTable["Inhibitory"]);

  CNeuron * tmpNeuron = NULL;//(*(getAssemblyByName(Ass2)->getNeurons()))[NeuronID];

  return ArchitectureGlobalPointer->Connect (ArchitectureGlobalPointer->getAssemblyByName(Ass1), tmpNeuron, &pparam);
}

/****************************************************************************/
int luaConnectNeuToAss (const char * Ass1, int NeuronID, const char * Ass2, PROJECTION_PARAMETERS PParamTable)
{
  ProjectionParameters pparam;

  pparam.ProjectionRate = object_cast<float>(PParamTable["ProjectionRate"]);
  pparam.SynParam.Weight = object_cast<NNLweight>(PParamTable["Weight"]);
  pparam.SynParam.DeltaWeight = object_cast<NNLweight>(PParamTable["DeltaWeight"]);
  pparam.SynParam.Delay = object_cast<NNLtime>(PParamTable["Delay"]);
  pparam.SynParam.DeltaDelay = object_cast<NNLtime>(PParamTable["DeltaDelay"]);
  pparam.SynParam.WeightAdaptation = object_cast<bool>(PParamTable["WeightAdaptation"]);
  pparam.SynParam.DelayAdaptation = object_cast<bool>(PParamTable["DelayAdaptation"]);
  pparam.SynParam.Inhibitory = object_cast<NNLbool>(PParamTable["Inhibitory"]);

  CNeuron * tmpNeuron = (*(ArchitectureGlobalPointer->getAssemblyByName(Ass1)->getNeurons()))[NeuronID];

  return ArchitectureGlobalPointer->Connect (tmpNeuron, ArchitectureGlobalPointer->getAssemblyByName(Ass2), &pparam);
}

/****************************************************************************/
int luaConnectNeuToNeu (const char * Ass1, int NeuronID1, const char * Ass2, int NeuronID2, PROJECTION_PARAMETERS PParamTable)
{
  ProjectionParameters pparam;

  pparam.ProjectionRate = object_cast<float>(PParamTable["ProjectionRate"]);
  pparam.SynParam.Weight = object_cast<NNLweight>(PParamTable["Weight"]);
  pparam.SynParam.DeltaWeight = object_cast<NNLweight>(PParamTable["DeltaWeight"]);
  pparam.SynParam.Delay = object_cast<NNLtime>(PParamTable["Delay"]);
  pparam.SynParam.DeltaDelay = object_cast<NNLtime>(PParamTable["DeltaDelay"]);
  pparam.SynParam.WeightAdaptation = object_cast<bool>(PParamTable["WeightAdaptation"]);
  pparam.SynParam.DelayAdaptation = object_cast<bool>(PParamTable["DelayAdaptation"]);
  pparam.SynParam.Inhibitory = object_cast<NNLbool>(PParamTable["Inhibitory"]);

  CNeuron * tmpNeuron1 = (*(ArchitectureGlobalPointer->getAssemblyByName(Ass1)->getNeurons()))[NeuronID1];
  CNeuron * tmpNeuron2 = (*(ArchitectureGlobalPointer->getAssemblyByName(Ass2)->getNeurons()))[NeuronID2];

  return ArchitectureGlobalPointer->Connect (tmpNeuron1, tmpNeuron2, &pparam);
}

/****************************************************************************/
int luaConnectSubAssemblies (const char * Ass1, int From1, int Nb1, const char * Ass2, int From2, int Nb2, PROJECTION_PARAMETERS PParamTable)
{
  ProjectionParameters pparam;

  pparam.ProjectionRate = object_cast<float>(PParamTable["ProjectionRate"]);
  pparam.SynParam.Weight = object_cast<NNLweight>(PParamTable["Weight"]);
  pparam.SynParam.DeltaWeight = object_cast<NNLweight>(PParamTable["DeltaWeight"]);
  pparam.SynParam.Delay = object_cast<NNLtime>(PParamTable["Delay"]);
  pparam.SynParam.DeltaDelay = object_cast<NNLtime>(PParamTable["DeltaDelay"]);
  pparam.SynParam.WeightAdaptation = object_cast<bool>(PParamTable["WeightAdaptation"]);
  pparam.SynParam.DelayAdaptation = object_cast<bool>(PParamTable["DelayAdaptation"]);
  pparam.SynParam.Inhibitory = object_cast<NNLbool>(PParamTable["Inhibitory"]);

  return ArchitectureGlobalPointer->ConnectSubAssemblies (ArchitectureGlobalPointer->getAssemblyByName(Ass1), From1, Nb1, ArchitectureGlobalPointer->getAssemblyByName(Ass2), From2, Nb2, &pparam);
}

/****************************************************************************/
int luaConnectOneToOne (const char * Ass1, const char * Ass2, PROJECTION_PARAMETERS PParamTable)
{
  ProjectionParameters pparam;

  pparam.ProjectionRate = object_cast<float>(PParamTable["ProjectionRate"]);
  pparam.SynParam.Weight = object_cast<NNLweight>(PParamTable["Weight"]);
  pparam.SynParam.DeltaWeight = object_cast<NNLweight>(PParamTable["DeltaWeight"]);
  pparam.SynParam.Delay = object_cast<NNLtime>(PParamTable["Delay"]);
  pparam.SynParam.DeltaDelay = object_cast<NNLtime>(PParamTable["DeltaDelay"]);
  pparam.SynParam.WeightAdaptation = object_cast<bool>(PParamTable["WeightAdaptation"]);
  pparam.SynParam.DelayAdaptation = object_cast<bool>(PParamTable["DelayAdaptation"]);
  pparam.SynParam.Inhibitory = object_cast<NNLbool>(PParamTable["Inhibitory"]);

  return ArchitectureGlobalPointer->ConnectOneToOne (ArchitectureGlobalPointer->getAssemblyByName(Ass1), ArchitectureGlobalPointer->getAssemblyByName(Ass2), &pparam);
}

/****************************************************************************/
void luaConnectRegular (const char * Ass, int kNeighbour, PROJECTION_PARAMETERS PParamTable)
{
  ProjectionParameters pparam;

  pparam.ProjectionRate = object_cast<float>(PParamTable["ProjectionRate"]);
  pparam.SynParam.Weight = object_cast<NNLweight>(PParamTable["Weight"]);
  pparam.SynParam.DeltaWeight = object_cast<NNLweight>(PParamTable["DeltaWeight"]);
  pparam.SynParam.Delay = object_cast<NNLtime>(PParamTable["Delay"]);
  pparam.SynParam.DeltaDelay = object_cast<NNLtime>(PParamTable["DeltaDelay"]);
  pparam.SynParam.WeightAdaptation = object_cast<bool>(PParamTable["WeightAdaptation"]);
  pparam.SynParam.DelayAdaptation = object_cast<bool>(PParamTable["DelayAdaptation"]);
  pparam.SynParam.Inhibitory = object_cast<NNLbool>(PParamTable["Inhibitory"]);

  ArchitectureGlobalPointer->ConnectRegular (ArchitectureGlobalPointer->getAssemblyByName(Ass), kNeighbour, &pparam);
}

/****************************************************************************/
void luaConnectRegularAndRewire (const char * Ass, int kNeighbour, double p, PROJECTION_PARAMETERS PParamTable)
{
  ProjectionParameters pparam;

  pparam.ProjectionRate = object_cast<float>(PParamTable["ProjectionRate"]);
  pparam.SynParam.Weight = object_cast<NNLweight>(PParamTable["Weight"]);
  pparam.SynParam.DeltaWeight = object_cast<NNLweight>(PParamTable["DeltaWeight"]);
  pparam.SynParam.Delay = object_cast<NNLtime>(PParamTable["Delay"]);
  pparam.SynParam.DeltaDelay = object_cast<NNLtime>(PParamTable["DeltaDelay"]);
  pparam.SynParam.WeightAdaptation = object_cast<bool>(PParamTable["WeightAdaptation"]);
  pparam.SynParam.DelayAdaptation = object_cast<bool>(PParamTable["DelayAdaptation"]);
  pparam.SynParam.Inhibitory = object_cast<NNLbool>(PParamTable["Inhibitory"]);

  ArchitectureGlobalPointer->ConnectRegularAndRewire (ArchitectureGlobalPointer->getAssemblyByName(Ass), kNeighbour, p, &pparam);
}

/****************************************************************************/
void luaConnectScaleFree (const char * Ass, PROJECTION_PARAMETERS PParamTable1 /*InitialRandNetParam*/, int InitialNumberNeurons, PROJECTION_PARAMETERS PParamTable2, int k, int kin, int kout, double alpha, double beta, double gamma) /* Make a scale free architecture using the ??  method */
{
  ProjectionParameters pparam1;
  ProjectionParameters pparam2;

  pparam1.ProjectionRate = object_cast<float>(PParamTable1["ProjectionRate"]);
  pparam1.SynParam.Weight = object_cast<NNLweight>(PParamTable1["Weight"]);
  pparam1.SynParam.DeltaWeight = object_cast<NNLweight>(PParamTable1["DeltaWeight"]);
  pparam1.SynParam.Delay = object_cast<NNLtime>(PParamTable1["Delay"]);
  pparam1.SynParam.DeltaDelay = object_cast<NNLtime>(PParamTable1["DeltaDelay"]);
  pparam1.SynParam.WeightAdaptation = object_cast<bool>(PParamTable1["WeightAdaptation"]);
  pparam1.SynParam.DelayAdaptation = object_cast<bool>(PParamTable1["DelayAdaptation"]);
  pparam1.SynParam.Inhibitory = object_cast<NNLbool>(PParamTable1["Inhibitory"]);

  pparam2.ProjectionRate = object_cast<float>(PParamTable2["ProjectionRate"]);
  pparam2.SynParam.Weight = object_cast<NNLweight>(PParamTable2["Weight"]);
  pparam2.SynParam.DeltaWeight = object_cast<NNLweight>(PParamTable2["DeltaWeight"]);
  pparam2.SynParam.Delay = object_cast<NNLtime>(PParamTable2["Delay"]);
  pparam2.SynParam.DeltaDelay = object_cast<NNLtime>(PParamTable2["DeltaDelay"]);
  pparam2.SynParam.WeightAdaptation = object_cast<bool>(PParamTable2["WeightAdaptation"]);
  pparam2.SynParam.DelayAdaptation = object_cast<bool>(PParamTable2["DelayAdaptation"]);
  pparam2.SynParam.Inhibitory = object_cast<NNLbool>(PParamTable2["Inhibitory"]);

  ArchitectureGlobalPointer->ConnectScaleFree (ArchitectureGlobalPointer->getAssemblyByName(Ass), &pparam1, InitialNumberNeurons, &pparam2, k, kin, kout, alpha, beta, gamma);
}

/****************************************************************************/
void CArchitecture::BuildFromLua (const char * LuaFilename)
{
  cout << "Building network \"" << LuaFilename << "\" ... " << flush;
  //create a lua state
  lua_State* pLua = lua_open();

  //open the lua libaries - new in lua5.1
  luaL_openlibs(pLua);

  //open luabind
  open(pLua);



  module(pLua)
  [
   //class_<CArchitecture> ("CArchitecture")
    def("nnlAddAssembly", &luaAddAssembly),
    def("nnlAddXNeurons", &luaAddXNeurons),
    def("nnlConnectAssToAss", &luaConnectAssToAss),
    def("nnlConnectAssToNeu", &luaConnectAssToNeu),
    def("nnlConnectNeuToAss", &luaConnectNeuToAss),
    def("nnlConnectNeuToNeu", &luaConnectNeuToNeu),
    def("nnlConnectSubAssemblies", &luaConnectSubAssemblies),
    def("nnlConnectOneToOne", &luaConnectOneToOne),
    def("nnlConnectRegular", &luaConnectRegular),
    def("nnlConnectRegularAndRewire", &luaConnectRegularAndRewire),
    def("nnlConnectScaleFree", &luaConnectScaleFree)
   /*
    class_<CArchitecture> ("CArchitecture")
    .def("nnlAddAssembly", &CArchitecture::luaAddAssembly)
    .def("nnlAddXNeurons", &CArchitecture::luaAddXNeurons)
    .def("nnlConnectAssToAss", &CArchitecture::luaConnectAssToAss)
    .def("nnlConnectAssToNeu", &CArchitecture::luaConnectAssToNeu)
    .def("nnlConnectNeuToAss", &CArchitecture::luaConnectNeuToAss)
    .def("nnlConnectNeuToNeu", &CArchitecture::luaConnectNeuToNeu)
    .def("nnlConnectSubAssemblies", &CArchitecture::luaConnectSubAssemblies)
    .def("nnlConnectOneToOne", &CArchitecture::luaConnectOneToOne)
    .def("nnlConnectRegular", &CArchitecture::luaConnectRegular)
    .def("nnlConnectRegularAndRewire", &CArchitecture::luaConnectRegularAndRewire)
    .def("nnlConnectScaleFree", &CArchitecture::luaConnectScaleFree)
    */
  ];


  int hasError;

  //load and run the script
  hasError = luaL_dofile(pLua, LuaFilename);

  if (hasError)
  {
    cerr << "-- " << lua_tostring(pLua, -1) << endl;
    luaL_error (pLua, "An error occured while loading %s", LuaFilename);
    exit (hasError);
  }


  //tidy up
  lua_close(pLua);

  cout << "done." << endl << flush;
}

/****************************************************************************/
void CArchitecture::BuildFromMetaLua (CMetaArchitecture & MetaArchitecture)
{
  MetaArchitecture.BuildArchitecture(this);
}

/****************************************************************************/
CAssembly * CArchitecture::operator[] (char * szAssemblyName)
{
  return getAssemblyByName (szAssemblyName);
}

/****************************************************************************/
CAssembly * CArchitecture::operator[] (string & strAssemblyName)
{
  return getAssemblyByName (strAssemblyName.c_str());
}
