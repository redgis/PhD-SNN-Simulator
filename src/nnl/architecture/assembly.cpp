/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


#include "assembly.h"

#include "synapse.h"
#include "neuron.h"
#include "datafile.h"
#include "parameters.h"

/*****************************************************************************/
CAssembly::CAssembly (string newName)
{
  Neurons.clear ();
  LastSpike = 0;
  Name = newName;
  NbSpikes = 0;
  
  NbAnswers = 0;
  NbGoodAnswers = 0;
  NbWrongAnswers = 0;
  NbNoAnswers = 0;
  NbNoSpike = 0;
}

/*****************************************************************************/
CAssembly::~CAssembly ()
{
  vector<CNeuron *>::iterator itNeurons = Neurons.begin ();
  
  cout << "  Destroying assembly \"" << Name << "\" ..." << endl;
  
  while (itNeurons != Neurons.end ())
  {
    delete *itNeurons;
    itNeurons++;
  }
}

/*****************************************************************************/
CNeuron * CAssembly::AddNeuron (NeuronParameters * param)
{
  CNeuron * NeuronTmp = new CNeuron (param, this);
  Neurons.push_back (NeuronTmp);
  NeuronTmp->setLocalID (Neurons.size ());
  
  return NeuronTmp;
}

/*****************************************************************************/
CNeuron * CAssembly::AddNeuron (CNeuron * NeuronTmp)
{
  NeuronTmp->setAssembly (this);
  Neurons.push_back (NeuronTmp);
  NeuronTmp->setLocalID (Neurons.size ());
  
  return NeuronTmp;
}

/*****************************************************************************/
void CAssembly::AddXNeurons (NeuronParameters * param, int NbNeurons)
{
  CNeuron * NeuronTemp;
  for (int Index = 0; Index < NbNeurons; Index ++)
  {
    NeuronTemp = new CNeuron (param, this);
    Neurons.push_back (NeuronTemp);
    NeuronTemp->setLocalID (Neurons.size ());
  }
}

/*****************************************************************************/
vector<CNeuron *> * CAssembly::getNeurons ()
{
  return & Neurons;
}

/*****************************************************************************/
void CAssembly::ResetNbSpikes ()
{
  NbSpikes = 0;
  Spiked = false;
}

/*****************************************************************************/
void CAssembly::AddSpike ()
{
  NbSpikes++;
}

/*****************************************************************************/
int CAssembly::getNbSpikes ()
{
  return NbSpikes;
}

/*****************************************************************************/
void CAssembly::setLastSpike (NNLtime newVal)
{
  LastSpike = newVal;
  Spiked = true;
}

/*****************************************************************************/
NNLtime CAssembly::getLastSpike ()
{
  return LastSpike;
}

/*****************************************************************************/
string & CAssembly::getName ()
{
  return Name;
}

/*****************************************************************************/
bool CAssembly::hasSpiked ()
{
  return Spiked;
}

/*****************************************************************************/
int CAssembly::LearnDelays ()
{
  int checksum = 0;
  
  NNLevent evt;

  NNLtime Epsilon = CParameters::DELAY_EPSILON;

  bool ModifN1 = false;
  bool ModifN2 = false;
  
  vector<CNeuron *>::iterator Neuron = Neurons.begin ();

  CNeuron * Neuron1 = (*Neuron);
  CNeuron * Neuron2 = (*(Neuron+1));

  CNeuron * NeuronTmp;

  NNLtime t1 = Neuron1->getLastSpike ();
  NNLtime t2 = Neuron2->getLastSpike ();

  //if ((CurrentTime - t1 < 100) && (CurrentTime - t2 < 100))
  if (t1 > t2)
  {
    /* We swap to have pointers in chronological order */
    NeuronTmp = Neuron1;
    Neuron1 = Neuron2;
    Neuron2 = NeuronTmp;

    t1 = Neuron1->getLastSpike ();
    t2 = Neuron2->getLastSpike ();
  }

  /*  Condition congruente n1 spike en premier et correspond a la classe */
  if (CurrentClass == (Neuron1->getLocalID () - 1))
  {
    if (t2 - t1 < Epsilon)
    {
      evt = Neuron1->getSpikingPSP ();
      ModifN1 = evt.ImpactingSynapse->DecreaseDelay ();
      checksum -= evt.ImpactingSynapse->getID ();
      
      //cout << evt.EmittingNeuron << " " << (*((Neuron1->getSpikingPSPs ())->end()--)).EmittingNeuron << endl;
      
      (*Synapses) << CurrentTime << " " << evt.ImpactingSynapse->getID () << endl;
  
      evt = Neuron2->getSpikingPSP ();
      ModifN2 = evt.ImpactingSynapse->IncreaseDelay ();
      checksum += evt.ImpactingSynapse->getID ();
      
      (*Synapses) << CurrentTime << " " << evt.ImpactingSynapse->getID () << endl;
    }
  }
  else /* Condition incongruente */
  {
    evt = Neuron1->getSpikingPSP ();
    ModifN1 = evt.ImpactingSynapse->IncreaseDelay ();
    checksum += evt.ImpactingSynapse->getID ();
    
    (*Synapses) << CurrentTime << " " << evt.ImpactingSynapse->getID () << endl;
    
    evt = Neuron2->getSpikingPSP ();
    ModifN2 = evt.ImpactingSynapse->DecreaseDelay ();
    checksum -= evt.ImpactingSynapse->getID ();
    
    (*Synapses) << CurrentTime << " " << evt.ImpactingSynapse->getID () << endl;
  }

  //cout << ModifN1 << " "<< ModifN2 << endl;
  if (Neuron1->getLocalID () == 1)
  {
    if (ModifN1)
      (*DelayNeuron1) << CurrentTime << " " << Neuron1->getSpikingPSP ().EmittingNeuron->getID () << endl;
    
    if (ModifN2)
      (*DelayNeuron2) << CurrentTime << " " << Neuron2->getSpikingPSP ().EmittingNeuron->getID () << endl;
  }
  else
  {
    if (ModifN1)
      (*DelayNeuron2) << CurrentTime << " " << Neuron1->getSpikingPSP ().EmittingNeuron->getID () << endl;
    
    if (ModifN2)
      (*DelayNeuron1) << CurrentTime << " " << Neuron2->getSpikingPSP ().EmittingNeuron->getID () << endl;
  }    

  return checksum;
}

/*****************************************************************************/
int CAssembly::LearnDelaysMulticlass (NNLtime StimInterval)
{
  
  //cout << "Multiclass" << endl;
  
  int checksum = 0;
  //bool resDecrease = true;
  
  NNLtime Epsilon = CParameters::DELAY_EPSILON;
  
  vector<CNeuron *>::iterator CurrentNeuron = Neurons.begin ();
  vector<CNeuron *>::iterator LastNeuron = Neurons.end ();
  
  //cout << "CurrentClass : " << CurrentClass << endl << flush;
  
  CNeuron * TargetNeuronX = Neurons[CurrentClass];   /* target neuron */
  vector <CNeuron *> FirstNonTargetNeuronsY;    /* non-target neurons which had the most early spikes */
  
  NNLtime tx = TargetNeuronX->getLastSpike (); /* spike time of X */
  NNLtime ty;                                  /* spike time of Ys */
  
  NNLtime ttmp;
  
  /* find ty, most early spike time among non-target neurons, checking special case of the first neuron */
  if ((*CurrentNeuron) == TargetNeuronX)
    CurrentNeuron++;
  
  ty = (*CurrentNeuron)->getLastSpike ();

  while (CurrentNeuron != LastNeuron)
  {
    ttmp = (*CurrentNeuron)->getLastSpike ();
    if ( ((*CurrentNeuron) != TargetNeuronX) && (ttmp < ty) )
      ty = ttmp;
    CurrentNeuron ++;
  }
  
  /****** FIXME : Check for ty and tx to be within the presentation interval ******/
  
  /* seems well classified ? no need change anything, return */
  if ((ty - tx) >= Epsilon)
    return 0;
  
  if ((CurrentTime - tx >= StimInterval) && (CurrentTime - ty >= StimInterval))
    return 0;

  
  /* get all non-target neurons that spiked at ty */
  CurrentNeuron = Neurons.begin ();
  while (CurrentNeuron != LastNeuron)
  {
    if ( ((*CurrentNeuron) != TargetNeuronX) && (ty == (*CurrentNeuron)->getLastSpike ()) )
      FirstNonTargetNeuronsY.push_back ((*CurrentNeuron));
    CurrentNeuron ++;
  }

  /* Change delays */
  list<NNLevent *>::iterator Current;
  list<NNLevent *>::iterator End;
  
  /* Decrease target neuron delays */
  //Current = TargetNeuronX->getSpikingPSPs ()->begin ();
  End = TargetNeuronX->getSpikingPSPs ()->end ();
  //random_shuffle (Current, End);

  Current = TargetNeuronX->getSpikingPSPs ()->end ();
  Current--;
  
  while (Current != End)
  {
    //cout << TargetNeuronX->getLocalID () << ":" << -(*Current).ImpactingSynapse->getID () << " ";
    checksum -= (*Current)->ImpactingSynapse->getID ();
    (*Current)->ImpactingSynapse->DecreaseDelay ();
    //resDecrease = (*Current).ImpactingSynapse->DecreaseDelay ();
    break;
    Current--;
  }
  
  /* Increase non-target neurons delays */
  /* for each non-target neuron Y */
  CurrentNeuron = FirstNonTargetNeuronsY.begin ();
  LastNeuron = FirstNonTargetNeuronsY.end ();
  random_shuffle (CurrentNeuron, LastNeuron);
  
  //CurrentNeuron = Neurons.begin ();
  //LastNeuron = Neurons.end ();
  while (CurrentNeuron != LastNeuron)
  {
    if ((*CurrentNeuron) != TargetNeuronX)
    {
      Current = (*CurrentNeuron)->getSpikingPSPs ()->end ();
      End = (*CurrentNeuron)->getSpikingPSPs ()->end ();
      Current--;
      
      while (Current != End)
      {
        //cout << (*CurrentNeuron)->getLocalID () << ":" << (*Current).ImpactingSynapse->getID () << " ";
        checksum += (*Current)->ImpactingSynapse->getID ();
        (*Current)->ImpactingSynapse->IncreaseDelay ();
        //if(!resDecrease)
          //(*Current).ImpactingSynapse->IncreaseDelay ();
        break;
        Current--;
      }
    break;
    }
    
    CurrentNeuron++;
  }
  
  //cout << endl;
  
  return checksum;
//if ((CurrentTime - tx < 100) && (CurrentTime - ty < 100))
}


/*****************************************************************************/
int CAssembly::LearnDelays2 ()
{
  
  int checksum = 0;
  //cout << "Biclass" << endl;
  
  NNLtime Epsilon = CParameters::DELAY_EPSILON;
  
  vector<CNeuron *>::iterator Neuron = Neurons.begin ();
  
  CNeuron * Neuron1 = (*Neuron);
  CNeuron * Neuron2 = (*(Neuron+1));
  
  CNeuron * NeuronTmp;
  
  NNLtime t1 = Neuron1->getLastSpike ();
  NNLtime t2 = Neuron2->getLastSpike ();
  
  if (t1 > t2)
  {
    /* We swap to have pointers in chronological order */
    NeuronTmp = Neuron1;
    Neuron1 = Neuron2;
    Neuron2 = NeuronTmp;
    
    t1 = Neuron1->getLastSpike ();
    t2 = Neuron2->getLastSpike ();
  }
  
  //cout << endl << endl << "Time " << CurrentTime << " ";
  
  list<NNLevent *>::iterator Current;
  list<NNLevent *>::iterator End;
  
  /*  Condition congruante n1 spike en premier et correspond à la classe */
  if ((CurrentTime - t1 < 100) && (CurrentTime - t2 < 100))
  {
    if (CurrentClass == (Neuron1->getLocalID () - 1))
    {
      if (t2 - t1 < Epsilon)
      {

        //cout << "Condition congruante, decrease : ";
        Current = Neuron1->getSpikingPSPs ()->begin ();
        End = Neuron1->getSpikingPSPs ()->end ();

        while (Current != End)
        {
          //cout << Neuron1->getLocalID () << ":" << -(*Current).ImpactingSynapse->getID () << " " ;
          checksum += -(*Current)->ImpactingSynapse->getID ();
          //cout << (*Current).ImpactingSynapse->getID () << ",";
          (*Current)->ImpactingSynapse->DecreaseDelay ();
          (*Synapses) << CurrentTime << " " << (*Current)->ImpactingSynapse->getID () << endl;

          if (Neuron1->getLocalID () == 1)
            (*DelayNeuron1) << CurrentTime << " " << (*Current)->EmittingNeuron->getID () << endl;
          else
            (*DelayNeuron2) << CurrentTime << " " << (*Current)->EmittingNeuron->getID () << endl;

          Current++;
        }

        //cout << endl << "    increase : ";
        Current = Neuron2->getSpikingPSPs ()->begin ();
        End = Neuron2->getSpikingPSPs ()->end ();

        while (Current != End)
        {
          //cout << (*Current).ImpactingSynapse->getID () << ",";
          //cout << Neuron2->getLocalID () << ":" << (*Current).ImpactingSynapse->getID () << " " ;
          checksum += (*Current)->ImpactingSynapse->getID ();
          (*Current)->ImpactingSynapse->IncreaseDelay ();
          (*Synapses) << CurrentTime << " " << (*Current)->ImpactingSynapse->getID () << endl;

          if (Neuron1->getLocalID () == 1)
            (*DelayNeuron2) << CurrentTime << " " << (*Current)->EmittingNeuron->getID () << endl;
          else
            (*DelayNeuron1) << CurrentTime << " " << (*Current)->EmittingNeuron->getID () << endl;

          Current++;
        }
      }
    }
    else /* Condition incongruante */
    {
      //cout << "Condition incongruante, increase : ";
      Current = Neuron1->getSpikingPSPs ()->begin ();
      End = Neuron1->getSpikingPSPs ()->end ();
      
      while (Current != End)
      {
        //cout << (*Current).ImpactingSynapse->getID () << ",";
        //cout << Neuron1->getLocalID () << ":" << (*Current).ImpactingSynapse->getID () << " ";
        checksum += (*Current)->ImpactingSynapse->getID ();
        (*Current)->ImpactingSynapse->IncreaseDelay ();
        (*Synapses) << CurrentTime << " " << (*Current)->ImpactingSynapse->getID () << endl;
        
        if (Neuron1->getLocalID () == 1)
          (*DelayNeuron1) << CurrentTime << " " << (*Current)->EmittingNeuron->getID () << endl;
        else
          (*DelayNeuron2) << CurrentTime << " " << (*Current)->EmittingNeuron->getID () << endl;
        
        Current++;
      }

      //cout << endl << "    increase : ";
      Current = Neuron2->getSpikingPSPs ()->begin ();
      End = Neuron2->getSpikingPSPs ()->end ();
      
      while (Current != End)
      {
        //cout << (*Current).ImpactingSynapse->getID () << ",";
        //cout << Neuron2->getLocalID () << ":" << -(*Current).ImpactingSynapse->getID () << " ";
        checksum += -(*Current)->ImpactingSynapse->getID ();
        (*Current)->ImpactingSynapse->DecreaseDelay ();
        (*Synapses) << CurrentTime << " " << (*Current)->ImpactingSynapse->getID () << endl;
        
        if (Neuron1->getLocalID () == 1)
          (*DelayNeuron2) << CurrentTime << " " << (*Current)->EmittingNeuron->getID () << endl;
        else
          (*DelayNeuron1) << CurrentTime << " " << (*Current)->EmittingNeuron->getID () << endl;
        
        Current++;
      }
  }
  }
  
  //cout << endl;
  
  if (Neuron1->getLocalID () == 1)
  {
    (*DelayNeuron1) << CurrentTime << " " << Neuron1->getSpikingPSP ().EmittingNeuron->getID () << endl;
    (*DelayNeuron2) << CurrentTime << " " << Neuron2->getSpikingPSP ().EmittingNeuron->getID () << endl;
  }
  else
  {
    (*DelayNeuron2) << CurrentTime << " " << Neuron1->getSpikingPSP ().EmittingNeuron->getID () << endl;
    (*DelayNeuron1) << CurrentTime << " " << Neuron2->getSpikingPSP ().EmittingNeuron->getID () << endl;
  }  


  return checksum;
  
}


/*****************************************************************************/
int CAssembly::GenePerfo ()
{
  vector<CNeuron *>::iterator Neuron = Neurons.begin ();
  
  CNeuron * Neuron1 = (*Neuron);
  CNeuron * Neuron2 = (*(Neuron+1));
  
  NNLtime t1 = Neuron1->getLastSpike ();
  NNLtime t2 = Neuron2->getLastSpike ();
  
  NbAnswers ++;
  
  //cout << "Bi-class : ";
  
  if ( ( (t1 < t2) && (CurrentClass == (Neuron1->getLocalID ()-1)) )
    || ( (t2 < t1) && (CurrentClass == (Neuron2->getLocalID ()-1)) ) )
  {   
    NbGoodAnswers ++;
    //cout << "Right" << endl;
    (*PredictedClasses) << CurrentClassTime << " " << CurrentClass << endl;
    return 1;
  }
  else
  {
    if (t1 == t2)
    {
      NbNoAnswers++;
      //cout << "Unknown" << endl;
      (*PredictedClasses) << CurrentClassTime << " " << -1 << endl;
      return 0;
    }
    else
    {
      NbWrongAnswers++;
      //cout << "Wrong" << endl;
      if (CurrentClass == 0)
        (*PredictedClasses) << CurrentClassTime << " " << 1 << endl;
      else
        (*PredictedClasses) << CurrentClassTime << " " << 0 << endl;
      return -1;
    }
  }
  
}


/*****************************************************************************/
int CAssembly::GenePerfoMulticlass (int StimInterval)
{
  
  //for classification output to PredictedClasses
  NNLtime TargetSpikeTime = Neurons[CurrentClass]->getLastSpike ();
  NNLtime NonTargetSpikeTime;
  int NonTargetPrediectedClass = 0;
  int myCurrentClass = 0;
  
  
  vector<CNeuron *>::iterator CurrentNeuron = Neurons.begin ();
  vector<CNeuron *>::iterator LastNeuron = Neurons.end ();
  
  CNeuron * TargetNeuronX = Neurons[CurrentClass];   /* target neuron */
  vector <CNeuron *> FirstNonTargetNeuronsY;    /* non-target neurons which had the most early spikes */
  
  NNLtime tx = TargetNeuronX->getLastSpike (); /* spike time of X */
  NNLtime ty;                                  /* spike time of Ys */
  
  NNLtime ttmp;
  
  /* find ty, most early spike time among non-target neurons, checking the special case of the first neuron */
  if ( ((*CurrentNeuron) == TargetNeuronX))
    CurrentNeuron++;
  ty = (*CurrentNeuron)->getLastSpike ();
  
  while (CurrentNeuron != LastNeuron)
  {
    ttmp = (*CurrentNeuron)->getLastSpike ();
    if ( ((*CurrentNeuron) != TargetNeuronX) && (ttmp < ty) )
    {
      NonTargetSpikeTime = ty = ttmp;
      NonTargetPrediectedClass = myCurrentClass;
    }
    CurrentNeuron ++;
    myCurrentClass ++;
  }
  
  
  /* get all non-target neurons that spiked at ty */
  CurrentNeuron = Neurons.begin ();
  while (CurrentNeuron != LastNeuron)
  {
    if ( ((*CurrentNeuron) != TargetNeuronX) && (ty == (*CurrentNeuron)->getLastSpike ()) )
      FirstNonTargetNeuronsY.push_back ((*CurrentNeuron));
    CurrentNeuron ++;
  }
  
  NbAnswers ++;
  
  //cout << "Multiclass : ";

  if (((CurrentTime - tx) > StimInterval) || ((CurrentTime - ty) > StimInterval))
  {
    NbNoSpike++;
    (*PredictedClasses) << CurrentClassTime << " " << -2 << endl;
    return -2;
  }

  /* seems well classified ? no need change anything, return */
  if (tx < ty)
  {
    NbGoodAnswers ++;
    (*PredictedClasses) << CurrentClassTime << " " << CurrentClass << endl;
    //cout << "Right" << endl;
    return 1;
  }
  if (tx == ty)
  {
    NbNoAnswers++;
    (*PredictedClasses) << CurrentClassTime << " " << -1 << endl;
    //cout << "Unknown" << endl;
    return 0;
  }
  if (tx > ty)
  {
    NbWrongAnswers++;
    if (FirstNonTargetNeuronsY.size() > 1) /* unclassified */
      (*PredictedClasses) << CurrentClassTime << " " << -1 << endl;
    else  /* not well classified */
      (*PredictedClasses) << CurrentClassTime << " " << NonTargetPrediectedClass << endl;
    //cout << "Wrong" << endl;
    return -1;
  }

  return -32768;
}


/*****************************************************************************/
void CAssembly::ShowStats ()
{
  cout << endl << "  Wrong: " << NbWrongAnswers << " (" << ((float)NbWrongAnswers)/((float)NbAnswers) * 100.0 << "%)" << endl;
  cout <<         "   Good: " << NbGoodAnswers << " (" << ((float)NbGoodAnswers)/((float)NbAnswers) * 100.0 << "%)" << endl;
  cout <<         "Unknown: " << NbNoAnswers << " (" << ((float)NbNoAnswers)/((float)NbAnswers) * 100.0 << "%)" << endl;
  cout <<         "NoSpike: " << NbNoSpike << " (" << ((float)NbNoSpike)/((float)NbAnswers) * 100.0 << "%)" << endl;
  cout <<         "  Total: " << NbAnswers << " (" << ((float)NbAnswers)/((float)NbAnswers) * 100.0 << "%)" << endl;
  cout <<         "Sat.Inf: " << SaturationInf << endl;
  cout <<         "Sat.Sup: " << SaturationSup << endl;
}

/*****************************************************************************/
void CAssembly::InitPerfo ()
{
  NbAnswers = 0;
  NbGoodAnswers = 0;
  NbWrongAnswers = 0;
  NbNoAnswers = 0;
  NbNoSpike = 0;
  SaturationInf = 0;
  SaturationSup = 0;
}


/*****************************************************************************/
TiXmlElement * CAssembly::XMLsave ()
{
  TiXmlElement * xmlAssembly = new TiXmlElement ("Assembly");
  xmlAssembly->SetAttribute ("name", Name.c_str());
  xmlAssembly->SetAttribute ("size", Neurons.size());
  
  vector<CNeuron *>::iterator itNeuron = Neurons.begin ();
  vector<CNeuron *>::iterator lastNeuron = Neurons.end ();
  
  while (itNeuron != lastNeuron)
  {
    TiXmlElement * xmlNeuron = (*(itNeuron))->XMLsave();
    xmlAssembly->LinkEndChild(xmlNeuron);
    itNeuron++;
  }

  return xmlAssembly;
}

/*****************************************************************************/
bool CAssembly::XMLload (TiXmlElement* pXmlAssembly, map<int, CNeuron*> * NeuronByID)
{
 
  /*
	<Document>
		<Element attributeA = "valueA">
			<Child attributeB = "value1" />
			<Child attributeB = "value2" />
		</Element>
	<Document>
	

Assuming you want the value of "attributeB" in the 2nd "Child" element, it's very easy to write a *lot* of code that looks like:

	TiXmlElement* root = document.FirstChildElement( "Document" );
	if ( root )
	{
		TiXmlElement* element = root->FirstChildElement( "Element" );
		if ( element )
		{
			TiXmlElement* child = element->FirstChildElement( "Child" );
			if ( child )
			{
				TiXmlElement* child2 = child->NextSiblingElement( "Child" );
				if ( child2 )
				{
					// Finally do something useful.
	*/
    
  //int maxID = 0;
  
  CNeuron * NewNeuron = NULL;
  
  Name = pXmlAssembly->Attribute ("name");
  
  TiXmlElement* pXmlNeuron = pXmlAssembly->FirstChildElement ("Neuron");
  int NeuronID;
  
  while (pXmlNeuron)
  {
    pXmlNeuron->Attribute ("id", &NeuronID);
    
    //if (NeuronID > maxID)
    //  maxID = NeuronID;
    //cout << NeuronID << "...";
    
    if ((*NeuronByID)[NeuronID] == NULL)
    {
      NewNeuron = new CNeuron (NULL, this);
      (*NeuronByID)[NeuronID] = NewNeuron;
      NewNeuron->XMLload (pXmlNeuron, NeuronByID);
    }
    else
    {
      //cout << "Existe déja !" << endl;
      (*NeuronByID)[NeuronID]->setAssembly (this);
      (*NeuronByID)[NeuronID]->XMLload (pXmlNeuron, NeuronByID);
    }
    
    Neurons.push_back ((*NeuronByID)[NeuronID]);
    (*NeuronByID)[NeuronID]->setLocalID (Neurons.size ());
    pXmlNeuron = pXmlNeuron->NextSiblingElement( "Neuron" );
    
    //cout << (*NeuronByID)[NeuronID] << " ";
  }
  
    
  //CNeuron::IDcounter = maxID+1;
  //cout << "ID counter : " << CNeuron::IDcounter << endl;
  cout << "\"" << getName () << "\"(" << Neurons.size() << ")...";
  
  return true;
}

/****************************************************************************/
CNeuron * CAssembly::operator[] (size_t pos)
{
  return Neurons[pos];
}
