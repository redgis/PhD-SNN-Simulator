/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


#include "neuron.h"

#include "parameters.h"
#include "synapse.h"
#include "assembly.h"
#include "eventmanager.h"

int CNeuron::IDcounter = 0;


/****************************************************************************/
CNeuron::CNeuron (CNeuron & neuron)
{
  MembranePotential = neuron.MembranePotential;
  Threshold = neuron.Threshold;
  PSPSum = neuron.PSPSum;
  AbsoluteRefractoryPeriode = neuron.AbsoluteRefractoryPeriode;
  AbsoluteRefractPotential = neuron.AbsoluteRefractPotential;
  RestingPotential = neuron.RestingPotential;
  TauPSP = neuron.TauPSP;
  TauRefract = neuron.TauRefract;
  
  Container = neuron.Container;
  InputSynapses.clear ();
  OutputSynapses.clear ();
  LastPSP.Time = -1;
  LastSpike = -1;
  OldSpike = -1;
  PSPSum = 0;
  
  Inhibitory = neuron.Inhibitory;
  
  /* Generates the tabbed exponential values */
  InitPSPShape ();
  InitRefractShape ();
  
  ID = IDcounter;
  IDcounter++;
  
  if (!Container)
    localID = -1;
}

/****************************************************************************/
CNeuron::CNeuron (NeuronParameters * param = NULL, CAssembly * pContainer = NULL)
{
  if (param == NULL)
  {
    MembranePotential = (NNLpotential) -65;
    RestingPotential = (NNLpotential) -65;
    Threshold = (NNLpotential) -50;
    PSPSum = (NNLpotential) 0;
    AbsoluteRefractoryPeriode = 10;
    AbsoluteRefractPotential = -10000;
    TauPSP = 10;
    TauRefract = 3;

    Inhibitory = false;
  }
  else
  {
    MembranePotential = (NNLpotential) param->MembranePotential;
    RestingPotential = (NNLpotential) param->MembranePotential;
    Threshold = (NNLpotential) param->Threshold;
    PSPSum = (NNLpotential) 0;
    AbsoluteRefractoryPeriode = (NNLtime) param->AbsoluteRefractoryPeriode;
    AbsoluteRefractPotential = (NNLpotential) param->AbsoluteRefractoryPotential;
    TauPSP = (NNLtime) param->TauPSP;
    TauRefract = (NNLtime) param->TauRefract;
  
    if ( ( ((float)rand()) / ((float)RAND_MAX) ) <= param->Inhibitory)
      Inhibitory = true;
    else
      Inhibitory = false;
  }

  Container = pContainer;
  InputSynapses.clear ();
  OutputSynapses.clear ();
  LastPSP.Time = -1;
  LastSpike = -1;
  OldSpike = -1;
  PSPSum = 0;
  
  /* Generates the tabbed exponential values */
  InitPSPShape ();
  InitRefractShape ();
  
  ID = IDcounter;
  IDcounter++;
  
  if (!Container)
    localID = -1;

}

/****************************************************************************/
CNeuron::~CNeuron ()
{
  //vector<CSynapse *>::iterator itInputSynapses = InputSynapses.begin ();
  vector<CSynapse *>::iterator itOutputSynapses = OutputSynapses.begin ();
  list<NNLevent *>::iterator itSpikingPSPs = SpikingPSPs.begin ();
  
  cout << "    Destroying neuron " << localID << " ..." << endl;
  
  delete PSPShape;      /* Shape of a PSP */
  delete RefractShape;  /* Shape of the relative refractory period */
  
  while (itSpikingPSPs != SpikingPSPs.end ())
  {
    delete *itSpikingPSPs;
    itSpikingPSPs++;
  }
  
  while (itOutputSynapses != OutputSynapses.end ())
  {
    delete *itOutputSynapses;
    itOutputSynapses++;
  }
  
  
  
}

/****************************************************************************/
void CNeuron::AddInputSynapse (CSynapse * syn)
{
  InputSynapses.push_back (syn);
}

/****************************************************************************/
void CNeuron::AddOutputSynapse (CSynapse * syn)
{
  OutputSynapses.push_back (syn);
}

/****************************************************************************/
/* This function is used to generate an array containing the PSP values from 0 to 5*TauPSP */
NNLpotential CNeuron::PSPValue (int t)
{
  return (NNLpotential) exp((double)(-((float)t)/((float)TauPSP)));
}

/****************************************************************************/
/* This function is used to generate an array containing the refract values from 0 to 5*TauRefract */
NNLpotential CNeuron::RefractValue (int t)
{
  return (NNLpotential) - exp((double)(-((float)t)/((float)TauRefract)));
}

/****************************************************************************/
NNLpotential CNeuron::PSPFunction (NNLtime t)
{
  //cout << t << " - " << ExpPSP(t) << ":" << (float) (*(PSPShapes[TauPSP]))[t] << ":" << exp((double)(-((float)t)/((float)TauPSP))) << endl;
  /* Decreasing exponential : from 1 to 0 (exp (-5) considered to be 0)*/
  if (t > 5*TauPSP)
    return 0;
  else /* NB : we only pass t as parameter, but the function calculates exp(-t/Tau) where Tau is the neuron time constant */
    return PSPShape[t];
}

/****************************************************************************/
NNLpotential CNeuron::RefractoryPotential (NNLtime t)
{
  /* Increasing exponential : from -1*RefractoryPotential to 0 (exp (5*Tau) considered to be 0)*/
  if (t < AbsoluteRefractoryPeriode)
  {
    return - AbsoluteRefractPotential;
  }
  if ((t >= AbsoluteRefractoryPeriode) && (t <= (AbsoluteRefractoryPeriode + 5*TauRefract))) /* NB : we only pass t as parameter, but the function calculates exp(-t/Tau) where Tau is the neuron time constant */
    return AbsoluteRefractPotential * RefractShape[t-AbsoluteRefractoryPeriode];
  
  return 0;
}

/****************************************************************************/
NNLpotential CNeuron::InputFunction (NNLevent & evt)
{
  NNLpotential RefractPotential = 0;
  NNLpotential newMembranePotential;
  
  NNLtime LastPSPage;
  
  /* Only if the last PSP was not at this time */
  if (LastPSP.Time != CurrentTime)
  {
    /* Recalculate refractory potential if needed */
    if (LastSpike != -1)
      RefractPotential = RefractoryPotential (CurrentTime - LastSpike);
    else
      RefractPotential = 0;

  
    /* If there was a PSP in the past, calculate the potential remaining from PSPs, since the last PSP */
    if (LastPSP.Time != -1)
    {
      LastPSPage = CurrentTime - LastPSP.Time;
      PSPSum = PSPSum * PSPFunction(LastPSPage);
    }
  }

  if (evt.ImpactingSynapse->isInhibitory ())
    PSPSum -= (NNLpotential) (evt.ImpactingSynapse->getWeight() * (NNLweight) CParameters::PSP_STRENGHT);
  else
    PSPSum += (NNLpotential) (evt.ImpactingSynapse->getWeight() * (NNLweight) CParameters::PSP_STRENGHT);

  newMembranePotential = (NNLpotential) RestingPotential + RefractPotential + PSPSum;
  
  /*if (PSPSum > -ABSOLUTE_REFRACTORY_POTENTIAL)
    cout << "************************************* WARNING OVERDRIVE DETECTED FOR NEURON " << ID << "\a" << endl;
  */
  /*if (ID == 132)
  {
    cout << ID << "- Membrane: " << newMembranePotential << "   PSPSum: " << PSPSum <<  "   Refraction: " << RefractPotential << "   delta-Membrane: " << PSPSum+RefractPotential << "  Poids: " << evt.ImpactingSynapse->getWeight() << endl;
  }*/

  LastPSP = evt;
  
  return newMembranePotential;
}

/****************************************************************************/
void CNeuron::emitSpike (NNLtime emissionTime)
{
  NNLevent * eventTemp;
  
  if (emissionTime < CurrentTime) 
  {
    cout << endl << " *** WARNING *** emissionTime < CurrentTime: " << emissionTime << " Current time: " << CurrentTime << endl;
    cout << " *** WARNING *** EmittingNeuron: " << getID() << "(" << getLocalID () << ")" << endl;
    if (Container != NULL)
      cout << "from " << Container->getName () << endl;
    
    //test ();
  }

  
  /* Enqueue this spike as an event */
  eventTemp = new NNLevent ();
  eventTemp->EventType = EVT_SPIKE;
  eventTemp->EmittingNeuron = this;
  eventTemp->Time = emissionTime;
  EventQueue.push (*eventTemp);
  delete eventTemp;
  
  /* Enqueue post synaptic impacts for this spike, for each synapse */
  vector<CSynapse*>::iterator it = OutputSynapses.begin();
  
  while (it != OutputSynapses.end())
  {
    eventTemp = new NNLevent ();
    eventTemp->EventType = EVT_SPIKE_IMPACT;
    eventTemp->EmittingNeuron = this;
    eventTemp->ImpactingSynapse = (CSynapse*)(*it);
    eventTemp->Time = emissionTime + (((CSynapse*)(*it))->getDelay());
    EventQueue.push (*eventTemp);
    delete eventTemp;
    it++;
  }
  
  /* Reset the PSP sum */
  PSPSum = 0;
  
  /* Say the last spike of this neuron is this spike */
  LastSpike = CurrentTime;
  
  /* Make the learning for all input synapses */
  it = InputSynapses.begin();
  
  while (it != InputSynapses.end())
  {
    ((CSynapse *)(*it))->Learn (LastSpike);
    it++;
  }
}

/****************************************************************************/
void CNeuron::ImpactedBy (NNLevent & evt)
{
  
  if (evt.EventType == EVT_SPIKE_IMPACT)
  {    
    /* Only if the last spike is older than 80 ms */
    if (CurrentTime - LastSpike > AbsoluteRefractoryPeriode)
    {
      //if (/*!evt.ImpactingSynapse->isInhibitory() &&*/ (evt.ImpactingSynapse->getWeight () >= 0.3) /*&& /*(evt.EmittingNeuron->getAssembly () == Container)*/)
      
      NNLevent * newEvt = new NNLevent (evt);
      SpikingPSPs.push_back (newEvt);
      
      
      /* Delete PSP that occured longer than 2 ms ago */
      while ( (SpikingPSPs.size() != 0) && (SpikingPSPs.front()->Time < CurrentTime - (TauPSP*5)))
      {
        delete SpikingPSPs.front();
        SpikingPSPs.pop_front ();
      }
    }
  
    
    /* Let us remember the last spike time for the learning (if the neuron spiked at 
       this time, the PSP-learning on the synapses must be done with the previous spike time) */
    if ((LastSpike != CurrentTime) && (LastSpike != -1))
      OldSpike = LastSpike;
    
    /* Make the learning for this impacting synapse */
    evt.ImpactingSynapse->Learn (OldSpike);
    
    
    /* Recalculate membrane potential with this PSP, only if the neuron is not in refractory period */
    if ((LastSpike == -1) || ((CurrentTime - LastSpike) >= AbsoluteRefractoryPeriode))
    {
      MembranePotential = InputFunction (evt);
            
      if (MembranePotential >= Threshold)
      {
        emitSpike (CurrentTime);
        SpikingPSP = evt;
        
        if ( Container->getName () == "Reservoir" )
        {
          NNLevent * evtTmp;
          evtTmp = new NNLevent ();
          evtTmp->EventType = EVT_SAVE_SPIKE_PSP;
          evtTmp->Time = CurrentTime + 1;
          evtTmp->EmittingNeuron = this;
          
          EventQueue.push (*evtTmp);
          delete evtTmp;
        }
      }
    }
    
  }
}

/****************************************************************************/
bool CNeuron::isInhibitory ()
{
  return Inhibitory;
}

/****************************************************************************/
/*istream & operator >> (istream & )
{
  
}
*/
/****************************************************************************/
/*ostream & operator << (ostream & )
{
  
  <NEURON>
    int (ID)
    NNLpotential (MembranePotential)
    NNLpotential (Threshold)

    int (nbinputsynapses)
      idsynapse
      ...
  
    int nboutputsynapses
      idsynapse
      ...
    
    int (nbevents)
      <event> (LastPSPs)
  
    NNLtime (LastSpike)
    bool (impacted)
    bool (inhib)
  </NEURON>
  
}*/

/****************************************************************************/
int CNeuron::getID ()
{
  return ID;
}

/****************************************************************************/
vector<CSynapse *> * CNeuron::getOutputSynapses ()
{
  return & OutputSynapses;
}

/****************************************************************************/
vector<CSynapse *> * CNeuron::getInputSynapses ()
{
  return & InputSynapses;
}

/****************************************************************************/
NNLpotential CNeuron::getMembranePotential ()
{
  return MembranePotential;
}

/****************************************************************************/
CAssembly * CNeuron::getAssembly ()
{
  return Container;
}

/****************************************************************************/
void CNeuron::setAssembly (CAssembly * newContainer)
{
  if (Container != NULL)
    Container->getNeurons ()->erase (find (Container->getNeurons ()->begin(), Container->getNeurons ()->end (), this));
  
  Container = newContainer;
}

/****************************************************************************/
int CNeuron::getLocalID ()
{
  return localID;
}

/****************************************************************************/
void CNeuron::setID (int newID)
{
  ID = newID;
}

/****************************************************************************/
void CNeuron::setLocalID (int newID)
{
  localID = newID;
}

/****************************************************************************/
NNLtime CNeuron::getLastSpike ()
{
  return LastSpike;
}

/****************************************************************************/
NNLevent & CNeuron::getSpikingPSP ()
{
  return SpikingPSP;
}

/****************************************************************************/
list<NNLevent *> * CNeuron::getSpikingPSPs ()
{
  return & SpikingPSPs;
}

/****************************************************************************/
NNLtime CNeuron::getTauPSP ()
{
  return TauPSP;
}

/****************************************************************************/
NNLtime CNeuron::getTauRefract ()
{
  return TauRefract;
}

/****************************************************************************/
void CNeuron::InitPSPShape ()
{
  int lowLimit = 0;
  int upLimit = 5*TauPSP;
  
  int NbVal = upLimit - lowLimit + 1;
  
  PSPShape = new NNLpotential[NbVal];
  
  for (int t = 0; t < NbVal; t++)
  {
    PSPShape[t] = PSPValue(t);
  }
}

/****************************************************************************/
void CNeuron::InitRefractShape ()
{
  int lowLimit = 0;
  int upLimit = 5*TauRefract;
  
  int NbVal = upLimit - lowLimit + 1;
  
  RefractShape = new NNLpotential[NbVal];
  
  for (int t = 0; t < NbVal; t++)
  {
    RefractShape[t] = RefractValue(t);
  }
}

/****************************************************************************/
bool CNeuron::hasOutput (CNeuron * neuron)
{
  bool Found = false; //did we find neuron in output of "this" neuron
  
  vector <CSynapse *>::iterator itOutSynapse = OutputSynapses.begin ();
  vector <CSynapse *>::iterator itLastOutSynapse = OutputSynapses.end ();
  
  while ( (itOutSynapse != itLastOutSynapse) && (!Found) )
  {
    if ( (*itOutSynapse)->getOutputNeuron () == neuron )
      Found = true;
    
    itOutSynapse++;
  }
  
  return Found;
}

/****************************************************************************/
TiXmlElement * CNeuron::XMLsave ()
{
  TiXmlElement * xmlNeuron = new TiXmlElement("Neuron");
  
  xmlNeuron->SetAttribute("id", ID);
  xmlNeuron->SetAttribute("inhibitory", (int) Inhibitory);
  xmlNeuron->SetDoubleAttribute("refractoryPeriod", (double)AbsoluteRefractoryPeriode/(double)CParameters::TIMESCALE);
  xmlNeuron->SetDoubleAttribute("refractoryPotential", AbsoluteRefractPotential);
  xmlNeuron->SetDoubleAttribute("restingPotential", (double)RestingPotential);
  xmlNeuron->SetDoubleAttribute("threshold", Threshold);
  xmlNeuron->SetDoubleAttribute("tauPSP", (double)TauPSP/(double)CParameters::TIMESCALE);
  xmlNeuron->SetDoubleAttribute("tauRefract", (double)TauRefract/(double)CParameters::TIMESCALE);
  
  vector<CSynapse *>::iterator itSynapse = OutputSynapses.begin ();
  vector<CSynapse *>::iterator lastSynapse = OutputSynapses.end ();
  
  while (itSynapse != lastSynapse)
  {
    TiXmlElement * xmlSynapse = (*(itSynapse))->XMLsave();
    xmlNeuron->LinkEndChild(xmlSynapse);
    itSynapse++;
  }
  
  return xmlNeuron;
}

/****************************************************************************/
bool CNeuron::XMLload (TiXmlElement * pXmlNeuron, map<int, CNeuron*> * NeuronByID)
{
  /*
	<Document>
		<Element attributeA = "valueA">
			<Child attributeB = "value1" />
			<Child attributeB = "value2" />
		</Element>
	<Document>
	

Assuming you want the value of "attributeB" in the 2nd "Child" element, it's very easy to write a *lot* of code that looks like:

	TiXmlElement* root = document.FirstChildElement( "Document" );
	if ( root )
	{
		TiXmlElement* element = root->FirstChildElement( "Element" );
		if ( element )
		{
			TiXmlElement* child = element->FirstChildElement( "Child" );
			if ( child )
			{
				TiXmlElement* child2 = child->NextSiblingElement( "Child" );
				if ( child2 )
				{
					// Finally do something useful.
	*/
  
  // id="466" inhibitory="0" refractoryPeriod="70" refractoryPotential="20000.000000" threshold="-50.000000"
  
  CSynapse * NewSynapse;
  int tmpInhibitory;
  double tmpAbsoluteRefractoryPeriode, tmpAbsoluteRefractPotential, tmpThreshold,tmpTauPSP, tmpTauRefract, tmpRestingPotential;
  
  pXmlNeuron->Attribute ("id", &ID);
  pXmlNeuron->Attribute ("refractoryPeriod", &tmpAbsoluteRefractoryPeriode);
  tmpAbsoluteRefractoryPeriode *= CParameters::TIMESCALE;
  AbsoluteRefractoryPeriode = (NNLtime) tmpAbsoluteRefractoryPeriode;
  
  pXmlNeuron->Attribute ("refractoryPotential", &tmpAbsoluteRefractPotential);
  AbsoluteRefractPotential = (NNLpotential) tmpAbsoluteRefractPotential;
  
  pXmlNeuron->Attribute ("inhibitory", &tmpInhibitory);
  Inhibitory = (bool) tmpInhibitory;
  
  pXmlNeuron->Attribute ("threshold", &tmpThreshold);
  Threshold = (NNLpotential) tmpThreshold;
  
  if(pXmlNeuron->Attribute ("restingPotential", &tmpRestingPotential))
    RestingPotential = (NNLpotential) tmpRestingPotential;
  else
    RestingPotential = (NNLpotential)-65;


  MembranePotential = (NNLpotential) RestingPotential;
  PSPSum = (NNLpotential) 0;
  //TauPSP = (NNLtime) CParameters::TAU_PSP;
  //TauRefract = (NNLtime) CParameters::TAU_REFRACT;
  pXmlNeuron->Attribute ("tauPSP", &tmpTauPSP);
  tmpTauPSP *= CParameters::TIMESCALE;
  TauPSP = (NNLtime) tmpTauPSP;
  
  pXmlNeuron->Attribute ("tauRefract", &tmpTauRefract);
  tmpTauRefract *= CParameters::TIMESCALE;
  TauRefract = (NNLtime) tmpTauRefract;
  
  InputSynapses.clear ();
  OutputSynapses.clear ();
  LastPSP.Time = -1;
  LastSpike = -1;
  OldSpike = -1;
  PSPSum = 0;
  
  /* Generates the tabbed exponential values */
  InitPSPShape ();
  InitRefractShape ();
  
  TiXmlElement* pXmlSynapse = pXmlNeuron->FirstChildElement( "SynapseOut" );

  while (pXmlSynapse)
  {
    //cout << root->Value() << endl;
    NewSynapse = new CSynapse ();
    NewSynapse->XMLload (pXmlSynapse, NeuronByID);
    AddOutputSynapse (NewSynapse);
    pXmlSynapse = pXmlSynapse->NextSiblingElement( "SynapseOut" );
  }
  

  
  /*
  TiXmlElement * xmlNeuron = new TiXmlElement("Neuron");
  
  xmlNeuron->SetAttribute("id", ID);
  xmlNeuron->SetAttribute("inhibitory", (int) Inhibitory);
  xmlNeuron->SetAttribute("refractoryPeriod", AbsoluteRefractoryPeriode);
  xmlNeuron->SetDoubleAttribute("refractoryPotential", AbsoluteRefractPotential);
  xmlNeuron->SetDoubleAttribute("threshold", Threshold);
  
  vector<CSynapse *>::iterator itSynapse = OutputSynapses.begin ();
  vector<CSynapse *>::iterator lastSynapse = OutputSynapses.end ();
  
  while (itSynapse != lastSynapse)
  {
    TiXmlElement * xmlSynapse = (*(itSynapse))->XMLsave();
    xmlNeuron->LinkEndChild(xmlSynapse);
    itSynapse++;
  }
  
  return xmlNeuron;
  */
  
  
  return true;
}
