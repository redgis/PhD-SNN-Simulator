/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
 
#ifndef _ASSEMBLY_H
#define _ASSEMBLY_H


#include "../simulator/globals.h"

using namespace std;
  
class CAssembly
{
  private:
    vector<CNeuron *> Neurons;    /* List of neurons in this assembly */
    int NbSpikes;                 /* Number of spike at last each step */
    string Name;                  /* Name given to the assembly */
    NNLtime LastSpike;            /* Date of the last spike emitted by this assembly */
    bool Spiked;                  /* Says if the assembly has spiked at current time */
  
    int NbAnswers;
    int NbGoodAnswers;
    int NbWrongAnswers;
    int NbNoAnswers;
    int NbNoSpike;
  
  public:
    /* Constructors / destructor */
    CAssembly (string newName = "unknown assembly");
    virtual ~CAssembly ();
    
    /* Access functions */
    CNeuron * AddNeuron (CNeuron *);
    CNeuron * AddNeuron (NeuronParameters *);
    void AddXNeurons (NeuronParameters *, int);
    vector<CNeuron *> * getNeurons ();
    
    void ResetNbSpikes ();
    void AddSpike ();
    int getNbSpikes ();
    
    void setLastSpike (NNLtime);
    NNLtime getLastSpike ();
    
    string & getName ();
    
    bool hasSpiked (); /* Say if the assembly has already emitted a spike at this time */
    
    int LearnDelays ();
    int LearnDelays2 ();
    int LearnDelaysMulticlass (NNLtime StimInterval);
    
    int GenePerfo ();
    int GenePerfoMulticlass (int StimInterval);
    void ShowStats ();
    
    void InitPerfo ();
    
    TiXmlElement * XMLsave (); /* XML output */
    bool XMLload (TiXmlElement *, map<int, CNeuron*> *); /* XML input */

    CNeuron * operator[] (size_t pos);
};




#endif /* _ASSEMBLY_H */
