/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef _NEURON_H
#define _NEURON_H

#include "../simulator/globals.h"
#include "../events/event.h"

class CNeuron
{
  protected:
    int ID;
    int localID;
    
    NNLpotential MembranePotential;
    NNLpotential PSPSum;
    NNLpotential AbsoluteRefractPotential; /* Absolute refractory potential */
    NNLpotential Threshold;
    NNLpotential RestingPotential;
    NNLtime AbsoluteRefractoryPeriode;
    NNLtime TauPSP;
    NNLtime TauRefract;
    NNLpotential * PSPShape;      /* Shape of a PSP */
    NNLpotential * RefractShape;  /* Shape of the relative refractory period */
    
    CAssembly * Container;
    vector<CSynapse *> InputSynapses;
    vector<CSynapse *> OutputSynapses;
    
    list<NNLevent *> SpikingPSPs; /* PSP which made the spike */
    NNLevent SpikingPSP;    /* PSP which made the spike */
    NNLevent LastPSP;       /* last PSP event recieved */
    NNLtime LastSpike;      /* -1 = never */
    NNLtime OldSpike;       /* backup of the previous spike emitted (before LastSpike) */

    bool Inhibitory; /* true if this is an inhibitory neuron */
  
    NNLpotential PSPFunction (NNLtime t);
    NNLpotential RefractoryPotential (NNLtime t);
    NNLpotential InputFunction (NNLevent &); /* Input calculation function */
    
  public:
    static int IDcounter;  
  
    void test ();
  
    /* Constructor / destructor */
    CNeuron (NeuronParameters * param, CAssembly * pContainer);
    CNeuron (CNeuron & neuron);
    virtual ~CNeuron ();
    
    NNLpotential PSPValue (int t);
    NNLpotential RefractValue (int t);
  
    int getID ();       /* Get the ID of the neuron */
    int getLocalID ();  /* Get ID of the neuron in the assembly */
    void setID (int);
    void setLocalID (int);
  
    NNLtime getTauPSP (); 
    NNLtime getTauRefract ();
  
    NNLpotential getMembranePotential ();
    CAssembly * getAssembly ();
    void setAssembly (CAssembly *);
  
    void emitSpike (NNLtime);  /* Post a spike event */
  
    void ImpactedBy (NNLevent &); /* Adds this SPIKE_IMPACT_EVENT to NewPSPs list */
    bool isInhibitory ();  /* Returns the inhibition flag */
  
    /* Adding synapses */
    void AddInputSynapse (CSynapse *);
    void AddOutputSynapse (CSynapse *);
  
    vector<CSynapse *> * getOutputSynapses ();
    vector<CSynapse *> * getInputSynapses ();
    
    NNLtime getLastSpike ();
    NNLevent & getSpikingPSP ();
    list<NNLevent *> * getSpikingPSPs ();
    
    void InitPSPShape ();     /* Sets the PSP discretised values */
    void InitRefractShape (); /* Sets the refractory discretised values */
    
    TiXmlElement * XMLsave (); /* XML output */
    bool XMLload (TiXmlElement *, map<int, CNeuron*> *); /* XML input */

    bool hasOutput (CNeuron * );
  
    /* istream & operator >> (istream & );
    ostream & operator << (ostream & ); */
};


#endif /* _NEURON_H */
