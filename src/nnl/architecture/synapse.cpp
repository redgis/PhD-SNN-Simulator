/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
 
 
#include "synapse.h"

#include "parameters.h"
#include "neuron.h"

int CSynapse::IDcounter = 0;

/****************************************************************************/
CSynapse::CSynapse ()
{
  Weight = (NNLweight) 1.0;
  Delay = (NNLtime) 1;
  
  WeightAdaptation = false;
  DelayAdaptation = false;
  Inhibitory = false;
  
  LastImpact = -1;

  ID = IDcounter;
  IDcounter++;
}

/****************************************************************************/
CSynapse::CSynapse (SynapseParameters * param, CNeuron * Neuron1 = NULL, CNeuron * Neuron2 = NULL)
{
  
  //NNLtime deltaD;
  //NNLweight deltaW;
  
  if (param->Weight == (NNLweight) -1)
  {
    do
    {
      Weight = (NNLweight) ((((float)rand()) / ((float)RAND_MAX)) * (float) CParameters::MAX_WEIGHT);
    } while ((Weight < CParameters::MIN_WEIGHT) || (Weight > CParameters::MAX_WEIGHT));
    //cout << Weight << endl;
  }
  else
  {
    if (param->DeltaWeight == 0)
      Weight = param->Weight;
    else
    {
      do
      {
        //deltaW = (NNLweight) ((((float)rand() - (float) (RAND_MAX/2)) / ((float)RAND_MAX)) * (float) param->DeltaWeight);
        //Weight = (NNLweight) param->Weight + deltaW;
        Weight = (NNLweight) ((((float)rand()) / ((float)RAND_MAX)) * (float) CParameters::MAX_WEIGHT);
      } while ((Weight < CParameters::MIN_WEIGHT) || ((param->DeltaWeight != 0) && (Weight > CParameters::MAX_WEIGHT)) || ((double)rand() > GaussianProba(Weight, param->DeltaWeight, param->Weight) * (double)RAND_MAX) );
      // condition (param->DeltaWeight != 0) && ... is to check if the weight is a fixed value (ie for input : Weight = 3.0, deltaweight = 0
    }
  }
  
  if (param->Delay == (NNLtime) -1)
  {
    do
    {
      Delay = (NNLtime) ((((float)rand()) / ((float)RAND_MAX)) * (float) CParameters::MAX_DELAY) + (NNLtime) 1;
    } while ((Delay < CParameters::MIN_DELAY) || (Delay > CParameters::MAX_DELAY));
  }
  else
  {
    if ( param->DeltaDelay == 0)
      Delay = param->Delay;
    else
    {
      do
      {
      //deltaD = (NNLtime) ((((float)rand() - (float) (RAND_MAX/2)) / ((float)RAND_MAX)) * (float) param->DeltaDelay);
      //Delay = (NNLtime) param->Delay + deltaD;
        Delay = (NNLtime) ((((float)rand()) / ((float)RAND_MAX)) * (float) CParameters::MAX_DELAY) + (NNLtime) 1;
      } while ((Delay < CParameters::MIN_DELAY) || ((param->DeltaDelay != 0) && (Delay > CParameters::MAX_DELAY)) || ((double)rand() > GaussianProba(Delay, param->DeltaDelay, param->Delay) * (double)RAND_MAX) );
    }
  }
  

//  cout << "Weight   : " << Weight << endl;
//  cout << "synWeight:" << param->Weight << endl;
//  cout << "synDeltaW:" << param->DeltaWeight << endl;
//  cout << "proba    :" << GaussianProba(Weight, param->DeltaWeight, param->Weight) << endl;
//  cout << "Delay    : " << Delay << endl;
//  cout << "synDelay :" << param->Delay << endl;
//  cout << "synDeltaD:" << param->DeltaDelay << endl;
//  cout << "proba    :" << GaussianProba(Delay, param->DeltaDelay, param->Delay) << endl;

  //if  (Weight < CParameters::MIN_WEIGHT)
  //  Weight = CParameters::MIN_WEIGHT;
  
  // commented for input => reservoir with weight = 3
  //if  (Weight > CParameters::MAX_WEIGHT)
  //  Weight = CParameters::MAX_WEIGHT;

  //if  (Delay < CParameters::MIN_DELAY)
  //  Delay = CParameters::MIN_DELAY;
  
  //if  (Delay > CParameters::MAX_DELAY)
  //  Delay = CParameters::MAX_DELAY;
  
  
  WeightAdaptation = param->WeightAdaptation;
  DelayAdaptation = param->DelayAdaptation;

  if (param->Inhibitory != UNSPECIFIED)
    Inhibitory = param->Inhibitory;
  else
  {
    if (Neuron1)
      Inhibitory = (NNLbool) Neuron1->isInhibitory ();
    else
      Inhibitory = false;
  }
  
  LastImpact = -1;
  
  InputNeuron = Neuron1;
  OutputNeuron = Neuron2;

  ID = IDcounter;
  IDcounter++;
}

/****************************************************************************/
CSynapse::~CSynapse ()
{
  
}

/****************************************************************************/
void CSynapse::Disconnect ()
{
  InputNeuron->getOutputSynapses ()->erase (find (InputNeuron->getOutputSynapses ()->begin (), InputNeuron->getOutputSynapses ()->end (), this));
  OutputNeuron->getInputSynapses ()->erase (find (OutputNeuron->getInputSynapses ()->begin (), OutputNeuron->getInputSynapses ()->end (), this));
  InputNeuron = NULL;
  OutputNeuron = NULL;
}

/****************************************************************************/
vector<CSynapse *>::iterator CSynapse::DisconnectInput ()
{
  vector<CSynapse *>::iterator res = InputNeuron->getOutputSynapses ()->erase (find (InputNeuron->getOutputSynapses ()->begin (), InputNeuron->getOutputSynapses ()->end (), this));
  InputNeuron = NULL;
  return res;
}

/****************************************************************************/
vector<CSynapse *>::iterator CSynapse::DisconnectOutput ()
{
  vector<CSynapse *>::iterator res = OutputNeuron->getInputSynapses ()->erase (find (OutputNeuron->getInputSynapses ()->begin (), OutputNeuron->getInputSynapses ()->end (), this));
  OutputNeuron = NULL;
  return res;
}

/****************************************************************************/
void CSynapse::Learn (NNLtime b)
{
  if (UnsupervisedLearning)
  {
    if (WeightAdaptation && (LastImpact != -1) && (b != -1))
    {
      if (CParameters::STDP_TYPE == MEUNIER_STDP)
        Weight += STDPFunction (LastImpact, b);//AdditiveSTDPFunction (LastImpact, b); // STDPFunction (LastImpact, b);
      else
        Weight += STDPClassicalFunction (LastImpact, b); // STDPAdditiveIzhikevichFunction (LastImpact, b); // STDPClassicalFunction (LastImpact, b);

      if (Weight < CParameters::MIN_WEIGHT)
        Weight = CParameters::MIN_WEIGHT;
      if (Weight > CParameters::MAX_WEIGHT)
        Weight = CParameters::MAX_WEIGHT;
    }
    
    if (DelayAdaptation && (LastImpact != -1) && (b != -1 ))
        Delay += DelayFunction (LastImpact, b);
  }
}

/****************************************************************************/
NNLweight CSynapse::AdditiveSTDPFunction (NNLtime tPre, NNLtime tPost)
{
  /* See Meunier and Paugam-Moisy, BioCyb 2006 for STDP functions used here */
  
  NNLweight Result = 0;
  
  NNLtime x = tPost-tPre;
  NNLweight aexc = CParameters::STDP_AEXC;
  NNLweight ainh = CParameters::STDP_AINH;
  
  //cout << "poids pre-STDP: " << Weight << endl;
  
  if (Inhibitory) /* Inhibitory STDP */
  {
    if ((x >= -20*CParameters::TIMESCALE) && (x <= 20*CParameters::TIMESCALE))
      Result = (NNLweight) 1.0;
    else
      Result = (NNLweight) -0.25;
    
    
    if (Result <= 0)
      Result =  ainh /* * (Weight) */ * Result;
    else
      Result = ainh /* * ((-Weight) + (NNLweight)1.0) */ * Result;
  }
  else  /* Excitatory STDP */
  {
    if ( (x >= -100*CParameters::TIMESCALE) && (x < 0*CParameters::TIMESCALE) )
      Result = ( ((NNLweight) -1.0) / ((NNLweight) 200.0) ) * (((NNLweight)x)/((NNLweight)CParameters::TIMESCALE)) - (NNLweight) 0.5;
    else if ( (x >= 0*CParameters::TIMESCALE) && (x <= 20*CParameters::TIMESCALE) )
      Result = (NNLweight) - fabs( ( ((NNLweight)1)/((NNLweight)10.0) ) * ((((NNLweight)x)/((NNLweight)CParameters::TIMESCALE)) - (NNLweight)10.0) ) + (NNLweight)1.0;
    
    
    if (Result <= 0)
      Result = aexc /* * Weight */ * Result;
    else
      Result = aexc /* * ((NNLweight) 1.0 - Weight) */ * Result;
  }

  //cout << "poids post-STDP: " << Weight + Result << endl;
  if (Result > 0)
  {
    WeightPositiveModificationAmount += fabs(Result);
    WeightPositiveModificationCount ++;
  }
  else if (Result < 0)
  {
    WeightNegativeModificationAmount += fabs(Result);
    WeightNegativeModificationCount ++;
  }

  return Result;
}

/****************************************************************************/
NNLweight CSynapse::STDPFunction (NNLtime tPre, NNLtime tPost)
{
  /* See Meunier and Paugam-Moisy, BioCyb 2006 for STDP functions used here */
  
  NNLweight Result = 0;
  
  NNLtime x = tPost-tPre;
  NNLweight aexc = CParameters::STDP_AEXC;
  NNLweight ainh = CParameters::STDP_AINH;
  
  //cout << "poids pre-STDP: " << Weight << endl;
  
  if (Inhibitory) /* Inhibitory STDP */
  {
    if ((x >= -20*CParameters::TIMESCALE) && (x <= 20*CParameters::TIMESCALE))
      Result = (NNLweight) 1.0;
    else
      Result = (NNLweight) -0.25;
    
    
    if (Result <= 0)
      Result =  ainh * (Weight) * Result;
    else
      Result = ainh * ((-Weight) + (NNLweight)1.0) * Result;
  }
  else  /* Excitatory STDP */
  {
    if ( (x >= -100*CParameters::TIMESCALE) && (x < 0*CParameters::TIMESCALE) )
      Result = ( ((NNLweight) -1.0) / ((NNLweight) 200.0) ) * (((NNLweight)x)/((NNLweight)CParameters::TIMESCALE)) - (NNLweight) 0.5;
    else if ( (x >= 0*CParameters::TIMESCALE) && (x <= 20*CParameters::TIMESCALE) )
      Result = (NNLweight) - fabs( ( ((NNLweight)1)/((NNLweight)10.0) ) * ((((NNLweight)x)/((NNLweight)CParameters::TIMESCALE)) - (NNLweight)10.0) ) + (NNLweight)1.0;
    
    
    if (Result <= 0)
      Result = aexc * Weight * Result;
    else
      Result = aexc * ((NNLweight) 1.0 - Weight) * Result;
  }

  //cout << "poids post-STDP: " << Weight + Result << endl;
  if (Result > 0)
  {
    WeightPositiveModificationAmount += fabs(Result);
    WeightPositiveModificationCount ++;
  }
  else if (Result < 0)
  {
    WeightNegativeModificationAmount += fabs(Result);
    WeightNegativeModificationCount ++;
  }

  return Result;
}

/****************************************************************************/
NNLweight CSynapse::STDPClassicalFunction (NNLtime tPre, NNLtime tPost)
{
  /* See Meunier and Paugam-Moisy, BioCyb 2006 for STDP functions used here */
  
  NNLweight Result = 0;
  
  NNLtime x = tPost-tPre;
  NNLweight aexc = CParameters::STDP_AEXC;
  NNLweight ainh = CParameters::STDP_AINH;
  
  //cout << "poids pre-STDP: " << Weight << endl;
  
  if (Inhibitory) /* Inhibitory STDP */
  {
    if ((x >= -20*CParameters::TIMESCALE) && (x <= 20*CParameters::TIMESCALE))
      Result = (NNLweight) 1.0;
    else
      Result = (NNLweight) -0.25;
    
    if (Result <= 0)
      Result =  ainh * (Weight) * Result;
    else
      Result = ainh * ((-Weight) + (NNLweight)1.0) * Result;
  }
  else  /* Excitatory STDP */
  {
    if ( (x >= -20*CParameters::TIMESCALE) && (x <= 0*CParameters::TIMESCALE) )
      Result = (NNLweight) (-x / (NNLweight)20 - (NNLweight) 1);
    else if ( (x > 0*CParameters::TIMESCALE) && (x <= 20*CParameters::TIMESCALE) )
      Result = (NNLweight) (-x / (NNLweight)20 + (NNLweight) 1);
    else if (x == 0*CParameters::TIMESCALE)
      Result = 0;
    
    
    if (Result <= 0)
      Result = aexc * Weight * Result;
    else
      Result = aexc * ((NNLweight) 1.0 - Weight) * Result;
  }

  //cout << "poids post-STDP: " << Weight + Result << endl;
  
  return Result;
}

/****************************************************************************/
NNLweight CSynapse::STDPAdditiveClassicalFunction (NNLtime tPre, NNLtime tPost)
{
  /* See Meunier and Paugam-Moisy, BioCyb 2006 for STDP functions used here */
  
  NNLweight Result = 0;
  
  NNLtime x = tPost-tPre;
  NNLweight aexc = CParameters::STDP_AEXC;
  NNLweight ainh = CParameters::STDP_AINH;
  
  //cout << "poids pre-STDP: " << Weight << endl;
  
  if (Inhibitory) /* Inhibitory STDP */
  {
    if ((x >= -20*CParameters::TIMESCALE) && (x <= 20*CParameters::TIMESCALE))
      Result = (NNLweight) 1.0;
    else
      Result = (NNLweight) -0.25;
    
    if (Result <= 0)
      Result =  ainh /* * (Weight) */ * Result;
    else
      Result = ainh /* * ((-Weight) + (NNLweight)1.0) */ * Result;
  }
  else  /* Excitatory STDP */
  {
    if ( (x >= -20*CParameters::TIMESCALE) && (x < 0*CParameters::TIMESCALE) )
      Result = (NNLweight) (-x / (NNLweight)20 - (NNLweight) 1);
    else if ( (x >= 0*CParameters::TIMESCALE) && (x <= 20*CParameters::TIMESCALE) )
      Result = (NNLweight) (-x / (NNLweight)20 + (NNLweight) 1);
/*    else if (x == 0)
      Result = 0;*/
    
    
    if (Result <= 0)
      Result = aexc /* * Weight */ * Result;
    else
      Result = aexc /* * ((NNLweight) 1.0 - Weight) */ * Result;
  }

  //cout << "poids post-STDP: " << Weight + Result << endl;
  
  return Result;
}

/****************************************************************************/
NNLweight CSynapse::STDPAdditiveIzhikevichFunction (NNLtime tPre, NNLtime tPost)
{
  /* See Meunier and Paugam-Moisy, BioCyb 2006 for STDP functions used here */
  
  if (Inhibitory)
    return 0;
        
  NNLweight Result = 0;
  
  NNLtime x = tPost-tPre;
  NNLweight aexc = 0.12;
  NNLweight ainh = 0.1;
  
  //cout << "poids pre-STDP: " << Weight << endl;
  
    //- exp((double)(-((float)t)/((float)TauRefract)));
    
  if ( (x >= -20*CParameters::TIMESCALE) && (x < 0*CParameters::TIMESCALE) )
    Result = (NNLpotential) - ainh * ExpTabIzhiSTDP[-x];
  else if ( (x >= 0*CParameters::TIMESCALE) && (x <= 20*CParameters::TIMESCALE) )
    Result = (NNLpotential) aexc * ExpTabIzhiSTDP[x]; 
  
    
    
  
  //cout << "poids post-STDP: " << Weight + Result << endl;
  
  return Result;
}

/****************************************************************************/
NNLtime CSynapse::DelayFunction (NNLtime tPre, NNLtime tPost)
{
  /* See Meunier and Paugam-Moisy, BioCyb 2006 for STDP functions used here */
  
  //NNLtime Result = 0;
  
  NNLtime Delta = 0;
  
  //NNLtime x = tPost-tPre;
  
  //cout << OutputNeuron->getLocalID () << " " << CurrentClass << endl;
  if (CurrentClass == OutputNeuron->getLocalID ())
  {
    if (Delay > 0)
    Delta = -CParameters::DELAY_LEARNING_JITTER;
    
    if (Weight < CParameters::MAX_WEIGHT)
      Weight += 0.01;
  }
  else
  {
    if (Delay < CParameters::MAX_DELAY)
    Delta = CParameters::DELAY_LEARNING_JITTER;
    
    if (Weight > 0.2)
      Weight -= 0.01;
  }    
  
  
  //cout << "delai pre-STDP: " << Delay;
  
  //~ if (Inhibitory) /* Inhibitory STDP */
  //~ {
    //~ if ((x >= -20) && (x <= 20))
      //~ Result = -1.0;
    //~ else
      //~ Result = 0.25;
    
    //~ if (Result <= 0)
      //~ Delta =  (NNLtime) (ainh * ((float)Delay) * Result);
    //~ else
      //~ Delta = (NNLtime) (ainh * ((float)(MAX_DELAY - Delay)) * Result);
  //~ }
  //~ else  /* Excitatory STDP */
  //~ {
    /*if ( (x >= -10) && (x <= -1))
      Result = -1;
    else if ( ((x >= -20) && (x <= -10)) 
          ||  ((x >= 10) && (x <= 20)) )
      Result = 1;
*/
    
    //cout << "(result= "<< Result << ") ";
   
    //Delta = Result;
    //~ if (Result <= 0)
      //~ Delta = (NNLtime) (aexc * ((float)Delay) * Result);
    //~ else
      //~ Delta = (NNLtime) (aexc * ((float) (MAX_DELAY - Delay)) * Result);
  //~}

  //cout << " ==> " << Delay + Delta << " (" << Delta << ")" << endl;

  return Delta;
}

/****************************************************************************/
NNLtime CSynapse::getDelay ()
{
  return Delay;
}

/****************************************************************************/
NNLpotential CSynapse::getWeight ()
{
  return Weight;
}

/****************************************************************************/
void CSynapse::setLastImpact (NNLtime t)
{
  LastImpact = t;
}

/****************************************************************************/
CNeuron * CSynapse::getOutputNeuron ()
{
  return OutputNeuron;
}

/****************************************************************************/
CNeuron * CSynapse::getInputNeuron ()
{
  return InputNeuron;
}

/****************************************************************************/
bool CSynapse::isInhibitory ()
{
  return Inhibitory;
}

/****************************************************************************/
bool CSynapse::isLearningWeight ()
{
  return WeightAdaptation;
}

/****************************************************************************/
bool CSynapse::isLearninDelay ()
{
  return DelayAdaptation;
}

/****************************************************************************/
int CSynapse::getID ()
{
  return ID;
}

/****************************************************************************/
bool CSynapse::IncreaseDelay ()
{
  bool Res = true;
  
  Delay += (int) CParameters::DELAY_LEARNING_JITTER;
  
  // cout << "Increase : " << Delay << endl;
  
  if (Delay > CParameters::MAX_DELAY)
  {
    Res = false;
    Delay = CParameters::MAX_DELAY;
    
    SaturationSup++;
    //cout << "Saturation a MAX_DELAY, neurone: " << InputNeuron->getID () << endl;
  }
  
  return Res;
}

/****************************************************************************/
bool CSynapse::DecreaseDelay ()
{
  bool Res = true;
  
  Delay -= (int) CParameters::DELAY_LEARNING_JITTER;
  
  // cout << "Decrease : " << Delay << endl;
  
  if (Delay < CParameters::MIN_DELAY)
  {
    Res = false;
    Delay = CParameters::MIN_DELAY;
    SaturationInf++;
    //cout << "Saturation a MIN_DELAY, neurone: " << InputNeuron->getID () << endl;
  }
  
  return Res;
}

/****************************************************************************/
TiXmlElement * CSynapse::XMLsave ()
{
  TiXmlElement * xmlSynapse = new TiXmlElement("SynapseOut");
  
  xmlSynapse->SetDoubleAttribute("weight", Weight);
  xmlSynapse->SetDoubleAttribute("delay", (double)((double)Delay)/(double)CParameters::TIMESCALE);
  xmlSynapse->SetAttribute("inhibitory", (int) Inhibitory);
  xmlSynapse->SetAttribute("weightSTDP", (int) WeightAdaptation);
  xmlSynapse->SetAttribute("delaySTDP", (int) DelayAdaptation);
  xmlSynapse->SetAttribute("inputNeuron", InputNeuron->getID());
  xmlSynapse->SetAttribute("outputNeuron", OutputNeuron->getID());

  return xmlSynapse;
}

/****************************************************************************/
TiXmlElement * CSynapse::XMLload (TiXmlElement * pXmlSynapse, map<int, CNeuron*> * NeuronByID)
{
  /*
	<Document>
		<Element attributeA = "valueA">
			<Child attributeB = "value1" />
			<Child attributeB = "value2" />
		</Element>
	<Document>
	

Assuming you want the value of "attributeB" in the 2nd "Child" element, it's very easy to write a *lot* of code that looks like:

	TiXmlElement* root = document.FirstChildElement( "Document" );
	if ( root )
	{
		TiXmlElement* element = root->FirstChildElement( "Element" );
		if ( element )
		{
			TiXmlElement* child = element->FirstChildElement( "Child" );
			if ( child )
			{
				TiXmlElement* child2 = child->NextSiblingElement( "Child" );
				if ( child2 )
				{
					// Finally do something useful.
	*/
  
  // weight="3.000000" delay="79" inhibitory="0" weightSTDP="0" delaySTDP="0" inputNeuron="470" outputNeuron="772"
  
  int tmpInputNeuron, tmpOutputNeuron;
  CNeuron * tmpNeuron;
  double tmpWeight, tmpDelay;
  int tmpInhibitory, tmpWeightAdaptation, tmpDelayAdaptation;
  
  
  pXmlSynapse->Attribute ("weight", &tmpWeight);
  Weight = (NNLweight) tmpWeight;
  //cout << Weight << " ";
  
  pXmlSynapse->Attribute ("delay", &tmpDelay);
  tmpDelay *= (double)CParameters::TIMESCALE;
  Delay = (NNLtime) tmpDelay;
  
  pXmlSynapse->Attribute ("inhibitory", &tmpInhibitory);
  Inhibitory = (bool) tmpInhibitory;
  
  pXmlSynapse->Attribute ("weightSTDP", &tmpWeightAdaptation);
  WeightAdaptation = (bool) tmpWeightAdaptation;
  
  pXmlSynapse->Attribute ("delaySTDP", &tmpDelayAdaptation);
  DelayAdaptation = (bool) tmpDelayAdaptation;
  
  pXmlSynapse->Attribute ("inputNeuron", &tmpInputNeuron);
  pXmlSynapse->Attribute ("outputNeuron", &tmpOutputNeuron);
  
  if ((*NeuronByID)[tmpOutputNeuron] == NULL)
  {
    tmpNeuron = new CNeuron (NULL, (CAssembly*) NULL);
    (*NeuronByID)[tmpOutputNeuron] = tmpNeuron;
  }
  
  (*NeuronByID)[tmpOutputNeuron]->setID (tmpOutputNeuron);
  (*NeuronByID)[tmpOutputNeuron]->AddInputSynapse (this);
  
  InputNeuron = (*NeuronByID)[tmpInputNeuron];
  OutputNeuron = (*NeuronByID)[tmpOutputNeuron];
  
  //cout << tmpInputNeuron << ";" << tmpOutputNeuron << " ";
  //cout << InputNeuron << ";" << OutputNeuron << endl;
  
  return pXmlSynapse;
}
