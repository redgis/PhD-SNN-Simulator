/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef _SYNAPSE_H
#define _SYNAPSE_H

#include "../simulator/globals.h"


class CSynapse
{
  protected:
    static int IDcounter;
    int ID;
  
    NNLweight Weight;       /* Weight of the synapse */
    NNLtime Delay;          /* Transmission delay from input neuron to output neuron */
    
    bool WeightAdaptation;  /* Weight online learning ? */
    bool DelayAdaptation;   /* Delay online learning ? */
    
    bool Inhibitory;        /* Inhibitory or excitatory synapse ? */
    
    CNeuron * OutputNeuron;
    CNeuron * InputNeuron;
    
    NNLtime LastImpact;
    
    virtual NNLweight AdditiveSTDPFunction (NNLtime, NNLtime);            /* Weight adaptation fucntion */
    virtual NNLweight STDPFunction (NNLtime, NNLtime);            /* Weight adaptation fucntion */
    virtual NNLweight STDPClassicalFunction (NNLtime, NNLtime);   /* Weight adaptation fucntion */
    virtual NNLweight STDPAdditiveClassicalFunction  (NNLtime, NNLtime);
    virtual NNLweight STDPAdditiveIzhikevichFunction (NNLtime, NNLtime);
    virtual NNLtime DelayFunction (NNLtime, NNLtime);             /* Delay adaptation function */
  
  public:
    /* Constructor / destructor */
    CSynapse ();
    CSynapse (SynapseParameters *, CNeuron *, CNeuron * );
    //CSynapse (CNeuron *, CNeuron * , SynapseParameters *);
    virtual ~CSynapse ();
    
    void Disconnect ();
    vector<CSynapse *>::iterator DisconnectInput ();
    vector<CSynapse *>::iterator DisconnectOutput ();
    
    virtual void Learn (NNLtime);   /* Modify weight and delay, based on temporal context */
  
    NNLtime getDelay ();
    NNLpotential getWeight ();
  
    bool isInhibitory ();
    bool isLearningWeight ();
    bool isLearninDelay ();
    
    CNeuron * getOutputNeuron ();
    CNeuron * getInputNeuron ();
    
    bool IncreaseDelay ();
    bool DecreaseDelay ();
  
    void setLastImpact (NNLtime t);
  
    int getID (); /* Get the ID of the synapse */
    
    TiXmlElement * XMLsave (); /* XML output */
    TiXmlElement * XMLload (TiXmlElement * , map<int, CNeuron*> *); /* XML input */
};



#endif /* _SYNAPSE_H */
