/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
 
#ifndef _ARCHITECTURE_H
#define _ARCHITECTURE_H

#include "../simulator/globals.h"
#include "../metastructure/metaarchitecture.h"
#include <luabind/luabind.hpp>

using namespace luabind;
using namespace std;

#define NEURON_PARAMETERS \
    const luabind::object &
/*    NNLpotential Threshold,  \
    NNLpotential MembranePotential, \
    NNLpotential AbsoluteRefractoryPotential, \
    NNLtime AbsoluteRefractoryPeriode, \
    float Inhibitory, \
    NNLtime TauPSP, \
    NNLtime TauRefract
*/

#define PROJECTION_PARAMETERS \
    const luabind::object &
/*    float ProjectionRate1, \
    NNLweight Weight1, \
    NNLtime Delay1, \
    NNLtime DeltaDelay1, \
    NNLweight DeltaWeight1, \
    bool WeightAdaptation1, \
    bool DelayAdaptation1, \
    NNLbool Inhibitory1
*/

void luaAddAssembly (const char * Name);
void luaAddXNeurons (int X, const char * Ass, NEURON_PARAMETERS);

int luaConnectAssToAss (const char * Ass1, const char * Ass2, PROJECTION_PARAMETERS);
int luaConnectAssToNeu (const char * Ass1, const char * Ass2, int NeuronID, PROJECTION_PARAMETERS);
int luaConnectNeuToAss (const char * Ass1, int NeuronID, const char * Ass2, PROJECTION_PARAMETERS);
int luaConnectNeuToNeu (const char * Ass1, int NeuronID1, const char * Ass2, int NeuronID2, PROJECTION_PARAMETERS);

int luaConnectSubAssemblies (const char * Ass1, int From1, int Nb1, const char * Ass2, int From2, int Nb2, PROJECTION_PARAMETERS);
int luaConnectOneToOne (const char * Ass1, const char * Ass2, PROJECTION_PARAMETERS);
void luaConnectRegular (const char * Ass, int kNeighbour, PROJECTION_PARAMETERS);
void luaConnectRegularAndRewire (const char * Ass, int kNeighbour, double p, PROJECTION_PARAMETERS);
void luaConnectScaleFree (const char * Ass, PROJECTION_PARAMETERS/* initial parameters*/, int InitialNumberNeurons, PROJECTION_PARAMETERS, int k, int kin, int kout, double alpha, double beta, double gamma);


class CArchitecture
{
  protected :
    vector<CAssembly *> Assemblies;
    


  public :
    /* Constructor / destructor */
    CArchitecture ();
    virtual ~CArchitecture ();

    /* Building functions */
    int Connect (CAssembly *, CAssembly *, ProjectionParameters *);
    int Connect (CAssembly *, CNeuron *, ProjectionParameters *);
    int Connect (CNeuron *, CAssembly *, ProjectionParameters *);
    int Connect (CNeuron *, CNeuron *, ProjectionParameters *);
    int ConnectSubAssemblies (CAssembly * Assembly1, int From1, int Nb1, CAssembly * Assembly2, int From2, int Nb2, ProjectionParameters * param);
    int ConnectOneToOne (CAssembly *, CAssembly *, ProjectionParameters *);  /* connect first neuron of a1 to first neuron of a2, second neuron of a1 to second neuron of a2, etc.*/
    void GaussianMapping (CAssembly *, CAssembly *, ProjectionParameters *);
    void LogarithmicMapping (CAssembly *, CAssembly *, ProjectionParameters *);
    
    void ConnectRegular (CAssembly *, int kNeighbour, ProjectionParameters * param); /* Make a regular connectivity in the assembly */
    void ConnectRegularAndRewire (CAssembly *, int kNeighbour, double p, ProjectionParameters * param); /* Make a small world architecture using the Watts and Strogatz method */
    void Rewire (CAssembly *, double p, ProjectionParameters * param); /* Rewire links with probability p */

    // kin and kou : minimal in- and out-degree for each neuron: k: number of preferentially attached edges created at each steps
    // k is the total number of edges at each step. In this number, kin are input of current node, kou are output of current node, the reste is fully preferentially attached
    // k must be even
    void ConnectScaleFree (CAssembly * Ass, ProjectionParameters * InitialRandNetParam, int InitialNumberNeurons, ProjectionParameters * param, int k, int kin, int kout, double alpha, double beta, double gamma); /* Make a scale free architecture using the ??  method */
    double ScaleFreeProba (CNeuron * Neuron, double alpha, double beta, double gamma, int NbNeurons, int NbEdges);
    
    void AddAssembly (CAssembly * ass);
    vector<CAssembly *> * getAssemblies ();
    CAssembly * getAssemblyByName ( string );
    void Run ();
  
    /* XML file management to load or save an architecture */
    bool XMLsave (string);
    bool XMLload (string);
    

    //Lua Bindings
    void BuildFromLua (const char * LuaFilename);
    void BuildFromMetaLua (CMetaArchitecture & MetaArchitecture);

    CAssembly * operator[] (char * szAssemblyName);
    CAssembly * operator[] (string & strAssemblyName);


};


#endif /* _ARCHITECTURE_H */
