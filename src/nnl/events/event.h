/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


#ifndef EVENT_H_
#define EVENT_H_

#include "../simulator/globals.h"

typedef enum {
  EVT_NULL =                 0,  /* Empty event */
  EVT_SPIKE =                1,  /* Spike event */
  EVT_SPIKE_IMPACT =         2,  /* Spike impact (after axonal transmission) */
  EVT_SAVE_SPIKE_PSP =       4,  /* Log a spike and its PSPs (used for PGs monitoring) */
  EVT_RECURSIVE =            8,  /* Treat a recursive event */
  EVT_NEXT_PHASE =          16,  /* Means we should enqueue the events of the next phase known by the eventmanager */
  } NNLeventType;


typedef struct sFuncParam
{
  union {
    struct
    {
      long int NbStimPresentations;
      long int NbStimRepetitionsInInterval;
      NNLtime StimRepetitionSeparation;
      long int PatternNumber;
    };

    struct
    {
      long int Param1;
      long int Param2;
      long int Param3;
      long int Param4;
    };
    long int Params[4];

    struct
    {
      double dParam1;
      double dParam2;
    };
    double dParams[2];
  };

  string strParam1;
  string strParam2;

} TFuncParam;


/* Event type */
class NNLevent
{
  public :
    NNLevent ();
    virtual ~NNLevent ();

  /* ALL events */
  NNLeventType EventType;       /* what sort of event is it ? */
  NNLtime Time;                 /* occuring time */

  /* EVT_SPIKE and EVT_SPIKE_IMPACT and EVT_SAVE_SPIKE_PSP */
  CNeuron * EmittingNeuron;     /* neuron emiting the spike */

  /* EVT_SPIKE_IMPACT */
  CSynapse * ImpactingSynapse;  /* synapse impacting post-syn neuron */

  /* EVT_SPIKING_RATIO and EVT_RECURSIVE */
  CAssembly * Assembly;

  /* EVT_RECURSIVE */
  NNLtime Duration;            /* Last occurence (the start time is the first post of the occurence */
  NNLtime RecurssionStep; /* Event will occur every RecurssionStep time units */
  void (*func) (CAssembly*, TFuncParam *);        /* Function containing the desired recursive treatment */
  TFuncParam fparam;




  void LoadFromLua (lua_State * pLua, const char * EventName);
  void SaveToLua (ofstream * LuaOutputFile, const char * EventName);

  bool operator < (NNLevent const &);
  bool operator == (NNLevent const &);

  friend bool operator < (NNLevent const &, NNLevent const &);
  friend bool operator == (NNLevent const &, NNLevent const &);
};


#endif /* EVENT_H_ */
