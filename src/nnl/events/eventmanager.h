/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
 
#ifndef _EVENTQUEUE_H
#define _EVENTQUEUE_H


#include "../simulator/globals.h"
#include "event.h"
#include "../common/datafile.h"

using namespace std;
  
class CEventManager
{
  private:
    priority_queue<NNLevent> Queue;
    list <CSimulationPhase *> SimulationPhaseList;
    
    NNLeventMask WatchedEventsMask;
    
    void TreatEvent (NNLevent &); /* Filter an event and eventually log it */
    bool isWatched (NNLeventType);  /* Check if an event is to be watched */
    
    
  public:
    /* Constructor and destructor */
    CEventManager ();
    virtual ~CEventManager ();
  
  
    /* Interface function for manipulating the Queue */
    void push (NNLevent &);
    void pop ();
    NNLevent & top ();
    bool empty ();
    int size ();
    
    /* Events management */
    void WatchEvent (NNLeventType);   /* Modify the WatchedEventsMask to watch this type of event */
    void IgnoreEvent (NNLeventType);  /* Modify the WatchedEventsMask to ignore this type of event */
  
    /* Using the simulation phases list */
    int getNbPhases ();
    NNLtime getTotalSimulationLength (); // is this necessary / possible ?
    void enqueueSimulationPhase (CSimulationPhase *);
    void enqueueSimulation (CSimulation *);
    void PostNextPhaseEvents ();
};


#endif /* _EVENTQUEUE_H */
