/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


#include "eventmanager.h"

#include "neuron.h"
#include "synapse.h"
#include "assembly.h"
#include "parameters.h"
#include "simulationphase.h"
#include "simulation.h"
//#include "graphmanager.h"

/*****************************************************************************/
CEventManager::CEventManager ()
{
  WatchedEventsMask = 0;

}

/*****************************************************************************/
CEventManager::~CEventManager ()
{
  /* Closing SpikeRaster files */
  if (SpikeRasterExcit)
    SpikeRasterExcit->close ();
  
  if (SpikeRasterInhib)
    SpikeRasterInhib->close ();
  
  /* Closing SpikePSPs file */
  if (SpikePSPs)
    SpikePSPs->close ();
    
}

/*****************************************************************************/
void CEventManager::push (NNLevent & newVal)
{ 
  if (isWatched (newVal.EventType))
  {
    try {
      //if (EventQueue.size () > 10000)
      //{
        if (newVal.Time < CurrentTime) 
          cout << " *** WARNING *** EventTime is before current time : event time: " << newVal.Time << " Current time: " << CurrentTime << " event type: " << newVal.EventType << endl;
        //else if (newVal.Time == CurrentTime)//&& (newVal.EventType != 32) && (newVal.EventType != 1) )
        //  cout << " *** WARNING *** EventTime is equal to current time : event time: " << newVal.Time << " Current time: " << CurrentTime << " event type: " << newVal.EventType << endl;
      //}
      Queue.push (newVal);
    }
    catch (exception& e)
    {
      cout << endl << e.what () << " " << EventQueue.size () << endl;
      exit (-1);
    }
  }
}

/*****************************************************************************/
void CEventManager::pop ()
{
  if (!Queue.empty ())
  {
    NNLevent evtTemp = Queue.top ();
    
    if (isWatched (evtTemp.EventType))
      TreatEvent (evtTemp);
  }
  
  Queue.pop ();
}

/*****************************************************************************/
NNLevent & CEventManager::top ()
{
  return (NNLevent &) Queue.top ();;
}

/*****************************************************************************/
bool CEventManager::empty ()
{
  return Queue.empty ();
}

/*****************************************************************************/
int CEventManager::size ()
{
  return Queue.size ();
}

/*****************************************************************************/
bool CEventManager::isWatched (NNLeventType EvtType)
{
  /* If  010110 (the mask) AND 000010 (the checked flag) is different from 0, 
     than it means the flag is active */
  
  bool Result = false;
  
  if (EvtType & WatchedEventsMask)
    Result = true;
  
  return Result;
}

/*****************************************************************************/
void CEventManager::WatchEvent (NNLeventType EvtType)
{
  /* 111101 (the mask) OR 000010 (the checked flag) = 111111 , we set the flag for this event */
  WatchedEventsMask = WatchedEventsMask | EvtType;
}

/*****************************************************************************/
void CEventManager::IgnoreEvent (NNLeventType EvtType)
{
  /* 101111 AND NOT(000010) = 
     101111 AND     111101  = 101101 , we reset the flag for this event */
  WatchedEventsMask = WatchedEventsMask & (~EvtType);
}

/*****************************************************************************/
void CEventManager::TreatEvent (NNLevent & evt)
{
  list<NNLevent *>::iterator CurrentPSP;
  list<NNLevent *>::iterator LastPSP;
  
  if (evt.Time < CurrentTime) 
  {
    cout << endl << " *** WARNING *** EventTime < CurrentTime: " << evt.Time << " Current time: " << CurrentTime << " event type: " << evt.EventType << endl;
    if (evt.EmittingNeuron != NULL)
      cout << " *** WARNING *** EmittingNeuron: " << evt.EmittingNeuron->getID() << "(" << evt.EmittingNeuron->getLocalID () << ")" << endl;
    if (evt.Assembly != NULL)
      cout << "from " << evt.Assembly->getName () << endl;
  }
  
  
  switch (evt.EventType)
  {
    /******************************/
    case EVT_NULL:
      break;
    
    /******************************/
    case EVT_SPIKE_IMPACT:
        
      //cout << "plante pas 1" << endl;
    
      evt.ImpactingSynapse->setLastImpact(CurrentTime);  
    
      //cout << "plante pas 2" << endl;
    
      evt.ImpactingSynapse->getOutputNeuron ()->ImpactedBy (evt);
    
      //cout << "plante pas 3" << endl;
      
      //cout << "Neuron " << evt.ImpactingSynapse->getOutputNeuron ()->getLocalID () << " impacted at " << CurrentTime << " (" << evt.ImpactingSynapse->getOutputNeuron ()->getMembranePotential () << ")" << endl;
    
      break;
    
    /******************************/
    case EVT_SPIKE:
      if (evt.EmittingNeuron->isInhibitory())
      {
        /* We create the files only when needed */
        //if (SpikeRasterInhib == NULL)
          //SpikeRasterInhib = new CDataFile ((CParameters::DATA_OUTPUT_DIR + "SpikeRasterInhib" + "." + CParameters::DATA_OUTPUT_EXT).c_str(), ios::out|ios::binary|ios::trunc);
        
        /* let us log each emitted spike */
        //GraphManager.push_data_point (SpikeRasterInhib, evt.Time, evt.EmittingNeuron->getID ());
        (*SpikeRasterInhib) << evt.Time << " " << evt.EmittingNeuron->getID () << endl;
        //cout << "Chunk number :" << SpikeRasterInhib->CurrentChunkNumber << endl;
      }
      else
      {
        /* We create the files only when needed */
        //if (SpikeRasterExcit == NULL)
          //SpikeRasterExcit = new CDataFile ((CParameters::DATA_OUTPUT_DIR + "SpikeRasterExcit" + "." + CParameters::DATA_OUTPUT_EXT).c_str(), ios::out|ios::binary|ios::trunc);
        
        /* let us log each emitted spike */
        //GraphManager.push_data_point (SpikeRasterExcit, evt.Time, evt.EmittingNeuron->getID ());
        (*SpikeRasterExcit) << evt.Time << " " << evt.EmittingNeuron->getID () << endl;
      }
      
      
      /* Increment the number of spike and set the time of the last spike of the containing assembly */
      evt.EmittingNeuron->getAssembly ()->AddSpike ();
      break;
    
    /******************************/
    case EVT_SAVE_SPIKE_PSP:
      if (evt.EmittingNeuron->getSpikingPSPs ()->size () >= 0)
      {
        //GraphManager.push_data_point (SpikePSPs
        (*SpikePSPs) << evt.EmittingNeuron->getID () << "," << CurrentTime-1 << " ";
        
        CurrentPSP = evt.EmittingNeuron->getSpikingPSPs ()->begin ();
        LastPSP = evt.EmittingNeuron->getSpikingPSPs ()->end ();
        
        while (CurrentPSP != LastPSP)
        {
          //TMP (*SpikePSPs) << (*CurrentPSP)->EmittingNeuron->getID () << "," << (*CurrentPSP)->Time - (*CurrentPSP)->ImpactingSynapse->getDelay () << " ";
          CurrentPSP++;
        }
        
        (*SpikePSPs) << endl;
      }
      
      break;
    
    /******************************/
    case EVT_RECURSIVE:
      
      /* Post next occurence of this event */
      if ( (evt.Time + evt.RecurssionStep) < evt.Duration )
      {
        evt.Time += evt.RecurssionStep;
        EventQueue.push (evt);
      }
    
      /* Make the treatment for this event */
      if (evt.func)
        evt.func (evt.Assembly, &(evt.fparam));
      
      break;
    
    /******************************/
    case EVT_NEXT_PHASE:
      /* Post events of the next phase starting from current time */
      PostNextPhaseEvents ();
      break;

  }
}

/*****************************************************************************/
int CEventManager::getNbPhases ()
{
  return SimulationPhaseList.size ();
}

/*****************************************************************************/
NNLtime CEventManager::getTotalSimulationLength ()
{
  list<CSimulationPhase *>::iterator itCurrentPhase;
  list<CSimulationPhase *>::iterator itLastPhase = SimulationPhaseList.end ();
  
  NNLtime SimulationLength = CurrentTime;
  
  //for each phase, count the length
  for (itCurrentPhase = SimulationPhaseList.begin (); itCurrentPhase != itLastPhase; itCurrentPhase++)
  {
    SimulationLength += (*itCurrentPhase)->getPhaseLength ();
  }
  
  return SimulationLength;
}

/*****************************************************************************/
void CEventManager::enqueueSimulationPhase (CSimulationPhase * newPhase)
{
  SimulationPhaseList.push_back (newPhase);
}

/*****************************************************************************/
void CEventManager::enqueueSimulation (CSimulation * sim)
{
  CSimulation::iterator itPhase = sim->begin();
  CSimulation::iterator itLastPhase = sim->end();

  while (itPhase != itLastPhase)
  {
    enqueueSimulationPhase (*itPhase);
    itPhase++;
  }
}

/*****************************************************************************/
void CEventManager::PostNextPhaseEvents ()
{
  if (SimulationPhaseList.size () == 0)
    return;

  cout << endl << endl << "============================ "<< CurrentTime << " ============================ Posting new phase : \"" << SimulationPhaseList.front()->getPhaseName () << "\" (" << SimulationPhaseList.front()->getPhaseDescription () << ")." << endl;

  CSimulationPhase * CurrentPhase = SimulationPhaseList.front();
  
  NNLevent evtTmp;
  
  NNLtime latestTime = CurrentTime;

  list<NNLevent *>::iterator itEvt;
  list<NNLevent *>::iterator itLastEvt = CurrentPhase->EventList.end ();
  int index = 0;
  for (itEvt = CurrentPhase->EventList.begin (); itEvt != itLastEvt; itEvt++)
  {
    index ++;
    evtTmp.EventType = (*itEvt)->EventType;
    evtTmp.Assembly = (*itEvt)->Assembly;
    evtTmp.EmittingNeuron = (*itEvt)->EmittingNeuron;    
    evtTmp.ImpactingSynapse = (*itEvt)->ImpactingSynapse;

    if ((*itEvt)->Time == -1)
      evtTmp.Time = latestTime;
    else
      evtTmp.Time = CurrentTime + (*itEvt)->Time;

    evtTmp.RecurssionStep = (*itEvt)->RecurssionStep;
    evtTmp.Duration = evtTmp.Time + (*itEvt)->Duration;
    //cout << "index:" << index << " time:" << evtTmp.Time;
    //latestTime += (*itEvt)->Duration;
    //cout << " Duration:" << evtTmp.Duration << " latestTime:" << latestTime << endl;
    evtTmp.func = (*itEvt)->func;
    evtTmp.fparam = (*itEvt)->fparam;

    latestTime += evtTmp.Duration;

    EventQueue.push (evtTmp);
  }
  
  SimulationPhaseList.pop_front();
}
