/*

NNL - Neuron Network Library - This program is a 
   spiking neuron network simulator.

Copyright (C) 2006-2017 Régis Martinez - regis.martinez3@gmail.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


#include "event.h"
#include "assembly.h"
#include "architecture.h"
#include "eventfunctions.h"
#include "parameters.h"

/*****************************************************************************/
NNLevent::NNLevent ()
{
  EmittingNeuron = NULL;
  Time = 0;
  EventType = EVT_NULL;
  ImpactingSynapse = NULL;
  Assembly = NULL;
  Duration = 0;
  RecurssionStep = 0;
  func = NULL;
  fparam.NbStimPresentations = 1;
  fparam.NbStimRepetitionsInInterval = 1;
  fparam.StimRepetitionSeparation = 0;
  fparam.PatternNumber = 1;
}

/*****************************************************************************/
NNLevent::~NNLevent ()
{
}

/*****************************************************************************/
void NNLevent::LoadFromLua (lua_State * pLua, const char * EventName)
{
  cout << EventName << flush ;

  EventType = (NNLeventType) lua_getIntTableElement (pLua, EventName, "EventType");

  Time = (NNLtime) lua_getDoubleTableElement (pLua, EventName, "Time");

  if ((strcmp (lua_getStringTableElement(pLua, EventName, "Assembly"), "") == 0) || (lua_getStringTableElement(pLua, EventName, "Assembly") == 0))
    Assembly = NULL;
  else
    Assembly = ArchitectureGlobalPointer->getAssemblyByName ( lua_getStringTableElement(pLua, EventName, "Assembly") );

  Duration = (NNLtime) lua_getDoubleTableElement (pLua, EventName, "Duration");
  RecurssionStep = (NNLtime) lua_getDoubleTableElement (pLua, EventName, "RecurssionStep");

  char * fctname =  (char * ) lua_getStringTableElement (pLua, EventName, "Function");

  if (fctname == 0)
    func = 0;
  else
  {

    if (strcmp(fctname, "") == 0)
      func = 0;
    else
      func = getFunctionByName(lua_getStringTableElement (pLua, EventName, "Function"));
  }

  if (lua_fieldExist(pLua, EventName, "FuncParam1"))
    fparam.Param1 = lua_getIntTableElement (pLua, EventName, "FuncParam1");

  if (lua_fieldExist(pLua, EventName, "FuncParam2"))
    fparam.Param2 = lua_getIntTableElement (pLua, EventName, "FuncParam2");

  if (lua_fieldExist(pLua, EventName, "FuncParam3"))
    fparam.Param3 = lua_getIntTableElement (pLua, EventName, "FuncParam3");

  if (lua_fieldExist(pLua, EventName, "FuncParam4"))
    fparam.Param4 = lua_getIntTableElement (pLua, EventName, "FuncParam4");



  if (lua_fieldExist(pLua, EventName, "FuncDParam1"))
    fparam.dParam1 = lua_getDoubleTableElement (pLua, EventName, "FuncDParam1");

  if (lua_fieldExist(pLua, EventName, "FuncDParam2"))
    fparam.dParam2 = lua_getDoubleTableElement (pLua, EventName, "FuncDParam2");



  if (lua_fieldExist(pLua, EventName, "FuncStrParam1"))
    fparam.strParam1 = lua_getStringTableElement (pLua, EventName, "FuncStrParam1");

  if (lua_fieldExist(pLua, EventName, "FuncStrParam2"))
    fparam.strParam2 = lua_getStringTableElement (pLua, EventName, "FuncStrParam2");



  // FIXME : Unsupported fields
  //EmittingNeuron = Assembly->getNeurons[lua_getIntTableElement(pLua, EventName, "EmittingNeuron")]; // FIXME : l'indice dans le tableau des neurones n'est pas forcément l'identifiant du neurone !
  //CSynapse * ImpactingSynapse;  /* synapse impacting post-syn neuron */
}

/*****************************************************************************/
void NNLevent::SaveToLua (ofstream * LuaOutputFile, const char * EventName)
{

  (*LuaOutputFile) << "--------- Event : " << EventName << " ---------" << endl;

  (*LuaOutputFile) << EventName << " = { " << endl;
  (*LuaOutputFile) << "  EventType = " << EventType << ", " << endl;

  if (Assembly == NULL)
    (*LuaOutputFile) << "  Assembly = " << "\"\"" << ", " << endl;
  else
    (*LuaOutputFile) << "  Assembly = \"" << Assembly->getName() << "\", " << endl;

  (*LuaOutputFile) << "  Time = " << (double) ((double)Time/(double)CParameters::TIMESCALE) << " * TIMESCALE, " << endl;
  (*LuaOutputFile) << "  Duration = " << Duration << ", " << endl;
  (*LuaOutputFile) << "  RecurssionStep = " << RecurssionStep << ", " << endl;

  if (func != NULL)
  {
    if (getFunctionByPtr(func) != NULL)
      (*LuaOutputFile) << "  Function = " << "\"" << getFunctionByPtr(func) << "\" ," << endl;
    else
      (*LuaOutputFile) << "  Function = " << "\"" << "\" , --no function found with ptr " << (void *) func << endl;
  }
  else
    (*LuaOutputFile) << "  Function = \"\" ," << endl;

  (*LuaOutputFile) << "  FuncParam1 = " << fparam.Param1 << ", " << endl;
  (*LuaOutputFile) << "  FuncParam2 = " << fparam.Param2 << ", " << endl;
  (*LuaOutputFile) << "  FuncParam3 = " << fparam.Param3 << ", " << endl;
  (*LuaOutputFile) << "  FuncParam4 = " << fparam.Param4 << ", " << endl;

  (*LuaOutputFile) << "}" << endl;

  // Unsupported fields
  //  (*LuaOutputFile) << "EmittingNeuron = " << EventType << ", " << end;
  //  (*LuaOutputFile) << "ImpactingSynapse = " << EventType << ", " << end;
}

/*****************************************************************************/
bool NNLevent::operator < (NNLevent const & b)
{
  return this->Time > b.Time;
}

/*****************************************************************************/
bool NNLevent::operator == (NNLevent const & b)
{
  return this->Time == b.Time;
}

/*****************************************************************************/
bool operator < (NNLevent const & a, NNLevent const & b)
{
  return a.Time > b.Time;
}

/*****************************************************************************/
bool operator == (NNLevent const  & a, NNLevent const & b)
{
  return a.Time == b.Time;
}
