
CXXFLAGS= $(OPTIONS) $(INCLUDES) -fPIC
CFLAGS= $(OPTIONS) $(INCLUDES) -fPIC
LDFLAGS= $(OPTIONS) $(LIBDIRS) $(LIBS) -D_REENTRANT -rdynamic

CC=g++
CXX=g++
AR=ar


OBJECTS=$(SRC:.cpp=.o)

all: $(TARGETS)

clean:
	rm -f $(OBJECTS) $(TARGETS)

cleanobj:
	rm -f $(OBJECTS)

depend: 
	makedepend $(INCLUDES) -Y -fMakefile $(SRC)

again: clean $(TARGETS)
