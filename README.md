# Spiking Neuron Network simulator

I developped this simulator during my PhD thesis. This source code is obviously old now, probably dirty.

This relates to my [PhD thesis](documentation/2011%20-%20Martinez%20-%20Thesis.pdf).

This software was developped thanks to public reseach fundings, thus, it is distrubuted under the license [GNU General Public License V3](LICENSE.md).

![](plots/topologies.png)

## Documentation
* [Getting started](documentation/getting-started.md)
* [Command line parameters](documentation/command-line-details.md)

## Quick Start

If something goes wrong in the following commands, it's propably because you didn't follow the [Getting started](documentation/getting-started.md) steps first.

Namely, make sure you have the dependencies:
* CMake : this is usually already installed, if not, there is surely a package for your linux distribution. ``cmake`` needs to be in the environment PATH.
* [Lua 5.1](https://www.lua.org) (5.2 won't work) : \[[Ubuntu](http://packages.ubuntu.com/search?suite=default&section=all&arch=any&keywords=lua5.1&searchon=names)\] \[[Gentoo](https://packages.gentoo.org/packages/dev-lang/lua)\] \[[Source code](https://www.lua.org/download.html)\]
* [GSL: GNU Scientific Library](https://www.gnu.org/software/gsl/) : \[[Ubuntu](http://packages.ubuntu.com/trusty/math/gsl-bin)\] \[[Gentoo](https://packages.gentoo.org/packages/sci-libs/gsl)\] \[[Source code](git.savannah.gnu.org/cgit/gsl.git)\]
* [Igraph](http://igraph.org/c/) : \[[Ubuntu](http://packages.ubuntu.com/search?keywords=igraph&searchon=names)\] \[[Gentoo](https://packages.gentoo.org/packages/dev-libs/igraph)\] \[[Source code](https://github.com/igraph/igraph)\]
* [Gnuplot](http://www.gnuplot.info) (optional)


Getting the source code:

> ``git clone <git repository>``

> ``cd <newly created repository>``

> ``git submodule update --init --``

Building:

> ``make``

If you have trouble building, please refer to more detailed building steps of the [Getting started](documentation/getting-started.md) page.

Running simulations:

> ``> ./nnl --metaarchitecture settings/examples/builder-meta.lua --settings settings/examples/settings.lua --patterns settings/examples/patterns.lua --simulation settings/examples/simulation.lua``

Viewing/analyzing the data:

> ``scripts/make-rasterplot.sh 0 200000``

This generates the file ``figures/SpikeRasterPlot--0000000000-0000200000.eps`` :

![Spike raster plot between 0 and 20 seconds](documentation/SpikeRasterPlot--0000000000-0000200000.png "Spike raster plot between 0 and 20 seconds")


