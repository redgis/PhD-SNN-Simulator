# Getting started

First thing you want to do is pull this source code and build the simulator and its associated tools.


## Getting the source code

Pull the source code with the usual :
> ``git clone <git repository>``

Enter the newly created local repository :
> ``cd <newly created repository>``

You will also need to pull the LuaBind submodule, needed to build : 
> ``git submodule update --init --``

## Getting the dependencies

The simulator has software dependencies. You need the following libraries installed on your system.

* [Lua 5.1](https://www.lua.org) (5.2 won't work) : a widely used programming language and interpreter. Probably available as a package on your linux distribution.
   * \[[Ubuntu](http://packages.ubuntu.com/search?suite=default&section=all&arch=any&keywords=lua5.1&searchon=names)\] \[[Gentoo](https://packages.gentoo.org/packages/dev-lang/lua)\] \[[Source code](https://www.lua.org/download.html)\]
* [GSL: GNU Scientific Library](https://www.gnu.org/software/gsl/) : a widely used library. Probably available as a package on your linux distribution.
   * \[[Ubuntu](http://packages.ubuntu.com/trusty/math/gsl-bin)\] \[[Gentoo](https://packages.gentoo.org/packages/sci-libs/gsl)\] \[[Source code](git.savannah.gnu.org/cgit/gsl.git)\]
* [Igraph](http://igraph.org/c/) : Graph analyzing library. Not so widely used, but you might find it as a package on your linux distribution :
   * \[[Ubuntu](http://packages.ubuntu.com/search?keywords=igraph&searchon=names)\] \[[Gentoo](https://packages.gentoo.org/packages/dev-libs/igraph)\] \[[Source code](https://github.com/igraph/igraph)\]

* Gnuplot : a diagram generation tool. This is a not mandatory to run simulations, but it is used by some scripts I wrote to create diagrams easily. Feel free to use your own tool, like _matlab_ or _octave_.

## Building the source code

The build system is based on the CMake tool. This is usually already installed on every Linux distribution. If not, there surely is a package for your distribution. The command ``cmake`` needs to be available in the environment PATH.

Before building the simulator, you should see these files in the directory:

```bash
> ls
architectures/  figures/  scripts/   Makefile            '2011 - Martinez - Thesis.pdf'
data/           lua/      settings/  help.sh.deprecated
datasets/       plots/    src/       README.md
```

In a console, enter the simulator's root folder and type :

> ``> make``

You should now have these files:

```bash
> ls
architectures/  lua/       src/      pg-search2          '2011 - Martinez - Thesis.pdf'
data/           plots/     distrib   topajek             libeventfunctions.so
datasets/       scripts/   Makefile  help.sh.deprecated  libluabind.so
figures/        settings/  nnl       README.md           liblua.so
```

The newly created files should be: ``nnl``, ``libeventfunctions.so``, ``libluabind.so``, ``liblua.so``, ``distrib``, ``pg-search2`` and ``topajek``.

The build process was tested on a Linux Gentoo distribution. Some users have reported build failures on other distributions due to differences like the path where Lua is installed, or library dependencies.

Namely, on CentOS, it seems the dependency to gsl library also pulls in a dependency to gslcblas library. Thus, one may have to apply the following diff on ``src/nnl/CMakeLists.txt`` and ``src/tools/CMakeLists.txt``:

```
diff --git "a/src\\nnl\\CMakeLists.txt" "b/src\\nnl\\CMakeLists.txt.nnl.CentOS.6.6"
index 74a412c..fa54cd0 100644
--- "a/src\\nnl\\CMakeLists.txt"
+++ "b/src\\nnl\\CMakeLists.txt.nnl.CentOS.6.6"
@@ -17,5 +17,5 @@ project(eventfunctions)

    add_library(eventfunctions SHARED eventfunctions/eventfunctions.cpp)

-target_link_libraries(nnl gsl igraph lua luabind dl pthread eventfunctions)
+target_link_libraries(nnl gslcblas gsl igraph lua luabind dl pthread eventfunctions)
 target_link_libraries(eventfunctions lua luabind dl pthread)
```

```
diff --git "a/src\\tools\\CMakeLists.txt" "b/src\\tools\\CMakeLists.txt.tools.CentOS.6.6"
index d56cc7f..90b6f6c 100644
--- "a/src\\tools\\CMakeLists.txt"
+++ "b/src\\tools\\CMakeLists.txt.tools.CentOS.6.6"
@@ -19,7 +19,7 @@ project(build-tools)

    file(GLOB_RECURSE avg_SRC avg/*.cpp)
    add_executable(avg ${avg_SRC})
-   target_link_libraries(avg gsl)
+   target_link_libraries(avg gslcblas gsl)

    file(GLOB_RECURSE distribs_SRC distribs/*.cpp ../nnl/parameters/*.cpp ../nnl/tinyxml/*.cpp)
    add_executable(distribs ${distribs_SRC})
@@ -45,4 +45,4 @@ project(build-tools)

    file(GLOB_RECURSE distrib_to_proba_distrib_SRC distrib-to-proba-distrib/*.cpp)
    add_executable(distrib-to-proba-distrib ${distrib_to_proba_distrib_SRC})
-   target_link_libraries(distrib-to-proba-distrib gsl)
+   target_link_libraries(distrib-to-proba-distrib gslcblas gsl)
```


## Running simulations

To run a simulation, you would typically run the following command line:

> ``> ./nnl --metaarchitecture settings/examples/builder-meta.lua --settings settings/examples/settings.lua --patterns settings/examples/patterns.lua --simulation settings/examples/simulation.lua``

You would see this output :

```
Random seed : 800065632
Loading network "settings/examples/builder-meta.lua" ...
  assemblies: Input... Reservoir... Output... done.
  projections: InputReservoir... ReservoirReservoir... ReservoirOutput... done.
Building network from meta-architecture ...
  assemblies: Input... Reservoir... Output... done.
  projections: Input->Reservoir ... Reservoir->Reservoir ... Reservoir->Output ... done.
Loading patterns using "settings/examples/patterns.lua" ...
  training patterns: 0:10... 1:10... 2:10... 3:10... 4:10... 5:10... 6:10... 7:10... 8:10... 9:10...  = 100 - done.
  testing patterns: 0:10... 1:10... 2:10... 3:10... 4:10... 5:10... 6:10... 7:10... 8:10... 9:10...  = 100 - done.
Loading settings "settings/examples/settings.lua" ... done.
Loading simulation "settings/examples/simulation.lua" ...
  phase Initialisation: evtRandomInput... evtNextPhase... done.
  phase SaveArchitecture: evtSaveArchi... evtNextPhase... done.
  phase Particular6: evtParticular6Pres... evtNextPhase... done.
  phase Life: evtDoNothing... evtNextPhase... done.
Random seed : 800065632
Generating gnuplot command file, please wait...done.
Starting simulation with virtual time duration : 0h 0m 20.420s (204200ms)


============================ 0 ============================ Posting new phase : "Initialisation" (random stimulation).
*** Time    200 ***      Estimated remaining time:  0h  0m  0s ( 0% done)

============================ 200 ============================ Posting new phase : "SaveArchitecture" (save current state of architecture).

Saving network to 'mynewarchi-0000000200--seed-800065632--size-1266--classes-10--timescale-10.xml' ... done.
*** Time    400 ***      Estimated remaining time:  0h 25m 30s ( 0% done)

============================ 400 ============================ Posting new phase : "Particular6" (stimulate with one example of 6).
*** Time 200800 ***      Estimated remaining time:  0h  0m  0s (98% done)

============================ 200800 ============================ Posting new phase : "Life" (Let them live...).
*** Time 405600 ***      Estimated remaining time:  0h  0m  0s (100% done)
Execution time: 0h 0m 30s
```

Note we generated a backup of the network architecture in ``architectures/mynewarchi-0000000200--seed-800065632--size-1266--classes-10--timescale-10.xml`` :

```xml
<?xml version="1.0" ?>
<Assembly name="Input" size="256">
    <Neuron id="0" inhibitory="0" refractoryPeriod="5.000000" refractoryPotential="20000.000000" restingPotential="-65.000000" threshold="-50.000000" tauPSP="3.000000" tauRefract="0.100000">
        <SynapseOut weight="1.000000" delay="15.600000" inhibitory="0" weightSTDP="0" delaySTDP="0" inputNeuron="0" outputNeuron="309" />
        <SynapseOut weight="1.000000" delay="7.000000" inhibitory="0" weightSTDP="0" delaySTDP="0" inputNeuron="0" outputNeuron="434" />
        <SynapseOut weight="1.000000" delay="0.800000" inhibitory="0" weightSTDP="0" delaySTDP="0" inputNeuron="0" outputNeuron="536" />
        <SynapseOut weight="1.000000" delay="7.800000" inhibitory="0" weightSTDP="0" delaySTDP="0" inputNeuron="0" outputNeuron="546" />
        <SynapseOut weight="1.000000" delay="5.500000" inhibitory="0" weightSTDP="0" delaySTDP="0" inputNeuron="0" outputNeuron="738" />
        <SynapseOut weight="1.000000" delay="9.200000" inhibitory="0" weightSTDP="0" delaySTDP="0" inputNeuron="0" outputNeuron="902" />
        <SynapseOut weight="1.000000" delay="3.000000" inhibitory="0" weightSTDP="0" delaySTDP="0" inputNeuron="0" outputNeuron="988" />
        <SynapseOut weight="1.000000" delay="0.300000" inhibitory="0" weightSTDP="0" delaySTDP="0" inputNeuron="0" outputNeuron="1132" />
        <SynapseOut weight="1.000000" delay="14.000000" inhibitory="0" weightSTDP="0" delaySTDP="0" inputNeuron="0" outputNeuron="1208" />
    </Neuron>
    <Neuron id="1" inhibitory="0" refractoryPeriod="5.000000" refractoryPotential="20000.000000" restingPotential="-65.000000" threshold="-50.000000" tauPSP="3.000000" tauRefract="0.100000">
        <SynapseOut weight="1.000000" delay="3.800000" inhibitory="0" weightSTDP="0" delaySTDP="0" inputNeuron="1" outputNeuron="341" />
        <SynapseOut weight="1.000000" delay="4.700000" inhibitory="0" weightSTDP="0" delaySTDP="0" inputNeuron="1" outputNeuron="685" />
        <SynapseOut weight="1.000000" delay="6.000000" inhibitory="0" weightSTDP="0" delaySTDP="0" inputNeuron="1" outputNeuron="777" />
        <SynapseOut weight="1.000000" delay="17.500000" inhibitory="0" weightSTDP="0" delaySTDP="0" inputNeuron="1" outputNeuron="981" />
        <SynapseOut weight="1.000000" delay="17.600000" inhibitory="0" weightSTDP="0" delaySTDP="0" inputNeuron="1" outputNeuron="1060" />
   [...]
```

## Viewing/analyzing the data

The resulting data is generated in ``data/``. You may then use some of the available scripts from ``scripts/`` to generate plots and diagrams. For instance:

> ``scripts/make-rasterplot.sh 0 200000``

This generates the file ``figures/SpikeRasterPlot--0000000000-0000200000.eps`` :

![Spike raster plot between 0 and 20 seconds](SpikeRasterPlot--0000000000-0000200000.png "Spike raster plot between 0 and 20 seconds")
