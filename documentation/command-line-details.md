# Command line parameters details

In this section, I explain what parameters you can give to the simulator, and how to exactly know what it does. Short answer : go read the code. In all this section, when refering to source code, I refer to files located in ``src/``. For instance ``nnl/simulator/nnl.cpp`` actually refers to ``src/nnl/simulator/nnl.cpp``.


Here are the possible command line arguments:

```
Usage : nnl  NETWORK  SETTINGS  PATTERNS  SIMULATION
  SETTINGS :
  --settings <settings.lua>                    Settings

  PATTERNS :
  --patterns <patterns.lua>                    Patterns

  SIMULATION :
  --simulation <simulation.lua>                Simulation protocol

  NETWORK :
  --build <builder.lua>                        Network building script
| --metaarchitecture <metaarchitecture.lua>    Network building via meta structure description
| --architecture <architecture.xml>            Prebuilt network
```

Here is a typical command line to run a simulation.

> ``../nnl --metaarchitecture settings/examples/builder-meta.lua --settings settings/examples/settings.lua --patterns settings/examples/patterns.lua --simulation settings/examples/simulation.lua``

But you could tell the program to just create arthictectures by only giving it no ``--simulation`` parameter. How this command line is interpreted can be seen in source code ``nnl/simulator/nnl.cpp``.

In general, I used Lua scripts for all settings.

### Neuron Network Architecture

What I call "architecture" is nothing but the structure of the neuron network itself. It is composed of one or more assemblies, one assembly contains neurons, each neuron is connected to other neurons.

When the simulator starts, the network architecture is defined using one of thoses three ways: 


1. Programmatically build a network:

> ``--build <builder.lua>``

We *program* in Lua how the network architecture should be built. 

Example available in ``settings/examples/builder-procedural.lua``.

See how this works in ``nnl/architecture/``, it begins with ``CArchitecture::BuildFromLua(const char * LuaFilename)``.


2. Describe the network:

> ``--metaarchitecture <metaarchitecture.lua>``

We *describe* the network structure with parameters, the simulator will build it on its own. 

Example available in ``settings/examples/builder-meta.lua``.

See how this works in ``nnl/architecture/`` and ``nnl/metastructure``, it begins with the meta-architecture ``CMetaArchitecture::LoadFromLua(char * LuaFilename)``, this meta architecture is then passed to ``CArchitecture::BuildFromMetaLua(CMetaArchitecture & MetaArchitecture)`` which immediately calls ``CMetaArchitecture::BuildArchitecture(CArchitecture * pArchitecture)`` ... yes ... snake biting its tail.


3. Passing a pre-made architecture

> ``--architecture <architecture.xml>``

We passe an XML file which contains the network architecture as a hierarchy of XML nodes representing assemblies, neurons and connections. I used to this reload a previously generated network and re-run with a different stimulation on it

See how this works in ``nnl/architecture/``, ``CArchitecture::XMLload(string InputXMLFile)``


## Datasets

You might want to provide datasets to be used during later in the simulation (see next "Simulation" section) in input for the network.

> ``--patterns <pattern-settins.lua>``

Example available in ``settings/examples/patterns.lua``.

See how this works in ``nnl/patterns/``, it begins with ``CPatternManager::CPatternManager(const char * luaFilename)`` which calls ``CPatternManager::LoadFromLua(...)``.


The input data must be provided in a particular file format.

For instance, the USPS dataset is available in ``datasets/`` : the training dataset is ``train.dat``, the generalization (test) dataset is ``test.dat``. Those two files contain all the possible USPS digits.

The files ``app.dat`` (for "apprentissage", which means "learning" un french) and ``gen.dat`` (for "généralisation") are respectively a subset of ``train.dat`` and ``test.dat``, with just fewer examples.

Those files follow this format:
* on the first line, 4 numbers :
   * ``Nc`` : the number of categories of examples (i.e. 10, since there are 10 different classes in the USPS: the digits)
   * ``Nv`` : the vector size of each example (256 in the case of the USPS : 16x16 pixels images)
   * ``Vmin`` and ``Vmax`` : the range in which each values of the vector are expressed (between 0 and 2 for the USPS)
* one example by line :
   * one number representing the category of the example (in the case of the USPS the category number is simply the corresponding digit)
   * the ``Nv`` vector values of the example (in the case of the USPS, there are 256 values between 0 and 2 representing the 16x16 greayscale pixels).

Of course, you can change the supported file format by changing the code... see ``nnl/patterns``.


## Global settings

You can set global parameters like STDP, input and output folders, etc.

> ``--settings <settings.lua>``

Example available in ``settings/examples/settings.lua``.

See how this works in ``nnl/parameters/``, it begins with ``CParameters::loadFromLuaFile(...)``.


## Simuation

> ``--simulation <simulation.lua>``

This parameter allows you to provide a simulation described in a Lua script. Basically you declare simulation steps, describe what it does and how long it lasts, then use these steps to define an entire simulation : the simulation is simply a list of steps.

Example available in ``settings/examples/simulation.lua``.

In this example, "Simulation" is a sequence of four steps : "Initialisation", "SaveArchitecture", "Particular6" and "Life".

See how this works in ``nnl/simulation/``, it begins with ``CSimulation::LoadFromLua(...)``


## Data output and analyzing

All the data is generated in ``data/``.

I have written quite a bunch of scripts in ``scripts/``, that I used essentially to generate diagrams and quickly visualize the results. Those scripts make use of some tools I developped, for instance to analyze polychronous groups (PGs) (see my PhD if you wonder what this is), which are in ``src/tools/``.

You might for isntance be interested in the tools named ``pg-search2/``, ``pg-active/``.

``pg-search2/`` is the tool to look for static PGs in an architecture, and ``pg-active/`` is for looking for _activated_ PGs in the simulation activity.

To be honnest, the code of thos tools is horrible, messy and undocumented. I strongly recommend making your own tools.
